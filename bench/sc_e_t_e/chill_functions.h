
class SpMP::LevelSchedule *chillForwardSolve(const class SpMP::CSR &A,double y[],const double b[]);

class SpMP::LevelSchedule *chillBackwardSolve(const class SpMP::CSR &A,double y[],const double b[]);

class SpMP::LevelSchedule *chill_ilu0(double*lu,  const class SpMP::CSR &A);

