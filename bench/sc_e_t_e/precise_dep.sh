#!/bin/bash -l

#SBATCH -p regular
#SBATCH -t 01:00:00
#SBATCH -c 16

export KMP_AFFINITY=granularity=fine,compact,1



#for f in $SCRATCH/intel-matrices/*
#do
    #filename= ${1%.mtx}
    #echo $filename
    #echo $1     
#echo  
export OMP_NUM_THREADS=$2
srun ~/pldi16-sparse/bench/sc_e_t_e/precise_dep $SCRATCH/intel-matrices/${1%.mtx}/$1 | tee mkl17/$2/$1.log
#done
