#!/bin/bash

for i in `cat matrix_single.lst` 
do
   for j in 12 
   do
      sbatch -N 1 -n 1 -c 12 -J $i -t 60 --qos=premium ~/pldi16-sparse/bench/sc_e_t_e/precise_dep.sh $i $j;
   done
done
