/*Copyright (c) 2015, Intel Corporation                                                                                                                                           
 All rights reserved.                                                                                                                                                            
 
 Redistribution and use in source and binary forms, with or without                                                                                                              
 modification, are permitted provided that the following conditions are met:                                                                                                     
 - Redistributions of source code must retain the above copyright notice,                                                                                                        
 this list of conditions and the following disclaimer.                                                                                                                         
 - Redistributions in binary form must reproduce the above copyright notice,                                                                                                     
 this list of conditions and the following disclaimer in the documentation                                                                                                     
 and/or other materials provided with the distribution.                                                                                                                        
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "../CSR.hpp"
#include <string.h> 
#include <iostream>
#include <algorithm>
using namespace std;
using namespace SpMP;

#include "../LevelSchedule.hpp" 
#include "../synk/barrier.hpp" 
#define REPEAT 1
//synk::Barrier *bar;

static void printTimes(double *times) {
	sort(times, times + REPEAT);
	sort(times + REPEAT, times + 2 * REPEAT);
	sort(times + 2 * REPEAT, times + 3 * REPEAT);
	sort(times + 3 * REPEAT, times + 4 * REPEAT);
	sort(times + 4 * REPEAT, times + 5 * REPEAT);

	double t1 = times[REPEAT / 2];
	double t2 = times[3 * REPEAT / 2];
	double t3 = times[5 * REPEAT / 2];
	double t4 = times[7 * REPEAT / 2];
	double t5 = times[9 * REPEAT / 2];
	printf("median REF time %f\n", t1);
	printf("median NAIVE-EXECUTOR time %f\n", t2);
	printf("median OPTIMIZED-EXECUTOR time %f\n", t3);
	printf("median NAIVE-INSPECTOR time %f\n", t4);
	printf("median OPTIMIZED-INSPECTOR time %f\n", t5);

	printf("EXECUTOR speedup %f\n", t2 / t3);
	printf("INSPECTOR speedup %f\n", t4 / t5);
	printf("EXECUTOR speedup over REF time %f\n", t1 / t3);

}

double ilu0Ref(CSR *B) {
	// copy A to B
	//for(int i=0; i < A->rowptr[A->m]; i++)
	//  B->values[i]=A->values[i];
	double tBegin = omp_get_wtime();

	for (int i = 0; i < B->m; i++) {
		// work on rows 0:i-1: aggregate them into row i
		// if corresponding a(i,*) is not zero
		// elements 0:i-1 of row i
		for (int k = B->rowptr[i]; k < B->diagptr[i]; k++) {
			int c = B->colidx[k];
			double tmp = B->values[k] /= B->values[B->diagptr[c]]; // a_ik /= a_kk

			int j1 = k + 1, j2 = B->diagptr[c] + 1;

			while (j1 < B->rowptr[i + 1] && j2 < B->rowptr[c + 1]) {
				if (B->colidx[j1] < B->colidx[j2])
					++j1;
				else if (B->colidx[j2] < B->colidx[j1])
					++j2;
				else {
					B->values[j1] -= tmp * B->values[j2]; // a_ij -= a_ik*a_kj
					++j1;
					++j2;
				}
			}
		}
	} // for each row

	double tEnd = omp_get_wtime();

	return (tEnd - tBegin);

}

//void ilu0_barrier

void ilu0Init(CSR *A, CSR *B) {

	for (int i = 0; i < A->rowptr[A->m]; i++)
		B->values[i] = A->values[i];
}

double ilu0WithBarrier(CSR *B, const LevelSchedule& schedule, const int *perm) {

	double tBegin = omp_get_wtime();

#pragma omp parallel 
	{
		int tid = omp_get_thread_num();

		const vector<int>& threadBoundaries = schedule.threadBoundaries;
		const vector<int>& taskBoundaries = schedule.taskBoundaries;
		//    for(int tid = 0; tid < omp_get_max_threads(); tid++)
		for (int task = threadBoundaries[tid]; task < threadBoundaries[tid + 1];
				++task) {
			for (int i = taskBoundaries[task]; i < taskBoundaries[task + 1];
					++i) {
				int row = perm[i];

				for (int k = B->rowptr[row]; k < B->diagptr[row]; k++) {
					int c = B->colidx[k];
					double tmp = B->values[k] /= B->values[B->diagptr[c]]; // a_ik /= a_kk                                                                                                                                                            

					int j1 = k + 1, j2 = B->diagptr[c] + 1;

					while (j1 < B->rowptr[row + 1] && j2 < B->rowptr[c + 1]) {
						if (B->colidx[j1] < B->colidx[j2])
							++j1;
						else if (B->colidx[j2] < B->colidx[j1])
							++j2;
						else {
							B->values[j1] -= tmp * B->values[j2]; // a_ij -= a_ik*a_kj                                                                                                                                                                      
							++j1;
							++j2;
						}
					}
				}

			} // for each row                                                                                                                                                                                                                     
			synk::Barrier::getInstance()->wait(tid);
			//bar->wait(tid);
		} // for each level                                                                                                                                                                                                                     
	} // omp parallel                                                                                                                                                                                                                         
	double tEnd = omp_get_wtime();

	return (tEnd - tBegin); //<<"s" <<endl;   
}

CSR * test_ilu0_dep_naive(CSR *A, double *time) {

//initialize rowptrs

	int *count = new int[A->m + 1];
	int *row = new int[A->m + 1];

	double tBegin = omp_get_wtime();

	for (int i = 0; i <= A->m; i++) {
		row[i] = 0;
		count[i] = 0;
	}
	for (int i2 = 0; i2 < A->m; i2++) {
		for (int i = 0; i < A->m; i++) {
			bool found = false;

			for (int k = A->rowptr[i]; k < A->diagptr[i]; k++) {
				for (int j1 = k + 1; j1 < A->rowptr[i + 1]; j1++) {
					for (int j2 = A->diagptr[A->colidx[k]] + 1;
							j2 < A->rowptr[A->colidx[k] + 1]; j2++) {
						for (int k2 = A->rowptr[i2]; k2 < A->diagptr[i2];
								k2++) {
							for (int j12 = k2 + 1; j12 < A->rowptr[i2 + 1];
									j12++) {
								for (int j22 = A->diagptr[A->colidx[k2]] + 1;
										j22 < A->rowptr[A->colidx[k2] + 1];
										j22++) {
									if (A->colidx[j1] == A->colidx[j2]
											&& A->colidx[j12]
													== A->colidx[j22]) {

										if (j1 == j22)
											found = true;
										else if (j1 == k2)
											found = true;
										else if (j1
												== A->diagptr[A->colidx[k2]])
											found = true;
										else if (j12 == j1)
											found = true;

										if (found)
											break;
									}
								}
								if (found)
									break;
							}
							if (found)
								break;
						}
						if (found)
							break;
					}
					if (found)
						break;
				}
				if (found)
					break;
			}
			if (found) {
				row[i2 + 1]++;
				if (i != i2)
					row[i + 1]++;
			}
			if (i == i2 && !found) {
				row[i2 + 1]++;

			}
		}
	}

	for (int i = 1; i <= A->m; i++) {
		row[i] += row[i - 1];
	}
	double tEnd = omp_get_wtime();

	double *nnz = new double[row[A->m]];
	int *col = new int[row[A->m]];

	double tStart2 = omp_get_wtime();
	for (int i2 = 0; i2 < A->m; i2++) {
		for (int i = 0; i < A->m; i++) {
			bool found = false;
			for (int k = A->rowptr[i]; k < A->diagptr[i]; k++) {
				for (int j1 = k + 1; j1 < A->rowptr[i + 1]; j1++) {
					for (int j2 = A->diagptr[A->colidx[k]] + 1;
							j2 < A->rowptr[A->colidx[k] + 1]; j2++) {
						for (int k2 = A->rowptr[i2]; k2 < A->diagptr[i2];
								k2++) {
							for (int j12 = k2 + 1; j12 < A->rowptr[i2 + 1];
									j12++) {

								for (int j22 = A->diagptr[A->colidx[k2]] + 1;
										j22 < A->rowptr[A->colidx[k2] + 1];
										j22++) {
									if (A->colidx[j1] == A->colidx[j2]
											&& A->colidx[j12]
													== A->colidx[j22]) {

										if (j1 == j22)
											found = true;
										else if (j1 == k2)
											found = true;
										else if (j1
												== A->diagptr[A->colidx[k2]])
											found = true;
										else if (j12 == j1)
											found = true;

										if (found)
											break;
									}
								}
								if (found)
									break;
							}
							if (found)
								break;
						}
						if (found)
							break;
					}
					if (found)
						break;
				}
				if (found)
					break;
			}
			if (found) {
				nnz[row[i2] + count[i2]] = 1.0;
				col[row[i2] + count[i2]++] = i;
				if (i != i2) {
					nnz[row[i] + count[i]] = 1.0;
					col[row[i] + count[i]++] = i2;
				}
			}
			if (i == i2 && !found) {
				nnz[row[i2] + count[i2]] = 1.0;
				col[row[i2] + count[i2]++] = i;

			}

		}
	}

	double tEnd2 = omp_get_wtime();
	*time = tEnd - tBegin + tEnd2 - tStart2;
	//cout<<"Time taken for naive "<<tEnd - tBegin + tEnd2 - tStart2<<"s" <<endl;		  
	return new CSR(A->m, A->n, row, col, nnz);

}

double test_ilu0_dep_opt(CSR *A, CSR *B) {

	// j1 == j2                                                                                                                                                    

	//cout<<B->m<<endl;
	int *count = new int[B->m + 1];

	double tBegin = omp_get_wtime();

//initialize rowptrs

//#pragma omp parallel for 
	for (int i = 0; i <= B->m; i++) {
		B->rowptr[i] = 0;
		count[i] = 0;
	}

////first record number of nonzeros per row
//#pragma omp parallel for 
	for (int i = 0; i < A->m; i++)
		for (int k = A->rowptr[i]; k < A->rowptr[i + 1]; k++)
			if (0 <= A->colidx[k] && A->colidx[k] < A->m)
				B->rowptr[i + 1]++;

	for (int i = 1; i <= B->m; i++)
		B->rowptr[i] += B->rowptr[i - 1];

	//#pragma omp parallel for 
	for (int i2 = 0; i2 < A->m; i2++)
		for (int k2 = A->rowptr[i2]; k2 < A->rowptr[i2 + 1]; k2++)
			if (0 <= A->colidx[k2] && A->colidx[k2] < B->m) {
				B->values[B->rowptr[i2] + count[i2]] = 1.0;
				B->colidx[B->rowptr[i2] + count[i2]++] = A->colidx[k2];

			}

	double tEnd = omp_get_wtime();
	//  cout<<"Time taken for optimized "<<tEnd - tBegin<<"s"<<endl;		  
	delete[] count;
	return tEnd - tBegin;
}

int main(int argc, char **argv) {

	/////////////////////////////////////////////////////////////////////////////                                                                                                                                                             
	// Initialize barrier                                                                                                                                                                                                                     
	/////////////////////////////////////////////////////////////////////////////                                                                                                                                                             

	int nthreads = omp_get_max_threads();

	/*
	 #ifdef __MIC__
	 bar = new synk::Barrier(omp_get_max_threads()/4, 4);
	 #else
	 bar = new synk::Barrier(omp_get_max_threads(), 1);
	 #endif

	 #pragma omp parallel
	 {
	 bar->init(omp_get_thread_num());
	 }
	 */
	omp_set_num_threads(nthreads);

	/////////////////////////////////////////////////////////////////////////////                                                                                                                                                             
	// Load input                                                                                                                                                                                                                             
	/////////////////////////////////////////////////////////////////////////////                                                                                                                                                            

	int m = argc > 1 ? atoi(argv[1]) : 64; // default input is 64^3 27-pt 3D Lap.                                                                                                                                                             
	if (argc < 2) {
		fprintf(stderr,
				"Using default 64^3 27-pt 3D Laplacian matrix\n"
						"-- Usage examples --\n"
						"  %s 128 : 128^3 27-pt 3D Laplacian matrix\n"
						"  %s inline_1.mtx: run with inline_1 matrix in matrix market format\n\n",
				argv[0], argv[0]);
	}
	char buf[1024];
	// sprintf(buf, "%d", m); 

	bool readFromFile =
			argc > 1 ? strcmp(buf, argv[1]) && !strstr(argv[1], ".mtx") : false;
	//printf("input=%s\n", argc > 1 ? argv[1] : buf);

	CSR *A_ref = new CSR(argc > 1 ? argv[1] : buf);
	//CSR *A_ref = new CSR("3");   
	CSR *A_copy = new CSR(argc > 1 ? argv[1] : buf);
	//  CSR *A_copy0 =  new CSR("3");  //
	CSR *A_copy0 = new CSR(argc > 1 ? argv[1] : buf);
	//CSR *matrix1 = new CSR();
	//CSR *A_copy0= new CSR();// =     new CSR(argc > 1 ? argv[1] : buf); 
	CSR *matrix2 = new CSR();
	// matrix1->alloc(A_ref->m, 1978);    

	matrix2->alloc(A_ref->m, A_ref->rowptr[A_ref->m]);

	for (int i = 0; i < A_ref->m; i++)
		for (int j = A_ref->rowptr[i]; j < A_ref->rowptr[i + 1]; j++) {
			if (A_ref->colidx[j] >= i) {
				matrix2->diagptr[i] = j;
				break;
			}
		}

	double *times = new double[5 * REPEAT];

	for (int i = 0; i < REPEAT; i++) {

		CSR *matrix1 = test_ilu0_dep_naive(A_ref, &times[i + 3 * REPEAT]);

		times[i + 4 * REPEAT] = test_ilu0_dep_opt(A_ref, matrix2);

		LevelSchedule *barrierSchedule0 = new LevelSchedule;
		barrierSchedule0->useBarrier = true;
		barrierSchedule0->transitiveReduction = false;
		barrierSchedule0->constructTaskGraph(*matrix1);

		LevelSchedule *barrierSchedule = new LevelSchedule;
		barrierSchedule->useBarrier = true;
		barrierSchedule->transitiveReduction = false;
		barrierSchedule->constructTaskGraph(*matrix2);

		times[i] = ilu0Ref(A_ref);

		const int *invPerm0 = barrierSchedule0->threadContToOrigPerm;
		const int *invPerm = barrierSchedule->threadContToOrigPerm;

		times[i + REPEAT] = ilu0WithBarrier(A_copy0, *barrierSchedule0,
				invPerm0);

		times[i + 2 * REPEAT] = ilu0WithBarrier(A_copy, *barrierSchedule,
				invPerm);

		correctnessCheck<double>(A_ref->values, A_copy->values,
				A_ref->rowptr[A_ref->m]);
		correctnessCheck<double>(A_ref->values, A_copy0->values,
				A_ref->rowptr[A_ref->m]);

		delete barrierSchedule0;
		delete barrierSchedule;

		delete matrix1->rowptr;
		delete matrix1->colidx;
		delete matrix1->values;

		delete matrix1;
	}

	printTimes(times);

	delete A_copy0;

	delete matrix2;
	delete A_copy;
	delete A_ref;
	delete times;

	return 0;
}

