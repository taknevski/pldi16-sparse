import subprocess
import StringIO
import os
import xlwt
import re


indir = '/home/anand/Dektop'
book = xlwt.Workbook(encoding="utf-8")
sheet1 = book.add_sheet("Sheet 1")
row1=0

sheet1.write(0,0,"Input")
sheet1.write(0,1,"CG_ilu0_precond_serial/GB/s")
sheet1.write(0,2,"CG_ilu0_precond_barrier_reordered/GB/s")
sheet1.write(0,3,"CG_ilu0_precond_p2p_reordered/GB/s")
sheet1.write(0,4,"CG_ilu0_ref_serial_time/s")
sheet1.write(0,5,"CG_ilu0_barrier_reordered_time/s")
sheet1.write(0,6,"CG_ilu0_p2p_reordered_time/s")
sheet1.write(0,7,"Total Inspector time/s")
sheet1.write(0,8,"ilu0 Inspector time/s")
sheet1.write(0,9,"CG_gs_serial_iter_time/s")
sheet1.write(0,10,"CG_gs_barrier_iter_time/s")
sheet1.write(0,11,"CG_gs_p2p_iter_time/s")

row1+=1

f  = open('12threadresults.txt', "r")


while True:

	params = f.readline()

	if not params: 
		break

	searchObj = re.search(r'input', params)  



	while  not searchObj : 
		params = f.readline()
		if not params: 
			break
	
		searchObj = re.search(r'input', params)  
	if not params: 
		break

	

	mtx = re.search(r'(.*)(/)(\S+)(\.mtx)' ,params)
	
	#if mtx:		
	sheet1.write(row1,0,mtx.group(3))	
	#sheet1.write(row1+1,0,"SpMV")
	#sheet1.write(row1+2,0,"SpTS")
	#sheet1.write(row1+3,0,"Relative Error")
	#sheet1.write(row1+4,0,"Convergence Iterations #")


	for x in range(0, 3):		
		params = f.readline()		
		if not params: 
			break		
		
		searchObj = re.search(r'median', params)  

		while  not searchObj : 
			params = f.readline()
			if not params: 
				break
			searchObj = re.search(r'median', params)  

		if not params
			break;


		
		if x == 0:
			ilu0_ref_time = re.search(r'(median\s)(ilu0\s)(ref\s)(time\s)([0-9]+\.[0-9]+)', params)
			sheet1.write(row1,4,float(ilu0_ref_time.group(5)))		
		if x == 1:
			#median ILU-INSPECTOR time 0.111766
			insp_time = re.search(r'(median\s)(OPTIMIZED\-INSPECTOR\s)(time\s)([0-9]+\.[0-9]+)', params)
			sheet1.write(row1,7,float(insp_time.group(4)))		

			params = f.readline()	
			
			searchObj = re.search(r'median\sILU', params)  
			while  not searchObj : 
				params = f.readline()
				if not params: 
					break

				searchObj = re.search(r'median\sILU', params)  

			if not params:
				break
			
			ilu_insp_time = re.search(r'(median\s)(ILU\-INSPECTOR\s)(time\s)([0-9]+\.[0-9]+)', params)
                        sheet1.write(row1,8,float(ilu_insp_time.group(4)))	

			params = f.readline()	
			
			searchObj = re.search(r'p2p\stime', params)  
			while  not searchObj : 
				params = f.readline()
				if not params: 
					break

				searchObj = re.search(r'p2p\stime', params)  

			if not params:
				break

			ilu0_p2p_time = re.search(r'(median\s)(ilu0\s)(p2p\s)(time\s)([0-9]+\.[0-9]+)', params)
			sheet1.write(row1,6,float(ilu0_p2p_time.group(5)))		

		if x == 2:
			//median ilu0 barrier time 0.009183
			//median ilu0 p2p time 0.008908
			ilu0_barrier_time = re.search(r'(median\s)(ilu0\s)(barrier\s)(time\s)([0-9]+\.[0-9]+)', params)
			sheet1.write(row1,5,float(ilu0_barrier_time.group(5)))		

	
		if not params:
			break
			

		params = f.readline()	
   		searchObj = re.search(r'Total', params)  

		while  not searchObj : 
			params = f.readline()
			if not params: 
				break
		searchObj = re.search(r'Total', params)  
	
		if not params: 
			break
					
		#Total pcg time: 25.610023, Average time per iteration  0.027072
		avg_pcg_time = re.search(r'(Total\s)(pcg\s)(time\s\s)([0-9]+\.[0-9]+)(\s\s\Average\s\time\s\per\iteration\s\s)([0-9]+\.[0-9]+)', params)
	
		if x==0:
			sheet1.write(row1,9,float(average_pcg_time.group(6)))		
		if x==1:
			sheet1.write(row1,11,float(average_pcg_time.group(6)))		
		if x==2:
			sheet1.write(row1,10,float(average_pcg_time.group(6)))		

		params = f.readline()	
   		searchObj = re.search(r'spmv_perf', params)  
			

		while  not searchObj : 
			params = f.readline()
			if not params: 
				break
			searchObj = re.search(r'spmv_perf', params)  
	
		if not params: 
			break

		ilu = re.search(r'(spmv\_perf\s\=\s)([0-9]+\.[0-9]+)(\sgbps\strsv\_perf\s\=\s)([0-9]+\.[0-9]+)(\sgbps\silu0\_perf\s\=\s)([0-9]+\.[0-9]+)', params)#([0-9]+)(rel_err\s\=\s)([0-9]+\.S+)' ,params)
	
		if x== 0:
			sheet1.write(row1,1,float(ilu.group(6)))
		if x== 1:
			sheet1.write(row1,3,float(ilu.group(6)))
		if x== 2:
			sheet1.write(row1,2,float(ilu.group(6)))
	
	if not params:
		break

	row1+= 1	


book.save("new-insp-results.xls")
 
