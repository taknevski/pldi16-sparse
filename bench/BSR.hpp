#pragma once

#include "SpMP/CSR.hpp"

namespace SpMP
{

/**
 * Similar to MKL BSR format but each block is stored in column-major
 * for more efficient vectorization
 */
class BSR {
public:
  int m; /**< # of block rows */
  int n; /**< # of block cols */
  int bs; /**< each block is bs*bs */

  int *rowptr; /**< with length (m + 1) */
  int *colidx; /**< with length rowptr[m] */
  double *values; /**< with length rowptr[m]*bs */

  double *idiag; /**< inverse of diagonal blocks */
  int *diagptr;

  BSR();
  BSR(const CSR& A, int bs);

  void alloc(int m, int nnzBlocks, bool createSeparateDiagData = true);
  void dealloc();

  int getNnzBlocks() const { return rowptr[m] - getBase(); }
  int getNnz() const { return getNnzBlocks()*bs*bs; }
  int getBase() const { return rowptr[0]; }
  
  void print() const;

  /**
   * Compute w = alpha*A*x + beta*y + gamma
   * where A is this matrix
   */
  void multiplyWithVector(
    double *w,
    double alpha, const double *x, double beta, const double *y, double gamma)
    const;

  /**
   * Compute w = A*x
   */
  void multiplyWithVector(double *w, const double *x) const;

private:
  bool ownData_;

}; // BSR

} // namespace SpMP
