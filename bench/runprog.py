import subprocess
import StringIO
import os
import xlwt
import re


indir = '/home/anand/Dektop'
book = xlwt.Workbook(encoding="utf-8")
sheet1 = book.add_sheet("Sheet 1")
row1=0

sheet1.write(0,0,"Input")
sheet1.write(0,1,"CG_SymGS_serial/GB/s")
sheet1.write(0,2,"CG_SymGS_p2p_reordered/GB/s")
sheet1.write(0,3,"CG_ilu0_serial/GB/s")
sheet1.write(0,4,"CG_ilu0_p2p_reorderd/GB/s")
sheet1.write(0,5,"CG_SymGS_barrier/GB/s")
sheet1.write(0,6,"CG_ilu0_barrier/GB/s")
sheet1.write(0,7,"CG_SymGS_barrier_reordered/GB/s")
#sheet1.write(0,8,"CG_ilu0_barrier_reordered/GB/s")
sheet1.write(0,8,"CG_ilu0_precond_serial/GB/s")
sheet1.write(0,9,"CG_ilu0_precond_barrier/GB/s")
#sheet1.write(0,11,"CG_ilu0_precond_barrier_reordered/GB/s")
sheet1.write(0,10,"CG_ilu0_precond_p2p_reordered/GB/s")
sheet1.write(0,11,"CG_inspector_SymGS_barrier/GB/s")
sheet1.write(0,12,"CG_inspector_SymGS_barrier_reordered/GB/s")
sheet1.write(0,13,"CG_inspector_SymGS_p2p_reordered/GB/s")
sheet1.write(0,14,"CG_inspector_ilu0_barrier/GB/s")
#sheet1.write(0,16,"CG_inspector_ilu0_barrier_reordered/GB/s")
sheet1.write(0,15,"CG_inspector_ilu0_p2p_reordered/GB/s")
sheet1.write(0,16,"Available Parallelism")

row1+=1

f  = open('12threadresults.txt', "r")


while True:

	params = f.readline()

	if not params: 
		break

	searchObj = re.search(r'input', params)  



	while  not searchObj : 
		params = f.readline()
		if not params: 
			break
	
		searchObj = re.search(r'input', params)  
	if not params: 
		break

	

	mtx = re.search(r'(.*)(/)(\S+)(\.mtx)' ,params)
	
	#if mtx:		
	sheet1.write(row1,0,mtx.group(3))	
	sheet1.write(row1+1,0,"SpMV")
	sheet1.write(row1+2,0,"SpTS")
	sheet1.write(row1+3,0,"Relative Error")
	sheet1.write(row1+4,0,"Convergence Iterations #")


	for x in range(0, 7):		
		params = f.readline()		
		if not params: 
			break		
		if not (x == 0) and not(x==2):
			searchObj = re.search(r'median', params)  

			while  not searchObj : 
				params = f.readline()
				if not params: 
					break
				searchObj = re.search(r'median', params)  
	
			if not params: 
				break

			insp_time = re.search(r'(median\s)(OPTIMIZED\-INSPECTOR\s)(time\s)([0-9]+\.[0-9]+)', params)
				

		if x==1:	
         	        sheet1.write(row1,13,float(insp_time.group(4)))		
		if x==3:	
         	        sheet1.write(row1,15,float(insp_time.group(4)))		
		if x==4:
         	        sheet1.write(row1,11,float(insp_time.group(4)))		
		if x==5:
         	        sheet1.write(row1,14,float(insp_time.group(4)))		
		if x==6:	
	        	sheet1.write(row1,12,float(insp_time.group(4)))		
		#if x==7:	
         	#        sheet1.write(row1,17,float(insp_time.group(4)))		



		if x== 1 or x==3:	
			params = f.readline()	
   			searchObj = re.search(r'parallelism', params)  

			while  not searchObj : 
				params = f.readline()
				if not params: 
					break
				searchObj = re.search(r'parallelism', params)  
	
			if not params: 
				break

			parallelism = re.search(r'(parallelism\s\=\s)([0-9]+\.[0-9]+)', params)

			#print parallelism.group(2)
			if(x == 1):
				sheet1.write(row1,16,float(parallelism.group(2)))		
			params = f.readline()
		

		if not params: 
			break

		searchObj = re.search(r'iter', params)  

		while  not searchObj : 
			params = f.readline()
			if not params: 
				break
			searchObj = re.search(r'iter', params)  
	
		if not params: 
			break
	
	

		iter_and_err = re.search(r'(iter\s\=\s)([0-9]+)(\,*)(\srel\_err\s\=\s)(.*)', params)#([0-9]+)(rel_err\s\=\s)([0-9]+\.S+)' ,params)
	
		#if iter_and_err:
		
		sheet1.write(row1+4,x+1,int(iter_and_err.group(2)))	
		sheet1.write(row1+3,x+1,float(iter_and_err.group(5)))
		#print iter_and_err.group(2)
		#print iter_and_err.group(5)				
	
		params = f.readline()

		searchObj = re.search(r'spmv_perf', params)  

		while  not searchObj : 
			params = f.readline()
			if not params: 
				break
			searchObj = re.search(r'spmv_perf', params)  
	
		if not params: 
			break
	
		#spmv_perf = 49.657 gbps trsv_perf = 9.07676 gbps
	
                
         	spmv_and_trslv = re.search(r'(spmv\_perf\s\=\s)([0-9]+\.[0-9]+)(\sgbps\strsv\_perf\s\=\s)([0-9]+\.[0-9]+)', params)#([0-9]+)(rel_err\s\=\s)([0-9]+\.S+)' ,params)
		sheet1.write(row1+1,x+1,float(spmv_and_trslv.group(2)))	
		sheet1.write(row1+2,x+1,float(spmv_and_trslv.group(4)))

	
		if (x == 2 or x == 3) or (x == 5): 
			ilu = re.search(r'(spmv\_perf\s\=\s)([0-9]+\.[0-9]+)(\sgbps\strsv\_perf\s\=\s)([0-9]+\.[0-9]+)(\sgbps\silu0\_perf\s\=\s)([0-9]+\.[0-9]+)', params)#([0-9]+)(rel_err\s\=\s)([0-9]+\.S+)' ,params)

		if x==2:	
			sheet1.write(row1+1,8,float(ilu.group(6)))
		if x==3:	
			sheet1.write(row1+1,10,float(ilu.group(6)))
		if x==5:	
			sheet1.write(row1+1,9,float(ilu.group(6)))
		#if x==7:	
		#	sheet1.write(row1+1,11,float(ilu.group(6)))
		


					

		#print spmv_and_trslv.group(2)
		#print spmv_and_trslv.group(4)				
	
	row1+= 5	

book.save("new-results.xls")
 
