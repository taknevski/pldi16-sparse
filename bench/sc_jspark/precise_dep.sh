#!/bin/bash -l

#SBATCH -p regular
#SBATCH -t 00:30:00
#SBATCH -c 16

export OMP_NUM_THREADS=12
export KMP_AFFINITY=granularity=fine,compact,1

srun ~/pldi16-sparse/bench/sc_jspark/precise_dep ~/matrices/$1 | tee logs/$1.log
