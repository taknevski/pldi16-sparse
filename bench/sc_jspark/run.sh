#!/bin/bash

for i in `cat matrix.lst`; do sbatch -N 1 -n 1 -c 16 -J $i -t 60 --qos=premium ~/pldi16-sparse/bench/sc_jspark/precise_dep.sh $i; done
