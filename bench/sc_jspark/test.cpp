#include "test.hpp"

bool  correctnessCheck(CSR *A, double *y)
{
  static double *yt = NULL;
  if (NULL == yt) {
    yt = new double[A->m];

    copy(y, y + A->m, yt);

    return true;
  }
  else {
    return correctnessCheck<double>(yt, y, A->m, 1e-8);
  }
}


static void printEfficiency(
  double *times, int REPEAT, double flop, double byte)
{
  sort(times, times + REPEAT);

  double t = times[REPEAT/2];

  printf(
    "%7.2f gflops %7.2f gbps\n",
    flop/t/1e9, byte/t/1e9);
}

//static const size_t LLC_CAPACITY = 32*1024*1024;
//static const double *bufToFlushLlc = NULL;

void flushLlc()
{
  double sum = 0;
#pragma omp parallel for reduction(+:sum)
  for (int i = 0; i < LLC_CAPACITY/sizeof(bufToFlushLlc[0]); ++i) {
    sum += bufToFlushLlc[i];
  }
  FILE *fp = fopen("/dev/null", "w");
  fprintf(fp, "%f\n", sum);
  fclose(fp);
}

void initializeX(double *x, int n) {
#pragma omp parallel for
  for (int i = 0; i < n; ++i) x[i] = n - i;
}

