#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define colidx_(i,k) colidx[1 * k]
#define colidx___(i,k) colidx[1 * k + 1]
#define rowptr_(i) rowptr[1 * i]
#define rowptr__(i) rowptr[1 * i + 1]
#define rowptr___(i) rowptr[1 * i + 1]
#define rowptr____(i) rowptr[1 * i]
#define rowptr__p(i,k,ip) rowptr[1 * ip + 1]
#define rowptr_p(i,k,ip) rowptr[1 * ip]
#define taskBoundaries_(i) schedule -> taskBoundaries[i]
#define taskBoundaries__(i) schedule -> taskBoundaries[i + 1]
#define threadBoundaries_(i) schedule -> threadBoundaries[i]
#define threadBoundaries__(i) schedule -> threadBoundaries[i + 1]
#include "LevelSchedule.hpp"
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "test.hpp"
using namespace std;

void gs_executor(const SpMP::CSR& A, double y[], const double b[],
      const SpMP::LevelSchedule *schedule) {

   int task;
   int idx;
   volatile int zplanes[256];
   int right;
   int left;
   int num_threads;
   int tid;
   int ub;
   int t8;
   int t4;
   int t2;
   int _t15;
   int _t14;
   int _t13;
   int _t12;
   int _t11;
   int _t10;
   int _t9;
   int _t8;
   int _t7;
   int _t6;
   int _t5;
   int _t4;
   int _t3;
   int In_1;
   int _t2;
   int _t1;
   int *perm_inv;
   int *perm;
   int cnt;
   int tmp;
   int j;
   int c;
   int source_offset;
   int acc;
   int t6;
   int found;
   int *_P_DATA4;
   int *_P_DATA3;
   int *_P_DATA2;
   int *_P_DATA1;
   int ip;
   int i;
   int k;
   double sum;

   ADJUST_FOR_BASE;

   perm = schedule -> SpMP::LevelSchedule::origToThreadContPerm;
   perm_inv = schedule -> SpMP::LevelSchedule::threadContToOrigPerm;
#pragma omp parallel  private(tid,t6,t4,t8, sum) 
   {
      int nthreads;
      tid = omp_get_thread_num();
      nthreads = omp_get_num_threads();
      const int ntasks = schedule -> ::SpMP::LevelSchedule::ntasks;
      int task;
      const short *nparents = schedule -> ::SpMP::LevelSchedule::nparentsForward;
      int nPerThread = (ntasks + nthreads - 1) / nthreads;
      int nBegin = __rose_lt(nPerThread * tid,ntasks);
      int nEnd = __rose_lt(nBegin + nPerThread,ntasks);
      volatile int *taskFinished = schedule -> ::SpMP::LevelSchedule::taskFinished;
      int **parents = schedule -> ::SpMP::LevelSchedule::parentsForward;
      memset(((char *)(taskFinished + nBegin)),0,(nEnd - nBegin) * sizeof(int ));
#pragma omp barrier 
      {
	 for (t4 = threadBoundaries_(tid); t4 <= threadBoundaries__(tid) - 1; t4 += 1) {{
	    task = t4;
	    SPMP_LEVEL_SCHEDULE_WAIT;
	    for (t6 = taskBoundaries_(t4); t6 <= taskBoundaries__(t4) - 1; t6 += 1) {
	       t8 = rowptr_(t6);
	       sum = b[t6];
	       for (t8 = rowptr_(t6); t8 <= rowptr__(t6) - 1; t8 += 1) 
		  sum -= (values[t8] * y[colidx[t8]]);
	       t8 = rowptr__(t6);
	       y[t6] = (sum * idiag[t6]);
	    }
	 }
	 SPMP_LEVEL_SCHEDULE_NOTIFY;
	 }
      }
   }
}


