#!/bin/bash

for i in `cat matrix1.lst` 
do
   for j in 2 1 
   do
      sbatch -N 1 -n 1 -c 12 -J $i -t 360 --qos=premium ~/pldi16-sparse/bench/sc_jspark/precise_dep.sh $i $j;
   done
done
