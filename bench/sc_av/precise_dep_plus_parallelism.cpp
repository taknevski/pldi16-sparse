
#include <algorithm>
#include <cmath>
#include <cstring>

#include <Vector.hpp>
#include <LevelSchedule.hpp>
#include <synk/barrier.hpp>
#include "gs_inspector.hpp"
#include "gs_executor.hpp"
#include "bgs_executor.hpp"
#include "ilu_inspector.hpp"
#include "ilu_executor.hpp"


#include "test.hpp"

using namespace SpMP;
//Added by Anand 11/02/2015
#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define colidx_(i,k) colidx[1 * k]

#define rowptr_(i) rowptr[1 * i]
#define rowptr__(i) rowptr[1+1 * i]
#define diagptr_(i) diagptr[1 * i]
#define taskBoundaries_(i) schedule -> taskBoundaries[i]
#define taskBoundaries__(i) schedule -> taskBoundaries[i + 1]
#define threadBoundaries_(i) schedule -> threadBoundaries[i]
#define threadBoundaries__(i) schedule -> threadBoundaries[i + 1]


/**
 * Reference sequential sparse triangular solver
 */

void forwardSolveRef(const CSR& A, double y[], const double b[]) {
   ADJUST_FOR_BASE;

   for (int i = base; i < A.m + base; ++i) {
      double sum = b[i];
      for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
	 sum -= values[j] * y[colidx[j]];
      }
      y[i] = sum * idiag[i];
   } // for each row
}

void backwardSolveRef(const CSR& A, double y[], const double b[]) {
   ADJUST_FOR_BASE;

   for (int i = A.m - 1 + base; i >= base; --i) {
      double sum = b[i];
      for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
	 sum -= values[j] * y[colidx[j]];
      }
      y[i] = sum * idiag[i];
   } // for each row
}



// serial ilu0
void ilu0_ref(double *lu, const CSR& A) {
   int base = A.getBase();

   const int *rowptr = A.rowptr - base;
   const int *colidx = A.colidx - base;
   const int *diagptr = A.diagptr - base;
   const double *values = A.values - base;

   lu -= base;

   int tid = omp_get_thread_num();

#pragma omp for
   for (int i = base; i < A.getNnz() + base; i++) {
      lu[i] = values[i];
   }

   for (int i = 0; i < A.m; ++i) {
      for (int j = rowptr[i]; j < diagptr[i]; ++j) {
	 int c = colidx[j];
	 double tmp = lu[j] /= lu[diagptr[c]];

	 int k1 = j + 1, k2 = diagptr[c] + 1;

	 while (k1 < rowptr[i + 1] && k2 < rowptr[c + 1]) {
	    if (colidx[k1] < colidx[k2])
	       ++k1;
	    else if (colidx[k1] > colidx[k2])
	       ++k2;
	    else {
	       lu[k1] -= tmp * lu[k2];
	       ++k1;
	       ++k2;
	    }
	 }
      }
   } // for each row
}


double dot(const double x[], const double y[], int len) {
   double sum = 0;
#pragma omp parallel for reduction(+:sum)
   for (int i = 0; i < len; ++i) {
      sum += x[i] * y[i];
   }
   return sum;
}

double norm(const double x[], int len) {
   return sqrt(dot(x, x, len));
}

// w = a*x + b*y
void waxpby(int n, double w[], double alpha, const double x[], double beta,
      const double y[]) {
   if (1 == alpha) {
      if (1 == beta) {
#pragma omp parallel for
	 for (int i = 0; i < n; ++i) {
	    w[i] = x[i] + y[i];
	 }
      } else if (-1 == beta) {
#pragma omp parallel for
	 for (int i = 0; i < n; ++i) {
	    w[i] = x[i] - y[i];
	 }
      } else {
#pragma omp parallel for
	 for (int i = 0; i < n; ++i) {
	    w[i] = x[i] + beta * y[i];
	 }
      }
   } else if (1 == beta) {
#pragma omp parallel for
      for (int i = 0; i < n; ++i) {
	 w[i] = alpha * x[i] + y[i];
      }
   } else if (-1 == alpha) {
      if (0 == beta) {
#pragma omp parallel for
	 for (int i = 0; i < n; ++i) {
	    w[i] = -x[i];
	 }
      } else {
#pragma omp parallel for
	 for (int i = 0; i < n; ++i) {
	    w[i] = beta * y[i] - x[i];
	 }
      }
   } else if (0 == beta) {
#pragma omp parallel for
      for (int i = 0; i < n; ++i) {
	 w[i] = alpha * x[i];
      }
   } else {
#pragma omp parallel for
      for (int i = 0; i < n; ++i) {
	 w[i] = alpha * x[i] + beta * y[i];
      }
   }
}



void pcg_ilu0(CSR *A, double *x, const double *b, double tol, int maxiter) {
   double spmv_time = 0;
   double trsv_time = 0;
   double ilu0_time = 0;
   double ilu0_time1 = 0;
   double ilu0_time2 = 0;
   double ilu_times[10];
   double pcg_time = 0;
   // Construct ILU0 pre-conditioner



   double *lu = MALLOC(double, A->getNnz());

   for(int num = 0; num < 10; num++){

      ilu0_time1 = omp_get_wtime();
      ilu0_ref(lu, *A);
      ilu0_time2 = omp_get_wtime();
      ilu_times[num] = ilu0_time2 - ilu0_time1;
      ilu0_time += ilu_times[num];	       

   }


   sort(ilu_times, ilu_times + 10);
   printf("median ilu0 ref time %f\n", ilu_times[5]);                                                                                                            
   //ilu0_time /= 10;       

   CSR LU(A->m, A->n, A->rowptr, A->colidx, lu);
   LU.computeInverseDiag();

   CSR *L = new CSR, *U = new CSR;
   splitLU(LU, L, U);
#pragma omp parallel for
   for (int i = 0; i < L->m; ++i) {
      L->idiag[i] = 1;
   }

   double *r = MALLOC(double, A->m);
   spmv_time -= omp_get_wtime();
   A->multiplyWithVector(r, -1, x, 1, b, 0); // r = b - A*x
   spmv_time += omp_get_wtime();
   double normr0 = norm(r, A->m);
   double rel_err = 1.;

   double *z = MALLOC(double, A->m);
   double *y = MALLOC(double, A->m);
   // z = M\r, where M is pre-conditioner
   trsv_time -= omp_get_wtime();
   forwardSolveRef(*L, y, r);
   backwardSolveRef(*U, z, y);
   trsv_time += omp_get_wtime();

   double *p = MALLOC(double, A->m);
   copyVector(p, z, A->m);
   double rz = dot(r, z, A->m);
   int k = 1;

   double *Ap = MALLOC(double, A->m);

   while (k <= maxiter) {
      pcg_time -= omp_get_wtime();
      double old_rz = rz;

      spmv_time -= omp_get_wtime();
      A->multiplyWithVector(Ap, p); // Ap = A*p
      spmv_time += omp_get_wtime();

      double alpha = old_rz / dot(p, Ap, A->m);
      waxpby(A->m, x, 1, x, alpha, p); // x += alpha*p
      waxpby(A->m, r, 1, r, -alpha, Ap); // r -= alpha*Ap
      rel_err = norm(r, A->m) / normr0;
      if (rel_err < tol){
	 pcg_time += omp_get_wtime();
	 break;
      }

      trsv_time -= omp_get_wtime();
      forwardSolveRef(*L, y, r);
      backwardSolveRef(*U, z, y);
      trsv_time += omp_get_wtime();

      rz = dot(r, z, A->m);
      double beta = rz / old_rz;
      waxpby(A->m, p, 1, z, beta, p); // p = z + beta*p
      pcg_time += omp_get_wtime();
      ++k;
   }

   printf("iter = %d rel_err = %g\n", k, rel_err);
   printf("Total pcg time: %f, Average time per iteration  %f\n", pcg_time, pcg_time/k);                                                                                                            

   double spmv_bytes = (k + 1) * (12. * A->getNnz() + (4. + 2 * 8) * A->m);
   double trsv_bytes = k
      * (12. * L->getNnz() + 12. * U->getNnz()
	    + (8. + 2 * 4 + 2 * 2 * 8) * L->m);
   double ilu0_bytes = 10*(16.*A->getNnz() + 8.*A->m + 4.*A->getNnz()); 
   printf("spmv_perf = %g gbps trsv_perf = %g gbps ilu0_perf = %g gbps\n",
	 spmv_bytes / spmv_time / 1e9, trsv_bytes / trsv_time / 1e9, ilu0_bytes / ilu0_time / 1e9);
   printf("spmv_time = %g s trsv_tme = %g s\n",
	 spmv_time , trsv_time);
   delete L;
   delete U;

   FREE (r);
   FREE (z);
   FREE (y);
   FREE (p);
   FREE (Ap);
}

void pcg_ilu0_compiler(CSR *A, double *x, const double *b, double tol, int maxiter) {
   double spmv_time = 0;
   double trsv_time = 0;
   double ilu0_time = 0;
   double pcg_time  = 0;


   double time[9];
   CSR *B, *C;

   for(int i = 0; i < 8; i++){
      B  =  gs_inspector(A, &time[i]);
      FREE(B->rowptr);
      //FREE(B->diagptr);
      FREE(B->colidx);
      //FREE(B->extptr);

      delete B;
   }
   B  =  gs_inspector(A, &time[8]);

   sort(time, time + 9);
   printf("median GS-DEP-COMPILER-TEST time %f\n", time[4]);
   time[4] -= omp_get_wtime(); 
   LevelSchedule schedule;
   schedule.constructTaskGraph(*B);
   time[4] += omp_get_wtime(); 
   printf("median GS-TOTAL-INSPECTOR time %f\n", time[4]);
   printf("GS parallelism = %g\n",
	 (double) A->m / (schedule.levIndices.size() - 1));


   for(int i = 0; i < 8; i++){
      C  =  ilu_inspector(A, &time[i]);
      FREE(C->rowptr);
      //FREE(B->diagptr);
      FREE(C->colidx);
      //FREE(B->extptr);

      delete C;
   }
   C  =  ilu_inspector(A, &time[8]);
   printf("median ILU-DEP-COMPILER-TEST time %f\n", time[4]);
   sort(time, time + 9);
   time[4] -= omp_get_wtime();
   LevelSchedule schedule2;
   schedule2.constructTaskGraph(*C);
   time[4] += omp_get_wtime();
   printf("median ILU-INSPECTOR time %f\n", time[4]);
   printf("ILU parallelism = %g\n",
	 (double) A->m / (schedule2.levIndices.size() - 1));

   // Construct ILU0 pre-conditioner
   double *lu = MALLOC(double, A->getNnz());

   for(int num = 0; num < 10; num++){
      ilu0_time -= omp_get_wtime();
      ilu_executor(lu, *A, &schedule2);
      ilu0_time += omp_get_wtime();
   }                                                        
   //ilu0_time /= 10;                                                              
   printf("median ilu0 p2p time %f\n", ilu0_time/10);
   CSR LU(A->m, A->n, A->rowptr, A->colidx, lu);
   LU.computeInverseDiag();

   CSR *L = new CSR, *U = new CSR;
   splitLU(LU, L, U);
#pragma omp parallel for
   for (int i = 0; i < L->m; ++i) {
      L->idiag[i] = 1;
   }

   const int *perm = schedule.origToThreadContPerm;
   const int *invPerm = schedule.threadContToOrigPerm;

   CSR *APerm = A->permute(perm, invPerm);
   CSR *LPerm = L->permute(perm, invPerm);
   CSR *UPerm = U->permute(perm, invPerm);

   delete L;
   delete U;
   FREE(B->rowptr);
   FREE(C->rowptr);
   FREE(B->colidx);
   FREE(C->rowptr);

   delete B;
   delete C;
   double *bPerm = getReorderVectorWithInversePerm(b, invPerm, A->m);
   double *xPerm = getReorderVectorWithInversePerm(x, invPerm, A->m);

   double *r = MALLOC(double, A->m);
   spmv_time -= omp_get_wtime();
   APerm->multiplyWithVector(r, -1, xPerm, 1, bPerm, 0); // r = b - A*x
   spmv_time += omp_get_wtime();
   double normr0 = norm(r, A->m);
   double rel_err = 1.;

   double *z = MALLOC(double, A->m);
   double *y = MALLOC(double, A->m);
   // z = M\r, where M is pre-conditioner
   trsv_time -= omp_get_wtime();
   gs_executor(*LPerm, y, r, &schedule);
   bgs_executor(*UPerm, z, y, &schedule);
   trsv_time += omp_get_wtime();

   double *p = MALLOC(double, A->m);
   copyVector(p, z, A->m);
   double rz = dot(r, z, A->m);
   int k = 1;

   double *Ap = MALLOC(double, A->m);

   while (k <= maxiter) {
      pcg_time -= omp_get_wtime();
      double old_rz = rz;

      spmv_time -= omp_get_wtime();
      APerm->multiplyWithVector(Ap, p); // Ap = A*p
      spmv_time += omp_get_wtime();

      double alpha = old_rz / dot(p, Ap, A->m);
      waxpby(A->m, xPerm, 1, xPerm, alpha, p); // x += alpha*p
      waxpby(A->m, r, 1, r, -alpha, Ap); // r -= alpha*Ap
      rel_err = norm(r, A->m) / normr0;
      if (rel_err < tol){
	 pcg_time += omp_get_wtime();
	 break;
      }

      trsv_time -= omp_get_wtime();
      gs_executor(*LPerm, y, r, &schedule);
      bgs_executor(*UPerm, z, y, &schedule);
      trsv_time += omp_get_wtime();

      rz = dot(r, z, A->m);
      double beta = rz / old_rz;
      waxpby(A->m, p, 1, z, beta, p); // p = z + beta*p

      pcg_time += omp_get_wtime();
      ++k;
   }

   reorderVectorOutOfPlaceWithInversePerm(x, xPerm, perm, A->m);

   printf("Total pcg time: %f, Average time per iteration  %f\n", pcg_time, pcg_time/k);                                                                                                            
   printf("iter = %d, rel_err = %g\n", k, rel_err);

   double spmv_bytes = (k + 1) * (12. * A->getNnz() + (4. + 2 * 8) * A->m);
   double trsv_bytes = k
      * (12. * LPerm->getNnz() + 12. * UPerm->getNnz()
	    + (8. + 2 * 4 + 2 * 2 * 8) * LPerm->m);
   double ilu0_bytes = 10*(16.*A->getNnz() + 8.*A->m + 4.*A->getNnz());   
   printf("spmv_perf = %g gbps trsv_perf = %g gbps ilu0_perf = %g gbps\n",
	 spmv_bytes / spmv_time / 1e9, trsv_bytes / trsv_time / 1e9, ilu0_bytes / ilu0_time / 1e9);
   printf("spmv_time = %g s trsv_tme = %g s\n",
	 spmv_time , trsv_time);                                        

   delete APerm;
   delete LPerm;
   delete UPerm;

   FREE (r);
   FREE (z);
   FREE (y);
   FREE (p);
   FREE (Ap);

   FREE(bPerm);
   FREE(xPerm);
}

int main(int argc, char *argv[]) {
   int m = argc > 1 ? atoi(argv[1]) : 64; // default input is 64^3 27-pt 3D Lap.
   if (argc < 2) {
      fprintf(stderr,
	    "Using default 64^3 27-pt 3D Laplacian matrix\n"
	    "-- Usage examples --\n"
	    "  %s 128 : 128^3 27-pt 3D Laplacian matrix\n"
	    "  %s inline_1: run with inline_1.mtx matrix in matrix market format\n\n",
	    argv[0], argv[0]);
   }

   char buf[1024];
   sprintf(buf, "%d", m);
   printf("input=%s\n", argc > 1 ? argv[1] : buf);

   CSR A(argc > 1 ? argv[1] : buf);
   double *x = MALLOC(double, A.m);
   double *x1 = MALLOC(double, A.m);
   double *y = MALLOC(double, A.m);
   double *x_ref = MALLOC(double, A.m);
   double *x_ref2 = MALLOC(double, A.m);
   double *b = NULL;
   if (argc > 1 && strlen(argv[1]) >= 4) {
      // load rhs file if it exists
      char buf2[1024];
      strncpy(buf2, argv[1], strlen(argv[1]) - 4);
      buf2[strlen(argv[1]) - 4] = 0;
      int m, n;
      loadVectorMatrixMarket((string(buf2) + "_b.mtx").c_str(), &b, &m, &n);
   }
   if (!b) {
      b = MALLOC(double, A.m);
      for (int i = 0; i < A.m; ++i)
	 b[i] = 1;
   }

   double tol = 1e-7;
   int maxiter = 20000;

   for (int i = 0; i < A.m; ++i)
      x_ref2[i] = 0;
   pcg_ilu0(&A, x_ref2, b, tol, maxiter);


   for (int i = 0; i < A.m; ++i)
      x[i] = 0;
   pcg_ilu0_compiler(&A, x, b, tol, maxiter);

   correctnessCheck<double>(x_ref2, x, A.m); //changed by Anand reference values

   for (int i = 0; i < A.m; ++i)
      x1[i] = 0;
   pcg_ilu0_handtuned(&A, x1, b, tol, maxiter);

   correctnessCheck<double>(x_ref2, x1, A.m);

   FREE(x1);
   FREE(x);
   FREE(b);
   FREE(x_ref);
   FREE(y);
   synk::Barrier::deleteInstance();
   return 0;
}

