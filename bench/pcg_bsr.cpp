#include <cmath>
#include <cstring>

#include <mkl.h>

#include "SpMP/Vector.hpp"
#include "SpMP/synk/barrier.hpp"

#include "SpMP/test/test.hpp"

#include "BSR.hpp"
#include "misc.hpp"

using namespace SpMP;

/**
 * Reference sequential sparse triangular solver
 */
template<int BS>
void forwardSolveRef_(const BSR& A, double y[], const double b[])
{
  ADJUST_FOR_BASE;

  double sum[BS];

  for (int i = base; i < A.m + base; ++i) {
    for (int ii = 0; ii < BS; ++ii) {
      sum[ii] = b[i*BS + ii];
    }
    for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
      int c = colidx[j]*BS;
      for (int jj = 0; jj < BS; ++jj) {
        double yVal = y[c + jj];
        for (int ii = 0; ii < BS; ++ii) {
          sum[ii] -= values[(j*BS + jj)*BS + ii]*yVal;
        }
      }
    }

    for (int ii = 0; ii < BS; ++ii) {
      y[i*BS + ii] = 0;
    }
    for (int jj = 0; jj < BS; ++jj) {
      for (int ii = 0; ii < BS; ++ii) {
        y[i*BS + ii] += idiag[(i*BS + jj)*BS + ii]*sum[jj];
      }
    }
  } // for each row
}

void forwardSolveRefGeneral_(const BSR& A, double y[], const double b[])
{
  ADJUST_FOR_BASE;

  int BS = A.bs;
  double sum[BS];

  for (int i = base; i < A.m + base; ++i) {
    for (int ii = 0; ii < BS; ++ii) {
      sum[ii] = b[i*BS + ii];
    }
    for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
      int c = colidx[j]*BS;
      for (int jj = 0; jj < BS; ++jj) {
        double yVal = y[c + jj];
        for (int ii = 0; ii < BS; ++ii) {
          sum[ii] -= values[(j*BS + jj)*BS + ii]*yVal;
        }
      }
    }

    for (int ii = 0; ii < BS; ++ii) {
      y[i*BS + ii] = 0;
    }
    for (int jj = 0; jj < BS; ++jj) {
      for (int ii = 0; ii < BS; ++ii) {
        y[i*BS + ii] += idiag[(i*BS + jj)*BS + ii]*sum[jj];
      }
    }
  } // for each row
}

void forwardSolveRef(const BSR& A, double y[], const double b[])
{
  switch (A.bs) {
  case 4:
    forwardSolveRef_<4>(A, y, b);
    break;
  default:
    forwardSolveRefGeneral_(A, y, b);
    break;
  }
}

template<int BS>
void backwardSolveRef_(const BSR& A, double y[], const double b[])
{
  ADJUST_FOR_BASE;

  double sum[BS];

  for (int i = A.m - 1 + base; i >= base; --i) {
    for (int ii = 0; ii < BS; ++ii) {
      sum[ii] = b[i*BS + ii];
    }
    for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
      int c = colidx[j]*BS;
      for (int jj = 0; jj < BS; ++jj) {
        double yVal = y[c + jj];
        for (int ii = 0; ii < BS; ++ii) {
          sum[ii] -= values[(j*BS + jj)*BS + ii]*yVal;
        }
      }
    }

    for (int ii = 0; ii < BS; ++ii) {
      y[i*BS + ii] = 0;
    }
    for (int jj = 0; jj < BS; ++jj) {
      for (int ii = 0; ii < BS; ++ii) {
        y[i*BS + ii] += idiag[(i*BS + jj)*BS + ii]*sum[jj];
      }
    }
  } // for each row
}

void backwardSolveRefGeneral_(const BSR& A, double y[], const double b[])
{
  ADJUST_FOR_BASE;

  int BS = A.bs;
  double sum[BS];

  for (int i = A.m - 1 + base; i >= base; --i) {
    for (int ii = 0; ii < BS; ++ii) {
      sum[ii] = b[i*BS + ii];
    }
    for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
      int c = colidx[j]*BS;
      for (int jj = 0; jj < BS; ++jj) {
        double yVal = y[c + jj];
        for (int ii = 0; ii < BS; ++ii) {
          sum[ii] -= values[(j*BS + jj)*BS + ii]*yVal;
        }
      }
    }

    for (int ii = 0; ii < BS; ++ii) {
      y[i*BS + ii] = 0;
    }
    for (int jj = 0; jj < BS; ++jj) {
      for (int ii = 0; ii < BS; ++ii) {
        y[i*BS + ii] += idiag[(i*BS + jj)*BS + ii]*sum[jj];
      }
    }
  } // for each row
}

void backwardSolveRef(const BSR& A, double y[], const double b[])
{
  switch (A.bs) {
  case 4:
    backwardSolveRef_<4>(A, y, b);
    break;
  default:
    backwardSolveRefGeneral_(A, y, b);
    break;
  }
}

BSR *expandCSRToBSR(const CSR& A, int bs)
{
  CSR A2(A.m*bs, A.n*bs, A.getNnz()*bs*bs);

  int nnz = 0;
  for (int i = 0; i < A.m; ++i) {
    for (int ii = 0; ii < bs; ++ii) {
      A2.rowptr[i*bs + ii] = nnz;
      for (int j = A.rowptr[i]; j < A.rowptr[i + 1]; ++j) {
        A2.colidx[nnz] = A.colidx[j]*bs + ii;
        A2.values[nnz] = A.values[j];
        ++nnz;
      } // for each nnz
    } // for each row
  } // for each block row
  A2.rowptr[A.m*bs] = nnz;
  assert(nnz == A.getNnz()*bs);

  //printf("A2\n");
  //A2.print();

  return new BSR(A2, bs);
}

double *expandVector(const double *x, int n, int bs)
{
  double *x2 = MALLOC(double, n*bs);
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < bs; ++j) {
      x2[i*bs + j] = x[i];
    }
  }
  return x2;
}

void transpose(double *dst, double *src, int bs)
{
  for (int j = 0; j < bs; ++j) {
    for (int i = 0; i < bs; ++i) {
      dst[j*bs + i] = src[i*bs + j];
    }
  }
}

void splitLU(BSR& A, BSR *L, BSR *U)
{
  int oldBase = A.getBase();
  //A.make0BasedIndexing();

  L->dealloc();
  U->dealloc();

  L->m = U->m = A.m;
  L->n = U->n = A.n;
  L->bs = U->bs = A.bs;

  const int *extptr = A.rowptr + 1;
  /*const int *extptr = A.extptr ? A.extptr : A.rowptr + 1;
  if (A.extptr) {
    U->extptr = MALLOC(int, U->m);
  }*/

  L->rowptr = MALLOC(int, L->m + 1);
  U->rowptr = MALLOC(int, U->m + 1);
  L->idiag = MALLOC(double, L->m*A.bs*A.bs);
  U->idiag = MALLOC(double, U->m*A.bs*A.bs);

  // Count # of nnz per row
  int rowPtrPartialSum[2][omp_get_max_threads() + 1];
  rowPtrPartialSum[0][0] = rowPtrPartialSum[1][0] = 0;

#pragma omp parallel
  {
    int nthreads = omp_get_num_threads();
    int tid = omp_get_thread_num();

    int iBegin, iEnd;
    getSimpleThreadPartition(&iBegin, &iEnd, A.m);

    // count # of nnz per row
    int sum0 = 0, sum1 = 0;
    for (int i = iBegin; i < iEnd; ++i) {
      L->rowptr[i] = sum0;
      U->rowptr[i] = sum1;

      for (int j = A.rowptr[i]; j < extptr[i]; ++j) {
        if (A.colidx[j] < i) sum0++;
        if (A.colidx[j] > i) sum1++;
      } // for each element
      sum1 += A.rowptr[i + 1] - extptr[i];
    } // for each row

    rowPtrPartialSum[0][tid + 1] = sum0;
    rowPtrPartialSum[1][tid + 1] = sum1;

#pragma omp barrier
#pragma omp single
    {
      for (int i = 1; i < nthreads; ++i) {
        rowPtrPartialSum[0][i + 1] += rowPtrPartialSum[0][i];
        rowPtrPartialSum[1][i + 1] += rowPtrPartialSum[1][i];
      }
      L->rowptr[L->m] = rowPtrPartialSum[0][nthreads];
      U->rowptr[U->m] = rowPtrPartialSum[1][nthreads];

      int nnzBlocksL = L->rowptr[L->m];
      int nnzBlocksU = U->rowptr[U->m];

      L->colidx = MALLOC(int, nnzBlocksL);
      L->values = MALLOC(double, nnzBlocksL*A.bs*A.bs);

      U->colidx = MALLOC(int, nnzBlocksU);
      U->values = MALLOC(double, nnzBlocksU*A.bs*A.bs);
    }

    for (int i = iBegin; i < iEnd; ++i) {
      L->rowptr[i] += rowPtrPartialSum[0][tid];
      U->rowptr[i] += rowPtrPartialSum[1][tid];

      /*if (A.extptr && i > iBegin) {
        U->extptr[i - 1] = U->rowptr[i] - (A.rowptr[i] - extptr[i - 1]);
      }*/

      int idx0 = L->rowptr[i], idx1 = U->rowptr[i];
      for (int j = A.rowptr[i]; j < A.rowptr[i + 1]; ++j) {
        if (A.colidx[j] < i) {
          L->colidx[idx0] = A.colidx[j];
          for (int k = 0; k < A.bs*A.bs; ++k) {
            L->values[idx0*A.bs*A.bs + k] = A.values[j*A.bs*A.bs + k];
          }
          ++idx0;
        }
        if (A.colidx[j] > i) {
          U->colidx[idx1] = A.colidx[j];
          for (int k = 0; k < A.bs*A.bs; ++k) {
            U->values[idx1*A.bs*A.bs + k] = A.values[j*A.bs*A.bs + k];
          }
          ++idx1;
        }
      }
    } // for each row
  } // omp parallel

  /*if (1 == oldBase) {
    A.make1BasedIndexing();
    L->make1BasedIndexing();
    U->make1BasedIndexing();
  }*/
}

// C = A*B, all matrices are bs x bs column major
void multiply(double *C, const double *A, const double *B, int bs)
{
  for (int j = 0; j < bs; ++j) {
    for (int i = 0; i < bs; ++i) {
      double sum = 0;
      for (int k = 0; k < bs; ++k) {
        sum += A[k*bs + i]*B[j*bs + k];
      }
      C[j*bs + i] = sum;
    }
  }
}

/**
 * Basic w/o pivoting factorization of square block
 * @param A column-major dense matrix
 */
void dsqrpanel(int n, double *A, int lda)
{
  for (int i = 0; i < n; ++i) {
    double d = A[i*lda + i];
    double id = 1/d;
    cblas_dscal(n - i - 1, id, A + i*lda + i + 1, 1);

    for (int r = i + 1; r < n; ++r) {
      for (int c = i + 1; c < n; ++c) {
        A[c*lda + r] -= A[i*lda + r]*A[c*lda + i];
      }
    }
  }
}

// serial ilu0
void ilu0_ref(double *lu, const BSR& A, double *lidiag, double *uidiag)
{
  int base = A.getBase();

  const int *rowptr = A.rowptr - base;
  const int *colidx = A.colidx - base;
  const int *diagptr = A.diagptr - base;
  const double *values = A.values - base;

  lu -= base;

  int tid = omp_get_thread_num();

#pragma omp for
  for (int i = base; i < A.getNnz() + base; i++) {
    lu[i] = values[i];
  }

  double buf[A.bs*A.bs];

  for (int i = 0; i < A.m; ++i) {
    for (int j = rowptr[i]; j < diagptr[i]; ++j) {
      int c = colidx[j];
      /*cblas_dtrsm(CblasColMajor, CblasRight, CblasUpper, CblasNoTrans,
                  CblasNonUnit, A.bs, A.bs, 1, lu + A.diagptr[c]*A.bs*A.bs, A.bs, lu + j*A.bs*A.bs, A.bs);*/
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, A.bs, A.bs, A.bs,
                  1, uidiag + c*A.bs*A.bs, A.bs, lu + j*A.bs*A.bs, A.bs, 0, buf, A.bs);
      memcpy(lu + j*A.bs*A.bs, buf, sizeof(buf));

      int k1 = j + 1, k2 = diagptr[c] + 1;

      while (k1 < rowptr[i + 1] && k2 < rowptr[c + 1]) {
        if (colidx[k1] < colidx[k2]) ++k1;
        else if (colidx[k1] > colidx[k2]) ++k2;
        else {
          cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, A.bs, A.bs, A.bs,
                      -1, lu + j*A.bs*A.bs, A.bs, lu + k2*A.bs*A.bs, A.bs, 1, lu + k1*A.bs*A.bs, A.bs);
          ++k1; ++k2;
        }
      }
    }

    // inverse normalized upper triangular of diagonal
    for (int ii = 0; ii < A.bs; ++ii) {
      for (int jj = 0; jj <= ii; ++jj) {
        uidiag[(i*A.bs + jj)*A.bs + ii] = lu[(diagptr[i]*A.bs + jj)*A.bs + ii];///lu[(diagptr[i]*A.bs + ii)*A.bs + ii];
      }
    }
    MKL_INT info;
    dtrti2("U", "N", &A.bs, uidiag + i*A.bs*A.bs, &A.bs, &info);

    // inverse normalized lower triangular of diagonal
    for (int ii = 0; ii < A.bs; ++ii) {
      for (int jj = ii; jj <= A.bs; ++jj) {
        lidiag[(i*A.bs + jj)*A.bs + ii] = lu[(diagptr[i]*A.bs + jj)*A.bs + ii]/lu[(diagptr[i]*A.bs + ii)*A.bs + ii];
      }
    }
    dtrti2("L", "N", &A.bs, lidiag + i*A.bs*A.bs, &A.bs, &info);

    //dtrti2("U", "N", &A.bs, lu + diagptr[i]*A.bs*A.bs, &A.bs, &info);

    // panel factorization
    /*dsqrpanel(A.bs, lu + diagptr[i]*A.bs*A.bs, A.bs);
    printf("%g\n", lu[diagptr[i]]);*/

    // multiply row panel by inverse of factorized panel's lower triangular
#if 0
    for (int j = diagptr[i] + 1; j < rowptr[i + 1]; ++j) {
      cblas_dgemm(CblasColMajor, CblasNoTrans, CblasNoTrans, A.bs, A.bs, A.bs,
                  1, lidiag + i*A.bs*A.bs, A.bs, lu + j*A.bs*A.bs, A.bs, 0, buf, A.bs);
      memcpy(lu + j*A.bs*A.bs, buf, sizeof(buf));

      /*cblas_dtrsm(CblasColMajor, CblasLeft, CblasLower, CblasNoTrans,
                  CblasUnit, A.bs, A.bs, 1, lu + diagptr[i]*A.bs*A.bs, A.bs, lu + j*A.bs*A.bs, A.bs);*/
    }
#endif
  } // for each row
}

/**
 * Reference sequential sparse triangular solver
 */
void forwardSolveRef(const CSR& A, double y[], const double b[])
{
  ADJUST_FOR_BASE;

  for (int i = base; i < A.m + base; ++i) {
    double sum = b[i];
    for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
      sum -= values[j]*y[colidx[j]];
    }
    y[i] = sum*idiag[i];
  } // for each row
}

void backwardSolveRef(const CSR& A, double y[], const double b[])
{
  ADJUST_FOR_BASE;

  for (int i = A.m - 1 + base; i >= base; --i) {
    double sum = b[i];
    for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
      sum -= values[j]*y[colidx[j]];
    }
    y[i] = sum*idiag[i];
  } // for each row
}

void pcg_symgs(BSR *A, double *x, const double *b, double tol, int maxiter)
{
  double spmv_time = 0;
  double trsv_time = 0;

  // Construct SymGS pre-conditioner
  BSR *L = new BSR, *U = new BSR;
  splitLU(*A, L, U);
#pragma omp parallel for
  for (int i = 0; i < U->m; ++i) {
    for (int j = U->rowptr[i]; j < U->rowptr[i + 1]; ++j) {
      for (int jj = 0; jj < A->bs; ++jj) {
        for (int ii = 0; ii < A->bs; ++ii) {
          U->values[(j*A->bs + jj)*A->bs + ii] /= A->values[(A->diagptr[i]*A->bs + ii)*A->bs + ii];
        }
      }
    }

    for (int ii = 0; ii < A->bs; ++ii) {
      for (int jj = ii; jj < A->bs; ++jj) {
        U->idiag[(i*A->bs + jj)*A->bs + ii] = A->values[(A->diagptr[i]*A->bs + jj)*A->bs + ii]/A->values[(A->diagptr[i]*A->bs + ii)*A->bs + ii];
      }
    }
    MKL_INT info;
    dtrti2("U", "U", &A->bs, U->idiag + i*A->bs*A->bs, &A->bs, &info);

    for (int ii = 0; ii < A->bs; ++ii) {
      for (int jj = 0; jj <= ii; ++jj) {
        L->idiag[(i*A->bs + jj)*A->bs + ii] = A->values[(A->diagptr[i]*A->bs + jj)*A->bs + ii];
      }
    }
    dtrti2("L", "N", &A->bs, L->idiag + i*A->bs*A->bs, &A->bs, &info);
  }

  double *r = MALLOC(double, A->m*A->bs);
  spmv_time -= omp_get_wtime();
  A->multiplyWithVector(r, -1, x, 1, b, 0); // r = b - A*x
  spmv_time += omp_get_wtime();
  double normr0 = norm(r, A->m*A->bs);
  double rel_err = 1.;

  double *z = MALLOC(double, A->m*A->bs);
  double *y = MALLOC(double, A->m*A->bs);
  trsv_time -= omp_get_wtime();
  forwardSolveRef(*L, y, r);
  backwardSolveRef(*U, z, y);
  trsv_time += omp_get_wtime();

  double *p = MALLOC(double, A->m*A->bs);
  copyVector(p, z, A->m*A->bs);
  double rz = dot(r, z, A->m*A->bs);
  int k = 1;

  double *Ap = MALLOC(double, A->m*A->bs);

  while (k <= maxiter) {
    double old_rz = rz;

    spmv_time -= omp_get_wtime();
    A->multiplyWithVector(Ap, p); // Ap = A*p
    spmv_time += omp_get_wtime();

    double alpha = old_rz/dot(p, Ap, A->m*A->bs);
    waxpby(A->m*A->bs, x, 1, x, alpha, p); // x += alpha*p
    waxpby(A->m*A->bs, r, 1, r, -alpha, Ap); // r -= alpha*Ap
    rel_err = norm(r, A->m*A->bs)/normr0;
    if (rel_err < tol) break;

    trsv_time -= omp_get_wtime();
    forwardSolveRef(*L, y, r);
    backwardSolveRef(*U, z, y);
    trsv_time += omp_get_wtime();

    rz = dot(r, z, A->m*A->bs);
    double beta = rz/old_rz;
    waxpby(A->m*A->bs, p, 1, z, beta, p); // p = z + beta*p
    ++k;
  }

  printf("iter = %d rel_err = %g\n", k, rel_err);

  double spmv_bytes = (k + 1)*(8.*A->getNnz() + 4.*A->getNnzBlocks() + 4.*A->m + 2*8*A->m*A->bs);
  double trsv_bytes = k*(8.*(L->getNnz() + U->getNnz()) + 4.*(L->getNnzBlocks() + U->getNnzBlocks()) + 2*4*L->m + (8. + 2*2*8)*L->m*A->bs);
  printf("spmv_perf = %g gbps trsv_perf = %g gbps\n", spmv_bytes/spmv_time/1e9, trsv_bytes/trsv_time/1e9);

  delete L;
  delete U;

  FREE(r);
  FREE(z);
  FREE(y);
  FREE(p);
  FREE(Ap);
}

void pcg_ilu0(BSR *A, double *x, const double *b, double tol, int maxiter)
{
  double spmv_time = 0;
  double trsv_time = 0;

  // Construct SymGS pre-conditioner
  double *lu = MALLOC(double, A->getNnz());
  double *lidiag = MALLOC(double, A->m*A->bs*A->bs);
  double *uidiag = MALLOC(double, A->m*A->bs*A->bs);
  ilu0_ref(lu, *A, lidiag, uidiag);

  BSR LU;
  LU.m = A->m;
  LU.n = A->n;
  LU.bs = A->bs;
  LU.rowptr = A->rowptr;
  LU.colidx = A->colidx;
  LU.values = lu;
  LU.diagptr = A->diagptr;
  LU.idiag = MALLOC(double, A->m*A->bs*A->bs);

  BSR *L = new BSR, *U = new BSR;
  splitLU(LU, L, U);
  L->idiag = lidiag;
  U->idiag = uidiag;
  //printf("U before normalization\n");
  //U->print();
#pragma omp parallel for
  for (int i = 0; i < L->m; ++i) {
#if 0
    for (int j = U->rowptr[i]; j < U->rowptr[i + 1]; ++j) {
      for (int jj = 0; jj < A->bs; ++jj) {
        for (int ii = 0; ii < A->bs; ++ii) {
          U->values[(j*A->bs + jj)*A->bs + ii] /= lu[(A->diagptr[i]*A->bs + ii)*A->bs + ii];
        }
      }
    }

    for (int ii = 0; ii < A->bs; ++ii) {
      /*for (int jj = 0; jj < ii; ++jj) {
        U->idiag[(i*A->bs + jj)*A->bs + ii] = 0;
      }*/
      for (int jj = ii; jj < A->bs; ++jj) {
        U->idiag[(i*A->bs + jj)*A->bs + ii] = lu[(A->diagptr[i]*A->bs + jj)*A->bs + ii]/lu[(A->diagptr[i]*A->bs + ii)*A->bs + ii];
      }
    }
    MKL_INT info;
    dtrti2("U", "U", &A->bs, U->idiag + i*A->bs*A->bs, &A->bs, &info);

    for (int ii = 0; ii < A->bs; ++ii) {
      for (int jj = 0; jj <= ii; ++jj) {
        L->idiag[(i*A->bs + jj)*A->bs + ii] = lu[(A->diagptr[i]*A->bs + jj)*A->bs + ii];
      }
      /*for (int jj = ii + 1; jj < A->bs; ++jj) {
        L->idiag[(i*A->bs + jj)*A->bs + ii] = 0;
      }*/
    }
    dtrti2("L", "N", &A->bs, L->idiag + i*A->bs*A->bs, &A->bs, &info);

    /*for (int ii = 0; ii < A->bs; ++ii) {
      for (int jj = 0; jj < ii; ++jj) {
        U->idiag[(i*A->bs + jj)*A->bs + ii] = 0;
      }
      for (int jj = ii; jj < A->bs; ++jj) {
        U->idiag[(i*A->bs + jj)*A->bs + ii] = lu[(A->diagptr[i]*A->bs + jj)*A->bs + ii];
        printf("%g\n", U->idiag[(i*A->bs + jj)*A->bs + ii]);
      }
    }
    MKL_INT info;
    dtrti2("U", "N", &A->bs, U->idiag + i*A->bs*A->bs, &A->bs, &info);
    printf("%g\n", U->idiag[(i*A->bs + 0)*A->bs + 0]);*/

    /*for (int jj = 0; jj < A->bs; ++jj) {
      for (int ii = 0; ii < jj; ++ii) {
        L->idiag[(i*A->bs + jj)*A->bs + ii] = 0;
      }
      L->idiag[(i*A->bs + jj)*A->bs + jj] = 1;
      for (int ii = jj + 1; ii < jj; ++ii) {
        L->idiag[(i*A->bs + jj)*A->bs + ii] = 0;
      }
    }*/

    /*for (int ii = 0; ii < A->bs; ++ii) {
      for (int jj = 0; jj <= ii; ++jj) {
        L->idiag[(i*A->bs + jj)*A->bs + ii] = lu[(A->diagptr[i]*A->bs + jj)*A->bs + ii]/lu[(A->diagptr[i]*A->bs + jj)*A->bs + ii];
      }
      for (int jj = ii + 1; jj < A->bs; ++jj) {
        L->idiag[(i*A->bs + jj)*A->bs + ii] = 0;
      }
    }
    dtrti2("L", "N", &A->bs, L->idiag + i*A->bs*A->bs, &A->bs, &info);*/
#endif
  }

  printf("U after normalization\n");
  U->print();
  printf("L\n");
  L->print();

  double *r = MALLOC(double, A->m*A->bs);
  spmv_time -= omp_get_wtime();
  A->multiplyWithVector(r, -1, x, 1, b, 0); // r = b - A*x
  spmv_time += omp_get_wtime();
  double normr0 = norm(r, A->m*A->bs);
  //printf("normr0 = %g\n", normr0);
  double rel_err = 1.;

  double *z = MALLOC(double, A->m*A->bs);
  double *y = MALLOC(double, A->m*A->bs);
  // z = M\r, where M is pre-conditioner
  trsv_time -= omp_get_wtime();
  forwardSolveRef(*L, y, r);
  backwardSolveRef(*U, z, y);
  trsv_time += omp_get_wtime();
  //printf("norm(y) = %g\n", norm(z, A->m*A->bs));
  //printf("norm(z) = %g\n", norm(z, A->m*A->bs));

  double *p = MALLOC(double, A->m*A->bs);
  copyVector(p, z, A->m*A->bs);
  double rz = dot(r, z, A->m*A->bs);
  int k = 1;

  double *Ap = MALLOC(double, A->m*A->bs);

  while (k <= maxiter) {
    double old_rz = rz;

    spmv_time -= omp_get_wtime();
    A->multiplyWithVector(Ap, p); // Ap = A*p
    //printf("norm(Ap) = %g\n", norm(Ap, A->m*A->bs));
    spmv_time += omp_get_wtime();

    double alpha = old_rz/dot(p, Ap, A->m*A->bs);
    //printf("alpha = %g\n", alpha);
    waxpby(A->m*A->bs, x, 1, x, alpha, p); // x += alpha*p
    /*for (int i = 0; i < A->m*A->bs; ++i) {
      printf("%g %g\n", r[i], Ap[i]);
    }*/
    waxpby(A->m*A->bs, r, 1, r, -alpha, Ap); // r -= alpha*Ap
    //printf("norm(r) = %g\n", norm(r, A->m*A->bs));
    rel_err = norm(r, A->m*A->bs)/normr0;
    //printf("%g\n", rel_err);
    if (rel_err < tol) break;

    trsv_time -= omp_get_wtime();
    forwardSolveRef(*L, y, r);
    backwardSolveRef(*U, z, y);
    trsv_time += omp_get_wtime();

    rz = dot(r, z, A->m*A->bs);
    double beta = rz/old_rz;
    waxpby(A->m*A->bs, p, 1, z, beta, p); // p = z + beta*p
    ++k;
  }

  printf("iter = %d rel_err = %g\n", k, rel_err);

  double spmv_bytes = (k + 1)*(8.*A->getNnz() + 4.*A->getNnzBlocks() + 4.*A->m + 2*8*A->m*A->bs);
  double trsv_bytes = k*(8.*(L->getNnz() + U->getNnz()) + 4.*(L->getNnzBlocks() + U->getNnzBlocks()) + 2*4*L->m + (8. + 2*2*8)*L->m*A->bs);
  printf("spmv_perf = %g gbps trsv_perf = %g gbps\n", spmv_bytes/spmv_time/1e9, trsv_bytes/trsv_time/1e9);

  delete L;
  delete U;

  FREE(r);
  FREE(z);
  FREE(y);
  FREE(p);
  FREE(Ap);
}

int main(int argc, char *argv[])
{
  if (argc < 3) {
    fprintf(
      stderr,
      "-- Usage examples --\n"
      "  %s 128 4 : 128^3 27-pt 3D Laplacian matrix with 4x4 blocks\n"
      "  %s inline_1 4: run with inline_1.mtx matrix in matrix market format with 4x4 blocks\n\n",
      argv[0], argv[0]);
    return -1;
  }

  printf("input=%s\n", argv[1]);

  int bs = atoi(argv[2]);

  CSR ACSR(argv[1], 0 /*base*/, false /*don't force symmetric*/, bs);
  BSR *A = new BSR(ACSR, bs);
  double *x = MALLOC(double, A->m*bs);
  double *b = NULL;
  if (strlen(argv[1]) >= 4) {
    // load rhs file if it exists
    char buf[1024];
    strncpy(buf, argv[1], strlen(argv[1]) - 4);
    buf[strlen(argv[1]) - 4] = 0;
    int m, n;
    loadVectorMatrixMarket((string(buf) + "_b.mtx").c_str(), &b, &m, &n);
  }
  if (!b) {
    b = MALLOC(double, A->m*bs);
    for (int i = 0; i < A->m*bs; ++i) b[i] = 1;
  }

  double tol = 1e-7;
  int maxiter = 20000;

  //printf("A\n");
  //A->print();
  for (int i = 0; i < A->m*bs; ++i) x[i] = 0;
  pcg_symgs(A, x, b, tol, maxiter);

  /*for (int i = 0; i < A->m*bs; ++i) x[i] = 0;
  pcg_symgs_opt(&A, x, b, tol, maxiter);*/

  /*for (int i = 0; i < A->m*bs; ++i) x[i] = 0;
  pcg_ilu0(A, x, b, tol, maxiter);*/

  /*for (int i = 0; i < A->m*bs; ++i) x[i] = 0;
  pcg_ilu0_opt(&A, x, b, tol, maxiter);*/

  FREE(x);
  FREE(b);

  return 0;
}
