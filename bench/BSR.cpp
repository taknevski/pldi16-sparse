#include <cstring>

#include <mkl.h>

#include "BSR.hpp"

namespace SpMP
{

BSR::BSR() : rowptr(NULL), colidx(NULL), values(NULL), idiag(NULL), diagptr(NULL), ownData_(false)
{
}

BSR::BSR(const CSR& A, int bs) : bs(bs)
{
  alloc((A.m + bs - 1)/bs, A.getNnz());

  MKL_INT job[6], info;
  MKL_INT ldabsr = bs*bs;
  job[0] = 0; // CSR -> BSR
  job[1] = A.getBase();
  job[2] = 0; // zero-based indexing in BSR
  job[5] = 1; // all output arrays filled

  mkl_dcsrbsr(
    job, &A.m, &bs, &ldabsr,
    A.values, A.colidx, A.rowptr,
    values, colidx, rowptr,
    &info);

  // transpose each block to column-major for efficient vectorization
#pragma omp parallel for
  for (int j = 0; j < rowptr[m]; ++j) {
    double buf[bs*bs];
    memcpy(buf, values + j*bs*bs, sizeof(double)*bs*bs);
    for (int jj = 0; jj < bs; ++jj) {
      for (int ii = 0; ii < bs; ++ii) {
        values[(j*bs + jj)*bs + ii] = buf[ii*bs + jj];
      }
    }
  }

#pragma omp parallel for
  for (int i = 0; i < m; ++i) {
    for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
      if (colidx[j] == i) {
        diagptr[i] = j;
        break;
      }
    }
  }

  /*if (idiag) {
#pragma omp parallel for
    for (int i = 0; i < m; ++i) {
      for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
        if (colidx[j] == i) {
          for (int ii = 0; ii < bs; ++ii) {
            idiag[i*bs + ii] = 1/values[(j*bs + ii)*bs + ii];
          }
        }
      }
    }
  }*/

  if (info) fprintf(stderr, "mkl_dcsrbsr returned info with value %d\n", info);
}

void BSR::alloc(int m, int nnzBlocks, bool createSeparateDiagData /*= true*/)
{
  this->m = m;

  rowptr = MALLOC(int, m + 1);
  colidx = MALLOC(int, nnzBlocks);
  values = MALLOC(double, nnzBlocks*bs*bs);
  diagptr = MALLOC(int, m);

  assert(rowptr);
  assert(colidx);
  assert(values);
  assert(diagptr);

  if (createSeparateDiagData) {
    idiag = MALLOC(double, m*bs*bs);
  }

  ownData_ = true;
}

void BSR::dealloc()
{
  if (ownData_) {
    FREE(rowptr);
    FREE(colidx);
    FREE(values);
  }

  FREE(idiag);
  FREE(diagptr);
}

void BSR::print() const
{
  for (int i = 0; i < m; ++i) {
    for (int ii = 0; ii < bs; ++ii) {
      for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
        for (int jj = 0; jj < bs; ++jj) {
          printf("%d %d %g\n", i*bs + ii , colidx[j]*bs + jj, values[(j*bs + jj)*bs + ii]);
        }
      }
    }
    for (int ii = 0; ii < bs; ++ii) {
      for (int jj = 0; jj < bs; ++jj) {
        printf(" %g", idiag[(i*bs + jj)*bs + ii]);
      }
      printf("\n");
    }
  }
}

template<class T, int BS>
static void SpMVBSR_(
  int m,
  T *w,
  T alpha,
  const int *rowptr, const int *colidx, const T* values,
  const T *x,
  T beta,
  const T *y,
  T gamma)
{
  assert(w != x);

  int base = rowptr[0];

  rowptr -= base;
  colidx -= base;
  if (values) values -= base;

  w -= base;
  x -= base;
  y -= base;

//#define MEASURE_LOAD_BALANCE
#ifdef MEASURE_LOAD_BALANCE
  double barrierTimes[omp_get_max_threads()];
  double tBegin = omp_get_wtime();
#endif

#pragma omp parallel
  {
    int iBegin, iEnd;
    getLoadBalancedPartition(&iBegin, &iEnd, rowptr + base, m);
    iBegin += base;
    iEnd += base;

    T sum[BS];

    if (1 == alpha && 0 == beta && 0 == gamma) {
      if (values) {
        for (int i = iBegin; i < iEnd; ++i) {
          for (int ii = 0; ii < BS; ++ii) {
            sum[ii] = 0;
          }
          for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
            int c = colidx[j]*BS;
            for (int jj = 0; jj < BS; ++jj) {
              T xVal = x[c + jj];
              for (int ii = 0; ii < BS; ++ii) {
                sum[ii] += values[(j*BS + jj)*BS + ii]*xVal;
              }
            }
          }
          for (int ii = 0; ii < BS; ++ii) {
            w[i*BS + ii] = sum[ii];
          }
        }
      }
      else {
        // pattern-only matrix: assume all non-zero values are 1
        for (int i = iBegin; i < iEnd; ++i) {
          for (int ii = 0; ii < BS; ++ii) {
            sum[ii] = 0;
          }
          for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
            int c = colidx[j]*BS;
            for (int jj = 0; jj < BS; ++jj) {
              T xVal = x[c + jj];
              for (int ii = 0; ii < BS; ++ii) {
                sum[ii] += xVal;
              }
            }
          }
          for (int ii = 0; ii < BS; ++ii) {
            w[i*BS + ii] = sum[ii];
          }
        }
      }
    }
    else {
      if (values) {
        for (int i = iBegin; i < iEnd; ++i) {
          for (int ii = 0; ii < BS; ++ii) {
            sum[ii] = 0;
          }
          for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
            int c = colidx[j]*BS;
            for (int jj = 0; jj < BS; ++jj) {
              T xVal = x[c + jj];
              for (int ii = 0; ii < BS; ++ii) {
                sum[ii] += values[(j*BS + jj)*BS + ii]*xVal;
              }
            }
          }
          for (int ii = 0; ii < BS; ++ii) {
            w[i*BS + ii] = alpha*sum[ii] + beta*y[i*BS + ii] + gamma;
          }
        }
      }
      else {
        // pattern-only matrix: assume all non-zero values are 1
        for (int i = iBegin; i < iEnd; ++i) {
          for (int ii = 0; ii < BS; ++ii) {
            sum[ii] = 0;
          }
          for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
            int c = colidx[j]*BS;
            for (int jj = 0; jj < BS; ++jj) {
              T xVal = x[c + jj];
              for (int ii = 0; ii < BS; ++ii) {
                sum[ii] += xVal;
              }
            }
          }
          for (int ii = 0; ii < BS; ++ii) {
            w[i*BS + ii] = alpha*sum[ii] + beta*y[i*BS + ii] + gamma;
          }
        }
      }
    }
#ifdef MEASURE_LOAD_BALANCE
    double t = omp_get_wtime();
#pragma omp barrier
    barrierTimes[tid] = omp_get_wtime() - t;

#pragma omp barrier
#pragma omp master
    {
      double tEnd = omp_get_wtime();
      double barrierTimeSum = 0;
      for (int i = 0; i < nthreads; ++i) {
        barrierTimeSum += barrierTimes[i];
      }
      printf("%f load imbalance = %f\n", tEnd - tBegin, barrierTimeSum/(tEnd - tBegin)/nthreads);
    }
#undef MEASURE_LOAD_BALANCE
#endif // MEASURE_LOAD_BALANCE
  } // omp parallel
}

template<class T>
static void SpMVBSRGeneral_(
  int m, int BS,
  T *w,
  T alpha,
  const int *rowptr, const int *colidx, const T* values,
  const T *x,
  T beta,
  const T *y,
  T gamma)
{
  assert(w != x);

  int base = rowptr[0];

  rowptr -= base;
  colidx -= base;
  if (values) values -= base;

  w -= base;
  x -= base;
  y -= base;

//#define MEASURE_LOAD_BALANCE
#ifdef MEASURE_LOAD_BALANCE
  double barrierTimes[omp_get_max_threads()];
  double tBegin = omp_get_wtime();
#endif

#pragma omp parallel
  {
    int iBegin, iEnd;
    getLoadBalancedPartition(&iBegin, &iEnd, rowptr + base, m);
    iBegin += base;
    iEnd += base;

    T sum[BS];

    if (1 == alpha && 0 == beta && 0 == gamma) {
      if (values) {
        for (int i = iBegin; i < iEnd; ++i) {
          for (int ii = 0; ii < BS; ++ii) {
            sum[ii] = 0;
          }
          for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
            int c = colidx[j]*BS;
            for (int jj = 0; jj < BS; ++jj) {
              T xVal = x[c + jj];
              for (int ii = 0; ii < BS; ++ii) {
                sum[ii] += values[(j*BS + jj)*BS + ii]*xVal;
              }
            }
          }
          for (int ii = 0; ii < BS; ++ii) {
            w[i*BS + ii] = sum[ii];
          }
        }
      }
      else {
        // pattern-only matrix: assume all non-zero values are 1
        for (int i = iBegin; i < iEnd; ++i) {
          for (int ii = 0; ii < BS; ++ii) {
            sum[ii] = 0;
          }
          for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
            int c = colidx[j]*BS;
            for (int jj = 0; jj < BS; ++jj) {
              T xVal = x[c + jj];
              for (int ii = 0; ii < BS; ++ii) {
                sum[ii] += xVal;
              }
            }
          }
          for (int ii = 0; ii < BS; ++ii) {
            w[i*BS + ii] = sum[ii];
          }
        }
      }
    }
    else {
      if (values) {
        for (int i = iBegin; i < iEnd; ++i) {
          for (int ii = 0; ii < BS; ++ii) {
            sum[ii] = 0;
          }
          for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
            int c = colidx[j]*BS;
            for (int jj = 0; jj < BS; ++jj) {
              T xVal = x[c + jj];
              for (int ii = 0; ii < BS; ++ii) {
                sum[ii] += values[(j*BS + jj)*BS + ii]*xVal;
              }
            }
          }
          for (int ii = 0; ii < BS; ++ii) {
            w[i*BS + ii] = alpha*sum[ii] + beta*y[i*BS + ii] + gamma;
          }
        }
      }
      else {
        // pattern-only matrix: assume all non-zero values are 1
        for (int i = iBegin; i < iEnd; ++i) {
          for (int ii = 0; ii < BS; ++ii) {
            sum[ii] = 0;
          }
          for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
            int c = colidx[j]*BS;
            for (int jj = 0; jj < BS; ++jj) {
              T xVal = x[c + jj];
              for (int ii = 0; ii < BS; ++ii) {
                sum[ii] += xVal;
              }
            }
          }
          for (int ii = 0; ii < BS; ++ii) {
            w[i*BS + ii] = alpha*sum[ii] + beta*y[i*BS + ii] + gamma;
          }
        }
      }
    }
#ifdef MEASURE_LOAD_BALANCE
    double t = omp_get_wtime();
#pragma omp barrier
    barrierTimes[tid] = omp_get_wtime() - t;

#pragma omp barrier
#pragma omp master
    {
      double tEnd = omp_get_wtime();
      double barrierTimeSum = 0;
      for (int i = 0; i < nthreads; ++i) {
        barrierTimeSum += barrierTimes[i];
      }
      printf("%f load imbalance = %f\n", tEnd - tBegin, barrierTimeSum/(tEnd - tBegin)/nthreads);
    }
#undef MEASURE_LOAD_BALANCE
#endif // MEASURE_LOAD_BALANCE
  } // omp parallel
}

void BSR::multiplyWithVector(
  double *w,
  double alpha, const double *x, double beta, const double *y, double gamma)
  const
{
  double *tmp_x = (double *)x;

  // if x and w point to the same array, copy x to a temporary buffer
  // in order to avoid possible race condition in SpMV_
  if (w == x) {
    tmp_x = MALLOC(double, m);
 
    #pragma omp parallel for
    for (int i=0; i<m*bs; i++) {
      tmp_x[i] = x[i];
    }
  }
 
  switch (bs) {
  case 4:
    SpMVBSR_<double, 4>(m, w, alpha, rowptr, colidx, values, tmp_x, beta, y, gamma);
    break;
  default:
    SpMVBSRGeneral_<double>(m, bs, w, alpha, rowptr, colidx, values, tmp_x, beta, y, gamma);
    break;
  }
 
  if (w == x) {
    FREE(tmp_x);
  }
}

void BSR::multiplyWithVector(double *w, const double *x) const
{
  return multiplyWithVector(w, 1, x, 0, w, 0);
}

} // namespace SpMP
