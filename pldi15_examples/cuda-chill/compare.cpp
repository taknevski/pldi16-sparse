void diag_inspector(int n,int m, int *index,float *ratings,int *col, int iter, int count_of_diagonals){

  int *devI3Ptr;
  float *devI2Ptr;
  unsigned short *devI1Ptr;
  float *devO1Ptr;
  int t8;
  int t6;
  int t4;
  unsigned short newVariable1;
  int _t75;
  int _t74;
  int _t73;
  int _t72;
  int _t71;
  int _t70;
  int _t69;
  int _t68;
  int t2;
  float _P3[2 * 16];
  int _t58;
  int _t57;
  int _t56;
  int _t55;
  float _P2[2];
  int _t47;
  int _t46;
  int _t45;
  int _t44;
  int _t40;
  int _t39;
  int _t38;
  int _t37;
  int _t36;
  int _t35;
  int _t33;
  int _t32;
  int _t31;
  int _t30;
  int newVariable0;
  int In_1;
  int _t29;
  int _t28;
  struct gen_a_list *_P_DATA4;
  struct gen_mk *_P_DATA3;
  float *_new_ratings;
  struct gen_a_list *_P1[155998];
  unsigned short *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t27;
  int _t26;
  int _t25;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  int _t18;
  int _t17;
  int _t16;
  int _t15;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t6;
  int _t4;
  int _t5;
  int l;
  int _t3;
  int _t2;
  int _t1;
  int jp;
  int i;
  int j;
  int k;
  float err;
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * 155999)));
  _P_DATA1[0] = 0;
  chill_count_1 = 0;
  for (_t24 = 0; _t24 <= 155997; _t24 += 1) {
    _P1[1 * _t24] = 0;
    _P_DATA1[1 * _t24 + 1] = 0;
  }
  for (t2 = 0; t2 <= 46640; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
      _t24 = col_____(t4) - t2 - 1;
      _P_DATA4 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
      _P_DATA4 -> next = _P1[1 * _t24];
      _P1[1 * _t24] = _P_DATA4;
      _P1[1 * _t24] -> ratings = -1;
      _P1[1 * _t24] -> col_ = t2;
      chill_count_1 += 1;
      _P_DATA1[1 * _t24 + 1] += 1;
      _P1[1 * _t24] -> ratings = ratings[t4];
    }
{
    _P_DATA2 = ((unsigned short *)(malloc(sizeof(unsigned short ) * chill_count_1)));
    _new_ratings = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 1))));
    _P_DATA4 = _P1[1 * 0];
    for (newVariable0 = 1 - _P_DATA1[1 * 0 + 1]; newVariable0 <= 0; newVariable0 += 1) {
      _P_DATA2[_P_DATA1[1 * 0] - newVariable0] = _P_DATA4 -> col_;
      _new_ratings[(_P_DATA1[1 * 0] - newVariable0) * 1] = _P_DATA4 -> ratings;
      _P_DATA4 = _P_DATA4 -> next;
    }
    _P_DATA1[1 * 0 + 1] += _P_DATA1[1 * 0];
    for (t2 = 1; t2 <= 155997; t2 += 1) {
      _P_DATA4 = _P1[1 * t2];
      for (newVariable0 = 1 - _P_DATA1[1 * t2 + 1]; newVariable0 <= 0; newVariable0 += 1) {
        _P_DATA2[_P_DATA1[1 * t2] - newVariable0] = _P_DATA4 -> col_;
        _new_ratings[(_P_DATA1[1 * t2] - newVariable0) * 1] = _P_DATA4 -> ratings;
        _P_DATA4 = _P_DATA4 -> next;
      }
      _P_DATA1[1 * t2 + 1] += _P_DATA1[1 * t2];
    }
  }

  for (_t24 = 0; _t24 <= 155997; _t24 += 1) {
    _P_DATA4 = _P1[1 * _t24];
    for (newVariable0 = _P_DATA1[1 * _t24]; newVariable0 <= _P_DATA1[1 * _t24 + 1] - 1; newVariable0 += 1) {
      _P1[1 * _t24] = _P1[1 * _t24] -> next;
      free(_P_DATA4);
      _P_DATA4 = _P1[1 * _t24];
    }
  }

	memcpy(new_ratings->host_ptr(), _new_ratings, sizeof(float ) * (chill_count_1 * 1) );
	memcpy(new_col->host_ptr(), _P_DATA2, sizeof(int ) * (chill_count_1 * 1) );
	memcpy(new_index->host_ptr(), _P_DATA1, sizeof(int ) * (155999) );


  free(_P_DATA1);
  free(_P_DATA2);
  free(_new_ratings);
}

