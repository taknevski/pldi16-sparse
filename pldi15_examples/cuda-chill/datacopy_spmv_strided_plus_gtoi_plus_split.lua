--init("datacopy_spmv.c", "spmv", 0)
--dofile("cudaize.lua")

init("spmv.cpp", "spmv", 0)
dofile("cudaize.lua")
NNZ=1666
N=494
Ti = 1024
Tj = 256
Tk = 32

--print_code(0)

flatten(0,{"new_index"},{1,2}, "NNZ")
print_space()
split_with_alignment(1,3,4096)
print_space()
distribute({1,2},2)

--tile for number of non zeros per CUDA block (1024)
tile_by_index(1,{"new_index"},{Ti},{l1_control="kk"},{"i","j","kk","new_index"})CU=1



--tile for number of nonzeros per warp (256)
tile_by_index(1,{"new_index"},{Tj},{l1_control="bbb"},{"i", "j", "kk","bbb","new_index"})CU=1


--tile for warp size(32) to get(256/32= 8) iterations
tile_by_index(1,{"new_index"},{Tk},{l1_control="bbbb"},{"i", "j", "kk", "bbb","bbbb","new_index"},strided)CU=2





--normalization of tiled bounds
shift_to(1,6,0)
tile(1,6,6)

print_code(1)

--scalar expansion of product expression
scalar_expand(1,{5,6},"RHS")



print_idx()
--separate copy to temp array from accumulation array)
distribute({1,3}, 3)




--peel last thread/iteration 31 from accumulation
peel(3,6,-1)



--fuse back the accumulation with the data copy statement
fuse({1,3}, 3)



--distribute cudaized statements from non-cudaized statements
--print_code(1)
distribute({1,4},3)
print_code(1)

scalar_expand(4,{4,5},"RHS")
print_code(1)


distribute({4,5},3)



fuse({1,4},3)
fuse({1,4},5)


--print_code(1)

--fuse({1,3,5},3)

distribute({1,2},1)
distribute({1,5},1)
distribute({1,3}, 6)
print_space()
cudaize(1,"spmv_GPU",{ a=NNZ,x=N,y=N,col=NNZ, temp=NNZ},{block={"kk"}, thread={"new_index", "bbb"}})
print_code(1)


