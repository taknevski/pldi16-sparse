--init("datacopy_spmv.c", "spmv", 0)
--dofile("cudaize.lua")

init("spmv.cpp", "spmv", 0)
dofile("cudaize.lua")
NNZ=1666
N=494
Ti = 1024
Tj = 256
Tk = 32

print_dep()
--flatten loop levels 1 and 2 with NNZ being uninterpreted omega function name
flatten(0,{"new_index"},{1,2}, "NNZ")

--split flattened loop level to be a perfect multiple of warp size (32)
split_with_alignment(1,3,1024)


--distribute remainder of splitted statement as it is not cudaized
distribute({1,2},2)

--tile for number of non zeros per CUDA block (1024)
tile_by_index(1,{"new_index"},{Ti},{l1_control="kk"},{"i","j","kk","new_index"})CU=1



--tile for number of nonzeros per warp (256)
tile_by_index(1,{"new_index"},{Tj},{l1_control="bbb"},{"i", "j", "kk","bbb","new_index"})CU=1


--tile for warp size(32) to get(256/32= 8) iterations
tile_by_index(1,{"new_index"},{Tk},{l1_control="bbbb"},{"i", "j", "kk", "bbb","bbbb","new_index"})CU=2


--normalization of tiled bounds
shift_to(1,6,0)



--scalar expansion of product expression
scalar_expand(1,{4,6},"NNZ.i",1)
scalar_expand(1,{4,6},"RHS",1)



peel(1,5,-1)
print_code(1)

peel(5,6,-1)
fuse({1,3,4},6)

distribute({5,8},3)

print_code(8)

scalar_expand(8,{3,4},"_P1",1)
scalar_expand(8,{3,4},"RHS",1)

fuse({5,6,7},6)

distribute({8,12,11},3)
fuse({1,12,11},3)
--fuse({1,8},3)
distribute({8,1},3)


cudaize(1,"spmv_GPU",{ a=NNZ,x=N,y=N,col=NNZ,temp=NNZ, NNZ_j=NNZ, NNZ_i=NNZ },{block={"kk"}, thread={"new_index", "bbb"}},{"_P_DATA1", "_P_DATA2"})


