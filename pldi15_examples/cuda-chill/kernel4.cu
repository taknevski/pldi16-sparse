__global__ void sgd_blk_diag_operator(float *fv,int *metadata, float *new_ratings,int *_P_DATA2,int *_P_DATA1,float step_size, int t2)
{
	int l;
	int i;
	int bx;
	bx = blockIdx.x;
	int tx;
	tx = threadIdx.x;
	int ty;
	ty = threadIdx.y;
	__device__ __shared__ float _P2[BLOCKSIZE];
	__device__ __shared__ float _P3[BLOCKSIZE* SGD_FEATURE_SIZE];
	int newVariable5;
	float _P4[C];
	float _P5[R];
	int t4;
	int t6;
	int t8;
	int t10;
	int t12;
	int movie_size = metadata[2];
	if (ty <= _P_DATA1__(t2) - _P_DATA1_(t2) - BLOCKSIZE* bx - 1)
		newVariable5 = _P_DATA2[_P_DATA1_(t2) + BLOCKSIZE * bx + ty];
	if (ty <= _P_DATA1__(t2) - _P_DATA1_(t2) - BLOCKSIZE* bx - 1) {
		for (t10 = 0; t10 <= R-1; t10 += 1)
			_P5[R* newVariable5 + t10 - R* newVariable5] = fv[(R* newVariable5 + t10)*SGD_FEATURE_SIZE + tx];
		for (i = 0; i <= R-1; i += 1) {
			for (t12 = 0; t12 <= C-1; t12 += 1)
				_P4[C* newVariable5 + C * t2 + t12 + 1 - (C* t2 + C* newVariable5 + 1)] = fv[(C* newVariable5 + C* t2 + t12 + 1)*SGD_FEATURE_SIZE + tx];
			for (l = 0; l <= C-1; l += 1) {
				if (0 <= new_ratings(t2, BLOCKSIZE* bx + _P_DATA1_(t2) + ty,i,l)){
					_P2[ty] = -new_ratings[(BLOCKSIZE* bx + _P_DATA1_(t2) + ty)*R*C +i*C + l];
					_P3[tx + ty * SGD_FEATURE_SIZE] =\
									  (_P5[R* newVariable5 + i - R* newVariable5] *\
									   _P4[C* t2 - movie_size + 1 + C* newVariable5 + l + movie_size - (C * t2 + C * newVariable5 + 1)]);
					segreduce_warp2(&_P2[ty],&_P3[0 + ty * SGD_FEATURE_SIZE]);
					_P5[R* newVariable5 + i - R * newVariable5] -=\
										       (step_size * ((_P2[ty] * _P4[C * t2 - movie_size + 1 + C* newVariable5 + l +\
												movie_size - (C* t2 + C* newVariable5 + 1)])\
												     + (0.05f * _P5[R* newVariable5 + i - R * newVariable5])));
					_P4[C * t2 - movie_size + 1 + C * newVariable5 + l \
										+ movie_size - (C * t2 + C * newVariable5 + 1)] -= (step_size * ((_P2[ty] *\
											_P5[R* newVariable5 + i - R * newVariable5]) + \
												(0.05f * _P4[C * t2 - movie_size + 1 +  C * newVariable5 +\
														l + movie_size - (C * t2 + C * newVariable5 + 1)])));
				}
				else if (new_ratings(t2, BLOCKSIZE* bx + _P_DATA1_(t2) + ty,i,l) <= -2){ 
					_P2[ty] = -new_ratings[(BLOCKSIZE* bx + _P_DATA1_(t2) + ty)*R*C +i*C + l];
					_P3[tx + ty * SGD_FEATURE_SIZE] =\
									  (_P5[R* newVariable5 + i - R* newVariable5] *\
									   _P4[C* t2 - movie_size + 1 + C* newVariable5 + l + movie_size - (C * t2 + C * newVariable5 + 1)]);
					segreduce_warp2(&_P2[ty],&_P3[0 + ty * SGD_FEATURE_SIZE]);
					_P5[R* newVariable5 + i - R * newVariable5] -= \
										       (step_size * ((_P2[ty] * _P4[C * t2 - movie_size + 1 + C* newVariable5 + l +\
												movie_size - (C* t2 + C* newVariable5 + 1)])\
												     + (0.05f * _P5[R* newVariable5 + i - R * newVariable5])));
					_P4[C * t2 - movie_size + 1 + C * newVariable5 + l \
										+ movie_size - (C * t2 + C * newVariable5 + 1)] -= (step_size * ((_P2[ty] *\
												 _P5[R* newVariable5 + i - R * newVariable5]) + \
													(0.05f * _P4[C * t2 - movie_size + 1 +  C * newVariable5 + \
														l + movie_size - (C * t2 + C * newVariable5 + 1)])));
				}
			}
			for (t12 = 0; t12 <= C-1; t12 += 1)
				fv[(C * newVariable5 + C * t2 + t12 + 1)*SGD_FEATURE_SIZE + tx] = _P4[C* newVariable5 + C * t2 + t12 + 1 - (C* t2 + C* newVariable5 + 1)];
		}
		for (t10 = 0; t10 <= R-1; t10 += 1)
			fv[(R* newVariable5 + t10)*SGD_FEATURE_SIZE + tx] = _P5[R* newVariable5 + t10 - R * newVariable5];
	}
}
