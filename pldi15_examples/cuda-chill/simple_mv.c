#define N 1024

void normalMM(int x[N], int a[N][N], int y[N]) {
  int i, j, k;

  for (i = 0; i < N; i++)
    for (j = 0; j < N; j++)
        y[i] += a[i][j]*x[j];
}
