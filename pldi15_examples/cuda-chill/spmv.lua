init("spmv.cpp", "spmv", 0)
dofile("cudaize.lua")
NZ=1666
N=494
Ti=1024
Tj =32
flatten(0,{"new_index"},{1,2}, "NNZ")

tile_by_index(1,{"new_index"},{Ti},{l1_control="bb"},{"i","j","bb","new_index"})CU=1
--tile_by_index(1,{"new_index"},{Tj},{l1_control="bbb"},{"i","j","bb", "bbb","new_index"})CU=1

--normalize_index(1,"new_index")
cudaize(1,"spmv_GPU",{a=NZ,x=N,y=N,col=NZ},{block={"bb"}, thread={"new_index"}})CU=1
print_code(1)



