init("spmv.cpp", "spmv", 0)
dofile("cudaize.lua")
N = 4096
Ti = 1024
Tj=32
NNZ=5120



tile_by_index(0,{"i"},{Ti},{l1_control="ii"},{"ii","i","j"})CU=1

tile_by_index(0,{"j"},{Tj},{l1_control="jj"},{"ii","i","j", "jj"},strided)CU=1

--tile(0,2,2)
print_code(0)
print_space()
scalar_expand(0,{2,4},"RHS",1,0,0)

cudaize(0,"spmv_GPU",{ a=NNZ,x=N,y=N,col=NNZ,index=NNZ},{block={"ii"}, thread={"j", "i"}},{})

--cudaize(0,"spmv_GPU",{ a=NNZ,x=N,y=N,col=NNZ,index=NNZ},{block={}, thread={"i"}},{})

print_code(0)
print_space()
--reduce(0,{5,6,7},0,"segreduce_warp",{})
print_code(0)
--print_space()
--distribute({0,1},4)


	

