#define sz 10

void octopi_test(double *D, double *t2i, double *t1i, double *A, double *C, double *B, double *U){

	int l, j, n, m, i, k;

	for(l = 0; l < sz; l++){
	
		for(j = 0; j < sz; j++){
			for(n = 0; n < sz; n++){
				for(m = 0; m < sz; m++){
					t1i[l*sz*sz + j*sz + n]  = t1i[l*sz*sz + j*sz + n] +  (B[m*sz + j] * U[l*sz*sz + m*sz + n]);
				}
			}
		}
	
	}
	for(i = 0; i < sz; i++){
	
		for(l = 0; l < sz; l++){
			for(j = 0; j < sz; j++){
				for(n = 0; n < sz; n++){
					t2i[i*sz*sz + l*sz + j]  = t2i[i*sz*sz + l*sz + j] +  (C[n*sz + i] * t1i[l*sz*sz + j*sz + n]);
				}
			}
		}
	
	}
	for(i = 0; i < sz; i++){
	
		for(j = 0; j < sz; j++){
			for(k = 0; k < sz; k++){
				for(l = 0; l < sz; l++){
					D[i*sz*sz + j*sz + k]  = D[i*sz*sz + j*sz + k] +  (A[l*sz + k] * t2i[i*sz*sz + l*sz + j]);
				}
			}
		}
	
	}

}
