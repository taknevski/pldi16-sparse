#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
__global__ void mm_GPU(int (*c)[1024],int (*a)[1024],int (*b)[1024]);
#define N 1024

void normalMM(int c[1024UL][1024UL],int a[1024UL][1024UL],int b[1024UL][1024UL])
{
  int *devI2Ptr;
  int *devI1Ptr;
  int *devO1Ptr;
  int t30;
  int t28;
  int t26;
  int t24;
  int t22;
  int t20;
  int t18;
  int t16;
  int t14;
  int t12;
  int t10;
  int t8;
  int t6;
  int t4;
  int t2;
  int _t119;
  int _t118;
  int _P3[4][8];
  int _t108;
  int _t106;
  int _t115;
  int _t114;
  int _t113;
  int _t112;
  int _t111;
  int _t110;
  int _t109;
  int _t107;
  int _t105;
  int _t104;
  int _t103;
  int _t73;
  int _t74;
  int _P2[16][65];
  int _t72;
  int _t71;
  int _t70;
  int _t69;
  int _t68;
  int _t67;
  int _t66;
  int _t65;
  int _t64;
  int _t63;
  int _t62;
  int _t61;
  int _t28;
  int _t29;
  int _P1[128][17];
  int _t16;
  int _t18;
  int _t27;
  int _t24;
  int _t17;
  int _t25;
  int _t22;
  int i;
  int j;
  int k;
  cudaMalloc(((void **)(&devO1Ptr)),1048576 * sizeof(int ));
  cudaMemcpy(devO1Ptr,c,1048576 * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),1048576 * sizeof(int ));
  cudaMemcpy(devI1Ptr,a,1048576 * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),1048576 * sizeof(int ));
  cudaMemcpy(devI2Ptr,b,1048576 * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid0 = dim3(8,16);
  dim3 dimBlock0 = dim3(16,16);
  mm_GPU<<<dimGrid0,dimBlock0>>>(((int (*)[1024])devO1Ptr),((int (*)[1024])devI1Ptr),((int (*)[1024])devI2Ptr));
  cudaMemcpy(c,devO1Ptr,1048576 * sizeof(int ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
}

__global__ void mm_GPU(int (*c)[1024],int (*a)[1024],int (*b)[1024])
{
  int k;
  int tmp_ty;
  int tmp_tx;
  int kk;
  int jjj;
  int iii;
  int bx;
  bx = blockIdx.x;
  int by;
  by = blockIdx.y;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  __device__ __shared__ int _P1[128][17];
  __device__ __shared__ int _P2[16][65];
  int _P3[4][8];
  int t2;
  int t4;
  int t6;
  int t8;
  int t10;
  int t12;
  int t14;
  int t22;
  for (iii = 64 * by + ty; iii <= 64 * by + ty + 48; iii += 16) 
    for (jjj = 128 * bx + tx; jjj <= 128 * bx + tx + 112; jjj += 16) 
      _P3[(iii - (64 * by + ty)) / 16][(jjj - (128 * bx + tx)) / 16] = c[iii][jjj];
  for (kk = 0; kk <= 63; kk += 1) {
    for (tmp_tx = 0; tmp_tx <= 7; tmp_tx += 1) 
      _P1[128 * bx + (tx + 16 * tmp_tx) - 128 * bx][ty + 16 * kk - 16 * kk] = a[ty + 16 * kk][128 * bx + (tx + 16 * tmp_tx)];
    __syncthreads();
    for (tmp_ty = 0; tmp_ty <= 3; tmp_ty += 1) 
      _P2[16 * kk + tx - 16 * kk][16 * tmp_ty + ty + 64 * by - 64 * by] = b[16 * tmp_ty + ty + 64 * by][16 * kk + tx];
    __syncthreads();
    for (k = 0; k <= 15; k += 1) {
      for (iii = 0; iii <= 7; iii += 1) 
        for (jjj = 0; jjj <= 3; jjj += 1) 
          _P3[(ty + 64 * by + 16 * jjj - (64 * by + ty)) / 16][(128 * bx + 16 * iii + tx - (128 * bx + tx)) / 16] = (_P3[(ty + 64 * by + 16 * jjj - (64 * by + ty)) / 16][(128 * bx + 16 * iii + tx - (128 * bx + tx)) / 16] + (_P1[128 * bx + 16 * iii + tx - 128 * bx][k + 16 * kk - 16 * kk] * _P2[k + 16 * kk - 16 * kk][ty + 64 * by + 16 * jjj - 64 * by]));
      __syncthreads();
    }
    __syncthreads();
  }
  for (iii = 64 * by + ty; iii <= 64 * by + ty + 48; iii += 16) 
    for (jjj = 128 * bx + tx; jjj <= 128 * bx + tx + 112; jjj += 16) 
      c[iii][jjj] = _P3[(iii - (64 * by + ty)) / 16][(jjj - (128 * bx + tx)) / 16];
}
