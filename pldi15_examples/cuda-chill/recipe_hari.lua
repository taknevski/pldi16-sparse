init("hari.c","MANGLL_TENSOR_AIIX_APPLY_ELEM",0)
dofile("cudaize.lua")

M=16

--print_code(0)
cudaize(0,"MANGLL_TENSOR_AIIX_APPLY_ELEM_GPU_0",{A=M*M,X=M*M*M,Y=M*M*M},{block={"l","k"},thread={"i"}},{})

