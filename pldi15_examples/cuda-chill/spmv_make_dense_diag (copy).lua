init("spmv_bcsr.cpp", "spmv", 0)
dofile("cudaize.lua")

make_dense(0,2,"k")

known("lb",0)
known("ub",999999)
known("n", 1000000)
skew({0},2,{-1,1})
permute(0,{"k","i","j"})
shift_to(0,1,0)
compact(0,1,"a_prime", 0, "a")
permute(2,{"i","k"})

N = 1000000
Ti = 256
tile_by_index(2,{"i"},{Ti},{l1_control="ii"},{"ii","i","k"})
tile_by_index(2,{"k"},{Ti},{l1_control="kk"},{"ii","i","kk","k"})
normalize_index(2,"k")
cudaize(2,"spmv_diag_GPU",{x=N,y=N},{block={"ii"}, thread={"i"}}, {"a_prime", "_P_DATA2"})
copy_to_shared(2,"k","_P_DATA2",-16)
print_code(2)
copy_to_registers(2, "kk", "y");

