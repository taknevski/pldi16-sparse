#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define col_(_t5) col[_t5]
#define index_(i) index[i]
#define index__(i) index[i + 1]
__global__ void spmv_diag_GPU(float *y,int *_P_DATA2,float *a_prime,float *x);
#define NZ 1666
#define NUMROWS 494

void spmv(int n,int index[494UL],float a[1666UL],float y[494UL],float x[494UL],int col[1666UL])
{
  float *devI3Ptr;
  float *devI2Ptr;
  int *devI1Ptr;
  float *devO1Ptr;
  int t12;
  int t10;
  int t8;
  int t6;
  int t4;
  int t2;
  float newVariable1;
  int _t30;
  int _t29;
  int _t28;
  int _t27;
  int _t26;
  int _t25;
  int _t19;
  int _P1[257];
  int _t16;
  int _t13;
  int _t14;
  int _t18;
  int _t7;
  int _t6;
  int newVariable0;
  int chill_count_1;
  int *_P_DATA3;
  float *a_prime;
  int *_P_DATA2;
  int *_P_DATA1;
  int chill_count_0;
  int _t5;
  int _t3;
  int _t4;
  int k;
  int _t2;
  int _t1;
  int i;
  int j;
  chill_count_0 = 0;
  for (_t4 = 0; _t4 <= 999999; _t4 += 1) 
    chill_count_0 += index__(_t4) + 1 - (index_(_t4) + 0) + 1;
  _P_DATA1 = malloc(sizeof(int ) * (chill_count_0 + 1));
  _P_DATA2 = malloc(sizeof(int ) * chill_count_0);
  a_prime = malloc(sizeof(float ) * (chill_count_0 * 1000000));
  _P_DATA3 = malloc(sizeof(int ) * 1999999);
  chill_count_1 = 0;
  _P_DATA2 = malloc(sizeof(int ) * chill_count_0);
  a_prime = malloc(sizeof(float ) * (chill_count_0 * 1000000));
  _P_DATA3 = malloc(sizeof(int ) * 1999999);
  for (t2 = 0; t2 <= 999999; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
      _t3 = col_(t4) - t2 + 999999;
      _P_DATA3[_t3] = -1;
    }
  for (t2 = 0; t2 <= 999999; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
      _t3 = col_(t4) - t2 + 999999;
      if (t2 + _t3 - 999999 == col[t4]) {
        if (_P_DATA3[_t3] == -1) {
          _P_DATA3[_t3] = chill_count_1;
          for (newVariable0 = 0; newVariable0 <= 999999; newVariable0 += 1) 
            a_prime[_P_DATA3[_t3] * 1000000 + 1 * newVariable0] = 0;
          _P_DATA2[_P_DATA3[_t3]] = _t3;
          chill_count_1 += 1;
        }
        a_prime[_P_DATA3[_t3] * 1000000 + 1 * t2] = a[t4];
      }
    }
  cudaMalloc(((void **)(&devO1Ptr)),1000000 * sizeof(float ));
  cudaMemcpy(devO1Ptr,y,1000000 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),chill_count_0 * sizeof(int ));
  cudaMemcpy(devI1Ptr,_P_DATA2,chill_count_0 * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),chill_count_0 * 1000000 * sizeof(float ));
  cudaMemcpy(devI2Ptr,a_prime,chill_count_0 * 1000000 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),1000000 * sizeof(float ));
  cudaMemcpy(devI3Ptr,x,1000000 * sizeof(float ),cudaMemcpyHostToDevice);
  dim3 dimGrid2 = dim3(3907,1);
  dim3 dimBlock2 = dim3(256,1);
  spmv_diag_GPU<<<dimGrid2,dimBlock2>>>(devO1Ptr,devI1Ptr,devI2Ptr,devI3Ptr);
  cudaMemcpy(y,devO1Ptr,1000000 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  free(_P_DATA1);
  free(_P_DATA2);
  free(a_prime);
  free(_P_DATA3);
}

int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[494 + 1];
  int col[1666UL];
  spmv(n,index,a,y,x,col);
  return 0;
}

__global__ void spmv_diag_GPU(float *y,int *_P_DATA2,float *a_prime,float *x)
{
  int k;
  int kk;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int chill_count_1;
  __device__ __shared__ int _P1[257];
  float newVariable1;
  int t2;
  int t4;
  int t8;
  int t10;
  if (1 <= chill_count_1) {
    if (tx <= -(256 * bx) + 999999) {
      newVariable1 = y[256 * bx + tx];
      for (kk = 0; kk <= (chill_count_1 - 1) / 256; kk += 1) {
        if (tx <= chill_count_1 - 256 * kk - 1) 
          _P1[256 * kk + tx - 256 * kk] = _P_DATA2[256 * kk + tx];
        __syncthreads();
        for (k = 0; k <= __rose_lt(chill_count_1 - 256 * kk - 1,255); k += 1) 
          newVariable1 += (a_prime[(256 * kk + k) * 1000000 + 1 * (tx + 256 * bx)] * x[tx + 256 * bx + _P1[256 * kk + k - 256 * kk] - 999999]);
        __syncthreads();
      }
      y[256 * bx + tx] = newVariable1;
    }
    __syncthreads();
  }
}
