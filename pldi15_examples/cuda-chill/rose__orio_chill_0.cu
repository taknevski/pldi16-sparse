#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
__global__ void c_std_d1_1_GPU_0(double *T3,double *T2i,double *v2);
void c_std_d1_1_inspector(double *T2i,double *v2,double *T3);
#define tilesize 16

void c_std_d1_1(double *T2i,double *v2,double *T3)
{
  double *devI2Ptr;
  double *devI1Ptr;
  double *devO1Ptr;
  int t16;
  int t14;
  int t12;
  int t10;
  int t8;
  int t6;
  int t4;
  int t2;
  int _t56;
  int _t55;
  int _t54;
  int _t53;
  int _t52;
  int _t51;
  int _t50;
  int _t49;
  int _t48;
  int _t47;
  int _t46;
  int _t45;
  int _t44;
  int _t43;
  int _t42;
  int _t41;
  int _t40;
  int _t39;
  int _t38;
  int _t37;
  int _t36;
  int _t35;
  int _t34;
  int _t33;
  double newVariable0;
  int _t31;
  int _t30;
  int _t29;
  int _t28;
  int _t27;
  int _t26;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  int _t18;
  int _t17;
  int h7;
  int p4;
  int p5;
  int h1;
  int h3;
  int h2;
  int p6;
  int dummyLoop;
  c_std_d1_1_inspector(T2i,v2,T3);
  cudaMalloc(((void **)(&devO1Ptr)),16777216 * sizeof(double ));
  cudaMemcpy(devO1Ptr,T3,16777216 * sizeof(double ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),65536 * sizeof(double ));
  cudaMemcpy(devI1Ptr,T2i,65536 * sizeof(double ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),65536 * sizeof(double ));
  cudaMemcpy(devI2Ptr,v2,65536 * sizeof(double ),cudaMemcpyHostToDevice);
  dim3 dimGrid0 = dim3(16,16);
  dim3 dimBlock0 = dim3(16,16);
  c_std_d1_1_GPU_0<<<dimGrid0,dimBlock0>>>(devO1Ptr,devI1Ptr,devI2Ptr);
  cudaMemcpy(T3,devO1Ptr,16777216 * sizeof(double ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
}

void c_std_d1_1_inspector(double *T2i,double *v2,double *T3)
{
  double newVariable0;
  int t2;
  int t4;
  int t6;
  int t8;
  int t12;
  int t14;
  int t16;
  for (tx = 0; tx <= 15; tx += 1) 
    for (ty = 0; ty <= 15; ty += 1) 
      for (h2 = 0; h2 <= 15; h2 += 1) 
        for (h1 = 0; h1 <= 15; h1 += 1) 
          for (by = 0; by <= 15; by += 1) 
            for (bx = 0; bx <= 15; bx += 1) {
              newVariable0 = T3[tx + 256 * h1 + 1048576 * bx + 4096 * ty + 65536 * by + 16 * h2];
              for (h7 = 0; h7 <= 7; h7 += 7) {
                newVariable0 = (newVariable0 - (T2i[h7 + (16 * (bx + (16 * (by + (16 * h1)))))] * v2[tx + (16 * (h2 + (16 * (ty + (16 * h7)))))]));
                newVariable0 = (newVariable0 - (T2i[h7 + 1 + (16 * (bx + (16 * (by + (16 * h1)))))] * v2[tx + (16 * (h2 + (16 * (ty + (16 * (h7 + 1))))))]));
                newVariable0 = (newVariable0 - (T2i[h7 + 2 + (16 * (bx + (16 * (by + (16 * h1)))))] * v2[tx + (16 * (h2 + (16 * (ty + (16 * (h7 + 2))))))]));
                newVariable0 = (newVariable0 - (T2i[h7 + 3 + (16 * (bx + (16 * (by + (16 * h1)))))] * v2[tx + (16 * (h2 + (16 * (ty + (16 * (h7 + 3))))))]));
                newVariable0 = (newVariable0 - (T2i[h7 + 4 + (16 * (bx + (16 * (by + (16 * h1)))))] * v2[tx + (16 * (h2 + (16 * (ty + (16 * (h7 + 4))))))]));
                newVariable0 = (newVariable0 - (T2i[h7 + 5 + (16 * (bx + (16 * (by + (16 * h1)))))] * v2[tx + (16 * (h2 + (16 * (ty + (16 * (h7 + 5))))))]));
                newVariable0 = (newVariable0 - (T2i[h7 + 6 + (16 * (bx + (16 * (by + (16 * h1)))))] * v2[tx + (16 * (h2 + (16 * (ty + (16 * (h7 + 6))))))]));
              }
              for (h7 = 14; h7 <= 15; h7 += 1) 
                newVariable0 = (newVariable0 - (T2i[h7 + (16 * (bx + (16 * (by + (16 * h1)))))] * v2[tx + (16 * (h2 + (16 * (ty + (16 * h7)))))]));
              T3[tx + 256 * h1 + 1048576 * bx + 4096 * ty + 65536 * by + 16 * h2] = newVariable0;
            }
}

__global__ void c_std_d1_1_GPU_0(double *T3,double *T2i,double *v2)
{
  int h7;
  int h1;
  int h2;
  int bx;
  bx = blockIdx.x;
  int by;
  by = blockIdx.y;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  double newVariable0;
  int t2;
  int t4;
  int t6;
  int t8;
  int t12;
  int t14;
  int t16;
  for (h2 = 0; h2 <= 15; h2 += 1) 
    for (h1 = 0; h1 <= 15; h1 += 1) {
      newVariable0 = T3[tx + 256 * h1 + 1048576 * bx + 4096 * ty + 65536 * by + 16 * h2];
      for (h7 = 0; h7 <= 7; h7 += 7) {
        newVariable0 = (newVariable0 - (T2i[h7 + (16 * (bx + (16 * (by + (16 * h1)))))] * v2[tx + (16 * (h2 + (16 * (ty + (16 * h7)))))]));
        newVariable0 = (newVariable0 - (T2i[h7 + 1 + (16 * (bx + (16 * (by + (16 * h1)))))] * v2[tx + (16 * (h2 + (16 * (ty + (16 * (h7 + 1))))))]));
        newVariable0 = (newVariable0 - (T2i[h7 + 2 + (16 * (bx + (16 * (by + (16 * h1)))))] * v2[tx + (16 * (h2 + (16 * (ty + (16 * (h7 + 2))))))]));
        newVariable0 = (newVariable0 - (T2i[h7 + 3 + (16 * (bx + (16 * (by + (16 * h1)))))] * v2[tx + (16 * (h2 + (16 * (ty + (16 * (h7 + 3))))))]));
        newVariable0 = (newVariable0 - (T2i[h7 + 4 + (16 * (bx + (16 * (by + (16 * h1)))))] * v2[tx + (16 * (h2 + (16 * (ty + (16 * (h7 + 4))))))]));
        newVariable0 = (newVariable0 - (T2i[h7 + 5 + (16 * (bx + (16 * (by + (16 * h1)))))] * v2[tx + (16 * (h2 + (16 * (ty + (16 * (h7 + 5))))))]));
        newVariable0 = (newVariable0 - (T2i[h7 + 6 + (16 * (bx + (16 * (by + (16 * h1)))))] * v2[tx + (16 * (h2 + (16 * (ty + (16 * (h7 + 6))))))]));
      }
      for (h7 = 14; h7 <= 15; h7 += 1) 
        newVariable0 = (newVariable0 - (T2i[h7 + (16 * (bx + (16 * (by + (16 * h1)))))] * v2[tx + (16 * (h2 + (16 * (ty + (16 * h7)))))]));
      T3[tx + 256 * h1 + 1048576 * bx + 4096 * ty + 65536 * by + 16 * h2] = newVariable0;
    }
}
