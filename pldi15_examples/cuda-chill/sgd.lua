init("sgd.c", "sgd_kernel_global_simd", 0)
dofile("cudaize.lua")

make_dense(0,2,"l")


known("lb",46641)
known("ub",155998)
known("n", 46641)

distribute({0,1,2}, 4)


skew({0,1,2},2,{-1,1})

permute(0,{"l", "i","j", "k"})
shift_to(0,1,0)
distribute({0,1,2}, 4)

compact(0,{2},{"new_ratings"}, -1, {"ratings"})
print_code(0)

distribute({0,1,2},3)


N = 155998
--M = 1000000


print_space()
known({"_P_DATA1_", "_P_DATA1__"}, {-1,1}, -1, 0)

tile_by_index(1,{"i"},{2},{l1_control="by_diag"},{"l","by_diag", "i","k"})
shift_to(1,3,0);
print_space()

print_code(1)


scalar_expand_by_index(0,{"i"},"err",1, 0, 1)
scalar_expand_by_index(1,{"i","k"},"RHS",1, 0, 1)
distribute({1,6}, 4)


cudaize(1,"sgd_ut_mul_col_operator",{fv=N},{block={"by_diag"}, thread={"k", "i"}}, {"new_ratings","_P_DATA2","err","step_size"})
--cudaize(1,"sgd_ut_mul_col_operator",{fv=N},{block={"by_diag"}, thread={"k", "i"}}, {"err", "new_ratings", "step_size"})
reduce_by_index(1,{"tx"},"segreduce_warp",{},{})

datacopy_privatized(2,"ty", "_P_DATA2", {"ty", "tx"})  	


known({"index_", "index__"}, {-1,1}, -1, 0)

