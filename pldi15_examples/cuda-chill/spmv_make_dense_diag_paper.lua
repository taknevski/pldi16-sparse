init("spmv_diag.cpp", "spmv", 0)
dofile("cudaize.lua")

make_dense(0,2,"k")

--enabling transformations
known("lb",0)
known("ub",UB-1)
known("n", UB)
skew(stmt,"k",{-1,1})
permute(stmt,{"k","i","j"})
shift_to(stmt,1,0)

compact(stmt,1,"a_prime", 0, "a")

--downstream transformations
permute(executor_stmt,{"i","k"})
tile_by_index(executor_stmt,{"i"},{Ti},{l1_control="ii"},{"ii","i","k"})
tile_by_index(executor_stmt,{"k"},{Ti},{l1_control="kk"},{"ii","i","kk","k"})
normalize_index(executor_stmt,"k")
cudaize(executor_stmt,"spmv_diag_GPU",{x=N,y=N},{block={"ii"}, thread={"i"}}, {"a_prime", "_P_DATA2"})

--shared memory and register copy optimizations
--for GPU
copy_to_shared(executor_stmt,"k","_P_DATA2",-16)
copy_to_registers(executor_stmt, "kk", "y");

