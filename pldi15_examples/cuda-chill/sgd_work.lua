init("sgd_work.c", "sgd_kernel_global_simd", 0)
dofile("cudaize.lua")

make_dense(0,2,"l")

known("lb",0)
known("ub",1499)
known("n", 1000)

distribute({0,1,2}, 4)
tile_by_index(0,{"l"},{2},{l1_control="ll"},{"ll","i","l","j","k"})
tile_by_index(0,{"i"},{2},{l1_control="ii"},{"ii","ll","i","l","j", "k"})

shift_to(0,3,0) 
shift_to(0,4,0) 


skew({0,1,2},2,{-1,1})

permute(0,{"ll", "ii","i","l","j", "k"})
shift_to(0,1,0)
distribute({0,1,2}, 6)

compact(0,{2},{"new_ratings"}, 0, {"ratings"})
distribute({0,1,2},5)


N = 1000
M = 1000000



tile_by_index(1,{"ll"},{2},{l1_control="by_diag"},{"ii","by_diag", "ll","i","l","k"})
shift_to(1,3,0);

cudaize(1,"sgd_blk_diag_operator",{fv=N},{block={"by_diag"}, thread={"ll"}}, {"_P_DATA1", "new_ratings", "_P_DATA2"})
print_code(1)
known({"index_", "index__"}, {-1,1}, -1, 0)

