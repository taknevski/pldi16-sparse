#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define index_(i) index[i]
#define index__(i) index[i + 1]
__global__ void spmv_GPU(float *y,float *a,float *x,int *col);
#include <iostream>
using namespace std;

struct NNZ 
{
  int i[100UL];
  int j[100UL];
}
;

class inspector 
{
  public: inspector();
  struct NNZ nonzeros;
  int count;
  int nnz(int ,int );
}
;

inspector::inspector()
{
  (this) -> count = 0;
}

int inspector::nnz(int i,int j)
{
  (this) -> nonzeros.NNZ::i[(this) -> count] = i;
  (this) -> nonzeros.NNZ::j[(this) -> count] = j;
  return (this) -> count++;
}

void spmv_inspector(int n,int index[100UL],float a[100UL],float y[10UL],float x[10UL],int col[100UL],class inspector &NNZ)
{
  int t8;
  int Out_1;
  int t6;
  int *devI3Ptr;
  float *devI2Ptr;
  float *devI1Ptr;
  float *devO1Ptr;
  int t4;
  int t2;
  int i;
  int j;
  cudaMalloc(((void **)(&devO1Ptr)),10 * sizeof(float ));
  cudaMemcpy(devO1Ptr,y,10 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),100 * sizeof(float ));
  cudaMemcpy(devI1Ptr,a,100 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),10 * sizeof(float ));
  cudaMemcpy(devI2Ptr,x,10 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),100 * sizeof(int ));
  cudaMemcpy(devI3Ptr,col,100 * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid = dim3(NNZ. nnz (t2,t4) / 32 + 1,1);
  dim3 dimBlock = dim3(32,1);
  if (index_(t2) + 1 <= index__(t2)) 
    for (t2 = 0; t2 <= n - 1; t2 += 1) 
      for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
        t6 = NNZ. nnz (t2,t4);;
      }
  spmv_GPU<<<dimGrid,dimBlock>>>(devO1Ptr,devI1Ptr,devI2Ptr,devI3Ptr);
  cudaMemcpy(y,devO1Ptr,10 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
}

int main()
{
  int n = 10;
  float a[100UL];
  float y[10UL];
  float x[10UL];
  int index[100UL];
  int col[100UL];
  class inspector insp;
  spmv_inspector(n,index,a,y,x,col,insp);
  return 0;
}

__global__ void spmv_GPU(float *y,float *a,float *x,int *col)
{
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int n;
  class inspector &NNZ;
  int i;
  int j;
  int t6;
  int Out_1;
  int t8;
  if (index_(Out_1) + 1 <= index__(Out_1) && 1 <= n) 
    if (tx <= -(32 * bx) + NNZ. nnz (Out_1,Out_1)) 
      y[i] += (a[j] * x[col[j]]);
}
