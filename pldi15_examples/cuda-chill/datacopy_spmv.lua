init("datacopy_spmv.c", "spmv", 0)
dofile("cudaize.lua")

NNZ=100
N=10
Ti = 1024
Tj = 256
Tk = 32

tile_by_index(0,{"k"},{Ti},{l1_control="kk"},{"i","kk","k"})CU=1
tile_by_index(0,{"k"},{Tj},{l1_control="bbb"},{"i","kk","bbb","k"})CU=1

print_code(0)

tile_by_index(0,{"k"},{Tk},{l1_control="bbbb", l1_tile="k"},{"i","kk", "bbb","k", "bbbb"})CU=2

--tile_by_index(0,{"k"},{Tk},{l1_control="bbbb"},{"i","kk", "bbb","bbbb","k"})CU=1


distribute({0,1}, 2)
print_code(0)

peel(1,4,-1)
print_code(0)

distribute({1,2}, 2)
print_code(0)

fuse({0,1}, 2)
print_code(0)

distribute({0,1},4)
print_code(0)

distribute({1,2},1)

print_code(0)

--tile(0, 5, 4);



--distribute({0,1},5)



cudaize(0,"spmv_GPU",{A=NNZ,x=N,y=N,col=NNZ, temp=NNZ, NNZ_i=NNZ, NNZ_j = NNZ},{block={"kk"}, thread={"k", "bbb"}})CU=1



