init("csr_scalar.cpp", "spmv", 0)
dofile("cudaize.lua")
N = 4096
Ti = 1024
NNZ=5120


tile_by_index(0,{"i"},{Ti},{l1_control="ii"},{"ii","i","j"})CU=1
cudaize(0,"spmv_GPU",{ a=NNZ,x=N,y=N,col=NNZ,index=NNZ},{block={"ii"}, thread={"i"}},{})
known({"index__","index_"},{1,-1}, -1, 0)
print_code(0)
--copy_to_registers(0, "j", "y");
