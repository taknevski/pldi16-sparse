#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define col_(_t11) col[1 * _t11]
#define col___(_t11) col[1 * _t11 + 1]
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
__global__ void spmv_diag_GPU(int chill_count_1,float *y,int *_P_DATA1,float *a_prime,float *x);

struct a_list 
{
  int col_;
  float a[1000000];
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void spmv(int n,int *index,float *a,float *y,float *x,int *col)
{
  float *devI3Ptr;
  float *devI2Ptr;
  int *devI1Ptr;
  float *devO1Ptr;
  int t12;
  int t10;
  int t8;
  int t6;
  int t4;
  int t2;
  float newVariable1;
  int _t36;
  int _t35;
  int _t34;
  int _t33;
  int _t32;
  int _t31;
  int _t25;
  int _P1[257];
  int _t22;
  int _t19;
  int _t20;
  int _t24;
  int In_2;
  int In_1;
  int _t13;
  int _t12;
  struct a_list *_P_DATA4;
  int newVariable0;
  struct mk *_P_DATA3;
  float *a_prime;
  struct a_list *_P_DATA2;
  int *_P_DATA1;
  int chill_count_1;
  int chill_count_0;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t6;
  int _t5;
  int _t3;
  int _t4;
  int k;
  int _t2;
  int _t1;
  int i;
  int j;
  _P_DATA2 = 0;
  _P_DATA3 = ((struct mk *)(malloc(sizeof(struct mk ) * 1999999)));
  chill_count_1 = 0;
  for (t2 = 0; t2 <= 999999; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
      _t9 = col_(t4) - t2 + 999999;
      _P_DATA3[_t9].mk::ptr = 0;
    }
  for (t2 = 0; t2 <= 999999; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
      _t9 = col_(t4) - t2 + 999999;
      if (_P_DATA3[_t9].mk::ptr == 0) {
        _P_DATA4 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
        _P_DATA4 -> a_list::next = _P_DATA2;
        _P_DATA2 = _P_DATA4;
        _P_DATA3[_t9].mk::ptr = _P_DATA2;
        for (newVariable0 = 0; newVariable0 <= 999999; newVariable0 += 1) 
          _P_DATA3[_t9].mk::ptr -> a_list::a[1 * newVariable0] = 0;
        _P_DATA3[_t9].mk::ptr -> a_list::col_ = _t9;
        chill_count_1 += 1;
      }
      _P_DATA3[_t9].mk::ptr -> a_list::a[1 * t2] = a[t4];
    }
{
    _P_DATA1 = ((int *)(malloc(sizeof(int ) * chill_count_1)));
    a_prime = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 1000000))));
    for (t4 = 0; t4 <= 999999; t4 += 1) 
      a_prime[-(-chill_count_1 + 1) * 1000000 + 1 * t4] = _P_DATA2 -> a_list::a[1 * t4];
    _P_DATA1[-(-chill_count_1 + 1)] = _P_DATA2 -> a_list::col_;
    _P_DATA4 = _P_DATA2 -> a_list::next;
    free(_P_DATA2);
    _P_DATA2 = _P_DATA4;
    for (t2 = -chill_count_1 + 2; t2 <= 0; t2 += 1) {
      for (t4 = 0; t4 <= 999999; t4 += 1) 
        a_prime[-t2 * 1000000 + 1 * t4] = _P_DATA2 -> a_list::a[1 * t4];
      _P_DATA1[-t2] = _P_DATA2 -> a_list::col_;
      _P_DATA4 = _P_DATA2 -> a_list::next;
      free(_P_DATA2);
      _P_DATA2 = _P_DATA4;
    }
  }
  cudaMalloc(((void **)(&devO1Ptr)),1000000 * sizeof(float ));
  cudaMemcpy(devO1Ptr,y,1000000 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),chill_count_1 * sizeof(int ));
  cudaMemcpy(devI1Ptr,_P_DATA1,chill_count_1 * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),chill_count_1 * 1000000 * sizeof(float ));
  cudaMemcpy(devI2Ptr,a_prime,chill_count_1 * 1000000 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),1000000 * sizeof(float ));
  cudaMemcpy(devI3Ptr,x,1000000 * sizeof(float ),cudaMemcpyHostToDevice);
  dim3 dimGrid3 = dim3(3907,1);
  dim3 dimBlock3 = dim3(256,1);
  spmv_diag_GPU<<<dimGrid3,dimBlock3>>>(chill_count_1,devO1Ptr,devI1Ptr,devI2Ptr,devI3Ptr);
  cudaMemcpy(y,devO1Ptr,1000000 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  free(_P_DATA1);
  free(a_prime);
  free(_P_DATA3);
}

int main()
{
  int n;
  float *a;
  float *y;
  float *x;
  int *index;
  int *col;
  spmv(n,index,a,y,x,col);
  return 0;
}

__global__ void spmv_diag_GPU(int chill_count_1,float *y,int *_P_DATA1,float *a_prime,float *x)
{
  int k;
  int kk;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  __device__ __shared__ int _P1[257];
  float newVariable1;
  int t2;
  int t4;
  int t8;
  int t10;
  if (tx <= -(256 * bx) + 999999) {
    newVariable1 = y[256 * bx + tx];
    for (kk = 0; kk <= (chill_count_1 - 1) / 256; kk += 1) {
      if (tx <= chill_count_1 - 256 * kk - 1) 
        _P1[256 * kk + tx - 256 * kk] = _P_DATA1[256 * kk + tx];
      __syncthreads();
      for (k = 0; k <= __rose_lt(255,chill_count_1 - 256 * kk - 1); k += 1) 
        newVariable1 += (a_prime[(256 * kk + k) * 1000000 + 1 * (tx + 256 * bx)] * x[tx + 256 * bx + _P1[256 * kk + k - 256 * kk] - 999999]);
      __syncthreads();
    }
    y[256 * bx + tx] = newVariable1;
  }
  __syncthreads();
}
