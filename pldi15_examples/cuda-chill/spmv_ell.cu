#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define col_(_t12,_t13) col[100 * _t12 + _t13 + index_(_t11)]
#define index_(i) index[i]
#define index__(i) index[i + 1]
__global__ void spmv_ell_GPU(float *y,float *_P_DATA7,float *x,int *_P_DATA8);
#define NZ 1666
#define NUMROWS 494

struct a_list 
{
  int col_;
  float a[100];
  int col[100];
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void spmv(int n,int index[],float *a,float *y,float *x,int *col)
{
  int *devI3Ptr;
  float *devI2Ptr;
  float *devI1Ptr;
  float *devO1Ptr;
  int t8;
  int t6;
  int t4;
  float newVariable1;
  int _t35;
  int _t37;
  int _t36;
  int _t34;
  int _t31;
  int _t30;
  int t2;
  int _t24;
  int _t25;
  int _t23;
  int _t22;
  int _t21;
  int *_P_DATA8;
  int _t20;
  int _t19;
  float *_P_DATA7;
  int _t17;
  int _t18;
  int In_2;
  int In_1;
  int _t16;
  int _t15;
  int _t14;
  struct a_list *_P_DATA6;
  int newVariable0;
  struct mk *_P_DATA5;
  int *col_prime;
  struct a_list *_P_DATA4;
  float *a_prime;
  struct a_list *_P_DATA3;
  int *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t5;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * (69031649 + 1))));
  _P_DATA3 = 0;
  _P_DATA4 = 0;
  _P_DATA5 = ((struct mk *)(malloc(sizeof(struct mk ) * 1)));
  chill_count_1 = 0;
  _P_DATA1[0] = 0;
  _P_DATA1 = (int*)malloc(sizeof(int ) * (69031649 + 1));
  _P_DATA2 = (int*)malloc(sizeof(int ) * (chill_count_1 + 1));
  a_prime = (float*)malloc(sizeof(float ) * (chill_count_1 * 100));
  col_prime = (int*)malloc(sizeof(int ) * (chill_count_1 * 1));
  _P_DATA7 = (float*)malloc(sizeof(float ) * (100 * (n - 1 + 1)));
  _P_DATA8 = (int*)malloc(sizeof(int ) * (100 * (n - 1 + 1)));
  for (t2 = 0; t2 <= n - 1; t2 += 1) {
    for (t4 = 0; t4 <= (index__(t2) - index_(t2) - 1) / 100; t4 += 1) 
      for (t6 = 0; t6 <= __rose_lt(99,index__(t2) - index_(t2) - 100 * t4 - 1); t6 += 1) 
        _P_DATA5[t4].mk::ptr = 0;
    for (t4 = 0; t4 <= (index__(t2) - index_(t2) - 1) / 100; t4 += 1) 
      for (t6 = 0; t6 <= __rose_lt(99,index__(t2) - index_(t2) - 100 * t4 - 1); t6 += 1) {
        if (_P_DATA5[t4].mk::ptr == 0) {
          _P_DATA6 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
          _P_DATA6 -> a_list::next = _P_DATA4;
          _P_DATA4 = _P_DATA6;
          _P_DATA5[t4].mk::ptr = _P_DATA4;
          for (newVariable0 = 0; newVariable0 <= 99; newVariable0 += 1) {
            _P_DATA5[t4].mk::ptr -> a_list::a[1 * newVariable0] = 0;
            _P_DATA5[t4].mk::ptr -> a_list::col[1 * newVariable0] = 0;
          }
          _P_DATA5[t4].mk::ptr -> a_list::col_ = t4;
          chill_count_1 += 1;
        }
        _P_DATA5[t4].mk::ptr -> a_list::a[1 * t4] = a[100 * t4 + t6 + index_(t2)];
        _P_DATA5[t4].mk::ptr -> a_list::col[1 * t4] = col[100 * t4 + t6 + index_(t2)];
      }
    _P_DATA1[t2 + 1] = chill_count_1;
  }
  for (t2 = -chill_count_1 + 1; t2 <= 0; t2 += 1) {
    if (chill_count_1 + t2 <= 1) {
      _P_DATA2 = ((int *)(malloc(sizeof(int ) * chill_count_1)));
      a_prime = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 100))));
      col_prime = ((int *)(malloc(sizeof(int ) * (chill_count_1 * 100))));
    }
    for (t4 = 0; t4 <= 99; t4 += 1) {
      a_prime[-t2 * 100 + 1 * t4] = _P_DATA3 -> a_list::a[1 * t4];
      col_prime[-t2 * 100 + 1 * t4] = _P_DATA3 -> a_list::col[1 * t4];
    }
    _P_DATA2[-t2] = _P_DATA3 -> a_list::col_;
    _P_DATA6 = _P_DATA3 -> a_list::next;
    free(_P_DATA3);
    _P_DATA3 = _P_DATA6;
  }
  if (1 <= n) 
    for (t2 = 0; t2 <= 99; t2 += 1) 
      for (t4 = 0; t4 <= n - 1; t4 += 1) {
        _P_DATA7[t4 + t2 * (n - 1 + 1)] = a_prime[t4 * 100 + 1 * t2];
        _P_DATA8[t4 + t2 * (n - 1 + 1)] = col_prime[t4 * 100 + 1 * t2];
      }
  cudaMalloc(((void **)(&devO1Ptr)),20480 * sizeof(float ));
  cudaMemcpy(devO1Ptr,y,20480 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),100 * (n - 1 + 1) * sizeof(float ));
  cudaMemcpy(devI1Ptr,_P_DATA7,100 * (n - 1 + 1) * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),20480 * sizeof(float ));
  cudaMemcpy(devI2Ptr,x,20480 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),100 * (n - 1 + 1) * sizeof(int ));
  cudaMemcpy(devI3Ptr,_P_DATA8,100 * (n - 1 + 1) * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid3 = dim3((n - 1) / 1024 + 1,1);
  dim3 dimBlock3 = dim3(1024,1);
  spmv_ell_GPU<<<dimGrid3,dimBlock3>>>(devO1Ptr,devI1Ptr,devI2Ptr,devI3Ptr);
  cudaMemcpy(y,devO1Ptr,20480 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  free(_P_DATA1);
  free(_P_DATA2);
  free(a_prime);
  free(col_prime);
  free(_P_DATA5);
}

int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[494 + 1];
  int col[1666UL];
  spmv(n,index,a,y,x,col);
  return 0;
}

__global__ void spmv_ell_GPU(float *y,float *_P_DATA7,float *x,int *_P_DATA8)
{
  int jj;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int n;
  int t2;
  float newVariable1;
  int t4;
  int t8;
  if (tx <= n - 1024 * bx - 1) {
    newVariable1 = y[1024 * bx + tx];
    for (jj = 0; jj <= 99; jj += 1) 
      newVariable1 += (_P_DATA7[1024 * bx + tx + jj * (n - 1 + 1)] * x[_P_DATA8[1024 * bx + tx + jj * (n - 1 + 1)]]);
    y[1024 * bx + tx] = newVariable1;
  }
}
