init("spmv_ell.cpp", "spmv", 0)
dofile("cudaize.lua")
Ti=1024
N=20480
M=100

ELLify(0,{"a","col"},100, true, "col")

print_code(0)
--permute(0,{"j", "i"})

--scalar_expand(0,{1,2}, "_P_DATA1")
--scalar_expand(0,{1,2}, "_P_DATA2")

--distribute({0,5,6}, 1)
--fuse({5,6},1)
--fuse({5,6},2)



--tile_by_index(0,{"i"},{Ti},{l1_control="ii"},{"j","ii","i"})
--cudaize(0,"spmv_ell_GPU",{ _P_DATA4=N*M,x=N,y=N,_P_DATA3=N*M},{block={"ii"}, thread={"i"}}, {"_P_DATA3", "_P_DATA4"})

