#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA1_(_t67) _P_DATA1[_t67]
#define _P_DATA1__(_t67) _P_DATA1[_t67 + 1]
#define col_(ip,jp) col[1 * jp]
#define col__(i,j) col[1 * j]
#define col___(ip,jp) col[1 * jp + 1]
#define col____(i,j) col[1 * j + 1]
#define col_____(_t71) col[1 * _t71]
#define col_______(_t71) col[1 * _t71 + 1]
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
#define new_ratings(_t67,_t68,_t69,_t70) new_ratings[_t68 * 4 + 2 * _t69 + 1 * _t70]
#define NZ 1666
#define NUMROWS 494
#define N 1000
#define SGD_LAMBDA 0.05f
#define SGD_FEATURE_SIZE 16

struct a_list 
{
  unsigned short col_;
  float ratings[4];
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void sgd_kernel_global_simd(int n,int *index,float *ratings,float *y,float *x,int *col,float *fv,float step_size)
{
  int _t152;
  int _t151;
  int _t150;
  int _t149;
  int _t148;
  int _t147;
  unsigned short newVariable5;
  int _t145;
  int _t144;
  int _t143;
  int _t142;
  int _t141;
  int _t140;
  int _t139;
  int _t138;
  int _t137;
  int _t136;
  int _t135;
  int _t134;
  int t2;
  int _t122;
  int _t121;
  int _t120;
  int _t119;
  int _t118;
  int _t117;
  float _P3[2 * 16];
  int _t116;
  int _t115;
  int _t114;
  int _t113;
  int _t112;
  int _t111;
  float _P2[2];
  int _t101;
  int _t100;
  int _t99;
  int _t98;
  int _t97;
  int _t96;
  int _t92;
  int _t91;
  int _t90;
  int _t89;
  int _t88;
  int _t87;
  int _t86;
  int _t85;
  int _t84;
  int _t83;
  int _t81;
  int _t80;
  int _t79;
  int _t78;
  int _t77;
  int _t76;
  int newVariable4;
  int newVariable3;
  int newVariable2;
  int In_3;
  int In_2;
  int In_1;
  int _t75;
  int _t74;
  int _t73;
  struct a_list *_P_DATA4;
  int newVariable1;
  int newVariable0;
  struct mk *_P_DATA3;
  float *new_ratings;
  struct a_list *_P1[77999];
  unsigned short *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t72;
  int _t71;
  int _t70;
  int _t69;
  int _t68;
  int _t67;
  int _t66;
  int _t65;
  int _t64;
  int _t63;
  int _t62;
  int _t61;
  int _t60;
  int _t59;
  int _t58;
  int _t57;
  int _t56;
  int _t55;
  int _t54;
  int _t53;
  int _t52;
  int _t51;
  int _t50;
  int _t49;
  int _t48;
  int _t47;
  int _t46;
  int _t45;
  int _t44;
  int _t43;
  int _t42;
  int _t41;
  int _t40;
  int _t39;
  int _t37;
  int _t38;
  int _t36;
  int _t35;
  int _t34;
  int _t33;
  int _t32;
  int _t31;
  int _t30;
  int _t29;
  int _t28;
  int _t27;
  int _t26;
  int _t25;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  int _t18;
  int _t17;
  int _t16;
  int _t15;
  int _t14;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t7;
  int _t6;
  int _t5;
  int _t4;
  int l;
  int _t3;
  int _t2;
  int _t1;
  int jp;
  int i;
  int j;
  int k;
  float err;
  for (i = 0; i < n; i++) {
    for (j = index[i]; j < index[i + 1]; j++) {
      err = -ratings[_t2];
      for (k = 0; k < 16; k++) 
        err += (fv[(_t1 * 16) + _t3] * fv[(col[_t2] * 16) + _t3]);
      for (k = 0; k < 16; k++) {
        fv[(_t1 * 16) + _t3] -= (step_size * ((err * fv[(col[_t2] * 16) + _t3]) + (0.05f * fv[(_t1 * 16) + _t3])));
        fv[(col[_t2] * 16) + _t3] -= (step_size * ((err * fv[(_t1 * 16) + _t3]) + (0.05f * fv[(col[_t2] * 16) + _t3])));
      }
    }
  }
}

int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[495UL];
  int col[1666UL];
  float *fv;
  float step_size = 0.09f;
  float err;;
  sgd_kernel_global_simd(n,index,a,y,x,col,fv,step_size);
  return 0;
}
