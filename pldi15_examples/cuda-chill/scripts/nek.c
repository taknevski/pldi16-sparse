/*This code was generated using the Tensor-Contraction Autotuning tool,
developed by Axel Y. Rivera (University of Utah).
Code Generated Date and hour: 2014-05-19 15:57*/

#define nelt 100
#define lz 10
#define lx 10
#define ly 10

void local_grad3(double *ut, double *us, double *ur, double *D, double *u1, double *u, double *Dt, double *u2){

	int e, j, i, k, m,k2;

	for(e = 0; e < nelt; e++){
		for(j = 0; j < lx*ly; j++){
			for(i = 0; i < lz; i++){
				for(k = 0; k < lx; k++){
					ur[e*lx*ly*lz + j*lz + i]  = ur[e*lx*ly*lz + j*lz + i] +  (D[k*ly + i] * u1[e*lx*ly*lz + j*lz + k]);
				}
			}
		}
	}
	for(e = 0; e < nelt; e++){
		for(j = 0; j < lx; j++){
			for(i = 0; i < ly; i++){
				for(k = 0; k < lx; k++){
					for(m = 0; m < ly; m++){
						us[e*lx*ly*lx + j*ly*lx + i*lx + k]  = us[e*lx*ly*lx + j*ly*lx + i*lx + k] +  (u[e*lx*ly*lx + j*ly*lx + m*lx + k] * Dt[i*ly + m]);
					}
				}
			}
		}
	}
	for(e = 0; e < nelt; e++){
		for(j = 0; j < lx; j++){
			for(i = 0; i < ly*lz; i++){
				for(k2 = 0; k2 < lx; k2++){
					ut[e*lx*ly*lz + j*ly*lz + i]  = ut[e*lx*ly*lz + j*ly*lz + i] +  (u2[e*lx*ly*lz + k2*ly*lz + i] * Dt[j*ly + k2]);
				}
			}
		}
	}
}
