init("spmv_ell.cpp", "spmv", 0)
dofile("cudaize.lua")

--non-affine shift,tile and compact embedded in ELLify
ELLify(0,{"a","col"},M)

--permute data order and copy data into permuted order
permute(0,{"j", "i"})
datacopy(0,{1,2}, "_P_DATA1")
datacopy(0,{1,2}, "_P_DATA2")


tile_by_index(0,{"i"},{Ti},{l1_control="ii"},{"ii","i","j"})
cudaize(0,"spmv_ell_GPU",{ _P_DATA4=N*M,x=N,y=N,_P_DATA3=N*M},{block={"ii"}, thread={"i"}}, {"_P_DATA3", "_P_DATA4"})
copy_to_registers(0, "j", "y");
