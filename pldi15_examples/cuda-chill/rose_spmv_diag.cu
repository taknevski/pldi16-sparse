#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define col_(_t5) col[_t5]
#define index_(i) index[i]
#define index__(i) index[i + 1]
__global__ void spmv_diag_GPU(float *y,int *_P_DATA1,float *a_prime,float *x, int n, int chill_count_1)
#define BLOCKDIM 256
#include <stdio.h>

struct a_list 
{
  float data[1000000];
  int col;
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void spmv_bcsr(int m, int n, int *index, float *a, float *y, float *x, int *col,int nnz)
{
  float *devI3Ptr;
  float *devI2Ptr;
  int *devI1Ptr;
  float *devO1Ptr;
  int chill_count_1';
  int t12;
  int t10;
  int t8;
  int t6;
  int t4;
  int t2;
  float newVariable1;
  int _t30;
  int _t29;
  int _t28;
  int _t27;
  int _t26;
  int _t25;
  int _t19;
  int _P1[BLOCKDIM+1];
  int _t16;
  int _t13;
  int _t14;
  int _t18;
  int In_2;
  int In_1;
  int _t7;
  int _t6;
  struct a_list *_P_DATA4;
  int newVariable0;
  struct mk *_P_DATA3;
  float *a_prime;
  struct a_list *_P_DATA2;
  int *_P_DATA1;
  int chill_count_1;
  int chill_count_0;
  int _t5;
  int _t3;
  int _t4;
  int k;
  int _t2;
  int _t1;
  int i;
  int j;

  cudaError_t err = cudaSuccess;
  cudaEvent_t start_event, stop_event;
  cudaEventCreate(&start_event);
  cudaEventCreate(&stop_event);
  float cuda_elapsed_time;	

  _P_DATA2 = 0;
  _P_DATA3 = ((struct mk *)(malloc(sizeof(struct mk ) * 1999999)));
  chill_count_1 = 0;
  _P_DATA1 = malloc(sizeof(int ) * (chill_count_1 + 1));
  a_prime = malloc(sizeof(float ) * (chill_count_1 * 1000000));
  for (t2 = 0; t2 <= 999999; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
      _t3 = col_(t4) - t2 + 999999;
      _P_DATA3[_t3].mk::ptr = 0;
    }
  for (t2 = 0; t2 <= 999999; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
      _t3 = col_(t4) - t2 + 999999;
      if (t2 + _t3 - 999999 == col[t4]) {
        if (_P_DATA3[_t3].mk::ptr == 0) {
          _P_DATA4 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
          _P_DATA4 -> a_list::next = _P_DATA2;
          _P_DATA2 = _P_DATA4;
          _P_DATA3[_t3].mk::ptr = _P_DATA2;
          for (newVariable0 = 0; newVariable0 <= 999999; newVariable0 += 1) 
            _P_DATA3[_t3].mk::ptr -> a_list::data[1 * newVariable0] = 0;
          _P_DATA3[_t3].mk::ptr -> a_list::col = _t3;
          chill_count_1 += 1;
        }
        _P_DATA3[_t3].mk::ptr -> a_list::data[1 * t2] = a[t4];
      }
    }
  for (t2 = -chill_count_1 + 1; t2 <= 0; t2 += 1) {
    if (chill_count_1 + t2 <= 1) {
      _P_DATA1 = ((int *)(malloc(sizeof(int ) * chill_count_1)));
      a_prime = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 1000000))));
    }
    for (t4 = 0; t4 <= 999999; t4 += 1) 
      a_prime[-t2 * 1000000 + 1 * t4] = _P_DATA2 -> a_list::data[1 * t4];
    _P_DATA1[-t2] = _P_DATA2 -> a_list::col;
    _P_DATA4 = _P_DATA2 -> a_list::next;
    free(_P_DATA2);
    _P_DATA2 = _P_DATA4;
  }

  int num_blocks = n % BLOCKDIM ? n/BLOCKDIM + 1 : n/BLOCKDIM;
  cudaMalloc(((void **)(&devO1Ptr)), N* sizeof(float ));
  cudaMemcpy(devO1Ptr,y,N* sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),(chill_count_1 + 1) * sizeof(int ));
  cudaMemcpy(devI1Ptr,_P_DATA1,(chill_count_1 + 1) * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),chill_count_1 * 1000000 * sizeof(float ));
  cudaMemcpy(devI2Ptr,a_prime,chill_count_1 * 1000000 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),1000000 * sizeof(float ));
  cudaMemcpy(devI3Ptr,x,1000000 * sizeof(float ),cudaMemcpyHostToDevice);
  dim3 dimGrid2 = dim3(num_blocks,1);
  dim3 dimBlock2 = dim3(BLOCKDIM,1);
  cudaEventRecord(start_event,0);
  spmv_diag_GPU<<<dimGrid2,dimBlock2>>>(devO1Ptr,devI1Ptr,devI2Ptr,devI3Ptr, n, chill_count_1);
  cudaEventRecord(stop_event, 0);
  cudaEventSynchronize(stop_event);
  cudaEventElapsedTime(&cuda_elapsed_time, start_event, stop_event);
  cudaMemcpy(y,devO1Ptr,1000000 * sizeof(float ),cudaMemcpyDeviceToHost);

  printf("Elapsed time : %f ms\n" ,cuda_elapsed_time);  
  double mflop_count = 1e-6 *(double)nnz * (double)2.0;
  printf("Performance :%.6lf GFLOPS\n", mflop_count/(double)cuda_elapsed_time);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  free(_P_DATA1);
  free(a_prime);
  free(_P_DATA3);
}

/*int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[494 + 1];
  int col[1666UL];
  spmv(n,index,a,y,x,col);
  return 0;
}
*/
__global__ void spmv_diag_GPU(float *y,int *_P_DATA1,float *a_prime,float *x, int n, int chill_count_1)
{
  int k;
  int kk;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
//  int chill_count_1;
  __device__ __shared__ int _P1[BLOCKDIM+1];
  float newVariable1;
  int t2;
  int t4;
  int t8;
  int t10;
  if (1 <= chill_count_1) {
    if (tx <= -(BLOCKDIM* bx) + n-1) {
      newVariable1 = y[BLOCKDIM* bx + tx];
      for (kk = 0; kk <= (chill_count_1 - 1) / BLOCKDIM; kk += 1) {
        if (tx <= chill_count_1 - BLOCKDIM* kk - 1)
          _P1[BLOCKDIM* kk + tx - BLOCKDIM* kk] = _P_DATA1[BLOCKDIM* kk + tx];
        __syncthreads();
        for (k = 0; k <= __rose_lt(chill_count_1 - BLOCKDIM* kk - 1,BLOCKDIM -1); k += 1)
          newVariable1 += (a_prime[(BLOCKDIM* kk + k) * n + 1 * (tx + BLOCKDIM* bx)] * x[tx + BLOCKDIM* bx + _P1[BLOCKDIM* kk + k - BLOCKDIM* kk] - n + 1]);
        __syncthreads();
      }
      y[BLOCKDIM* bx + tx] = newVariable1;
    }
    __syncthreads();
  }
}
