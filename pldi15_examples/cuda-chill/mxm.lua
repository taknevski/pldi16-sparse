init("mxm.c", "mxm", 0)
dofile("cudaize.lua")
N=1024
Ti=128
Tj=64
Tk=16
Tii=16
Tjj=16



--permute(1,3,2)
permute(0,{"i","j","k"})
tile_by_index(0,{"i"},{Ti},{l1_control="ii"},{"ii","i","j","k"})
tile_by_index(0,{"i"},{Tii},{l1_control="iii"},{"ii","iii","i","j","k"})
tile_by_index(0,{"j"},{Tj},{l1_control="jj"},{"ii","iii","i","jj","j","k"})
tile_by_index(0,{"j"},{Tjj},{l1_control="jjj"},{"ii","iii","i","jj","jjj","j","k"})
cudaize(0,"mxm",{a=0,b=0,c=0},{block={"ii","iii"},thread={"jj","jjj"}}, {})
copy_to_shared("tx","b",-16)
--copy_to_shared("tx","a",-16)
--copy_to_registers("i","c")
