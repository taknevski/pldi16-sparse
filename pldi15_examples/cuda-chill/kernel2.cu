__global__ void sgd_diag_operator(float *fv,int *metadata, float *new_ratings,int *_P_DATA2, int *_P_DATA1, float step_size,int t2)
{
	int bx;
	bx = blockIdx.x;
	int tx;
	tx = threadIdx.x;
	int ty;
	ty = threadIdx.y;
	__device__ __shared__ float _P2[BLOCKSIZE];
	__device__ __shared__ float _P3[BLOCKSIZE * SGD_FEATURE_SIZE];
	int newVariable1;
	int t4;
	int t6;
	int t8;
	if (ty <= _P_DATA1__(t2) - _P_DATA1_(t2) - BLOCKSIZE* bx - 1)
		newVariable1 = _P_DATA2[BLOCKSIZE* bx + _P_DATA1_(t2) + ty];
	if (ty <= _P_DATA1__(t2) - _P_DATA1_(t2) - BLOCKSIZE* bx - 1) {
		_P2[ty] = -new_ratings[(BLOCKSIZE*bx + _P_DATA1_(t2) + ty) * 1];
		_P3[tx + ty * SGD_FEATURE_SIZE] = (fv[newVariable1*SGD_FEATURE_SIZE + tx] * fv[(newVariable1 + (t2 + 1))*SGD_FEATURE_SIZE + tx]);
		segreduce_warp2(&_P2[ty],&_P3[0 + ty * SGD_FEATURE_SIZE]);
		fv[newVariable1*SGD_FEATURE_SIZE + tx] -= (step_size * ((_P2[ty] * fv[(newVariable1 + (t2 + 1))*SGD_FEATURE_SIZE + tx]) + (0.05f * fv[newVariable1*SGD_FEATURE_SIZE + tx])));
		fv[(newVariable1 + (t2 + 1))*SGD_FEATURE_SIZE+tx] -= (step_size * ((_P2[ty] * fv[newVariable1*SGD_FEATURE_SIZE + tx]) + (0.05f * fv[(newVariable1 + (t2 + 1))*SGD_FEATURE_SIZE + tx])));
	}
}
