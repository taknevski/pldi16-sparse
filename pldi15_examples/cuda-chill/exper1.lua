--init("datacopy_spmv.c", "spmv", 0)
--dofile("cudaize.lua")

init("spmv.cpp", "spmv", 0)
dofile("cudaize.lua")
NNZ=1666
N=494
Ti = 1024
Tj = 256
Tk = 32

--flatten loop levels 1 and 2 with NNZ being uninterpreted omega function name
flatten(0,{"new_index"},{1,2}, "c")


--cudaize(1,"spmv_GPU",{ a=NNZ,x=N,y=N,col=NNZ,temp=NNZ, c_j=NNZ, c_i=NNZ },{block={"kk"}, thread={"new_index", "bbb"}},{"_P_DATA1", "_P_DATA2"})

