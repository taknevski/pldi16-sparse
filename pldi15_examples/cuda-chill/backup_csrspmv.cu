#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define rows_(row) rows[row]
#define rows__(row) rows[row + 1]
__global__ void spmv_GPU(double *y,double *vals,double *x,int *cols);
#define n 1024

void spmv(int *rows,int *cols,double *vals,double *x,double *y,int n1)
{
  int t8;
  int t4;
  int t2;
  int t10;
  int t6;
  int *devI3Ptr;
  double *devI2Ptr;
  double *devI1Ptr;
  double *devO1Ptr;
  int row;
  int col_index;
  double temp;
  cudaMalloc(((void **)(&devO1Ptr)),1024 * sizeof(double ));
  cudaMemcpy(devO1Ptr,y,1024 * sizeof(double ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),100000 * sizeof(double ));
  cudaMemcpy(devI1Ptr,vals,100000 * sizeof(double ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),1024 * sizeof(double ));
  cudaMemcpy(devI2Ptr,x,1024 * sizeof(double ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),100000 * sizeof(int ));
  cudaMemcpy(devI3Ptr,cols,100000 * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid = dim3(8,1);
  dim3 dimBlock = dim3(128,1);
  spmv_GPU<<<dimGrid,dimBlock>>>(devO1Ptr,devI1Ptr,devI2Ptr,devI3Ptr);
  cudaMemcpy(y,devO1Ptr,1024 * sizeof(double ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
}

__global__ void spmv_GPU(double *y,double *vals,double *x,int *cols)
{
  int col_index;
  int jj;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int t10;
  int t2;
  int t4;
  int t8;
  for (jj = 0; jj <= (rows__(128 * bx + tx) - rows_(128 * bx + tx) - 1) / 32; jj += 1) 
    for (col_index = rows_(128 * bx + tx) + 32 * jj; col_index <= __rose_lt(rows__(128 * bx + tx) - 1,rows_(128 * bx + tx) + 32 * jj + 31); col_index += 1) 
      y[128 * bx + tx] += (vals[col_index] * x[cols[col_index]]);
}
