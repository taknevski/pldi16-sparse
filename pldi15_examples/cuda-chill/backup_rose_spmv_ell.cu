#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define index_(i) index[i]
#define index__(i) index[i + 1]
__global__ void spmv_ell_GPU(float *y,float *_P_DATA3,float *x,int *_P_DATA4);
#define NZ 1666
#define NUMROWS 494

void spmv(int n,int index[494UL],float a[1666UL],float y[494UL],float x[494UL],int col[1666UL])
{
  int *devI3Ptr;
  float *devI2Ptr;
  float *devI1Ptr;
  float *devO1Ptr;
  int t8;
  int t6;
  int t4;
  float newVariable0;
  int _t28;
  int _t29;
  int _t31;
  int t2;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  int *_P_DATA4;
  int _t18;
  int _t17;
  float *_P_DATA3;
  int _t15;
  int _t16;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int *_P_DATA2;
  int _t8;
  int _t7;
  float *_P_DATA1;
  int _t6;
  int _t5;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  _P_DATA1 = malloc(sizeof(float ) * ((n - 1 + 1) * 100));
  _P_DATA2 = malloc(sizeof(int ) * ((n - 1 + 1) * 100));
  _P_DATA3 = malloc(sizeof(float ) * (100 * (n - 1 + 1)));
  _P_DATA4 = malloc(sizeof(int ) * (100 * (n - 1 + 1)));
  for (t2 = 0; t2 <= n - 1; t2 += 1) {
    for (t4 = 0; t4 <= index__(t2) - index_(t2) - 1; t4 += 1) {
      _P_DATA1[t4 + t2 * 100] = a[t4 + index_(t2)];
      _P_DATA2[t4 + t2 * 100] = col[t4 + index_(t2)];
    }
    for (t4 = index__(t2) - index_(t2); t4 <= 99; t4 += 1) {
      _P_DATA1[t4 + t2 * 100] = 0;
      _P_DATA2[t4 + t2 * 100] = 0;
    }
  }
  if (1 <= n) 
    for (t2 = 0; t2 <= 99; t2 += 1) 
      for (t4 = 0; t4 <= n - 1; t4 += 1) {
        _P_DATA3[t4 + t2 * (n - 1 + 1)] = _P_DATA1[t2 + t4 * 100];
        _P_DATA4[t4 + t2 * (n - 1 + 1)] = _P_DATA2[t2 + t4 * 100];
      }
  cudaMalloc(((void **)(&devO1Ptr)),20480 * sizeof(float ));
  cudaMemcpy(devO1Ptr,y,20480 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),2048000 * sizeof(float ));
  cudaMemcpy(devI1Ptr,_P_DATA3,2048000 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),20480 * sizeof(float ));
  cudaMemcpy(devI2Ptr,x,20480 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),2048000 * sizeof(int ));
  cudaMemcpy(devI3Ptr,_P_DATA4,2048000 * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid2 = dim3((n - 1) / 1024 + 1,1);
  dim3 dimBlock2 = dim3(1024,1);
  spmv_ell_GPU<<<dimGrid2,dimBlock2>>>(devO1Ptr,devI1Ptr,devI2Ptr,devI3Ptr);
  cudaMemcpy(y,devO1Ptr,20480 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
}

int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[494 + 1];
  int col[1666UL];
  spmv(n,index,a,y,x,col);
  return 0;
}

__global__ void spmv_ell_GPU(float *y,float *_P_DATA3,float *x,int *_P_DATA4)
{
  int j;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int n;
  int t2;
  float newVariable0;
  int t4;
  int t8;
  if (tx <= n - 1024 * bx - 1) {
    newVariable0 = y[1024 * bx + tx];
    for (j = 0; j <= 99; j += 1) 
      newVariable0 += (_P_DATA3[tx + 1024 * bx + j * (n - 1 + 1)] * x[_P_DATA4[tx + 1024 * bx + j * (n - 1 + 1)]]);
    y[1024 * bx + tx] = newVariable0;
  }
}
