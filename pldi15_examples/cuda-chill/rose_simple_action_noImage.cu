#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
__global__ void Action_No_image_GPU(double *D_,double (*SolventMols_)[1024][3]);
//this is only  used for cuda-chill
//heavy simplification
#define NsolventMolecules_ 1024
#define NsolventAtoms_ 1024
// struct MolDist {
//       int mol;        ///< Original solvent molecule number (starts from 1).
//       double D;       ///< Closest distance of solvent molecule to atoms in distanceMask.
//       //AtomMask mask;  ///< Original topology solvent molecule atom mask.
//       double solventAtoms[NsolventAtoms_][3]; ///< Actual solvent atom #s to loop over.
//   };
//using dist for no image 
// and kernel for when we use solute molecule center
//extracting pulling out arrays out from struct 

void Action_NoImage_Center(double SolventMols_[1024UL][1024UL][3UL],double D_[1024UL],double maskCenter[3UL],double maxD)
{
  double *devI1Ptr;
  double *devO1Ptr;
  int t10;
  int t4;
  int t8;
  int t6;
  int t2;
  int _t28;
  int _t27;
  int _t25;
  int _t24;
  int _t15;
  int _t14;
  int _t12;
  int _t5;
  int _t4;
  int _t2;
  int _t1;
  double Dist;
  int solventMol;
  int solventAtom;
  cudaMalloc(((void **)(&devO1Ptr)),3072 * sizeof(double ));
  cudaMalloc(((void **)(&devI1Ptr)),3145728 * sizeof(double ));
  cudaMemcpy(devI1Ptr,SolventMols_,3145728 * sizeof(double ),cudaMemcpyHostToDevice);
  dim3 dimGrid0 = dim3(32,16);
  dim3 dimBlock0 = dim3(32,64);
  Action_No_image_GPU<<<dimGrid0,dimBlock0>>>(devO1Ptr,((double (*)[1024][3])devI1Ptr));
  cudaMemcpy(D_,devO1Ptr,3072 * sizeof(double ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
}

__global__ void Action_No_image_GPU(double *D_,double (*SolventMols_)[1024][3])
{
  int bx;
  bx = blockIdx.x;
  int by;
  by = blockIdx.y;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  double maskCenter[3UL];
  double maxD;
  double Dist;
  int solventMol;
  int t2;
  int t6;
  int t4;
  int t10;
  D_[tx] = maxD;
//main dist2_noImage code
//double *a1 = maskCenter.Dptr(); //center of solute molecule
//double *a2 = frmIn.XYZ(*solvent_atom);
//center of solute molecule
  double *a1 = maskCenter;
//double *a2 = SolventMols_[solventMol][solventAtom];  
//double x = a1[0] - a2[0];
//double y = a1[1] - a2[1];
//double z = a1[2] - a2[2];
//Dist = (x*x + y*y + z*z);
  Dist = (((SolventMols_[32 * bx + tx][64 * by + ty][0] * SolventMols_[32 * bx + tx][64 * by + ty][0]) + (SolventMols_[32 * bx + tx][64 * by + ty][1] * SolventMols_[32 * bx + tx][64 * by + ty][1])) + (SolventMols_[32 * bx + tx][64 * by + ty][2] * SolventMols_[32 * bx + tx][64 * by + ty][2]));
  D_[32 * bx + tx] = ((Dist < D_[solventMol])?Dist : D_[solventMol]);
}
