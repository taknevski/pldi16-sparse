init("spmv.cpp", "spmv", 0)
dofile("cudaize.lua")
N = 4096
Ti = 1024
Tj=32
NNZ=5120




tile_by_index(0,{"j"},{Tj},{l1_control="jj"},{"i","jj","j"},strided)CU=1
tile_by_index(0,{"i"},{Ti},{l1_control="ii"},{"ii","i","jj","j"})CU=1

--scalar_expand(0,{4},"_P1",1)
--shift_to(0,4,0)
print_code(0)
tile(0,4,4)
scalar_expand(0,{4},"RHS",1, 64)

cudaize(0,"spmv_GPU",{ a=NNZ,x=N,y=N,col=NNZ,index=NNZ},{block={"ii"}, thread={"j", "i"}},{})

reduce(0,{4},0,"segreduce_warp",{})


