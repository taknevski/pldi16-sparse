#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
__global__ void spmv_GPU(float *temp,float *y,float *A,int *NNZ_j,float *x,int *col,int *NNZ_i);
#define NONZEROS 100
#define NUMROWS 10

void spmv(int *NNZ_i,int *NNZ_j,int *col,float *temp,float *A,float *x,float *y,int count)
{
  int *devI5Ptr;
  int *devI4Ptr;
  float *devI3Ptr;
  int *devI2Ptr;
  float *devI1Ptr;
  float *devO2Ptr;
  float *devO1Ptr;
  int t6;
  int t4;
  int t8;
  int t10;
  int t2;
  int _t5;
  int _t1;
  int k;
  int i;
  cudaMalloc(((void **)(&devO1Ptr)),100 * sizeof(float ));
  cudaMalloc(((void **)(&devO2Ptr)),10 * sizeof(float ));
  cudaMemcpy(devO2Ptr,y,10 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),100 * sizeof(float ));
  cudaMemcpy(devI1Ptr,A,100 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),100 * sizeof(int ));
  cudaMemcpy(devI2Ptr,NNZ_j,100 * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),10 * sizeof(float ));
  cudaMemcpy(devI3Ptr,x,10 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI4Ptr)),100 * sizeof(int ));
  cudaMemcpy(devI4Ptr,col,100 * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI5Ptr)),100 * sizeof(int ));
  cudaMemcpy(devI5Ptr,NNZ_i,100 * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid = dim3(4,1);
  dim3 dimBlock = dim3(32,4);
  spmv_GPU<<<dimGrid,dimBlock>>>(devO1Ptr,devO2Ptr,devI1Ptr,devI2Ptr,devI3Ptr,devI4Ptr,devI5Ptr);
  cudaMemcpy(temp,devO1Ptr,100 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaMemcpy(y,devO2Ptr,10 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO2Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  cudaFree(devI4Ptr);
  cudaFree(devI5Ptr);
  for (t4 = 0; t4 <= 3; t4 += 1) 
    for (t6 = 0; t6 <= 3; t6 += 1) 
      for (t8 = 1024 * t4 + 256 * t6; t8 <= 1024 * t4 + 256 * t6 + 224; t8 += 32) 
        y[NNZ_i[t8 + 31]] += temp[t8 + 31];
}

int main()
{
  int NNZ_i[100UL];
  int NNZ_j[100UL];
  int col[100UL];
  float A[100UL];
  float x[10UL];
  float y[10UL];
  float temp[100UL];
  spmv(NNZ_i,NNZ_j,col,temp,A,x,y,100);
  return 0;
}

__global__ void spmv_GPU(float *temp,float *y,float *A,int *NNZ_j,float *x,int *col,int *NNZ_i)
{
  int bbbb;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  int t10;
  int t8;
  int t4;
  int t6;
  for (bbbb = 1024 * bx + 256 * ty; bbbb <= 1024 * bx + 256 * ty + 224; bbbb += 32) {
    temp[-(256 * ty) - 1024 * bx + bbbb + tx + 1024 * bx + 256 * ty] = (A[NNZ_j[-(256 * ty) - 1024 * bx + bbbb + tx + 1024 * bx + 256 * ty]] * x[col[NNZ_j[-(256 * ty) - 1024 * bx + bbbb + tx + 1024 * bx + 256 * ty]]]);
    y[NNZ_i[-(256 * ty) - 1024 * bx + bbbb + tx + 1024 * bx + 256 * ty]] += temp[-(256 * ty) - 1024 * bx + bbbb + tx + 1024 * bx + 256 * ty];
  }
}
