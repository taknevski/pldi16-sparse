#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA1_(_t49) _P_DATA1[_t49]
#define _P_DATA1_(_t64) _P_DATA1[_t64]
#define _P_DATA1_(_t70) _P_DATA1[_t70]
#define _P_DATA1__(_t49) _P_DATA1[_t49 + 1]
#define _P_DATA1__(_t64) _P_DATA1[_t64 + 1]
#define _P_DATA1__(_t70) _P_DATA1[_t70 + 1]
#define col_(_t53) col[_t53]
#define col_(j) col[j]
#define index_(i) index[i]
#define index__(i) index[i + 1]
#define new_ratings(_t49,_t50,_t51,_t52) new_ratings[_t50][_t51][_t52]
__global__ void sgd_blk_diag_operator(float step_size,float (*fv)[16UL],int *_P_DATA2,float (*new_ratings)[2][2],int *_P_DATA1,int t2);
#define NZ 1666
#define NUMROWS 494
#define N 1000
#define SGD_LAMBDA 0.05f
#define SGD_FEATURE_SIZE 16

struct a_list 
{
  int col_[1];
  float ratings[2][2];
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void sgd_kernel_global_simd(int n,int *index,float *ratings,float *y,float *x,int *col,float (*fv)[16UL],float step_size)
{
  int *devI3Ptr;
  float *devI2Ptr;
  int *devI1Ptr;
  float *devO1Ptr;
  int t12;
  int t10;
  int t8;
  int t6;
  int t4;
  int _t172;
  int _t171;
  int _t170;
  int _t169;
  int _t168;
  int _t167;
  int _t166;
  int _t165;
  int _t164;
  int _t163;
  int _t162;
  int _t161;
  int _t160;
  int _t159;
  int _t158;
  int _t157;
  int _t156;
  int _t155;
  int _t154;
  int _t153;
  int _t152;
  int _t151;
  int _t149;
  float _P5[2];
  int _t148;
  int _t147;
  int _t146;
  int _t145;
  int _t144;
  int _t143;
  int _t141;
  float _P4[2];
  int _t140;
  int _t139;
  int _t138;
  int _t137;
  int _t136;
  int _t135;
  int newVariable5;
  int _t133;
  int _t132;
  int _t131;
  int _t130;
  int _t129;
  int _t128;
  int _t127;
  int _t126;
  int _t125;
  int _t124;
  int _t123;
  int _t122;
  int t2;
  float _P3[2 * 16];
  int _t110;
  int _t109;
  int _t108;
  int _t107;
  int _t106;
  int _t105;
  float _P2[2];
  int _t95;
  int _t94;
  int _t93;
  int _t92;
  int _t91;
  int _t90;
  int _t86;
  int _t85;
  int _t84;
  int _t83;
  int _t81;
  int _t80;
  int _t79;
  int _t78;
  int _t77;
  int _t76;
  int _t75;
  int _t74;
  int _t73;
  int _t72;
  int _t71;
  int _t70;
  int _t69;
  int _t68;
  int _t67;
  int _t66;
  int _t65;
  int _t64;
  int _t63;
  int _t62;
  int _t61;
  int _t60;
  int _t59;
  int _t58;
  int newVariable4;
  int newVariable3;
  int newVariable2;
  int In_3;
  int In_2;
  int In_1;
  int _t57;
  int _t56;
  int _t55;
  struct a_list *_P_DATA4;
  int newVariable1;
  int newVariable0;
  struct mk *_P_DATA3;
  float (*new_ratings)[2][2];
  struct a_list *_P1[77999];
  int *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t54;
  int _t53;
  int _t52;
  int _t51;
  int _t50;
  int _t49;
  int _t48;
  int _t47;
  int _t46;
  int _t45;
  int _t44;
  int _t43;
  int _t42;
  int _t41;
  int _t40;
  int _t39;
  int _t38;
  int _t37;
  int _t36;
  int _t35;
  int _t34;
  int _t33;
  int _t32;
  int _t31;
  int _t30;
  int _t29;
  int _t28;
  int _t27;
  int _t25;
  int _t26;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  int _t18;
  int _t17;
  int _t16;
  int _t15;
  int _t14;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t7;
  int _t6;
  int _t5;
  int _t4;
  int l;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  int k;
  float err;
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * 78000)));
  _P_DATA1[0] = 0;
  _P_DATA3 = ((struct mk *)(malloc(sizeof(struct mk ) * 77999)));
  chill_count_1 = 0;
  _P_DATA1[0] = 0;
  for (_t49 = 0; _t49 <= 77998; _t49 += 1) {
    _P1[1 * _t49] = 0;
    _P_DATA1[1 * _t49 + 1] = 0;
  }
  for (t2 = 0; t2 <= 23320; t2 += 1) {
    for (t4 = 0; t4 <= 1; t4 += 1) 
      for (t6 = index_(2 * t2 + t4); t6 <= index__(2 * t2 + t4) - 1; t6 += 1) {
        _t49 = (col_(t6) - -1) / 2 - t2;
        _P_DATA3[_t49].ptr = 0;
      }
    for (t4 = 0; t4 <= 1; t4 += 1) 
      for (t6 = index_(2 * t2 + t4); t6 <= index__(2 * t2 + t4) - 1; t6 += 1) {
        _t49 = (col_(t6) - -1) / 2 - t2;
        _t52 = (col_(t6) - -1) % 2;
        if (_P_DATA3[_t49].ptr == 0) {
          _P_DATA4 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
          _P_DATA4 -> next = _P1[_t49];
          _P1[_t49] = _P_DATA4;
          _P_DATA3[_t49].ptr = _P1[_t49];
          for (newVariable0 = 0; newVariable0 <= 1; newVariable0 += 1) 
            for (newVariable1 = 0; newVariable1 <= 1; newVariable1 += 1) 
              _P_DATA3[_t49].ptr -> ratings[newVariable0][newVariable1] = -1;
          _P_DATA3[_t49].ptr -> col_[0] = t2;
          chill_count_1 += 1;
          _P_DATA1[_t49 + 1] += 1;
        }
        _P_DATA3[_t49].ptr -> ratings[t4][_t52] = ratings[t6];
      }
  }
{
    _P_DATA2 = ((int *)(malloc(sizeof(int ) * chill_count_1)));
    new_ratings = ((float (*)[2][2])(malloc(sizeof(float [2][2]) * chill_count_1)));
  }
  for (t2 = 0; t2 <= 77998; t2 += 1) {
    for (newVariable2 = 1 - _P_DATA1[1 * t2 + 1]; newVariable2 <= 0; newVariable2 += 1) {
      _P_DATA2[_P_DATA1[1 * t2] - newVariable2] = _P1[1 * t2] -> col_[0];
      for (newVariable3 = 0; newVariable3 <= 1; newVariable3 += 1) 
        for (newVariable4 = 0; newVariable4 <= 1; newVariable4 += 1) 
          new_ratings[_P_DATA1[1 * t2] - newVariable2][newVariable3][newVariable4] = _P1[1 * t2] -> ratings[newVariable3][newVariable4];
      _P_DATA4 = _P1[1 * t2] -> next;
      free(_P1[1 * t2]);
      _P1[1 * t2] = _P_DATA4;
    }
    _P_DATA1[1 * t2 + 1] += _P_DATA1[1 * t2];
  }
  cudaMalloc(((void **)(&devO1Ptr)),16000 * sizeof(float ));
  cudaMemcpy(devO1Ptr,fv,16000 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),(chill_count_1 + 1) * sizeof(int ));
  cudaMemcpy(devI1Ptr,_P_DATA2,(chill_count_1 + 1) * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),chill_count_1 * 2 * 2 * sizeof(float ));
  cudaMemcpy(devI2Ptr,new_ratings,chill_count_1 * 2 * 2 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),(77999 + 1) * sizeof(int ));
  cudaMemcpy(devI3Ptr,_P_DATA1,(77999 + 1) * sizeof(int ),cudaMemcpyHostToDevice);
  for (t2 = 0; t2 <= 77998; t2 += 1) {
    dim3 dimGrid3 = dim3((_P_DATA1__(t2) - _P_DATA1_(t2) - 1) / 2 + 1,1);
    dim3 dimBlock3 = dim3(16,2);
    sgd_blk_diag_operator<<<dimGrid3,dimBlock3>>>(step_size,((float (*)[16UL])devO1Ptr),devI1Ptr,((float (*)[2][2])devI2Ptr),devI3Ptr,t2);
  }
  cudaMemcpy(fv,devO1Ptr,16000 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  free(_P_DATA1);
  free(_P_DATA2);
  free(new_ratings);
  free(_P_DATA3);
}

int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[495UL];
  int col[1666UL];
  float (*fv)[16UL];
  float step_size = 0.09f;
  float err;;
  sgd_kernel_global_simd(n,index,a,y,x,col,fv,step_size);
  return 0;
}

__global__ void sgd_blk_diag_operator(float step_size,float (*fv)[16UL],int *_P_DATA2,float (*new_ratings)[2][2],int *_P_DATA1,int t2)
{
  int l;
  int i;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  __device__ __shared__ float _P2[2];
  __device__ __shared__ float _P3[2 * 16];
  int newVariable5;
  float _P4[2];
  float _P5[2];
  int t4;
  int t6;
  int t8;
  int t10;
  int t12;
  if (ty <= _P_DATA1__(t2) - _P_DATA1_(t2) - 2 * bx - 1) 
    newVariable5 = _P_DATA2[ty + 2 * bx + _P_DATA1_(t2)];
  if (ty <= _P_DATA1__(t2) - _P_DATA1_(t2) - 2 * bx - 1) {
    for (t10 = 0; t10 <= 1; t10 += 1) 
      _P5[2 * newVariable5 + t10 - 2 * newVariable5] = fv[2 * newVariable5 + t10][tx];
    for (i = 0; i <= 1; i += 1) {
      for (t12 = 0; t12 <= 1; t12 += 1) 
        _P4[2 * newVariable5 + 2 * t2 + t12 + 1 - (2 * t2 + 2 * newVariable5 + 1)] = fv[2 * newVariable5 + 2 * t2 + t12 + 1][tx];
      for (l = 0; l <= 1; l += 1) 
        if (0 <= new_ratings(t2,2 * bx + _P_DATA1_(t2) + ty,i,l)) {
          _P2[ty] = -new_ratings[2 * bx + _P_DATA1_(t2) + ty][i][l];
          _P3[tx + ty * 16] = (_P5[2 * newVariable5 + i - 2 * newVariable5] * _P4[2 * (t2 - 23320) + 2 * newVariable5 + l + 46641 - (2 * t2 + 2 * newVariable5 + 1)]);
          segreduce_warp(&_P2[ty],&_P3[0 + ty * 16]);
          _P5[2 * newVariable5 + i - 2 * newVariable5] -= (step_size * ((_P2[ty] * _P4[2 * (t2 - 23320) + 2 * newVariable5 + l + 46641 - (2 * t2 + 2 * newVariable5 + 1)]) + (0.05f * _P5[2 * newVariable5 + i - 2 * newVariable5])));
          _P4[2 * (t2 - 23320) + 2 * newVariable5 + l + 46641 - (2 * t2 + 2 * newVariable5 + 1)] -= (step_size * ((_P2[ty] * _P5[2 * newVariable5 + i - 2 * newVariable5]) + (0.05f * _P4[2 * (t2 - 23320) + 2 * newVariable5 + l + 46641 - (2 * t2 + 2 * newVariable5 + 1)])));
        }
        else if (new_ratings(t2,2 * bx + _P_DATA1_(t2) + ty,i,l) <= -2) {
          _P2[ty] = -new_ratings[2 * bx + _P_DATA1_(t2) + ty][i][l];
          _P3[tx + ty * 16] = (_P5[2 * newVariable5 + i - 2 * newVariable5] * _P4[2 * (t2 - 23320) + 2 * newVariable5 + l + 46641 - (2 * t2 + 2 * newVariable5 + 1)]);
          segreduce_warp(&_P2[ty],&_P3[0 + ty * 16]);
          _P5[2 * newVariable5 + i - 2 * newVariable5] -= (step_size * ((_P2[ty] * _P4[2 * (t2 - 23320) + 2 * newVariable5 + l + 46641 - (2 * t2 + 2 * newVariable5 + 1)]) + (0.05f * _P5[2 * newVariable5 + i - 2 * newVariable5])));
          _P4[2 * (t2 - 23320) + 2 * newVariable5 + l + 46641 - (2 * t2 + 2 * newVariable5 + 1)] -= (step_size * ((_P2[ty] * _P5[2 * newVariable5 + i - 2 * newVariable5]) + (0.05f * _P4[2 * (t2 - 23320) + 2 * newVariable5 + l + 46641 - (2 * t2 + 2 * newVariable5 + 1)])));
        }
      for (t12 = 0; t12 <= 1; t12 += 1) 
        fv[2 * newVariable5 + 2 * t2 + t12 + 1][tx] = _P4[2 * newVariable5 + 2 * t2 + t12 + 1 - (2 * t2 + 2 * newVariable5 + 1)];
    }
    for (t10 = 0; t10 <= 1; t10 += 1) 
      fv[2 * newVariable5 + t10][tx] = _P5[2 * newVariable5 + t10 - 2 * newVariable5];
  }
}
