init("sgd_blk_diag.c", "sgd_kernel_global_simd", 0)
dofile("cudaize.lua")

make_dense(0,2,"l")

--known("lb",0)
--known("ub",1499)
--known("n", 1000)

known("lb",46641)
known("ub",155997)
known("n", 46641)
distribute({0,1,2}, 4)
tile_by_index(0,{"l"},{2},{l1_control="ll"},{"ll","i","l","j","k"})
tile_by_index(0,{"i"},{2},{l1_control="ii"},{"ii","ll","i","l","j", "k"})

shift_to(0,3,0) 
shift_to(0,4,0) 


skew({0,1,2},2,{-1,1})

permute(0,{"ll", "ii","i","l","j", "k"})
shift_to(0,1,0)

compact(0,{2},{"new_ratings"}, -1, {"ratings"})


N = 1000
M = 1000000
print_code(1)


distribute_by_index({0,1,2}, "k")
tile_by_index(1,{"ii"},{2},{l1_control="by_diag"},{"ll","by_diag", "ii","i","l","k"})
--shift_to(0,3,0);
shift_to(1,3,0)
scalar_expand_by_index(0,{"ii"},"err",1, 0, 1)
scalar_expand_by_index(1,{"ii","k"},"RHS",1, 0, 1)


cudaize(1,"sgd_blk_diag_operator",{fv=N},{block={"by_diag"}, thread={"k","ii"}}, {"new_ratings", "_P_DATA2", "step_size", "_P_DATA1"})

reduce_by_index(1,{"tx"},"segreduce_warp",{},{})

datacopy_privatized(2, "ty", "_P_DATA2", {"ty", "tx"}, true, -1, 1,1, false)
datacopy_privatized_by_ref(2, "l", {{2,3},{2,5},{2,9}, {7,2}}, {"tx"},true,-1,1,1, false)
datacopy_privatized_by_ref(2, "i", {{7,1}, {2,0},{2,4},{2,8}}, {"tx"}, true, -1, 1,1,  false)
--shift_to(9,6,0)
--shift_to(10,5,0)
shift_to(9,6,0)
shift_to(10,6,0)
shift_to(11,5,0)
shift_to(12,5,0)
known({"index_", "index__"}, {-1,1}, -1, 0)



print_code(1)
