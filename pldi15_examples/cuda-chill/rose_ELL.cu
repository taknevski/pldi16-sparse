#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
__global__ void spmv_ell_GPU(int m,int n,float *y,float *_P_DATA4,float *x,int *_P_DATA5);

void spmv(int n,int index[],float *a,float *y,float *x,int *col)
{
  int *devI3Ptr;
  float *devI2Ptr;
  float *devI1Ptr;
  float *devO1Ptr;
  int t8;
  int t6;
  int t4;
  float newVariable1;
  int _t44;
  int _t46;
  int _t45;
  int _t43;
  int _t40;
  int _t39;
  int t2;
  int _t33;
  int _t34;
  int _t32;
  int _t31;
  int _t30;
  int *_P_DATA5;
  int _t29;
  int _t28;
  float *_P_DATA4;
  int _t23;
  int _t24;
  int _t19;
  int _t18;
  int _t17;
  int newVariable0;
  int *_P_DATA3;
  int *col_prime;
  float *a_prime;
  unsigned short *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t16;
  int _t15;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t5;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  int m = 0;

   for(i = 0; i < n; i++)
      if(index[i+1] - index[i] > m)
         m = index[i+1] - index[i];




  a_prime = ((float *)(malloc(sizeof(float ) * ((n - 1 - 0 + 1) * m))));
  col_prime = ((int *)(malloc(sizeof(int ) * ((n - 1 - 0 + 1) * m))));
  _P_DATA3 = ((int *)(malloc(sizeof(int ) * 1)));
  chill_count_1 = 0;
  _P_DATA2 = ((unsigned short *)(malloc(sizeof(unsigned short ) * ((n - 1 - 0 + 1) * 1))));
  _P_DATA4 = ((float *)(malloc(sizeof(float ) * (m * (n - 1 + 1)))));
  _P_DATA5 = ((int *)(malloc(sizeof(int ) * (m * (n - 1 + 1)))));
  for (t2 = 0; t2 <= n - 1; t2 += 1) {
    for (t4 = 0; t4 <= (index__(t2) - index_(t2) - 1) / m; t4 += 1) 
      for (t6 = 0; t6 <= __rose_lt(m-1,index__(t2) - index_(t2) - m * t4 - 1); t6 += 1) 
        _P_DATA3[t4] = -1;
    for (t4 = 0; t4 <= (index__(t2) - index_(t2) - 1) / m; t4 += 1) 
      for (t6 = 0; t6 <= __rose_lt(m-1,index__(t2) - index_(t2) - m * t4 - 1); t6 += 1) {
        if (_P_DATA3[0] == -1) {
          _P_DATA3[t4] = chill_count_1;
          for (newVariable0 = 0; newVariable0 <= m-1; newVariable0 += 1) {
            a_prime[_P_DATA3[t4] * m + 1 * newVariable0] = 0;
            col_prime[_P_DATA3[t4] * m + 1 * newVariable0] = 0;
          }
          _P_DATA2[_P_DATA3[t4]] = t4;
          chill_count_1 += 1;
        }
        a_prime[_P_DATA3[t4] * m + 1 * t6] = a[m * t4 + t6 + index_(t2)];
        col_prime[_P_DATA3[t4] * m + 1 * t6] = col[m * t4 + t6 + index_(t2)];
      }
  }
  for (t2 = 0; t2 <= m-1; t2 += 1) 
    for (t4 = 0; t4 <= n - 1; t4 += 1) {
      _P_DATA4[t4 + t2 * (n - 1 + 1)] = a_prime[t4 *m + 1 * t2];
      _P_DATA5[t4 + t2 * (n - 1 + 1)] = col_prime[t4 * m + 1 * t2];
    }
  cudaMalloc(((void **)(&devO1Ptr)),n * sizeof(float ));
  cudaMemcpy(devO1Ptr,y,n * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),n*m * sizeof(float ));
  cudaMemcpy(devI1Ptr,_P_DATA4,n*m * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),n * sizeof(float ));
  cudaMemcpy(devI2Ptr,x,n * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),m * (n - 1 + 1) * sizeof(int ));
  cudaMemcpy(devI3Ptr,_P_DATA5,m * (n - 1 + 1) * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid2 = dim3((n - 1) / 1024 + 1,1);
  dim3 dimBlock2 = dim3(1024,1);
  spmv_ell_GPU<<<dimGrid2,dimBlock2>>>(n,devO1Ptr,devI1Ptr,devI2Ptr,devI3Ptr);
  cudaMemcpy(y,devO1Ptr,n * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  free(_P_DATA2);
  free(a_prime);
  free(col_prime);
  free(_P_DATA3);
}

int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[494 + 1];
  int col[1666UL];
  spmv(n,index,a,y,x,col);
  return 0;
}

__global__ void spmv_ell_GPU(int m,int n,float *y,float *_P_DATA4,float *x,int *_P_DATA5)
{
  int j;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int t2;
  float newVariable1;
  int t4;
  int t8;
  if (tx <= n - 1024 * bx - 1) {
    newVariable1 = y[1024 * bx + tx];
    for (j = 0; j <= m-1; j += 1) 
      newVariable1 += (_P_DATA4[1024 * bx + tx + j * (n - 1 + 1)] * x[_P_DATA5[1024 * bx + tx + j * (n - 1 + 1)]]);
    y[1024 * bx + tx] = newVariable1;
  }
}
