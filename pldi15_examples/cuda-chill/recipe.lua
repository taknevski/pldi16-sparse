init("_orio_chill_.c","*u,",0)
dofile("cudaize.lua")

N = 10
nelt = 100
distribute({0,1,2},1)
cudaize(0,"tensor1_GPU",{u=nelt*N*N*N,ur=nelt*N*N*N,us=nelt*N*N*N,ut=nelt*N*N*N,D=N*N,Dt=N*N},{block={"e"}, thread={"i","j"}},{})CU=3
print_code(0)
copy_to_registers(0,"k","ur")
--cudaize(1,"tensor2_GPU",{u=nelt*N*N*N,ur=nelt*N*N*N,us=nelt*N*N*N,ut=nelt*N*N*N,D=N*N,Dt=N*N},{block={"e","j"}, thread={"k","i"}},{})CU=4
--copy_to_registers(1,"m","us")
--cudaize(2,"tensor3_GPU",{u=nelt*N*N*N,ur=nelt*N*N*N,us=nelt*N*N*N,ut=nelt*N*N*N,D=N*N,Dt=N*N},{block={"e"}, thread={"i","j"}},{})CU=5
--copy_to_registers(2,"m","ut")


