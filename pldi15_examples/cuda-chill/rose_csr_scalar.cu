#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
__global__ void spmv_GPU(float *y,float *a,float *x,int *col);
#define NZ 1666
#define NUMROWS 494

void spmv(int n,int index[494UL],float a[1666UL],float y[494UL],float x[494UL],int col[1666UL])
{
  int *devI3Ptr;
  float *devI2Ptr;
  float *devI1Ptr;
  float *devO1Ptr;
  int t8;
  int t6;
  int t4;
  float newVariable0;
  int _t9;
  int _t6;
  int _t7;
  int t2;
  int i;
  int j;
  cudaMalloc(((void **)(&devO1Ptr)),4096 * sizeof(float ));
  cudaMemcpy(devO1Ptr,y,4096 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),5120 * sizeof(float ));
  cudaMemcpy(devI1Ptr,a,5120 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),4096 * sizeof(float ));
  cudaMemcpy(devI2Ptr,x,4096 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),5120 * sizeof(int ));
  cudaMemcpy(devI3Ptr,col,5120 * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid0 = dim3((n - 1) / 1024 + 1,1);
  dim3 dimBlock0 = dim3(1024,1);
  spmv_GPU<<<dimGrid0,dimBlock0>>>(devO1Ptr,devI1Ptr,devI2Ptr,devI3Ptr);
  cudaMemcpy(y,devO1Ptr,4096 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
}

int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[494 + 1];
  int col[1666UL];
  spmv(n,index,a,y,x,col);
  return 0;
}

__global__ void spmv_GPU(float *y,float *a,float *x,int *col)
{
  int j;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int n;
  int t2;
  float newVariable0;
  int t4;
  int t8;
  if (tx <= n - 1024 * bx - 1) {
    newVariable0 = y[1024 * bx + tx];
    for (j = index_(tx + 1024 * bx); j <= index__(tx + 1024 * bx) - 1; j += 1) 
      newVariable0 += (a[j] * x[col[j]]);
    y[1024 * bx + tx] = newVariable0;
  }
}
