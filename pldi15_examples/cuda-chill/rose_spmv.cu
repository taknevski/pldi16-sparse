#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
__global__ void spmv_final_level_GPU(int *c_count,float *y,int *c_i,float *a,float *x,int *col);
__global__ void spmv_second_level_GPU(int *c_count,float *y,int *_P_DATA1,float *_P_DATA2);
__global__ void spmv_GPU(int *c_count,float *y,int *_P_DATA1,float *_P_DATA2,int *c_i,float *a,float *x,int *col);
void spmv_inspector(int n,struct inspector &c,int *index);
#define NZ 1666
#define NUMROWS 494

struct inspector 
{
  int *j;
  int *i;
  int count;
}
;

void spmv(int n,int index[494UL],float a[1666UL],float y[494UL],float x[494UL],int col[1666UL])
{
  int t14;
  int *devI4Ptr;
  float *devI3Ptr;
  float *devI2Ptr;
  int *devI1Ptr;
  float *devO3Ptr;
  int *devO2Ptr;
  float *devO1Ptr;
  int *c_count;
  int t12;
  int _t171;
  int _t170;
  int _t169;
  int _t168;
  int _t167;
  int _t166;
  int _t165;
  int _t164;
  int _t163;
  int _t162;
  int _t161;
  int _t160;
  int _t159;
  int _t158;
  int _t157;
  int _t156;
  int _t155;
  int _t154;
  int _t153;
  int _t152;
  int _t151;
  int _t150;
  int _t149;
  int _t148;
  float _P6[512];
  int _t121;
  int _t120;
  int _t119;
  int _t118;
  int _t117;
  int _P5[512];
  int _t111;
  int _t112;
  int _t109;
  int _t108;
  int t4;
  int t2;
  float _P4[256 * 4];
  int _t103;
  int _t102;
  int _t101;
  int _t100;
  int _t99;
  int _t98;
  int _t97;
  int _P3[256 * 4];
  int _t96;
  int _t95;
  int _t94;
  int _t93;
  int _t92;
  int _t91;
  int _t90;
  int _t89;
  int _t88;
  int _t87;
  int _t86;
  int _t85;
  int _t84;
  int _t83;
  int _t82;
  int _t81;
  int _t80;
  int _t79;
  int _t78;
  int _t77;
  int _t76;
  int _t75;
  int _t74;
  int _t73;
  int _t72;
  int _t71;
  int _t70;
  int _t69;
  int _t68;
  int _t67;
  int _t66;
  int _t65;
  float *_P_DATA2;
  int _t64;
  int _t63;
  int _t62;
  int _t61;
  int _t60;
  int _t59;
  int *_P_DATA1;
  int _t58;
  int _t57;
  int _t56;
  int _t55;
  int _t54;
  int _t53;
  int _t52;
  int _t51;
  int _t50;
  int _t49;
  int _t48;
  int _t47;
  int _t46;
  int _t45;
  int _t44;
  int _t43;
  int _t42;
  int _t41;
  int _t40;
  int _t39;
  int _t38;
  int _t37;
  int _t36;
  int _t35;
  float _P2[4 * 64];
  int _t34;
  int _t33;
  int _t32;
  int _t31;
  int _t30;
  int _t29;
  int _P1[4 * 64];
  int _t28;
  int _t27;
  int _t26;
  int _t25;
  int _t24;
  int _t23;
  int _t22;
  int _t20;
  int _t19;
  int _t21;
  int _t18;
  int _t17;
  int t10;
  int t8;
  int t6;
  int _t6;
  int _t5;
  int _t4;
  int _t3;
  int c_inv;
  int coalesced_index;
  int *c_i;
  int *c_j;
  struct inspector c;
  int i;
  int j;
  c.inspector::count = 0;
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * (((c(_t54,_t56) - 1024) / 1024 + 1) * 4))));
  _P_DATA2 = ((float *)(malloc(sizeof(float ) * (((c(_t60,_t62) - 1024) / 1024 + 1) * 4))));
  spmv_inspector(n,c,index);
  c_count = &c.inspector::count;
  cudaMalloc(((void **)(&devO1Ptr)),494 * sizeof(float ));
  cudaMemcpy(devO1Ptr,y,494 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devO2Ptr)),((c(_t54,_t56) - 1024) / 1024 + 1) * 4 * sizeof(int ));
  cudaMalloc(((void **)(&devO3Ptr)),((c(_t60,_t62) - 1024) / 1024 + 1) * 4 * sizeof(float ));
  cudaMalloc(((void **)(&devI1Ptr)),1666 * sizeof(int ));
  cudaMemcpy(devI1Ptr,c.inspector::i,1666 * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),1666 * sizeof(float ));
  cudaMemcpy(devI2Ptr,a,1666 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),494 * sizeof(float ));
  cudaMemcpy(devI3Ptr,x,494 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI4Ptr)),1666 * sizeof(int ));
  cudaMemcpy(devI4Ptr,col,1666 * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid1 = dim3((c(t2,t4) - 1024) / 1024 + 1,1);
  dim3 dimBlock1 = dim3(32,4);
  cudaMemcpy(c_count,&c.inspector::count,sizeof(int ),cudaMemcpyHostToDevice);
  spmv_GPU<<<dimGrid1,dimBlock1>>>(c_count,devO1Ptr,devO2Ptr,devO3Ptr,devI1Ptr,devI2Ptr,devI3Ptr,devI4Ptr);
  cudaMemcpy(y,devO1Ptr,494 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaMemcpy(_P_DATA1,devO2Ptr,((c(_t54,_t56) - 1024) / 1024 + 1) * 4 * sizeof(int ),cudaMemcpyDeviceToHost);
  cudaFree(devO2Ptr);
  cudaMemcpy(_P_DATA2,devO3Ptr,((c(_t60,_t62) - 1024) / 1024 + 1) * 4 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO3Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  cudaFree(devI4Ptr);
  cudaFree(c_count);
  c_count = &c.inspector::count;
  cudaMalloc(((void **)(&devO1Ptr)),494 * sizeof(float ));
  cudaMemcpy(devO1Ptr,y,494 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),((c(_t54,_t56) - 1024) / 1024 + 1) * 4 * sizeof(int ));
  cudaMemcpy(devI1Ptr,_P_DATA1,((c(_t54,_t56) - 1024) / 1024 + 1) * 4 * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),((c(_t60,_t62) - 1024) / 1024 + 1) * 4 * sizeof(float ));
  cudaMemcpy(devI2Ptr,_P_DATA2,((c(_t60,_t62) - 1024) / 1024 + 1) * 4 * sizeof(float ),cudaMemcpyHostToDevice);
  dim3 dimGrid2 = dim3(1,1);
  dim3 dimBlock2 = dim3(4,256);
  cudaMemcpy(c_count,&c.inspector::count,sizeof(int ),cudaMemcpyHostToDevice);
  spmv_second_level_GPU<<<dimGrid2,dimBlock2>>>(c_count,devO1Ptr,devI1Ptr,devI2Ptr);
  cudaMemcpy(y,devO1Ptr,494 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(c_count);
  c_count = &c.inspector::count;
  cudaMalloc(((void **)(&devO1Ptr)),494 * sizeof(float ));
  cudaMemcpy(devO1Ptr,y,494 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),1666 * sizeof(int ));
  cudaMemcpy(devI1Ptr,c.inspector::i,1666 * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),1666 * sizeof(float ));
  cudaMemcpy(devI2Ptr,a,1666 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),494 * sizeof(float ));
  cudaMemcpy(devI3Ptr,x,494 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI4Ptr)),1666 * sizeof(int ));
  cudaMemcpy(devI4Ptr,col,1666 * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid3 = dim3(1,1);
  dim3 dimBlock3 = dim3(512,1);
  cudaMemcpy(c_count,&c.inspector::count,sizeof(int ),cudaMemcpyHostToDevice);
  spmv_final_level_GPU<<<dimGrid3,dimBlock3>>>(c_count,devO1Ptr,devI1Ptr,devI2Ptr,devI3Ptr,devI4Ptr);
  cudaMemcpy(y,devO1Ptr,494 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  cudaFree(devI4Ptr);
  cudaFree(c_count);
}

int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[494 + 1];
  int col[1666UL];
  spmv(n,index,a,y,x,col);
  return 0;
}

void spmv_inspector(int n,struct inspector &c,int *index)
{
  int t2;
  int t4;
  for (t2 = 0; t2 <= n - 1; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
      c.inspector::j[c.inspector::count] = t4;
      c.inspector::i[c.inspector::count] = t2;
      c.inspector::count = c.inspector::count + 1;
    }
}

__global__ void spmv_GPU(int *c_count,float *y,int *_P_DATA1,float *_P_DATA2,int *c_i,float *a,float *x,int *col)
{
  int by_warp;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  struct inspector c;
  int t6;
  int t8;
  int t10;
  __device__ __shared__ int _P1[4 * 64];
  __device__ __shared__ float _P2[4 * 64];
  int t2;
  int t4;
  int t12;
  for (by_warp = 0; by_warp <= 6; by_warp += 1) {
    _P1[tx + ty * 64] = c_i[32 * by_warp + 1024 * bx + 256 * ty + tx];
    _P2[tx + ty * 64] = (a[32 * by_warp + 1024 * bx + 256 * ty + tx] * x[col[32 * by_warp + 1024 * bx + 256 * ty + tx]]);
    segreduce_warp(&y[0],&_P1[0 + ty * 64],&_P2[0 + ty * 64],by_warp);
  }
  _P1[tx + ty * 64] = c_i[32 * 7 + 1024 * bx + 256 * ty + tx];
  _P2[tx + ty * 64] = (a[32 * 7 + 1024 * bx + 256 * ty + tx] * x[col[32 * 7 + 1024 * bx + 256 * ty + tx]]);
  segreduce_warp(&y[0],&_P1[0 + ty * 64],&_P2[0 + ty * 64]);
  _P_DATA1[ty + bx * 4] = _P1[31 + ty * 64];
  _P_DATA2[ty + bx * 4] = _P2[31 + ty * 64];
}

__global__ void spmv_second_level_GPU(int *c_count,float *y,int *_P_DATA1,float *_P_DATA2)
{
  int k;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  int t6;
  int t8;
  int t10;
  __device__ __shared__ int _P3[256 * 4];
  __device__ __shared__ float _P4[256 * 4];
  int t2;
  int t4;
  for (k = 0; k <= (c(t2,t4) - 1024) / 262144; k += 1) 
    if (ty <= (c(t2,t4) - 262144 * k - 1024) / 1024) {
      _P3[tx + ty * 4] = _P_DATA1[tx + (256 * k + ty) * 4];
      _P4[tx + ty * 4] = _P_DATA2[tx + (256 * k + ty) * 4];
      segreduce_block(&y[0],&_P3[0 + 0 * 4],&_P4[0 + 0 * 4],k,__rose_lt(255,(-(262144 * k) + c(1,0) - 1024) / 1024));
    }
}

__global__ void spmv_final_level_GPU(int *c_count,float *y,int *c_i,float *a,float *x,int *col)
{
  int block;
  int tx;
  tx = threadIdx.x;
  struct inspector c;
  int t6;
  int t8;
  int t2;
  int t4;
  __device__ __shared__ int _P5[512];
  __device__ __shared__ float _P6[512];
  for (block = 1024 * (c(t2,t4) / 1024); block <= c(t2,t4) - 1; block += 512) 
    if (tx <= c(t2,t4) - block - 1) {
      _P5[tx] = c_i[tx + block];
      _P6[tx] = (a[tx + block] * x[col[tx + block]]);
      segreduce_block2(&y[0],&_P5[0],&_P6[0],block,__rose_lt(c(1,0) - block - 1,511));
    }
}
