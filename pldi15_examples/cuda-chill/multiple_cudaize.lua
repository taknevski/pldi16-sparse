init("multiple_cudaize.c", "test", 0)
dofile("cudaize.lua")
NNZ=1666
N=494
Ti = 128
Tj = 128
Tk = 128

tile_by_index(0,{"i","j"},{Ti,Tj},{l1_control="ii",l2_control="jj"},{"ii","jj","i","j","k"})
--tile_by_index(0,{"k"},{Tk},{l1_control="kk"},{"ii","jj","kk","i","j","k"})CU=3

print_code(0)
cudaize(0,"test_GPU",{},{block={"ii","jj"}, thread={"i","j"}},{})
--cudaize(0,"first_kernel",{},{block={"block"}, thread={"coalesced_index", "warp"}},{"_P_DATA1", "_P_DATA2"})
--cudaize(1,"second_kernel",{},{block={}, thread={"warp","block"}}, {"_P_DATA1", "_P_DATA2"})
--cudaize(2,"third_kernek",{ a=NNZ,x=N,y=N,col=NNZ, temp=NNZ, c_j=NNZ, c_i=NNZ},{block={}, thread={"coalesced_index"}}, {})



