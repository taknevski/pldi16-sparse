#define tilesize 16

void c_std_d1_1(double *T2i,double *v2,double *T3)
{

  int h7,p4,p5,h1,h3,h2,p6,dummyLoop;

  for (h3=0; h3<tilesize; h3++){
   for (h2=0; h2<tilesize; h2++){
    for (h1=0; h1<tilesize; h1++){
     for (p6=0; p6<tilesize; p6++){
      for (p5=0; p5<tilesize; p5++){
       for (p4=0; p4<tilesize; p4++){
        for (h7=0; h7<tilesize; h7++){
         T3[h3 + tilesize*(h2 + tilesize*(h1 + tilesize*(p6 + tilesize*(p5 + tilesize*(p4)))))] = T3[h3 + tilesize*(h2 + tilesize*(h1 + tilesize*(p6 + tilesize*(p5 + tilesize*(p4)))))] - (T2i[h7 + tilesize*(p4 + tilesize*(p5 + tilesize*(h1)))] * v2[h3 + tilesize*(h2 + tilesize*(p6 + tilesize*(h7)))]);
        }
       }
      }
     }
    }
   }
  }

}
