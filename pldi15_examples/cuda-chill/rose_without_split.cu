#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define c(i,j) c.count
#define index_(i) index[i]
#define index__(i) index[i + 1]

struct inspector 
{
  int *j;
  int *i;
  int count;
}
;
#define NZ 1666
#define NUMROWS 494

void spmv(int n,int index[494UL],float a[1666UL],float y[494UL],float x[494UL],int col[1666UL])
{
  int _t63;
  int _t62;
  int _t61;
  int _t60;
  int _t59;
  int _t58;
  int _t57;
  int _t56;
  int _t55;
  int _t54;
  int _t53;
  int _t52;
  int _t51;
  int _t50;
  int _t49;
  int _t48;
  int _t47;
  int _t46;
  float _P4[1024 * 32];
  int _t45;
  int _t44;
  int _t43;
  int _t42;
  int _t41;
  int _t40;
  int _P3[1024 * 32];
  int _t39;
  int _t38;
  int _t37;
  int _t36;
  int _t35;
  int _t34;
  int _t33;
  int _t32;
  int _t31;
  int _t30;
  int _t29;
  int _t28;
  int _t27;
  int _t26;
  int _t25;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  int _t18;
  int _t17;
  int _t16;
  float _P2[4 * 32];
  int _t15;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _P1[4 * 32];
  int _t9;
  int _t7;
  int _t6;
  int _t8;
  int _t5;
  int _t4;
  int _t2;
  int _t1;
  int _t3;
  int c_inv;
  int new_index;
  int *c_i;
  int *c_j;
  struct inspector c;
  int i;
  int j;
  for (i = 0; i < n; i++) 
    for (j = index[i]; j < index[i + 1]; j++) 
      y[i] += (a[j] * x[col[j]]);
}

int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[494 + 1];
  int col[1666UL];
  spmv(n,index,a,y,x,col);
  return 0;
}
