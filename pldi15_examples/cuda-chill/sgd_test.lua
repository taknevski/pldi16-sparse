init("sgd_work.c", "sgd_kernel_global_simd", 0)
dofile("cudaize.lua")

make_dense(0,2,"l")


known("lb",46641)
known("ub",155997)
known("n", 46641)

distribute({0,1,2}, 4)


skew({0,1,2},2,{-1,1})

permute(0,{"l", "i","j", "k"})
shift_to(0,1,0)
distribute({0,1,2}, 4)

compact(0,{2},{"new_ratings"}, -1, {"ratings"})
distribute({0,1,2},3)


--N = 1000
--M = 1000000



tile_by_index(1,{"i"},{2},{l1_control="by_diag"},{"l","by_diag", "i","k"})
shift_to(0,3,0);

scalar_expand_by_index(0,{"i"},"err",1, 0, 1)
scalar_expand_by_index(1,{"i","k"},"RHS",1, 0, 1)
distribute({1,6}, 4)

known({"index_", "index__"}, {-1,1}, -1, 0)

