#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
__global__ void mxm(float (*c)[1024],float (*a)[1024]);
__global__ void mxm(float (*c)[1024],float (*a)[1024]);
#define n 1024
#define m 1024
#define N 1024

void mxm(float c[1024UL][1024UL],float a[1024UL][1024UL],float b[1024UL][1024UL])
{
  float *devI1Ptr;
  float *devO1Ptr;
  int t14;
  int t12;
  int t10;
  int t8;
  int t6;
  int t4;
  int t2;
  int _t20;
  int _t21;
  float _P1[1024][1025];
  int _t18;
  int _t19;
  int _t15;
//  float a[n][m], b[m][n],c[n][n];
  int i;
  int j;
  int k;
  cudaMalloc(((void **)(&devO1Ptr)),1048576 * sizeof(float ));
  cudaMalloc(((void **)(&devI1Ptr)),1048576 * sizeof(float ));
  cudaMemcpy(devI1Ptr,a,1048576 * sizeof(float ),cudaMemcpyHostToDevice);
  dim3 dimGrid0 = dim3(8,8);
  dim3 dimBlock0 = dim3(16,4);
  mxm<<<dimGrid0,dimBlock0>>>(((float (*)[1024])devO1Ptr),((float (*)[1024])devI1Ptr));
  cudaMemcpy(c,devO1Ptr,1048576 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaMalloc(((void **)(&devO1Ptr)),1048576 * sizeof(float ));
  cudaMalloc(((void **)(&devI1Ptr)),1048576 * sizeof(float ));
  cudaMemcpy(devI1Ptr,a,1048576 * sizeof(float ),cudaMemcpyHostToDevice);
  dim3 dimGrid1 = dim3(8,8);
  dim3 dimBlock1 = dim3(16,4);
  mxm<<<dimGrid1,dimBlock1>>>(((float (*)[1024])devO1Ptr),((float (*)[1024])devI1Ptr));
  cudaMemcpy(c,devO1Ptr,1048576 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
}

__global__ void mxm(float (*c)[1024],float (*a)[1024])
{
  int k;
  int j;
  int i;
  int bx;
  bx = blockIdx.x;
  int by;
  by = blockIdx.y;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  __device__ __shared__ float _P1[1024][1025];
  int t2;
  int t4;
  int t6;
  int t8;
  int t10;
  int t12;
  int t14;
  for (i = 128 * bx + 16 * by; i <= 128 * bx + 16 * by + 15; i += 1) {
    for (j = 64 * tx + 16 * ty; j <= 64 * tx + 16 * ty + 15; j += 1) 
      for (k = 0; k <= 1023; k += 1) 
        c[i][j] = (c[i][j] + (a[i][k] * _P1[j - 0][k - 0]));
    __syncthreads();
  }
}

__global__ void mxm(float (*c)[1024],float (*a)[1024])
{
  int k;
  int j;
  int i;
  int bx;
  bx = blockIdx.x;
  int by;
  by = blockIdx.y;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  __device__ __shared__ float _P1[1024][1025];
  int t2;
  int t4;
  int t6;
  int t8;
  int t10;
  int t12;
  int t14;
  for (i = 128 * bx + 16 * by; i <= 128 * bx + 16 * by + 15; i += 1) {
    for (j = 64 * tx + 16 * ty; j <= 64 * tx + 16 * ty + 15; j += 1) 
      for (k = 0; k <= 1023; k += 1) 
        c[i][j] = (c[i][j] + (a[i][k] * _P1[j - 0][k - 0]));
    __syncthreads();
  }
}
