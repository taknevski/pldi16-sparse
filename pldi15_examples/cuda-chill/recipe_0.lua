init("_orio_chill_0.c","c_std_d1_1",0)
dofile("cudaize.lua")

tilesize=16
cudaize(0,"c_std_d1_1_GPU_0",{T2i=tilesize*tilesize*tilesize*tilesize,v2=tilesize*tilesize*tilesize*tilesize,T3=tilesize*tilesize*tilesize*tilesize*tilesize*tilesize},{block={"p4","p5"},thread={"h3","p6"}},{})
copy_to_registers(0,"h7","T3")
print_code(0)

unroll(0,8,7)

print_dep()
