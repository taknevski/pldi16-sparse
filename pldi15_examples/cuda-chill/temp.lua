init("spmv_bcsr.cpp", "spmv", 0)
dofile("cudaize.lua")

make_dense(0,2,"k")

known("lb",0)
known("ub",155997)
known("n", 155998)
skew({0},2,{-1,1})
permute(0,{"k","i","j"})
shift_to(0,1,0)
--print_code(5)

