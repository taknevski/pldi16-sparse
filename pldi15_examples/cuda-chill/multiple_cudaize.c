#define N 1024

void test(int c[N][N], int a[N][N], int b[N][N]) {
  int i, j, k;

  for (i = 0; i < N; i++)
    for (j = 0; j < N; j++)
      for (k = 0; k < N; k++)
        c[j][i] = c[j][i] + a[k][i] * b[j][k];
}
