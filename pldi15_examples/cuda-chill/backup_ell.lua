init("spmv_ell.cpp", "spmv", 0)
dofile("cudaize.lua")
Tj=100
N=2048
M=100
Ti=1024
--known(index_ < index__)
normalize(0,2)

--tile(0,2,10,2,counted)
tile_by_index(0,{"j"},{Tj},{l1_control="jj"},{"i","jj","j"})


shift_to(0,2,0)
shift_to(0,3,0)

compact(0,2,{"a_prime","col_prime"}, 0, {"a","col"}, {1,-1}, {"index_", "index__"}, 99, 1)
print_code(6)
permute(6,{"jj", "i"})


scalar_expand(6,{1,2}, "a_prime")
scalar_expand(6,{1,2}, "col_prime")


--scalar_expand(6,{1,2}, "a_prime", 0)
--scalar_expand(6,[1,2], col_prime, 0)
distribute({6,7,8}, 1)
fuse({7,8},1)

tile_by_index(6,{"i"},{Ti},{l1_control="ii"},{"ii","i","jj"})
cudaize(6,"spmv_ell_GPU",{ _P_DATA4=N*M,x=N,y=N,_P_DATA3=N*M},{block={"ii"}, thread={"i"}}, {"_P_DATA7", "_P_DATA8"})
copy_to_registers(6, "jj", "y");







