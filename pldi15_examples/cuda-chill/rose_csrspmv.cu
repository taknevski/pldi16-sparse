#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define rows_(row) rows[row]
#define rows__(row) rows[row + 1]
__global__ void spmv_GPU(double *y,double *vals,double *x,int *cols,int *rows);
#define n 1024

void spmv(int *rows,int *cols,double *vals,double *x,double *y,int n1)
{
  int *devI4Ptr;
  int *devI3Ptr;
  double *devI2Ptr;
  double *devI1Ptr;
  double *devO1Ptr;
  int t8;
  int t10;
  int t4;
  int t2;
  int row;
  int col_index;
  double temp;
  cudaMalloc(((void **)(&devO1Ptr)),1024 * sizeof(double ));
  cudaMemcpy(devO1Ptr,y,1024 * sizeof(double ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),100000 * sizeof(double ));
  cudaMemcpy(devI1Ptr,vals,100000 * sizeof(double ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),1024 * sizeof(double ));
  cudaMemcpy(devI2Ptr,x,1024 * sizeof(double ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),100000 * sizeof(int ));
  cudaMemcpy(devI3Ptr,cols,100000 * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI4Ptr)),1024 * sizeof(int ));
  cudaMemcpy(devI4Ptr,rows,1024 * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid = dim3(8,1);
  dim3 dimBlock = dim3(128,1);
  spmv_GPU<<<dimGrid,dimBlock>>>(devO1Ptr,devI1Ptr,devI2Ptr,devI3Ptr,devI4Ptr);
  cudaMemcpy(y,devO1Ptr,1024 * sizeof(double ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  cudaFree(devI4Ptr);
}

int main()
{
  int rows[10UL];
  int cols[10UL];
  double vals[100UL];
  double x[100UL];
  double y[100UL];
  spmv(rows,cols,vals,x,y,100);
  return 0;
}

__global__ void spmv_GPU(double *y,double *vals,double *x,int *cols,int *rows)
{
  int col_index;
  int jj;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int t2;
  int t4;
  int t10;
  int t8;
  for (jj = rows_(128 * bx + tx); jj <= rows__(128 * bx + tx) - 1; jj += 32) 
    for (col_index = jj; col_index <= __rose_lt(jj + 31,rows__(128 * bx + tx) - 1); col_index += 1) 
      y[128 * bx + tx] += (vals[col_index] * x[cols[col_index]]);
}
