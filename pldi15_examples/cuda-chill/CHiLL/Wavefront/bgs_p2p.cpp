
#include "LevelSchedule.hpp"
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

using namespace std;
void backwardSolveRef(int n, int *rowptr, int *colidx,int *idiag, double *values, double *y, const double *b)
{
  int i,k;
 // double sum;	
  
  for (i = n-1; i >= 0; i--) {
    double sum = b[i];
    for (k = rowptr[i]; k < rowptr[i + 1]; k++) {
      sum -= values[k]*y[colidx[k]];
    }
    y[i] = sum*idiag[i];
  } // for each row
}

int main(){
  int n =500;
  int rowptr[501];
  int diagptr[500];
  int colidx[5000];
  double values[5000];
  double b[5000];
  double y[5000];

  backwardSolveRef(n, rowptr, colidx, diagptr, values, y, b);

  return 0;
}
