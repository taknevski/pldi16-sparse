#include "LevelSchedule.hpp"
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
using namespace std;

void forwardSolveRef(int n, int *Ap, int *Ai,double *x, double *A, double *y, double *B)
{
  int p,j;
 // double sum;	
  
  for (j = 0; j < n; j++) {
    B[j] = y[j+1];
    for (p = Ap[j]; p < Ap[j + 1]; p++) {
      y[Ai[p]] -= A[p]*x[j];
    }
  } // for each row
}

int main(){
  int n =500;
  int rowptr[501];
  double diagptr[500];
  int colidx[5000];
  double values[5000];
  double b[5000];
  double y[5000];

  forwardSolveRef(n, rowptr, colidx, diagptr, values, y, b);

  return 0;
}
