#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA2_(_t20) _P_DATA2[_t20]
#define _P_DATA2__(_t20) _P_DATA2[_t20 + 1]
#define _P_DATA3_(_t28,_t29) _P_DATA3[1 * _t29]
#define _P_DATA3___(_t28,_t29) _P_DATA3[1 * _t29 + 1]
#define col_(_t24) col[1 * _t24]
#define col___(_t24) col[1 * _t24 + 1]
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
//#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "smv.h"
#include "matrix_io.h"
#include "sparse_matrix.h"
//#include "spmv.h"
#include <sys/time.h>
#include <math.h>
//void spmv_bcsr(int n, int *index, float *a, float *y, float *x, int *col,int nnz);

struct a_list 
{
  unsigned short col_;
  float a[64];
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void spmv(int n,int *index,float *a,float *y,float *x,int *col)
{
  int t8;
  int t6;
  int t4;
  int t2;
  int _t91;
  int _t90;
  int _t89;
  int _t88;
  int _t87;
  int _t86;
  int _t85;
  int _t84;
  int _t83;
  int _t82;
  int _t81;
  int _t80;
  int _t79;
  int _t78;
  int _t77;
  int _t76;
  int _t75;
  int _t74;
  int _t73;
  int _t72;
  int _t71;
  int _t70;
  int _t69;
  int _t68;
  int _t67;
  int _t66;
  int _t65;
  int _t64;
  int _t63;
  int _t62;
  int _t61;
  int _t60;
  int _t59;
  int _t58;
  int _t57;
  int _t56;
  int _t55;
  int _t54;
  int _t53;
  int _t52;
  int _t51;
  int _t50;
  int _t49;
  int _t48;
  int _t47;
  int _t46;
  int _t45;
  int _t44;
  int _t43;
  int _t42;
  int _t41;
  int _t40;
  int _t39;
  int _t38;
  int _t37;
  float _P2[8];
  int _t36;
  int _t35;
  int _t34;
  int _t33;
  int _t32;
  float _P1[8];
  int _t31;
  int _t30;
  int _t29;
  int _t28;
  int In_3;
  int In_2;
  int In_1;
  int _t27;
  int _t26;
  int _t25;
  struct a_list *_P_DATA6;
  int newVariable2;
  int newVariable1;
  struct mk *_P_DATA5;
  float *a_prime;
  struct a_list *_P_DATA4;
  unsigned short *_P_DATA3;
  int chill_count_1;
  int *_P_DATA2;
  int chill_count_0;
  int newVariable0;
  float *_P_DATA1;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  int _t18;
  int _t17;
  int _t16;
  int _t15;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t6;
  int _t7;
  int k;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  _P_DATA1 = ((float *)(malloc(sizeof(float ) * 62456)));
  for (newVariable0 = 0; newVariable0 <= 62450; newVariable0 += 1) 
    _P_DATA1[newVariable0] = x[newVariable0];
  _P_DATA2 = ((int *)(malloc(sizeof(int ) * 7807)));
  _P_DATA2[0] = 0;
  _P_DATA4 = 0;
  _P_DATA5 = ((struct mk *)(malloc(sizeof(struct mk ) * 7807)));
  chill_count_1 = 0;
  _P_DATA2[0] = 0;
  for (t2 = 0; t2 <= 7805; t2 += 1) {
    for (t4 = 0; t4 <= 7; t4 += 1) 
      for (t6 = index_(8 * t2 + t4); t6 <= index__(8 * t2 + t4) - 1; t6 += 1) {
        _t21 = (col_(t6) - 0) / 8;
        _P_DATA5[_t21].mk::ptr = 0;
      }
    for (t4 = 0; t4 <= 7; t4 += 1) 
      for (t6 = index_(8 * t2 + t4); t6 <= index__(8 * t2 + t4) - 1; t6 += 1) {
        _t21 = (col_(t6) - 0) / 8;
        _t23 = (col_(t6) - 0) % 8;
        if (_P_DATA5[_t21].mk::ptr == 0) {
          _P_DATA6 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
          _P_DATA6 -> a_list::next = _P_DATA4;
          _P_DATA4 = _P_DATA6;
          _P_DATA5[_t21].mk::ptr = _P_DATA4;
          for (newVariable1 = 0; newVariable1 <= 7; newVariable1 += 1) 
            for (newVariable2 = 0; newVariable2 <= 7; newVariable2 += 1) 
              _P_DATA5[_t21].mk::ptr -> a_list::a[8 * newVariable1 + 1 * newVariable2] = 0;
          _P_DATA5[_t21].mk::ptr -> a_list::col_ = _t21;
          chill_count_1 += 1;
        }
        _P_DATA5[_t21].mk::ptr -> a_list::a[8 * t4 + 1 * _t23] = a[t6];
      }
    _P_DATA2[t2 + 1] = chill_count_1;
  }
  for (t2 = -chill_count_1 + 1; t2 <= 0; t2 += 1) {
    if (chill_count_1 + t2 <= 1) {
      _P_DATA3 = ((unsigned short *)(malloc(sizeof(unsigned short ) * chill_count_1)));
      a_prime = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 64))));
    }
    for (t4 = 0; t4 <= 7; t4 += 1) 
      for (t6 = 0; t6 <= 7; t6 += 1) 
        a_prime[-t2 * 64 + 8 * t4 + 1 * t6] = _P_DATA4 -> a_list::a[8 * t4 + 1 * t6];
    _P_DATA3[-t2] = _P_DATA4 -> a_list::col_;
    _P_DATA6 = _P_DATA4 -> a_list::next;
    free(_P_DATA4);
    _P_DATA4 = _P_DATA6;
  }
  for (t2 = 0; t2 <= 7805; t2 += 1) 
    for (t4 = _P_DATA2_(t2); t4 <= _P_DATA2__(t2) - 1; t4 += 1) {
      _P2[8 * t2 - 8 * t2] = y[8 * t2];
      _P2[8 * t2 + 1 - 8 * t2] = y[8 * t2 + 1];
      _P2[8 * t2 + 2 - 8 * t2] = y[8 * t2 + 2];
      _P2[8 * t2 + 3 - 8 * t2] = y[8 * t2 + 3];
      _P2[8 * t2 + 4 - 8 * t2] = y[8 * t2 + 4];
      _P2[8 * t2 + 5 - 8 * t2] = y[8 * t2 + 5];
      _P2[8 * t2 + 6 - 8 * t2] = y[8 * t2 + 6];
      _P2[8 * t2 + 7 - 8 * t2] = y[8 * t2 + 7];
      t8 = 8 * _P_DATA3_(t2,t4);
      _P1[t8 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8];
      _P1[t8 + 1 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 1];
      _P1[t8 + 2 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 2];
      _P1[t8 + 3 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 3];
      _P1[t8 + 4 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 4];
      _P1[t8 + 5 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 5];
      _P1[t8 + 6 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 6];
      _P1[t8 + 7 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 7];
      _P1[t8 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8];
      _P1[t8 + 1 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 1];
      _P1[t8 + 2 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 2];
      _P1[t8 + 3 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 3];
      _P1[t8 + 4 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 4];
      _P1[t8 + 5 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 5];
      _P1[t8 + 6 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 6];
      _P1[t8 + 7 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 7];
      _P1[t8 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8];
      _P1[t8 + 1 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 1];
      _P1[t8 + 2 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 2];
      _P1[t8 + 3 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 3];
      _P1[t8 + 4 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 4];
      _P1[t8 + 5 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 5];
      _P1[t8 + 6 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 6];
      _P1[t8 + 7 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 7];
      _P1[t8 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8];
      _P1[t8 + 1 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 1];
      _P1[t8 + 2 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 2];
      _P1[t8 + 3 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 3];
      _P1[t8 + 4 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 4];
      _P1[t8 + 5 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 5];
      _P1[t8 + 6 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 6];
      _P1[t8 + 7 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 7];
      _P1[t8 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8];
      _P1[t8 + 1 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 1];
      _P1[t8 + 2 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 2];
      _P1[t8 + 3 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 3];
      _P1[t8 + 4 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 4];
      _P1[t8 + 5 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 5];
      _P1[t8 + 6 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 6];
      _P1[t8 + 7 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 7];
      _P1[t8 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8];
      _P1[t8 + 1 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 1];
      _P1[t8 + 2 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 2];
      _P1[t8 + 3 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 3];
      _P1[t8 + 4 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 4];
      _P1[t8 + 5 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 5];
      _P1[t8 + 6 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 6];
      _P1[t8 + 7 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 7];
      _P1[t8 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8];
      _P1[t8 + 1 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 1];
      _P1[t8 + 2 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 2];
      _P1[t8 + 3 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 3];
      _P1[t8 + 4 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 4];
      _P1[t8 + 5 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 5];
      _P1[t8 + 6 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 6];
      _P1[t8 + 7 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 7];
      _P1[t8 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8];
      _P1[t8 + 1 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 1];
      _P1[t8 + 2 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 2];
      _P1[t8 + 3 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 3];
      _P1[t8 + 4 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 4];
      _P1[t8 + 5 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 5];
      _P1[t8 + 6 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 6];
      _P1[t8 + 7 - 8 * _P_DATA3_(t2,t4)] = _P_DATA1[t8 + 7];
      _P2[8 * t2 + 0 - 8 * t2] += (a_prime[t4 * 64 + 8 * 0 + 1 * 0] * _P1[8 * _P_DATA3[t4] + 0 - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + 0 - 8 * t2] += (a_prime[t4 * 64 + 8 * 0 + 1 * (0 + 1)] * _P1[8 * _P_DATA3[t4] + (0 + 1) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + 0 - 8 * t2] += (a_prime[t4 * 64 + 8 * 0 + 1 * (0 + 2)] * _P1[8 * _P_DATA3[t4] + (0 + 2) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + 0 - 8 * t2] += (a_prime[t4 * 64 + 8 * 0 + 1 * (0 + 3)] * _P1[8 * _P_DATA3[t4] + (0 + 3) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + 0 - 8 * t2] += (a_prime[t4 * 64 + 8 * 0 + 1 * (0 + 4)] * _P1[8 * _P_DATA3[t4] + (0 + 4) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + 0 - 8 * t2] += (a_prime[t4 * 64 + 8 * 0 + 1 * (0 + 5)] * _P1[8 * _P_DATA3[t4] + (0 + 5) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + 0 - 8 * t2] += (a_prime[t4 * 64 + 8 * 0 + 1 * (0 + 6)] * _P1[8 * _P_DATA3[t4] + (0 + 6) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + 0 - 8 * t2] += (a_prime[t4 * 64 + 8 * 0 + 1 * (0 + 7)] * _P1[8 * _P_DATA3[t4] + (0 + 7) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 1) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 1) + 1 * 0] * _P1[8 * _P_DATA3[t4] + 0 - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 1) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 1) + 1 * (0 + 1)] * _P1[8 * _P_DATA3[t4] + (0 + 1) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 1) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 1) + 1 * (0 + 2)] * _P1[8 * _P_DATA3[t4] + (0 + 2) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 1) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 1) + 1 * (0 + 3)] * _P1[8 * _P_DATA3[t4] + (0 + 3) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 1) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 1) + 1 * (0 + 4)] * _P1[8 * _P_DATA3[t4] + (0 + 4) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 1) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 1) + 1 * (0 + 5)] * _P1[8 * _P_DATA3[t4] + (0 + 5) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 1) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 1) + 1 * (0 + 6)] * _P1[8 * _P_DATA3[t4] + (0 + 6) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 1) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 1) + 1 * (0 + 7)] * _P1[8 * _P_DATA3[t4] + (0 + 7) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 2) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 2) + 1 * 0] * _P1[8 * _P_DATA3[t4] + 0 - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 2) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 2) + 1 * (0 + 1)] * _P1[8 * _P_DATA3[t4] + (0 + 1) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 2) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 2) + 1 * (0 + 2)] * _P1[8 * _P_DATA3[t4] + (0 + 2) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 2) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 2) + 1 * (0 + 3)] * _P1[8 * _P_DATA3[t4] + (0 + 3) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 2) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 2) + 1 * (0 + 4)] * _P1[8 * _P_DATA3[t4] + (0 + 4) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 2) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 2) + 1 * (0 + 5)] * _P1[8 * _P_DATA3[t4] + (0 + 5) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 2) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 2) + 1 * (0 + 6)] * _P1[8 * _P_DATA3[t4] + (0 + 6) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 2) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 2) + 1 * (0 + 7)] * _P1[8 * _P_DATA3[t4] + (0 + 7) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 3) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 3) + 1 * 0] * _P1[8 * _P_DATA3[t4] + 0 - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 3) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 3) + 1 * (0 + 1)] * _P1[8 * _P_DATA3[t4] + (0 + 1) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 3) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 3) + 1 * (0 + 2)] * _P1[8 * _P_DATA3[t4] + (0 + 2) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 3) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 3) + 1 * (0 + 3)] * _P1[8 * _P_DATA3[t4] + (0 + 3) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 3) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 3) + 1 * (0 + 4)] * _P1[8 * _P_DATA3[t4] + (0 + 4) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 3) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 3) + 1 * (0 + 5)] * _P1[8 * _P_DATA3[t4] + (0 + 5) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 3) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 3) + 1 * (0 + 6)] * _P1[8 * _P_DATA3[t4] + (0 + 6) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 3) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 3) + 1 * (0 + 7)] * _P1[8 * _P_DATA3[t4] + (0 + 7) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 4) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 4) + 1 * 0] * _P1[8 * _P_DATA3[t4] + 0 - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 4) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 4) + 1 * (0 + 1)] * _P1[8 * _P_DATA3[t4] + (0 + 1) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 4) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 4) + 1 * (0 + 2)] * _P1[8 * _P_DATA3[t4] + (0 + 2) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 4) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 4) + 1 * (0 + 3)] * _P1[8 * _P_DATA3[t4] + (0 + 3) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 4) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 4) + 1 * (0 + 4)] * _P1[8 * _P_DATA3[t4] + (0 + 4) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 4) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 4) + 1 * (0 + 5)] * _P1[8 * _P_DATA3[t4] + (0 + 5) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 4) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 4) + 1 * (0 + 6)] * _P1[8 * _P_DATA3[t4] + (0 + 6) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 4) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 4) + 1 * (0 + 7)] * _P1[8 * _P_DATA3[t4] + (0 + 7) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 5) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 5) + 1 * 0] * _P1[8 * _P_DATA3[t4] + 0 - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 5) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 5) + 1 * (0 + 1)] * _P1[8 * _P_DATA3[t4] + (0 + 1) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 5) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 5) + 1 * (0 + 2)] * _P1[8 * _P_DATA3[t4] + (0 + 2) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 5) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 5) + 1 * (0 + 3)] * _P1[8 * _P_DATA3[t4] + (0 + 3) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 5) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 5) + 1 * (0 + 4)] * _P1[8 * _P_DATA3[t4] + (0 + 4) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 5) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 5) + 1 * (0 + 5)] * _P1[8 * _P_DATA3[t4] + (0 + 5) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 5) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 5) + 1 * (0 + 6)] * _P1[8 * _P_DATA3[t4] + (0 + 6) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 5) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 5) + 1 * (0 + 7)] * _P1[8 * _P_DATA3[t4] + (0 + 7) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 6) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 6) + 1 * 0] * _P1[8 * _P_DATA3[t4] + 0 - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 6) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 6) + 1 * (0 + 1)] * _P1[8 * _P_DATA3[t4] + (0 + 1) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 6) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 6) + 1 * (0 + 2)] * _P1[8 * _P_DATA3[t4] + (0 + 2) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 6) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 6) + 1 * (0 + 3)] * _P1[8 * _P_DATA3[t4] + (0 + 3) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 6) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 6) + 1 * (0 + 4)] * _P1[8 * _P_DATA3[t4] + (0 + 4) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 6) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 6) + 1 * (0 + 5)] * _P1[8 * _P_DATA3[t4] + (0 + 5) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 6) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 6) + 1 * (0 + 6)] * _P1[8 * _P_DATA3[t4] + (0 + 6) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 6) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 6) + 1 * (0 + 7)] * _P1[8 * _P_DATA3[t4] + (0 + 7) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 7) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 7) + 1 * 0] * _P1[8 * _P_DATA3[t4] + 0 - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 7) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 7) + 1 * (0 + 1)] * _P1[8 * _P_DATA3[t4] + (0 + 1) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 7) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 7) + 1 * (0 + 2)] * _P1[8 * _P_DATA3[t4] + (0 + 2) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 7) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 7) + 1 * (0 + 3)] * _P1[8 * _P_DATA3[t4] + (0 + 3) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 7) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 7) + 1 * (0 + 4)] * _P1[8 * _P_DATA3[t4] + (0 + 4) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 7) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 7) + 1 * (0 + 5)] * _P1[8 * _P_DATA3[t4] + (0 + 5) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 7) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 7) + 1 * (0 + 6)] * _P1[8 * _P_DATA3[t4] + (0 + 6) - 8 * _P_DATA3_(t2,t4)]);
      _P2[8 * t2 + (0 + 7) - 8 * t2] += (a_prime[t4 * 64 + 8 * (0 + 7) + 1 * (0 + 7)] * _P1[8 * _P_DATA3[t4] + (0 + 7) - 8 * _P_DATA3_(t2,t4)]);
      y[8 * t2] = _P2[8 * t2 - 8 * t2];
      y[8 * t2 + 1] = _P2[8 * t2 + 1 - 8 * t2];
      y[8 * t2 + 2] = _P2[8 * t2 + 2 - 8 * t2];
      y[8 * t2 + 3] = _P2[8 * t2 + 3 - 8 * t2];
      y[8 * t2 + 4] = _P2[8 * t2 + 4 - 8 * t2];
      y[8 * t2 + 5] = _P2[8 * t2 + 5 - 8 * t2];
      y[8 * t2 + 6] = _P2[8 * t2 + 6 - 8 * t2];
      y[8 * t2 + 7] = _P2[8 * t2 + 7 - 8 * t2];
    }
  for (t2 = 62448; t2 <= 62450; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) 
      y[t2] += (a[t4] * x[col[t4]]);
  free(_P_DATA1);
  free(_P_DATA2);
  free(_P_DATA3);
  free(a_prime);
  free(_P_DATA5);
}

int main(int argc,char **argv)
{
/////////////////////////////////////////////////////////////////////
  int num;
  int retval;
  struct timeval tv1;
  struct timeval tv2;
  struct timeval tv3;
  struct timeval tv4;
  struct timeval tv5;
//struct timezone tz;
  char filename[255UL];
  struct sparse_matrix A;
  struct sparse_matrix M;
  float *x;
  float *b;
  float *b2;
  int num_threads;
//pthread_attr_t attr;
  strcpy(filename,argv[1]);
  if (1) {
    fprintf(stderr,"main::Loading matrix %s\n",filename);
//fprintf(stderr, "R: %d C: %d\n", R, C);	
  }
  load_sparse_matrix(filename,&A);
//initialize b to 0 and x to 1
  int i;
  int ii;
  x = ((float *)(malloc((sizeof(float ) * A.sparse_matrix::ncols))));
  b = ((float *)(malloc((sizeof(float ) * A.sparse_matrix::nrows))));
  b2 = ((float *)(malloc((sizeof(float ) * A.sparse_matrix::nrows))));
  FILE *fp = 0L;
  FILE *fp1 = 0L;
  FILE *fp2 = 0L;
  FILE *fp3 = 0L;
  for (i = 0; i < A.sparse_matrix::ncols; i++) 
    x[i] = 1.0;
  for (i = 0; i < A.sparse_matrix::nrows; i++) {
    b[i] = 0;
    b2[i] = 0;
  }
  int nrows;
  int nnz;
  int *rows;
  int *cols;
  REAL *vals;
  smv(&A,x,b);
//spmv(A.nrows,A.rows,A.vals,b2,x,A.cols);
  spmv(A.sparse_matrix::nrows,A.sparse_matrix::rows,A.sparse_matrix::vals,b2,x,A.sparse_matrix::cols);
  for (i = 0; i < A.sparse_matrix::nrows; i++) {
    if (fabs((b2[i] - b[i])) >= 0.1) {
      printf("Values don\'t match at %d, expected %f obtained %f\n",i,b[i],b2[i]);
      break; 
    }
//else	
//	printf("Values match at %d, expected %f obtained %f\n", i, b[i], b2[i]);
  }
  free(x);
  free(b);
  free(b2);
  free(A.sparse_matrix::rows);
  free(A.sparse_matrix::cols);
  free(A.sparse_matrix::vals);
  if (i != A.sparse_matrix::nrows) 
//printf("i  is %d\n", i) ;
    exit(1);
  return 0;
}
