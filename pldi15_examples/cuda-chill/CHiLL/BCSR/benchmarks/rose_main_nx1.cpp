#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA1_(_t11) _P_DATA1[_t11]
#define _P_DATA1__(_t11) _P_DATA1[_t11 + 1]
#define col_(_t14) col[1 * _t14]
#define col___(_t14) col[1 * _t14 + 1]
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
//#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "smv.h"
#include "matrix_io.h"
#include "sparse_matrix.h"
//#include "spmv.h"
#include <sys/time.h>
#include <math.h>
//void spmv_bcsr(int n, int *index, float *a, float *y, float *x, int *col,int nnz);

struct a_list 
{
  int col_;
  float a[2];
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void spmv(int n,int *index,float *a,float *y,float *x,int *col)
{
  int t6;
  int t4;
  int t2;
  int _t48;
  int _t47;
  int _t46;
  int _t45;
  int _t44;
  int _t43;
  int _t42;
  int _t41;
  int _t40;
  int _t39;
  int _t38;
  int _t37;
  int _t36;
  int _t35;
  int _t34;
  int _t33;
  int _t32;
  int _t31;
  int _t30;
  int _t29;
  int _t28;
  int _t27;
  int _t26;
  int _t25;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  float _P1[4];
  int _t20;
  int _t19;
  int _t18;
  int In_2;
  int In_1;
  int _t17;
  int _t16;
  int _t15;
  struct a_list *_P_DATA5;
  int newVariable0;
  struct mk *_P_DATA4;
  float *a_prime;
  struct a_list *_P_DATA3;
  int *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t6;
  int _t4;
  int _t5;
  int _t3;
  int k;
  int _t2;
  int _t1;
  int i;
  int j;
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * 103251)));
  _P_DATA1[0] = 0;
  _P_DATA3 = 0;
  _P_DATA4 = ((struct mk *)(malloc(sizeof(struct mk ) * 206500)));
  chill_count_1 = 0;
  _P_DATA1[0] = 0;
  for (t2 = 0; t2 <= 103249; t2 += 1) {
    for (t4 = 0; t4 <= 1; t4 += 1) 
      for (t6 = index_(2 * t2 + t4); t6 <= index__(2 * t2 + t4) - 1; t6 += 1) {
        _t12 = col_(t6);
        _P_DATA4[_t12].mk::ptr = 0;
      }
    for (t4 = 0; t4 <= 1; t4 += 1) 
      for (t6 = index_(2 * t2 + t4); t6 <= index__(2 * t2 + t4) - 1; t6 += 1) {
        _t12 = col_(t6);
        if (_P_DATA4[_t12].mk::ptr == 0) {
          _P_DATA5 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
          _P_DATA5 -> a_list::next = _P_DATA3;
          _P_DATA3 = _P_DATA5;
          _P_DATA4[_t12].mk::ptr = _P_DATA3;
          for (newVariable0 = 0; newVariable0 <= 1; newVariable0 += 1) 
            _P_DATA4[_t12].mk::ptr -> a_list::a[1 * newVariable0] = 0;
          _P_DATA4[_t12].mk::ptr -> a_list::col_ = _t12;
          chill_count_1 += 1;
        }
        _P_DATA4[_t12].mk::ptr -> a_list::a[1 * t4] = a[t6];
      }
    _P_DATA1[t2 + 1] = chill_count_1;
  }
  for (t2 = -chill_count_1 + 1; t2 <= 0; t2 += 1) {
    if (chill_count_1 + t2 <= 1) {
      _P_DATA2 = ((int *)(malloc(sizeof(int ) * chill_count_1)));
      a_prime = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 2))));
    }
    for (t4 = 0; t4 <= 1; t4 += 1) 
      a_prime[-t2 * 2 + 1 * t4] = _P_DATA3 -> a_list::a[1 * t4];
    _P_DATA2[-t2] = _P_DATA3 -> a_list::col_;
    _P_DATA5 = _P_DATA3 -> a_list::next;
    free(_P_DATA3);
    _P_DATA3 = _P_DATA5;
  }
  for (t2 = 0; t2 <= 103249; t2 += 1) 
    for (t4 = _P_DATA1_(t2); t4 <= _P_DATA1__(t2) - 1; t4 += 1) {
      _P1[2 * t2 - 2 * t2] = y[2 * t2];
      _P1[2 * t2 + 1 - 2 * t2] = y[2 * t2 + 1];
      _P1[2 * t2 + 0 - 2 * t2] += (a_prime[t4 * 2 + 1 * 0] * x[_P_DATA2[t4]]);
      _P1[2 * t2 + (0 + 1) - 2 * t2] += (a_prime[t4 * 2 + 1 * (0 + 1)] * x[_P_DATA2[t4]]);
      y[2 * t2] = _P1[2 * t2 - 2 * t2];
      y[2 * t2 + 1] = _P1[2 * t2 + 1 - 2 * t2];
    }
  free(_P_DATA1);
  free(_P_DATA2);
  free(a_prime);
  free(_P_DATA4);
}

int main(int argc,char **argv)
{
/////////////////////////////////////////////////////////////////////
  int num;
  int retval;
  struct timeval tv1;
  struct timeval tv2;
  struct timeval tv3;
  struct timeval tv4;
  struct timeval tv5;
//struct timezone tz;
  char filename[255UL];
  struct sparse_matrix A;
  struct sparse_matrix M;
  float *x;
  float *b;
  float *b2;
  int num_threads;
//pthread_attr_t attr;
  strcpy(filename,argv[1]);
  if (1) {
    fprintf(stderr,"main::Loading matrix %s\n",filename);
//fprintf(stderr, "R: %d C: %d\n", R, C);	
  }
  load_sparse_matrix(filename,&A);
//initialize b to 0 and x to 1
  int i;
  int ii;
  x = ((float *)(malloc((sizeof(float ) * A.sparse_matrix::ncols))));
  b = ((float *)(malloc((sizeof(float ) * A.sparse_matrix::nrows))));
  b2 = ((float *)(malloc((sizeof(float ) * A.sparse_matrix::nrows))));
  FILE *fp = 0L;
  FILE *fp1 = 0L;
  FILE *fp2 = 0L;
  FILE *fp3 = 0L;
  for (i = 0; i < A.sparse_matrix::ncols; i++) 
    x[i] = 1.0;
  for (i = 0; i < A.sparse_matrix::nrows; i++) {
    b[i] = 0;
    b2[i] = 0;
  }
  int nrows;
  int nnz;
  int *rows;
  int *cols;
  REAL *vals;
  smv(&A,x,b);
//spmv(A.nrows,A.rows,A.vals,b2,x,A.cols);
  spmv(A.sparse_matrix::nrows,A.sparse_matrix::rows,A.sparse_matrix::vals,b2,x,A.sparse_matrix::cols);
  for (i = 0; i < A.sparse_matrix::nrows; i++) {
    if (fabs((b2[i] - b[i])) >= 0.1) {
      printf("Values don\'t match at %d, expected %f obtained %f\n",i,b[i],b2[i]);
      break; 
    }
//else	
//	printf("Values match at %d, expected %f obtained %f\n", i, b[i], b2[i]);
  }
  free(x);
  free(b);
  free(b2);
  free(A.sparse_matrix::rows);
  free(A.sparse_matrix::cols);
  free(A.sparse_matrix::vals);
  if (i != A.sparse_matrix::nrows) 
//printf("i  is %d\n", i) ;
    exit(1);
  return 0;
}
