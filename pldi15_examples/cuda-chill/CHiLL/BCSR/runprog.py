import subprocess
import StringIO
import os
import xlwt






 		
def generate_and_run_script(i,j,n,c):

	m = c-1
	file = open("spmv_bcsr.script", "w")
	shift = 3
        if n % i != 0:
                init_string = "source: spmv_bcsr.cpp \nprocedure: spmv\nformat : rose\nloop: 0\noriginal()\nsplit_with_alignment(0,1,"+str(i)+")\nknown(n == "+str(n)+")\nmake_dense(0,2,k)\nknown(lb == 0)\nknown(ub == "+str(m)+ ")\n"
        	shift += 1
	else:
                init_string = "source: spmv_bcsr.cpp \nprocedure: spmv\nformat : rose\nloop: 0\noriginal()\nmake_dense(0,2,k)\nknown(lb == 0)\nknown(ub == "+str(m)+ ")\nknown(n == "+str(n)+")"

        if c % j != 0:
                init_string += "\nset_array_size(x, "+str(c)+")"
		x = "_P_DATA1"
	else:
		x= "x"

	if i != 1 and j != 1:
		file.write(init_string +
	   	"\ntile(0,2,"+str(j)+ ",1,counted)\ntile(0,2,"+str(i)+",1,counted)\nshift_to(0,4,0)\nshift_to(0,3,0)\ncompact(0,[2],[a_prime], 0, [a])"
		"\ndatacopy("+str(0)+", 4,"+x+")\ndatacopy("+str(0)+", 3, y)\nunroll("+str(4+shift)+",4,"+str(j)+")\nunroll("+str(0)+",4,"+str(j)+")\nunroll("+str(0)+",3,"+str(i)+")\nunroll("+str(5+shift)+",3,"+str(i)+")\nunroll("+str(6+shift)+",3,"+str(i)+")\n")
	if i == 1 and j != 1:
		file.write(init_string +
		"\ntile(0,2,"+str(j)+ ",2,counted)\nshift_to(0,3,0)\ncompact(0,[2],[a_prime], 0, [a])"
		"\ndatacopy("+str(0)+",3,"+x+")\nunroll("+str(4+shift)+",3,"+str(j)+")\nunroll("+str(0)+",3,"+str(j)+")\n")


	if i != 1 and j == 1:
		file.write(init_string +
		"\ntile(0,1,"+str(i)+ ",1,counted)\npermute(0,1,[1,3,2,4])\nshift_to(0,3,0)\ncompact(0,[2],[a_prime], 0, [a])"
		"\ndatacopy("+str(0)+",3,y)\nunroll("+str(0)+",3,"+str(i)+")\nunroll("+str(4+shift)+",3,"+str(i)+")\nunroll("+str(5+shift)+",3,"+str(i)+")\n")
        
	if i == 1 and j == 1:    
		file.write(init_string +
		"\ncompact(0,[2],[a_prime], 0, [a])\n")
	
	file.close()

	subprocess.call(["../../../../chill", "spmv_bcsr.script"])
 	return



def generate_code(file, file2):

	found = 0
	for line in file:

		str1 ="#include <stdlib.h>\n#include <stdio.h>\n#include <sys/time.h>\n"
        	str2 = "#define NUMROWS"	       
        	str3 = "void spmv_bcsr(int m, int n, int *index, float *a, float *y, float *x, int *col,int nnz)\n"
        	str4 = "void spmv"
        	str5 = "int j"
       		str6 = "struct timeval t0, t1, t_2;\ngettimeofday(&t0, NULL);\n"
        	str7=  "gettimeofday(&t1, NULL);\ndouble elapsed = t1.tv_sec - t0.tv_sec  + (t1.tv_usec - t0.tv_usec) / 1000000.0;\nprintf(\"Inspector Time: %.6lf seconds\\n\", elapsed);\nprintf(\"Starting actual computation\\n\");\ngettimeofday(&t1, NULL);\n"
        	str8 = "_P_DATA3 = _P_DATA5;" 
        	str12 = "_P_DATA4 = _P_DATA6;" 
        	#str12 = "_P_DATA2[t2 + 1] = chill_count_1;\n" 
        	str9 =  "free(_P_DATA1);"
        	str10 = "gettimeofday(&t_2, NULL);\nelapsed = t_2.tv_sec - t1.tv_sec + (t_2.tv_usec - t1.tv_usec) / 1000000.0;\nprintf(\"%.6lf seconds elapsed\\n\", elapsed);\ndouble mflop_count = 1e-6 * (double) nnz * (double) 2.0;\nprintf(\"Performance :%.6lf MFLOPS\\n\", mflop_count / elapsed);\n"
        	str11= "int main()"


        	if line.find(str11) != -1:
        		break


        	if line.find(str9) != -1:
        		file2.write(str10)

		if line.find(str4) != -1:
        	     file2.write(str3)
        	else:
	             file2.write(line)     
        
 
		if found == 1 and line.find("}") != -1:
			found = 2	
                if found == 1 and line.find("}") == -1:
                        found = 0

        	if found == 2:
   			file2.write(str7)
			found = 0

        	if line.find(str2) != -1:
			file2.write(str1)
        	if line.find(str5) != -1:
			file2.write(str6)
        	if line.find(str8) != -1 or line.find(str12) != -1:
			found  = 1

	return



def run_config(sheet, row, config, matrix,i,j):

	os.chdir("./tests")

        sheet.write(row,0,config)
  	ins_sum=0
	ex_sum =0
        flop = 0
	for x in :q
vi range(0,10):
   		#try:
     		buf = subprocess.check_output(["./smv", matrix], stderr=subprocess.STDOUT)
		#except subprocess.CalledProcessError as e:
    		#	print matrix +" "+str(i) +" "+ str(j) + "\n"
		#	os.chdir("..")
		#	return (-1, -1, -1)
        	cmd_output = StringIO.StringIO(buf)
        	for y in range(1,11):
                	cmd_output.readline()
        	il = cmd_output.readline()
        	il2 = il.split(" ")
        	cmd_output.readline()
		ins_sum += float(il2[2])
        	sheet.write(row+1,x,float(il2[2]))
        	el = cmd_output.readline()
        	el2 = el.split(" ")
		ex_sum += float(el2[0])

        	sheet.write(row+2,x,float(el2[0]))

        	fl = cmd_output.readline()
        	fl2 = fl.split(" ")
        	fl3 = fl2[1].split(":")
  		flop += float(fl3[1])
		sheet.write(row+3,x,float(fl3[1]))

        	#print fl3[1]
        	#print "Read Line: %s" % (fl)
	os.chdir("..")
	
	return (ins_sum/10, ex_sum/10, flop/10)


def run_oski_config(sheet,row,matrix,i,j,n):

  	ins_sum=0
	ex_sum =0
        flop =0 
        file = open("my_xform.txt", "w")
        file.write("return BCSR(InputMat,"+str(i)+", "+str(j)+")\n") 
	file.close()

	for x in range(0,10):
        	#try:
		buf = subprocess.check_output(["./smv",matrix], stderr=subprocess.STDOUT)
		#except subprocess.CalledProcessError as e:
    		#	print matrix +" "+str(i) +" "+ str(j) + "\n"
		#	return (-1,-1,-1)

        	cmd_output = StringIO.StringIO(buf)
        	for y in range(0,11):
                	cmd_output.readline()
        	il = cmd_output.readline()
       		 #print il
        	il2 = il.split(" ")
        	#print il2
        	#cmd_output.readline()
        	#print x
        	ins_sum += float(il2[4])  
		extra_insp=float(0);
        	if n%i != 0 :
                	il2_ex = cmd_output.readline()
			el2_ex = il2_ex.split(" ")
			extra_insp = float(el2_ex[4])

		ins_sum += extra_insp
		sheet.write(row,x,float(il2[4])+extra_insp)


		el = cmd_output.readline()
		
        	el2 = el.split(" ")
        	
                ex_sum += float(el2[3])
         	sheet.write(row+1,x,float(el2[3]))

        	fl = cmd_output.readline()
        	fl2 = fl.split(" ")
        	fl3 = fl2[1].split(":")
        	#print fl3[1]
                flop += float(fl3[1])   
        	sheet.write(row+2,x,float(fl3[1]))
      	
	return (ins_sum/10, ex_sum/10, flop/10)
      	
	



rc = [1,2,3,4,5,6,7,8]
#rc = [2,3,4]


indir = '/home/anand/csr/matrices/temp'
book = xlwt.Workbook(encoding="utf-8")
sheet1 = book.add_sheet("Sheet 1")
sheet2 = book.add_sheet("Sheet 2")
row1=0
row2=0
sheet1.write(0,0,"Matrix")
sheet1.write(0,1,"Best RxC")
sheet1.write(0,2,"Best OSKI RxC")
sheet1.write(0,3,"Non-zeros")
sheet1.write(0,4,"N")
sheet1.write(0,5,"Inspector Time/s")
sheet1.write(0,6,"Unrolled BCSR Time/s")
sheet1.write(0,7,"Unrolled BCSR MFLOPS")
sheet1.write(0,8,"OSKI Inspector Time/s")
sheet1.write(0,9,"OSKI BCSR Time/s")
sheet1.write(0,10,"OSKI BCSR MFLOPS")
sheet1.write(0,11,"Inspector Speedup")
sheet1.write(0,12,"Executor Speedup")
row1+=1

for root, dirs, filenames in os.walk(indir):
    for f1 in filenames:
	f  = open(os.path.join(root, f1), "r")
        f.readline()
	params = f.readline()
        params2 = params.split(" ")
        n = int(params2[0])
        c = int(params2[1])
        nnz = int(params2[2])
	#print  n 
	#print  nnz      
        fname = f1.split(".")
	matrix_name = fname[0]
	#combinations = []
	#combinations2 = []
	f.close()



	#for x in rc:
	#	if n % x == 0:
	#		combinations.append(x)
	#	if c % x == 0:
	#		combinations2.append(x)


	max_mine = (-1,-1,-1,-1,-1)
	max_oski = (-1,-1,-1,-1,-1)


	for i in range(1,9):
		for j in range(1,9):
					
			generate_and_run_script(i,j,n,c);

			file2 = open("./test_codes/rose_spmv_bcsr_"+matrix_name+str(i)+"x"+str(j)+".c", "w")
			file = open("rose_spmv_:.cpp", "r")

			generate_code(file, file2);	     

			file.close()
			file2.close()

			

			subprocess.call(["cp", "./test_codes/rose_spmv_bcsr_"+matrix_name+str(i)+"x"+str(j)+".c", "./tests/rose_spmv_bcsr.c"])
			os.chdir("./tests")
			subprocess.call(["./build3"], shell=True)
			#subprocess.call(["./smv", "/home/anand/csr/matrices/cant.csr"])
			os.chdir("..")

			res = run_config(sheet2, row2, matrix_name+"_"+str(i)+"x"+str(j), os.path.join(root, f1),i,j)
			row2 += 4
			os.chdir("./tests/oski_test")
			res2 = run_oski_config(sheet2, row2, os.path.join(root, f1), i,j,n)
			os.chdir("../..") 
			row2 += 3

			if res[2] > max_mine[2]:
			        max_mine = (res[0], res[1], res[2], i, j)
		
			if res2[2] > max_oski[2]:
        			max_oski = (res2[0], res2[1], res2[2], i, j)


	sheet1.write(row1,0,matrix_name)
	sheet1.write(row1,1,str(max_mine[3])+"x"+str(max_mine[4]))
	sheet1.write(row1,2,str(max_oski[3])+"x"+str(max_oski[4]))
	sheet1.write(row1,3,nnz)
	sheet1.write(row1,4,n)
	sheet1.write(row1,5,max_mine[0])
	sheet1.write(row1,6,max_mine[1])
	sheet1.write(row1,7, max_mine[2])
	sheet1.write(row1,8,max_oski[0])
	sheet1.write(row1,9,max_oski[1])
	sheet1.write(row1,10,max_oski[2])
	sheet1.write(row1,11,max_oski[0]/max_mine[0])
	sheet1.write(row1,12,max_oski[1]/max_mine[1])
	row1 +=1



book.save("BCSR.xls")
 
