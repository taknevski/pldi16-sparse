#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define c(i,j) c_count
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
__global__ void spmv_second_level_GPU(int c_count,float *y,int *_P_DATA1,float *_P_DATA2);
__global__ void spmv_GPU(int c_count,float *y,int *_P_DATA1,float *_P_DATA2,int *c_i,float *a,float *x,int *col);
void spmv_inspector(int n,struct inspector &c,int *index);
//#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "smv.h"
#include "matrix_io.h"
#include "sparse_matrix.h"
//#include "spmv.h"
#include <sys/time.h>
#include <math.h>
#include <vector>
//#include "reduce.h"
//void spmv_bcsr(int n, int *index, float *a, float *y, float *x, int *col,int nnz);
//extern void reduce_warp(float *y,float *val, int bound);

struct inspector 
{
  class std::vector< int  > j;
  class std::vector< int  > i;
  int count;
}
;

void spmv(int n,int *index,float *a,float *y,float *x,int *col)
{
  int t14;
  int *devI4Ptr;
  float *devI3Ptr;
  float *devI2Ptr;
  int *devI1Ptr;
  float *devO3Ptr;
  int *devO2Ptr;
  float *devO1Ptr;
  int c_count;
  int t12;
  int _t162;
  int _t161;
  int _t160;
  int _t159;
  int _t158;
  int _t157;
  int _t156;
  int _t155;
  int _t154;
  int _t153;
  int _t152;
  int _t151;
  int _t150;
  int _t149;
  int _t148;
  int _t147;
  int _t146;
  int _t145;
  int _t144;
  float _P6[512];
  int _t121;
  int _t120;
  int _t119;
  int _t118;
  int _t117;
  class std::vector< int  > [512]_P5;
  int _t111;
  int _t112;
  int _t109;
  int _t108;
  int t4;
  int t2;
  float _P4[256 * 4];
  int _t103;
  int _t102;
  int _t101;
  int _t100;
  int _t99;
  int _t98;
  int _t97;
  class std::vector< int  > [256 * 4]_P3;
  int _t96;
  int _t95;
  int _t94;
  int _t93;
  int _t92;
  int _t91;
  int _t90;
  int _t89;
  int _t88;
  int _t87;
  int _t86;
  int _t85;
  int _t84;
  int _t83;
  int _t82;
  int _t81;
  int _t80;
  int _t79;
  int _t78;
  int _t77;
  int _t76;
  int _t75;
  int _t74;
  int _t73;
  int _t72;
  int _t71;
  int _t70;
  int _t69;
  int _t68;
  int _t67;
  int _t66;
  int _t65;
  float *_P_DATA2;
  int _t64;
  int _t63;
  int _t62;
  int _t61;
  int _t60;
  int _t59;
  class std::vector< int  > *_P_DATA1;
  int _t58;
  int _t57;
  int _t56;
  int _t55;
  int _t54;
  int _t53;
  int _t52;
  int _t51;
  int _t50;
  int _t49;
  int _t48;
  int _t47;
  int _t46;
  int _t45;
  int _t44;
  int _t43;
  int _t42;
  int _t41;
  int _t40;
  int _t39;
  int _t38;
  int _t37;
  int _t36;
  int _t35;
  float _P2[4 * 64];
  int _t34;
  int _t33;
  int _t32;
  int _t31;
  int _t30;
  int _t29;
  class std::vector< int  > [4 * 64]_P1;
  int _t28;
  int _t27;
  int _t26;
  int _t25;
  int _t24;
  int _t23;
  int _t22;
  int _t20;
  int _t19;
  int _t21;
  int _t18;
  int _t17;
  int t10;
  int t8;
  int t6;
  int _t6;
  int _t5;
  int _t4;
  int _t3;
  int c_inv;
  int coalesced_index;
  int *c_i;
  int *c_j;
  struct inspector c;
  int i;
  int j;
  c.inspector::count = 0;
  spmv_inspector(n,c,index);
  c_count = c.inspector::count;
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * (((c(_t54,_t56) - 1024) / 1024 + 1) * 4))));
  _P_DATA2 = ((float *)(malloc(sizeof(float ) * (((c(_t60,_t62) - 1024) / 1024 + 1) * 4))));
  cudaMalloc(((void **)(&devO1Ptr)),62451 * sizeof(float ));
  cudaMemcpy(devO1Ptr,y,62451 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devO2Ptr)),((c(_t54,_t56) - 1024) / 1024 + 1) * 4 * sizeof(int ));
  cudaMalloc(((void **)(&devO3Ptr)),((c(_t60,_t62) - 1024) / 1024 + 1) * 4 * sizeof(float ));
  cudaMalloc(((void **)(&devI1Ptr)),4007383 * sizeof(int ));
  cudaMemcpy(devI1Ptr,c.i,4007383 * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),4007383 * sizeof(float ));
  cudaMemcpy(devI2Ptr,a,4007383 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),62451 * sizeof(float ));
  cudaMemcpy(devI3Ptr,x,62451 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI4Ptr)),4007383 * sizeof(int ));
  cudaMemcpy(devI4Ptr,col,4007383 * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid1 = dim3((c(t2,t4) - 1024) / 1024 + 1,1);
  dim3 dimBlock1 = dim3(32,4);
  spmv_GPU<<<dimGrid1,dimBlock1>>>(c_count,devO1Ptr,devO2Ptr,devO3Ptr,devI1Ptr,devI2Ptr,devI3Ptr,devI4Ptr);
  cudaMemcpy(y,devO1Ptr,62451 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaMemcpy(_P_DATA1,devO2Ptr,((c(_t54,_t56) - 1024) / 1024 + 1) * 4 * sizeof(int ),cudaMemcpyDeviceToHost);
  cudaFree(devO2Ptr);
  cudaMemcpy(_P_DATA2,devO3Ptr,((c(_t60,_t62) - 1024) / 1024 + 1) * 4 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO3Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  cudaFree(devI4Ptr);
  c_count = c.inspector::count;
  cudaMalloc(((void **)(&devO1Ptr)),62451 * sizeof(float ));
  cudaMemcpy(devO1Ptr,y,62451 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),((c(_t54,_t56) - 1024) / 1024 + 1) * 4 * sizeof(int ));
  cudaMemcpy(devI1Ptr,_P_DATA1,((c(_t54,_t56) - 1024) / 1024 + 1) * 4 * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),((c(_t60,_t62) - 1024) / 1024 + 1) * 4 * sizeof(float ));
  cudaMemcpy(devI2Ptr,_P_DATA2,((c(_t60,_t62) - 1024) / 1024 + 1) * 4 * sizeof(float ),cudaMemcpyHostToDevice);
  dim3 dimGrid2 = dim3(1,1);
  dim3 dimBlock2 = dim3(4,256);
  spmv_second_level_GPU<<<dimGrid2,dimBlock2>>>(c_count,devO1Ptr,devI1Ptr,devI2Ptr);
  cudaMemcpy(y,devO1Ptr,62451 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  for (t6 = 1024 * (c(t2,t4) / 1024); t6 <= c(t2,t4) - 1; t6 += 512) 
    for (t8 = 0; t8 <= __rose_lt(511,c(t2,t4) - t6 - 1); t8 += 1) {
      _P5[t8] = c.inspector::i[t8 + t6];
      _P6[t8] = (a[t8 + t6] * x[col[t8 + t6]]);
      y[_P5[t8]] += _P6[t8];
    }
}

int main(int argc,char **argv)
{
/////////////////////////////////////////////////////////////////////
  int num;
  int retval;
  struct timeval tv1;
  struct timeval tv2;
  struct timeval tv3;
  struct timeval tv4;
  struct timeval tv5;
//struct timezone tz;
  char filename[255UL];
  struct sparse_matrix A;
  struct sparse_matrix M;
  float *x;
  float *b;
  float *b2;
  int num_threads;
//pthread_attr_t attr;
  strcpy(filename,argv[1]);
  if (1) {
    fprintf(stderr,"main::Loading matrix %s\n",filename);
//fprintf(stderr, "R: %d C: %d\n", R, C);	
  }
  load_sparse_matrix(filename,&A);
//initialize b to 0 and x to 1
  int i;
  int ii;
  x = ((float *)(malloc((sizeof(float ) * A.sparse_matrix::ncols))));
  b = ((float *)(malloc((sizeof(float ) * A.sparse_matrix::nrows))));
  b2 = ((float *)(malloc((sizeof(float ) * A.sparse_matrix::nrows))));
  FILE *fp = 0L;
  FILE *fp1 = 0L;
  FILE *fp2 = 0L;
  FILE *fp3 = 0L;
  for (i = 0; i < A.sparse_matrix::ncols; i++) 
    x[i] = 1.0;
  for (i = 0; i < A.sparse_matrix::nrows; i++) {
    b[i] = 0;
    b2[i] = 0;
  }
  int nrows;
  int nnz;
  int *rows;
  int *cols;
  REAL *vals;
  smv(&A,x,b);
//spmv(A.nrows,A.rows,A.vals,b2,x,A.cols);
  spmv(A.sparse_matrix::nrows,A.sparse_matrix::rows,A.sparse_matrix::vals,b2,x,A.sparse_matrix::cols);
  for (i = 0; i < A.sparse_matrix::nrows; i++) {
    if (fabs((b2[i] - b[i])) >= 0.1) {
      printf("Values don\'t match at %d, expected %f obtained %f\n",i,b[i],b2[i]);
      break; 
    }
//else	
//	printf("Values match at %d, expected %f obtained %f\n", i, b[i], b2[i]);
  }
  free(x);
  free(b);
  free(b2);
  free(A.sparse_matrix::rows);
  free(A.sparse_matrix::cols);
  free(A.sparse_matrix::vals);
  if (i != A.sparse_matrix::nrows) 
//printf("i  is %d\n", i) ;
    exit(1);
  return 0;
}

void spmv_inspector(int n,struct inspector &c,int *index)
{
  int t2;
  int t4;
  for (t2 = 0; t2 <= n - 1; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
      c.inspector::j[c.inspector::count] = t4;
      c.inspector::i[c.inspector::count] = t2;
      c.inspector::count = c.inspector::count + 1;
    }
}

__global__ void spmv_GPU(int c_count,float *y,int *_P_DATA1,float *_P_DATA2,int *c_i,float *a,float *x,int *col)
{
  int by_warp;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  struct inspector c;
  int t6;
  int t8;
  int t10;
  __device__ __shared__ class std::vector< int  > [4 * 64]_P1;
  __device__ __shared__ float _P2[4 * 64];
  int t2;
  int t4;
  int t12;
  for (by_warp = 0; by_warp <= 6; by_warp += 1) {
    _P1[tx + ty * 64] = c_i[32 * by_warp + 1024 * bx + 256 * ty + tx];
    _P2[tx + ty * 64] = (a[32 * by_warp + 1024 * bx + 256 * ty + tx] * x[col[32 * by_warp + 1024 * bx + 256 * ty + tx]]);
    segreduce_warp(&y[0],&_P1[0 + ty * 64],&_P2[0 + ty * 64],by_warp);
  }
  _P1[tx + ty * 64] = c_i[32 * 7 + 1024 * bx + 256 * ty + tx];
  _P2[tx + ty * 64] = (a[32 * 7 + 1024 * bx + 256 * ty + tx] * x[col[32 * 7 + 1024 * bx + 256 * ty + tx]]);
  segreduce_warp(&y[0],&_P1[0 + ty * 64],&_P2[0 + ty * 64]);
  _P_DATA1[ty + bx * 4] = _P1[31 + ty * 64];
  _P_DATA2[ty + bx * 4] = _P2[31 + ty * 64];
}

__global__ void spmv_second_level_GPU(int c_count,float *y,int *_P_DATA1,float *_P_DATA2)
{
  int k;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  int t6;
  int t8;
  int t10;
  __device__ __shared__ class std::vector< int  > [256 * 4]_P3;
  __device__ __shared__ float _P4[256 * 4];
  int t2;
  int t4;
  for (k = 0; k <= (c(t2,t4) - 1024) / 262144; k += 1) 
    if (ty <= (c(t2,t4) - 262144 * k - 1024) / 1024) {
      _P3[tx + ty * 4] = _P_DATA1[tx + (256 * k + ty) * 4];
      _P4[tx + ty * 4] = _P_DATA2[tx + (256 * k + ty) * 4];
      segreduce_block(&y[0],&_P3[0 + 0 * 4],&_P4[0 + 0 * 4],__rose_lt(255,(-(262144 * k) + c(1,0) - 1024) / 1024));
    }
}
