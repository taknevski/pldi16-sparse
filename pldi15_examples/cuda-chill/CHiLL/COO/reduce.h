__device__ void segreduce_warp(float *y,int* idx, float *val, int iteration)
{
    float left = 0;
    int tx = threadIdx.x;
    int id = idx[tx];
   if(tx > 0)
    idx[tx+32] = -1;
   if(iteration > 0)
    if(tx == 0)
     if(idx[32] == id)
        val[0] +=  val[32];

    if( id == idx[(tx + 63) & 63] ) { left = val[tx -   1]; }val[tx] += left; left = 0;
    if( id == idx[(tx + 62) & 63] ) { left = val[tx -   2]; }val[tx] += left; left = 0;
    if( id == idx[(tx + 60) & 63] ) { left = val[tx -   4]; }val[tx] += left; left = 0;
    if( id == idx[(tx + 56) & 63] ) { left = val[tx -   8]; }val[tx] += left; left = 0;
    if( id == idx[(tx + 48) & 63] ) { left = val[tx -  16]; }val[tx] += left; left = 0;


   if( tx < 31 && id != idx[tx + 1])
           y[id] += val[tx];
    if(tx == 31){
       if(iteration > 0)
         if(idx[32] != idx[0])
           y[idx[32]] += val[32];


         idx[32] = id;
         val[32] = val[31];
 }
}

__device__ void segreduce_warp(float *y,int* idx, float *val)
{
    float left = 0;
    int tx = threadIdx.x;
    int id = idx[tx];
   if(tx > 0)
    idx[tx+32] = -1;

    if(tx == 0)
     if(idx[32] == id)
        val[0] +=  val[32];

    if( id == idx[(tx + 63) & 63] ) { left = val[tx -   1]; }val[tx] += left; left = 0;
    if( id == idx[(tx + 62) & 63] ) { left = val[tx -   2]; }val[tx] += left; left = 0;
    if( id == idx[(tx + 60) & 63] ) { left = val[tx -   4]; }val[tx] += left; left = 0;
    if( id == idx[(tx + 56) & 63] ) { left = val[tx -   8]; }val[tx] += left; left = 0;
    if( id == idx[(tx + 48) & 63] ) { left = val[tx -  16]; }val[tx] += left; left = 0;


   if( tx < 31 && id != idx[tx + 1])
           y[id] += val[tx];
    if(tx == 31){
         if(idx[32] != idx[0])
           y[idx[32]] += val[32];
 }
}

__device__ void segreduce_block(float *y, int* idx, float *val, int bound)
{

  __syncthreads();
    float left = 0;

    int tx = threadIdx.x + blockDim.x*threadIdx.y; //+ DIM_Y2*DIM_X*threadIdx.z;
    int ty = threadIdx.y;
    int id = idx[tx];

    if( tx >=   1 && id  == idx[tx -   1] ) { left = val[tx -   1]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=   2 && id  == idx[tx -   2] ) { left = val[tx -   2]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=   4 && id == idx[tx -   4] ) { left = val[tx -   4]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=   8 && id == idx[tx -   8] ) { left = val[tx -   8]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=  16 && id == idx[tx -  16] ) { left = val[tx -  16]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=  32 && id == idx[tx -  32] ) { left = val[tx -  32]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=  64 && id == idx[tx -  64] ) { left = val[tx -  64]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >= 128 && id == idx[tx - 128] ) { left = val[tx - 128]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >= 256 && id == idx[tx - 256] ) { left = val[tx - 256]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >= 512 && id == idx[tx - 512] ) { left = val[tx - 512]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();


  if(ty < bound){
   if(id != idx[tx + 1])
         y[id] += val[tx];
   }
   else if(tx < blockDim.x - 1){
    if(id != idx[tx + 1])
     y[id] += val[tx];
   }else
     y[id] += val[tx];


  __syncthreads();
}

__device__ void segreduce_block2(float *y, int* idx, float *val, int bound)
{
     __syncthreads();
    float left = 0;
    int tx = threadIdx.x; //+ DIM_Y2*DIM_X*threadIdx.z;
    int id = idx[tx];

    if( tx >=   1 && id  == idx[tx -   1] ) { left = val[tx -   1]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=   2 && id  == idx[tx -   2] ) { left = val[tx -   2]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=   4 && id == idx[tx -   4] ) { left = val[tx -   4]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=   8 && id == idx[tx -   8] ) { left = val[tx -   8]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=  16 && id == idx[tx -  16] ) { left = val[tx -  16]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=  32 && id == idx[tx -  32] ) { left = val[tx -  32]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=  64 && id == idx[tx -  64] ) { left = val[tx -  64]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >= 128 && id == idx[tx - 128] ) { left = val[tx - 128]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >= 256 && id == idx[tx - 256] ) { left = val[tx - 256]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >= 512 && id == idx[tx - 512] ) { left = val[tx - 512]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
 if(tx  < bound ){

   if(id != idx[tx + 1])
         y[id] += val[tx];
 }
 else
   y[id] += val[tx];
 __syncthreads();
}
