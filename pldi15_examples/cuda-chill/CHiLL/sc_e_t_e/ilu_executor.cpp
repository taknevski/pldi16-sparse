#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define colidx_(i,k) colidx[1 * k]
#define colidx__(ip,kp) colidx[1 * kp]
#define colidx___(i,k) colidx[1 * k + 1]
#define colidx____(ip,kp) colidx[1 * kp + 1]
#define colidx_p(i,k,ip,kp) colidx[1 * kp]
#define diagptr_(i) diagptr[1 * i]
#define diagptr___(i) diagptr[1 * i + 1]
#define diagptr___p(i,k,ip) diagptr[1 * ip + 1]
#define diagptr_colidx__(i,k) diagptr[1 * colidx_(i,k)]
#define diagptr_colidx___(ip,kp) diagptr[1 * colidx__(i,k)]
#define diagptr_colidx____(i,k) diagptr[1 * colidx_(i,k) + 1]
#define diagptr_colidx_____(ip,kp) diagptr[1 * colidx__(i,k) + 1]
#define diagptr_colidx____p(i,k,ip,kp) diagptr[1 * colidx_(i,kp) + 1]
#define diagptr_colidx__p(i,k,ip,kp) diagptr[1 * colidx_(i,kp)]
#define diagptr_p(i,k,ip) diagptr[1 * ip]
#define rowptr_(i) rowptr[1 * i]
#define rowptr__(i) rowptr[1 * i + 1]
#define rowptr___(i) rowptr[1 * i + 1]
#define rowptr____(i) rowptr[1 * i]
#define rowptr__p(i,k,ip) rowptr[1 * ip + 1]
#define rowptr_colidx__(i,k) rowptr[1 * colidx_(i,k) + 1]
#define rowptr_colidx____(i,k) rowptr[1 * colidx_(i,k)]
#define rowptr_colidx____p(i,k,ip,kp) rowptr[1 * colidx_(i,kp)]
#define rowptr_colidx__p(i,k,ip,kp) rowptr[1 * colidx_(i,kp) + 1]
#define rowptr_p(i,k,ip) rowptr[1 * ip]
#define taskBoundaries_(i) schedule -> taskBoundaries[i]
#define taskBoundaries__(i) schedule -> taskBoundaries[i + 1]
#define threadBoundaries_(i) schedule -> threadBoundaries[i]
#define threadBoundaries__(i) schedule -> threadBoundaries[i + 1]
#include <omp.h>
#include "LevelSchedule.hpp"
#include <stdio.h>
#include <string.h>
#include <algorithm>
#include <stdlib.h>
//CSR A;
using namespace std;

void ilu_executor(double *lu, const SpMP::CSR& A, const SpMP::LevelSchedule *schedule) {

   int task;
   int idx;
   volatile int zplanes[256];
   int right;
   int left;
   int num_threads;
   int tid;
   int ub;
   int t8;
   int t4;
   int t2;
   int _t5;
   int _t4;
   int _t3;
   int In_1;
   int _t2;
   int _t1;
   int *perm_inv;
   int *perm;
   int cnt;
   int j;
   int c;
   int source_offset;
   int acc;
   int t6;
   int found;

   int *_P_DATA4;
   int *_P_DATA3;
   int *_P_DATA2;
   int *_P_DATA1;
   int ip;
   int kp;
   int i;
   int k;
   int j1;
   int j2;
   double tmp;

   perm = schedule -> SpMP::LevelSchedule::origToThreadContPerm;
   perm_inv = schedule -> SpMP::LevelSchedule::threadContToOrigPerm;
   int base = A.getBase();

   const int *rowptr = A.rowptr - base;
   const int *colidx = A.colidx - base;
   const int *diagptr = A.diagptr - base;
   const double *values = A.values - base;

   lu -= base;
#pragma omp parallel  private(tid,t6,t4,t8, j1,j2) 
   {



      int nthreads;
      tid = omp_get_thread_num();
      nthreads = omp_get_num_threads();


#pragma omp for
      for (int i = base; i < A.getNnz() + base; i++) {
	 lu[i] = values[i];
      }


      const int ntasks = schedule -> ::SpMP::LevelSchedule::ntasks;
      int task;
      const short *nparents = schedule -> ::SpMP::LevelSchedule::nparentsForward;
      int nPerThread = (ntasks + nthreads - 1) / nthreads;
      int nBegin = __rose_lt(nPerThread * tid,ntasks);
      int nEnd = __rose_lt(nBegin + nPerThread,ntasks);
      volatile int *taskFinished = schedule -> ::SpMP::LevelSchedule::taskFinished;
      int **parents = schedule -> ::SpMP::LevelSchedule::parentsForward;
      memset(((char *)(taskFinished + nBegin)),0,(nEnd - nBegin) * sizeof(int ));
#pragma omp barrier 
      {
	 for (t4 = threadBoundaries_(tid); t4 <= threadBoundaries__(tid) - 1; t4 += 1) {{
	    task = t4;
	    SPMP_LEVEL_SCHEDULE_WAIT;
	    for (t6 = taskBoundaries_(t4); t6 <= taskBoundaries__(t4) - 1; t6 += 1) 
	       if(rowptr_(perm_inv[t6]) < diagptr_(perm_inv[t6])) 
	       for (t8 = rowptr_(perm_inv[t6]); t8 <= diagptr_(perm_inv[t6]) - 1; t8 += 1) {
		  //c = colidx[k];
		  // a_ik /= a_kk
		  lu[t8] = (lu[t8] / lu[diagptr[colidx[t8]]]);
		  j1 = (t8 + 1);
		  j2 = (diagptr[colidx[t8]] + 1);
		  while((j1 < rowptr[perm_inv[t6] + 1]) && (j2 < rowptr[colidx[t8] + 1])){
		     if (colidx[j1] == colidx[j2]) {
			// a_ij -= a_ik*a_kj
			lu[j1] -= (lu[t8] * lu[j2]);
			++j1;
			++j2;
		     }
		     else if (colidx[j1] < colidx[j2]) 
			++j1;
		     else 
			++j2;
		  }
	       }
	 }
	 SPMP_LEVEL_SCHEDULE_NOTIFY;
	 }
      }
   }
}


