#include "LevelSchedule.hpp"
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "test.hpp"
#include <omp.h>

LevelSchedule  *chill_ilu0(double *lu, const CSR& A) {
   int base = A.getBase();

   const int *rowptr = A.rowptr - base;
   const int *colidx = A.colidx - base;
   const int *diagptr = A.diagptr - base;
   double *values = A.values - base;

   lu -= base;
   int m = A.m;
   int i,k;
//   int tid = omp_get_thread_num();

#pragma omp for
   for (int i = base; i < A.getNnz() + base; i++) {
      lu[i] = values[i];
   }


  for(i=0; i < m; i++)
    {
       for(k= rowptr[i]; k < diagptr[i]; k++)
        {
          //c = colidx[k];
          lu[k] = lu[k]/lu[diagptr[colidx[k]]]; // a_ik /= a_kk
          int j1 = k + 1;
          int j2 = diagptr[colidx[k]] + 1;

          while (j1 < rowptr[i + 1] && j2 <rowptr[colidx[k] + 1]) {
            if(colidx[j1] == colidx[j2]){
              lu[j1] -= lu[k]*lu[j2]; // a_ij -= a_ik*a_kj
              ++j1; ++j2;
            }
            else if (colidx[j1] < colidx[j2]) ++j1;
            else
                ++j2;
          }
        }
    }
   return NULL;
}

