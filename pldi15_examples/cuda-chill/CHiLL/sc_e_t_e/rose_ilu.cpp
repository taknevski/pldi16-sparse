#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define colidx_(i,k) colidx[1 * k]
#define colidx__(ip,kp) colidx[1 * kp]
#define colidx___(i,k) colidx[1 * k + 1]
#define colidx____(ip,kp) colidx[1 * kp + 1]
#define colidx_p(i,k,ip,kp) colidx[1 * kp]
#define diagptr_(i) diagptr[1 * i]
#define diagptr___(i) diagptr[1 * i + 1]
#define diagptr___p(i,k,ip) diagptr[1 * ip + 1]
#define diagptr_colidx__(i,k) diagptr[1 * colidx_(i,k)]
#define diagptr_colidx___(ip,kp) diagptr[1 * colidx__(i,k)]
#define diagptr_colidx____(i,k) diagptr[1 * colidx_(i,k) + 1]
#define diagptr_colidx_____(ip,kp) diagptr[1 * colidx__(i,k) + 1]
#define diagptr_colidx____p(i,k,ip,kp) diagptr[1 * colidx_(i,kp) + 1]
#define diagptr_colidx__p(i,k,ip,kp) diagptr[1 * colidx_(i,kp)]
#define diagptr_p(i,k,ip) diagptr[1 * ip]
#define rowptr_(i) rowptr[1 * i]
#define rowptr__(i) rowptr[1 * i + 1]
#define rowptr___(i) rowptr[1 * i + 1]
#define rowptr____(i) rowptr[1 * i]
#define rowptr__p(i,k,ip) rowptr[1 * ip + 1]
#define rowptr_colidx__(i,k) rowptr[1 * colidx_(i,k) + 1]
#define rowptr_colidx____(i,k) rowptr[1 * colidx_(i,k)]
#define rowptr_colidx____p(i,k,ip,kp) rowptr[1 * colidx_(i,kp)]
#define rowptr_colidx__p(i,k,ip,kp) rowptr[1 * colidx_(i,kp) + 1]
#define rowptr_p(i,k,ip) rowptr[1 * ip]
#define taskBoundaries_(i) schedule.taskBoundaries[i]
#define taskBoundaries__(i) schedule.taskBoundaries[i + 1]
#define threadBoundaries_(i) schedule.threadBoundaries[i]
#define threadBoundaries__(i) schedule.threadBoundaries[i + 1]
#include "LevelSchedule.hpp"
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "test.hpp"
#include <omp.h>

class SpMP::LevelSchedule *chill_ilu0(double *lu,const class SpMP::CSR &A)
{
  int task;
  int idx;
  volatile int zplanes[256];
  int right;
  int left;
  int num_threads;
  int tid;
  int ub;
  int t8;
  int t4;
  int t2;
  int _t21;
  int _t20;
  int _t19;
  int In_1;
  int _t18;
  int _t17;
  int *perm_inv;
  int *perm;
  int cnt;
  int tmp;
  int j;
  int c;
  int source_offset;
  int acc;
  int t6;
  int _t16;
  int _t15;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t6;
  int _t5;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int found;
  static class SpMP::LevelSchedule schedule;
  class SpMP::CSR *B;
  int *_P_DATA4;
  int *_P_DATA3;
  int *_P_DATA2;
  int *_P_DATA1;
  static int inspected_already = 0;
  int ip;
  int kp;
  int base = A. getBase ();
  const int *rowptr = (A.SpMP::CSR::rowptr - base);
  const int *colidx = (A.SpMP::CSR::colidx - base);
  const int *diagptr = (A.SpMP::CSR::diagptr - base);
  double *values = (A.SpMP::CSR::values - base);
  lu -= base;
  int m = A.SpMP::CSR::m;
  int i;
  int k;
//   int tid = omp_get_thread_num();
  
#pragma omp for
  for (int i = base; i < (A. getNnz () + base); i++) {
    lu[i] = values[i];
  }
  if (inspected_already == 0) {
    _P_DATA1 = ((int *)(malloc(sizeof(int ) * m)));
    _P_DATA2 = ((int *)(malloc(sizeof(int ) * m)));
  }
  if (inspected_already == 0) {
    B = (new SpMP::CSR );
    B -> SpMP::CSR::rowptr = ((int *)(malloc(sizeof(int ) * (m + 1))));
    B -> SpMP::CSR::diagptr = ((int *)(_mm_malloc(sizeof(int ) * m,64)));
    B -> SpMP::CSR::extptr = 0;
    B -> SpMP::CSR::m = m;
  }
  if (inspected_already == 0) {
#pragma omp parallel 
{
      
#pragma omp for 
      for (t2 = 0; t2 <= m - 1; t2 += 1) {
        B -> ::SpMP::CSR::rowptr[t2] = 0;
        _P_DATA1[t2] = 0;
      }
    }
    B -> SpMP::CSR::rowptr[m] = 0;
#pragma omp parallel  private(t6,found,t4,t8)
{
      
#pragma omp for 
      for (t2 = 0; t2 <= m - 1; t2 += 1) 
        if (rowptr_(t2) + 1 <= diagptr_(t2)) 
          for (t4 = rowptr_(t2); t4 <= diagptr_(t2) - 1; t4 += 1) {
            if (t2 + 1 <= colidx_(t2,t4)) {
              found = 0;
              found = 0;
            }
            else if (colidx_(t2,t4) + 1 <= t2) {
              found = 0;
              found = 0;
            }
            t6 = colidx_(t2,t4);
            if (diagptr_colidx__(t2,t4) + 2 <= rowptr_colidx__(t2,t4)) 
              if (colidx_(t2,t4) + 1 <= t2) 
                for (t8 = rowptr_p(t2,t4,t6); t8 <= diagptr_p(t2,t4,t6) - 1; t8 += 1) {
                  found = 1;
                  if (found == 1) 
                    break; 
                }
              else if (t2 + 1 <= colidx_(t2,t4)) 
                for (t8 = rowptr_p(t2,t4,t6); t8 <= diagptr_p(t2,t4,t6) - 1; t8 += 1) {
                  found = 1;
                  if (found == 1) 
                    break; 
                }
            if (t2 + 1 <= colidx_(t2,t4)) 
              if (found == 1) 
                _P_DATA1[t2] = _P_DATA1[t2] + 1; else {}
            else if (colidx_(t2,t4) + 1 <= t2) 
              if (found == 1) 
                _P_DATA1[t2] = _P_DATA1[t2] + 1;
          }
    }
    for (t2 = 0; t2 <= m - 1; t2 += 1) 
      B -> SpMP::CSR::rowptr[t2 + 1] = B -> SpMP::CSR::rowptr[t2] + _P_DATA1[t2];
#pragma omp parallel  private(t4,t8)
{
      
#pragma omp for 
      for (t2 = 0; t2 <= m - 1; t2 += 1) 
        _P_DATA1[t2] = 0;
    }
    B -> SpMP::CSR::colidx = ((int *)(malloc(sizeof(int ) * B -> SpMP::CSR::rowptr[m])));
    memset(B -> SpMP::CSR::colidx,-1,B -> SpMP::CSR::rowptr[m]);
#pragma omp parallel  private(t6,found,t4,t8)
{
      
#pragma omp for 
      for (t2 = 0; t2 <= m - 1; t2 += 1) 
        if (rowptr_(t2) + 1 <= diagptr_(t2)) 
          for (t4 = rowptr_(t2); t4 <= diagptr_(t2) - 1; t4 += 1) {
            if (colidx_(t2,t4) + 1 <= t2) 
              found = 0;
            else if (t2 + 1 <= colidx_(t2,t4)) 
              found = 0;
            if (colidx_(t2,t4) + 1 <= t2) {
              t6 = colidx_(t2,t4);
              if (diagptr_p(t2,t4,t6) + 2 <= rowptr__p(t2,t4,t6)) 
                for (t8 = rowptr_p(t2,t4,t6); t8 <= diagptr_p(t2,t4,t6) - 1; t8 += 1) {
                  found = 1;
                  if (found == 1) 
                    break; 
                }
            }
            else if (t2 + 1 <= colidx_(t2,t4)) {
              t6 = colidx_(t2,t4);
              if (diagptr_p(t2,t4,t6) + 2 <= rowptr__p(t2,t4,t6)) 
                for (t8 = rowptr_p(t2,t4,t6); t8 <= diagptr_p(t2,t4,t6) - 1; t8 += 1) {
                  found = 1;
                  if (found == 1) 
                    break; 
                }
            }
            if (colidx_(t2,t4) + 1 <= t2) 
              if (found == 1) 
                if (find(B -> ::SpMP::CSR::colidx + B -> ::SpMP::CSR::rowptr[t2],B -> ::SpMP::CSR::colidx + (B -> ::SpMP::CSR::rowptr[t2] + _P_DATA1[t2]),t6) == B -> ::SpMP::CSR::colidx + B -> ::SpMP::CSR::rowptr[t2] + _P_DATA1[t2]) {
                  B -> ::SpMP::CSR::colidx[B -> ::SpMP::CSR::rowptr[t2] + _P_DATA1[t2]] = t6;
                  _P_DATA1[t2] = _P_DATA1[t2] + 1;
                } else {} else {}
            else if (t2 + 1 <= colidx_(t2,t4)) 
              if (found == 1) 
                if (find(B -> ::SpMP::CSR::colidx + B -> ::SpMP::CSR::rowptr[t2],B -> ::SpMP::CSR::colidx + (B -> ::SpMP::CSR::rowptr[t2] + _P_DATA1[t2]),t6) == B -> ::SpMP::CSR::colidx + B -> ::SpMP::CSR::rowptr[t2] + _P_DATA1[t2]) {
                  B -> ::SpMP::CSR::colidx[B -> ::SpMP::CSR::rowptr[t2] + _P_DATA1[t2]] = t6;
                  _P_DATA1[t2] = _P_DATA1[t2] + 1;
                }
          }
    }
    acc = 0;
    for (t2 = 0; t2 <= m - 1; t2 += 1) 
      acc += _P_DATA1[t2];
    _P_DATA3 = ((int *)(malloc(sizeof(int ) * acc)));
    source_offset = 0;
    for (t2 = 0; t2 <= m - 1; t2 += 1) {
      memcpy(_P_DATA3 + B -> SpMP::CSR::rowptr[t2],B -> SpMP::CSR::colidx + source_offset,_P_DATA1[t2] * sizeof(int ));
      source_offset = B -> SpMP::CSR::rowptr[t2 + 1];
      B -> SpMP::CSR::rowptr[t2 + 1] = B -> SpMP::CSR::rowptr[t2] + _P_DATA1[t2];
    }
    free(B -> SpMP::CSR::colidx);
    B -> SpMP::CSR::colidx = _P_DATA3;
#pragma omp parallel  private(t4,t8)
{
      
#pragma omp for 
      for (t2 = 0; t2 <= m - 1; t2 += 1) 
        sort(B -> ::SpMP::CSR::colidx + B -> ::SpMP::CSR::rowptr[t2],B -> ::SpMP::CSR::colidx + B -> ::SpMP::CSR::rowptr[t2 + 1]);
    }
#pragma omp parallel  private(t4,t8)
{
      
#pragma omp for 
      for (t2 = 0; t2 <= m - 1; t2 += 1) 
        _P_DATA2[t2] = 0;
    }
#pragma omp parallel  private(j,c,t4,t8)
{
      
#pragma omp for 
      for (t2 = 0; t2 <= m - 1; t2 += 1) {
        if (!binary_search(B -> ::SpMP::CSR::colidx + B -> ::SpMP::CSR::rowptr[t2],B -> ::SpMP::CSR::colidx + B -> ::SpMP::CSR::rowptr[t2 + 1],t2)) 
          __sync_fetch_and_add(_P_DATA2 + t2,1);
        for (j = B -> ::SpMP::CSR::rowptr[t2]; j <= B -> ::SpMP::CSR::rowptr[t2 + 1] - 1; j += 1) {
          c = B -> ::SpMP::CSR::colidx[j];
          if (!binary_search(B -> ::SpMP::CSR::colidx + B -> ::SpMP::CSR::rowptr[c],B -> ::SpMP::CSR::colidx + B -> ::SpMP::CSR::rowptr[c + 1],t2)) 
            __sync_fetch_and_add(_P_DATA2 + c,1);
        }
      }
    }
    acc = 0;
    for (t2 = 0; t2 <= m - 1; t2 += 1) 
      acc += _P_DATA2[t2];
    _P_DATA4 = ((int *)(malloc(sizeof(int ) * (B -> SpMP::CSR::rowptr[m] + acc))));
    source_offset = 0;
    for (t2 = 0; t2 <= m - 1; t2 += 1) {
      memcpy(_P_DATA4 + B -> SpMP::CSR::rowptr[t2],B -> SpMP::CSR::colidx + source_offset,(_P_DATA1[t2] + _P_DATA2[t2]) * sizeof(int ));
      tmp = _P_DATA2[t2];
      _P_DATA2[t2] = B -> SpMP::CSR::rowptr[t2 + 1] - source_offset;
      source_offset = B -> SpMP::CSR::rowptr[t2 + 1];
      B -> SpMP::CSR::rowptr[t2 + 1] = B -> SpMP::CSR::rowptr[t2] + _P_DATA1[t2] + tmp;
    }
    free(B -> SpMP::CSR::colidx);
    B -> SpMP::CSR::colidx = _P_DATA4;
#pragma omp parallel  private(cnt,c,j,t4,t8)
{
      
#pragma omp for 
      for (t2 = 0; t2 <= m - 1; t2 += 1) {
        if (!binary_search(B -> ::SpMP::CSR::colidx + B -> ::SpMP::CSR::rowptr[t2],B -> ::SpMP::CSR::colidx + B -> ::SpMP::CSR::rowptr[t2] + _P_DATA2[t2],t2)) {
          cnt = __sync_fetch_and_add(_P_DATA1 + t2,1);
          B -> ::SpMP::CSR::colidx[B -> ::SpMP::CSR::rowptr[t2] + cnt] = t2;
        }
        for (j = B -> ::SpMP::CSR::rowptr[t2]; j <= B -> ::SpMP::CSR::rowptr[t2] + _P_DATA2[t2] - 1; j += 1) {
          c = B -> ::SpMP::CSR::colidx[j];
          if (!binary_search(B -> ::SpMP::CSR::colidx + B -> ::SpMP::CSR::rowptr[c],B -> ::SpMP::CSR::colidx + B -> ::SpMP::CSR::rowptr[c] + _P_DATA2[c],t2)) {
            cnt = __sync_fetch_and_add(_P_DATA1 + c,1);
            B -> ::SpMP::CSR::colidx[B -> ::SpMP::CSR::rowptr[c] + cnt] = t2;
          }
        }
      }
    }
#pragma omp parallel  private(j,t4,t8)
{
      
#pragma omp for 
      for (t2 = 0; t2 <= m - 1; t2 += 1) {
        sort(B -> ::SpMP::CSR::colidx + B -> ::SpMP::CSR::rowptr[t2],B -> ::SpMP::CSR::colidx + B -> ::SpMP::CSR::rowptr[t2 + 1]);
        for (j = B -> ::SpMP::CSR::rowptr[t2]; j <= B -> ::SpMP::CSR::rowptr[t2 + 1] - 1; j += 1) 
          if (B -> ::SpMP::CSR::colidx[j] == t2) 
            B -> ::SpMP::CSR::diagptr[t2] = j;
      }
    }
    schedule. constructTaskGraph ( *B);
  }
  if (inspected_already == 1) {
    perm = schedule.SpMP::LevelSchedule::threadContToOrigPerm;
    perm_inv = schedule.SpMP::LevelSchedule::origToThreadContPerm;
#pragma omp parallel  private(tid,t6,t4,t8) num_threads(12)
{
      int nthreads;
      tid = omp_get_thread_num();
      nthreads = omp_get_num_threads();
      const int ntasks = schedule.::SpMP::LevelSchedule::ntasks;
      int task;
      const short *nparents = schedule.::SpMP::LevelSchedule::nparentsForward;
      int nPerThread = (ntasks + nthreads - 1) / nthreads;
      int nBegin = __rose_lt(nPerThread * tid,ntasks);
      int nEnd = __rose_lt(nBegin + nPerThread,ntasks);
      volatile int *taskFinished = schedule.::SpMP::LevelSchedule::taskFinished;
      int **parents = schedule.::SpMP::LevelSchedule::parentsForward;
      memset(((char *)(taskFinished + nBegin)),0,(nEnd - nBegin) * sizeof(int ));
#pragma omp barrier 
{
        for (t4 = threadBoundaries_(tid); t4 <= threadBoundaries__(tid) - 1; t4 += 1) {{
            task = t4;
            SPMP_LEVEL_SCHEDULE_WAIT;
            for (t6 = taskBoundaries_(t4); t6 <= taskBoundaries__(t4) - 1; t6 += 1) 
              if (rowptr_(perm[t6]) + 1 <= diagptr_(perm[t6])) 
                for (t8 = rowptr_(perm[t6]); t8 <= diagptr_(perm[t6]) - 1; t8 += 1) {
//c = colidx[k];
// a_ik /= a_kk
                  lu[t8] = (lu[t8] / lu[diagptr[colidx[t8]]]);
                  int j1 = (t8 + 1);
                  int j2 = (diagptr[colidx[t8]] + 1);
                  while((j1 < rowptr[perm[t6] + 1]) && (j2 < rowptr[colidx[t8] + 1])){
                    if (colidx[j1] == colidx[j2]) {
// a_ij -= a_ik*a_kj
                      lu[j1] -= (lu[t8] * lu[j2]);
                      ++j1;
                      ++j2;
                    }
                    else if (colidx[j1] < colidx[j2]) 
                      ++j1;
                    else 
                      ++j2;
                  }
                }
          }
          SPMP_LEVEL_SCHEDULE_NOTIFY;
        }
      }
    }
  }
  if (inspected_already == 0) {
    free(_P_DATA1);
    free(_P_DATA2);
    free(B -> SpMP::CSR::rowptr);
    free(B -> SpMP::CSR::colidx);
  }
  if (inspected_already == 0) 
    delete B;
  inspected_already = 1;
  return &schedule;
  return 0L;
}
