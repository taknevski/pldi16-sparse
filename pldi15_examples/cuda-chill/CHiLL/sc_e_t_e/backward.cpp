#include "LevelSchedule.hpp"
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "test.hpp"
#include <omp.h>
using namespace std;
using namespace SpMP;


LevelSchedule *chillBackwardSolve(const CSR& A, double y[], const double b[]) {
   ADJUST_FOR_BASE;
   int i,j; 
   int n = A.m;
   assert(base == 0);
   for (i = n - 1; i >= 0; i--) {
      double sum = b[i];
      for (j = rowptr[i]; j < rowptr[i + 1];j++) {
         sum -= values[j] * y[colidx[j]];
      }
      y[i] = sum * idiag[i];
   } // for each row

   return NULL;
}




/*
void chillBackwardSolve(const CSR& A, double y[], const double b[]) {
   ADJUST_FOR_BASE;



  int i,k;
  int n = A.m;
 // double sum;
  assert(base==0);  

  for (i = 0; i < n; i++) {
    double sum = b[i];
    for (k = rowptr[i]; k < rowptr[i + 1]; k++) {
      sum -= values[k]*y[colidx[k]];
    }
    y[i] = sum*idiag[i];
  } // for each row












}
*/
