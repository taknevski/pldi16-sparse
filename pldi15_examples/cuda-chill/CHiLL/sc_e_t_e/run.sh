#!/bin/bash

for i in `cat matrix.lst` 
do
      j=1
      sbatch -N 1 -n 1 -c 12 -J $i -t 360  --qos=premium ~/pldi16-sparse/bench/sc_av/precise_dep.sh $i $j; 
done
