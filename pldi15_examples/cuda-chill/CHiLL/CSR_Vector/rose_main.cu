#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
__global__ void spmv_GPU(int n,float *y,float *a,float *x,int *col,int *index);
//#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "smv.h"
#include "matrix_io.h"
#include "sparse_matrix.h"
//#include "spmv.h"
#include <sys/time.h>
#include <math.h>
#include "reduce.h"
//void spmv_bcsr(int n, int *index, float *a, float *y, float *x, int *col,int nnz);
//extern void reduce_warp(float *y,float *val, int bound);

void spmv(int n,int *index,float *a,float *y,float *x,int *col)
{
  int *devI4Ptr;
  int *devI3Ptr;
  float *devI2Ptr;
  float *devI1Ptr;
  float *devO1Ptr;
  int t14;
  int t12;
  int t10;
  int t8;
  int t6;
  int t4;
  int _t27;
  int _t26;
  int _t25;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  float _P1[16 * 32];
  int _t10;
  int _t13;
  int _t7;
  int _t8;
  int t2;
  int i;
  int j;
  cudaMalloc(((void **)(&devO1Ptr)),62451 * sizeof(float ));
  cudaMemcpy(devO1Ptr,y,62451 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),4007383 * sizeof(float ));
  cudaMemcpy(devI1Ptr,a,4007383 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),62451 * sizeof(float ));
  cudaMemcpy(devI2Ptr,x,62451 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),4007383 * sizeof(int ));
  cudaMemcpy(devI3Ptr,col,4007383 * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI4Ptr)),62452 * sizeof(int ));
  cudaMemcpy(devI4Ptr,index,62452 * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid0 = dim3((n - 1) / 16 + 1,1);
  dim3 dimBlock0 = dim3(32,16);
  spmv_GPU<<<dimGrid0,dimBlock0>>>(n,devO1Ptr,devI1Ptr,devI2Ptr,devI3Ptr,devI4Ptr);
  cudaMemcpy(y,devO1Ptr,62451 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  cudaFree(devI4Ptr);
}

int main(int argc,char **argv)
{
/////////////////////////////////////////////////////////////////////
  int num;
  int retval;
  struct timeval tv1;
  struct timeval tv2;
  struct timeval tv3;
  struct timeval tv4;
  struct timeval tv5;
//struct timezone tz;
  char filename[255UL];
  struct sparse_matrix A;
  struct sparse_matrix M;
  float *x;
  float *b;
  float *b2;
  int num_threads;
//pthread_attr_t attr;
  strcpy(filename,argv[1]);
  if (1) {
    fprintf(stderr,"main::Loading matrix %s\n",filename);
//fprintf(stderr, "R: %d C: %d\n", R, C);	
  }
  load_sparse_matrix(filename,&A);
//initialize b to 0 and x to 1
  int i;
  int ii;
  x = ((float *)(malloc((sizeof(float ) * A.sparse_matrix::ncols))));
  b = ((float *)(malloc((sizeof(float ) * A.sparse_matrix::nrows))));
  b2 = ((float *)(malloc((sizeof(float ) * A.sparse_matrix::nrows))));
  FILE *fp = 0L;
  FILE *fp1 = 0L;
  FILE *fp2 = 0L;
  FILE *fp3 = 0L;
  for (i = 0; i < A.sparse_matrix::ncols; i++) 
    x[i] = 1.0;
  for (i = 0; i < A.sparse_matrix::nrows; i++) {
    b[i] = 0;
    b2[i] = 0;
  }
  int nrows;
  int nnz;
  int *rows;
  int *cols;
  REAL *vals;
  smv(&A,x,b);
//spmv(A.nrows,A.rows,A.vals,b2,x,A.cols);
  spmv(A.sparse_matrix::nrows,A.sparse_matrix::rows,A.sparse_matrix::vals,b2,x,A.sparse_matrix::cols);
  for (i = 0; i < A.sparse_matrix::nrows; i++) {
    if (fabs((b2[i] - b[i])) >= 0.1) {
      printf("Values don\'t match at %d, expected %f obtained %f\n",i,b[i],b2[i]);
      break; 
    }
//else	
//	printf("Values match at %d, expected %f obtained %f\n", i, b[i], b2[i]);
  }
  free(x);
  free(b);
  free(b2);
  free(A.sparse_matrix::rows);
  free(A.sparse_matrix::cols);
  free(A.sparse_matrix::vals);
  if (i != A.sparse_matrix::nrows) 
//printf("i  is %d\n", i) ;
    exit(1);
  return 0;
}

__global__ void spmv_GPU(int n,float *y,float *a,float *x,int *col,int *index)
{
  int jj;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  int t2;
  __device__ __shared__ float _P1[16 * 32];
  int t4;
  int t8;
  int t10;
  if (ty <= n - 16 * bx - 1) {
    if (tx <= index__(ty + 16 * bx) - index_(ty + 16 * bx) - 1) 
      _P1[tx + ty * 32] = 0;
    if (tx <= index__(ty + 16 * bx) - index_(ty + 16 * bx) - 1) {
      for (jj = index_(ty + 16 * bx); jj <= index__(ty + 16 * bx) - tx - 1; jj += 32) 
        _P1[tx + ty * 32] += (a[tx + jj] * x[col[tx + jj]]);
      reduce_warp(&y[ty + 16 * bx],&_P1[tx + ty * 32],__rose_lt(31,-index_(ty + 16 * bx) + index__(ty + 16 * bx) - 1));
    }
  }
}
