__device__ void reduce_warp(float *y,float *val, int bound)
{
    int tx = threadIdx.x;
    float left=0;

    if(tx >= 1) { left = *(val -   1); *val += left; left = 0;}
    if(tx >= 2) { left = *(val -   2); *val += left; left = 0;}
    if(tx >= 4) { left = *(val -   4); *val += left; left = 0;}
    if(tx >= 8) { left = *(val -   8); *val += left; left = 0;}
    if(tx >= 16) { left = *(val -  16);*val += left; left = 0;}

    if (tx == bound)
       *y = *val;


}
