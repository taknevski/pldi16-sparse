init("main.cpp", "spmv", 0)
dofile("cudaize.lua")

--specific to "cant" matrix
N = 62451
Ti = 512
NNZ = 4007383
 

tile_by_index(0,{"i"},{Ti},{l1_control="ii"},{"ii","i","j"})CU=1
cudaize(0,"spmv_GPU",{ a=NNZ,x=N,y=N,col=NNZ,index=N+1},{block={"ii"}, thread={"i"}},{"index", "n"})
known({"index__","index_"},{1,-1}, -1, 0)
print_code(0)
--copy_to_registers(0, "j", "y");
