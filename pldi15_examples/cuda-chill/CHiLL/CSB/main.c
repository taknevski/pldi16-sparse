//#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "smv.h"
#include "matrix_io.h"
#include "sparse_matrix.h"
//#include "csb.h"
#include <sys/time.h>
#include <math.h>
#include <omp.h>
#define NumVc 2

void csb(int m, int n, int *index, int *col, float (*x)[NumVc], float (*y)[NumVc], float *A){

int i,j,k;

for(i=0; i < n; i++){
  for(j=index[i]; j < index[i+1]; j++){
    for(k=0; k < NumVc; k++)
      y[i][k]+= A[j]*x[col[j]][k]; 
  }
  for(j=index[i]; j < index[i+1]; j++){
    for(k=0; k < NumVc; k++)
      y[col[j]][k]+= A[j]*x[i][k]; 
  }
}

}




int main(int argc, char **argv)
{

/////////////////////////////////////////////////////////////////////
   int num, retval;

        struct timeval tv1, tv2,tv3,tv4,tv5;
        //struct timezone tz;
	
	char filename[255], NumV[255];
	struct sparse_matrix A;
	struct sparse_matrix M;
//	float **x, **b, **b2;
	float (*x)[NumVc], (*b)[NumVc], (*b2)[NumVc];
//	float *x, *b, *b2;
	int num_threads, j;
int num_t=0;
//			int NumVc=1;
		double time =0;
	
	strcpy(filename, argv[1]);
//	strcpy(NumV, argv[2]);
//	NumVc = atoi(NumV);

	if (DEBUG){
		fprintf(stderr, "main::Loading matrix %s\n", filename);
	  	//fprintf(stderr, "R: %d C: %d\n", R, C);	
        }

	load_sparse_matrix(filename, &A);

	
	//initialize b to 0 and x to 1
	int i,ii;
	x = (float (*)[NumVc]) malloc(sizeof(float[NumVc]) *A.ncols);
	b = (float (*)[NumVc]) malloc(sizeof(float[NumVc]) * A.ncols);
	b2 = (float (*)[NumVc]) malloc(sizeof(float[NumVc]) * A.ncols);

        FILE *fp = NULL, *fp1 = NULL, *fp2 = NULL, *fp3=NULL;


/*	for(i = 0; i < A.ncols; i++){
            x[i] = (float*)malloc(sizeof(float)*NumVc);
            b[i] = (float*)malloc(sizeof(float)*NumVc);
            b2[i] = (float*)malloc(sizeof(float)*NumVc);

	}
*/
	for(i = 0; i < A.ncols; i++)
	 for(j=0; j < NumVc;j++){	
		x[i][j] = 1.0;
		b[i][j] = 0;
		b2[i][j] = 0;
	 }	


/*
	//for(i = 0; i < A.ncols; i++){
            x = (float*)malloc(sizeof(float)*NumVc*A.ncols);
            b = (float*)malloc(sizeof(float)*NumVc*A.ncols);
            b2 = (float*)malloc(sizeof(float)*NumVc*A.ncols);

	//}

	for(i = 0; i < A.ncols; i++)
	 for(j=0; j < NumVc;j++){	
		x[i*NumVc + j] = 1.0;
		b[i*NumVc + j] = 0;
		b2[i*NumVc + j] = 0;
	 }	
*/

        int nrows;
        int nnz;
        int *rows;
        int *cols;
        REAL *vals;
		
		//time = smv(&A,x,b);
		
 //      	printf("Total SpMM time for %d vectors %.6lf seconds elapsed\n",NumVc ,time);  
		time=0;
		
		//for (num_t =0 ; num_t <10 ; num_t++)
		smv(&A,x,b);

        //time = spmv_bcsr(A.max_row_len,A.nrows,A.rows,A.vals,b2,x,A.cols,A.nnz, A.ncols);

		//time = //spmv_bcsr(A.ncols,A.nrows,A.rows,A.cols,x,b2,A.vals);
                       csb(NumVc,A.nrows,A.rows,A.cols,x,b2,A.vals);


		//printf("Total SpMM CSB Executor time: %.6lf seconds elapsed for %d vectors and for the %dth iteration\n", time, NumVc, num_t); 
		//time=0;
		//}
		for(i = 0; i < A.nrows; i++){
 		 char wrong = 0;
		  for(j=0; j < NumVc; j++ )  
		  if(fabs(b2[i][j] - b[i][j]) >= 0.1){	
  			wrong = 1;
			printf("Values don't match at %d, expected %f obtained %f\n", i, b[i][j], b2[i][j]);
	  	        break;
		   }
		  if(wrong== 1)
			break;	
		}

	/*	for(i = 0; i < A.nrows; i++)
 		  for(j=0; j < NumVc; j++ )  
		  if(fabs(b2[i*NumVc+j] - b[i*NumVc+ j]) >= 0.1){	
  			printf("Values don't match at %d, expected %f obtained %f\n", i, b[i*NumVc + j], b2[i*NumVc + j]);
	  	        
		   }
	*/

		    //else	
  		//	printf("Values match at %d, expected %f obtained %f\n", i, b[i], b2[i]);
	
	//}	
	

/*	for(i = 0; i < A.ncols; i++){
            free(x[i]); 
            free(b[i]);// = (float*)malloc(sizeof(float)*NumVc);
            free(b2[i]); //= (float*)malloc(sizeof(float)*NumVc);

	}
*/		

        free(x);
        free(b);
        free(b2);
        free(A.rows);
        free(A.cols);        
        free(A.vals);
	//if( i != A.nrows)	
        //printf("i  is %d\n", i) ;
	//	exit(EXIT_FAILURE);
  	return 0;
}

