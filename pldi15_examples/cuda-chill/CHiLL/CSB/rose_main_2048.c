#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA1_(_t37,_t38) _P_DATA1[1179 * _t37 + _t38]
#define _P_DATA1__(_t37,_t38) _P_DATA1[1179 * _t37 + _t38 + 1]
#define col_(ip,jp) col[1 * jp]
#define col__(i,j) col[1 * j]
#define col___(ip,jp) col[1 * jp + 1]
#define col____(i,j) col[1 * j + 1]
#define col_____(_t41) col[1 * _t41]
#define col_______(_t41) col[1 * _t41 + 1]
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
//#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "smv.h"
#include "matrix_io.h"
#include "sparse_matrix.h"
//#include "csb.h"
#include <sys/time.h>
#include <math.h>
#include <omp.h>
#define NumVc 2

struct a_list 
{
  unsigned short col_[2];
  float A;
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void csb(int m,int n,int *index,int *col,float (*x)[2UL],float (*y)[2UL],float *A)
{
  int t8;
  int t6;
  int t4;
  int t2;
  int newVariable0;
  int In_1;
  int _t45;
  int _t44;
  int _t43;
  struct a_list *_P_DATA5;
  struct mk *_P_DATA4;
  float *A_prime;
  struct a_list *_P1[1387683];
  unsigned short *_P_DATA3;
  unsigned short *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t42;
  int _t41;
  int _t40;
  int _t39;
  int _t38;
  int _t37;
  int _t36;
  int _t35;
  int _t34;
  int _t33;
  int _t32;
  int _t31;
  int _t30;
  int _t29;
  int _t28;
  int _t27;
  int _t26;
  int _t25;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  int _t18;
  int _t17;
  int _t16;
  int _t14;
  int _t15;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t6;
  int _t5;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int jp;
  int i;
  int j;
  int k;
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * 1387684)));
  _P_DATA1[0] = 0;
  chill_count_1 = 0;
  for (_t37 = 0; _t37 <= 1176; _t37 += 1) 
    for (_t38 = 0; _t38 <= 1178; _t38 += 1) {
      _P1[1179 * _t37 + 1 * _t38] = 0;
      _P_DATA1[1179 * _t37 + 1 * _t38 + 1] = 0;
    }
  for (t2 = 0; t2 <= 1176; t2 += 1) 
    for (t4 = 0; t4 <= 2047; t4 += 1) 
      for (t6 = index_(2048 * t2 + t4); t6 <= index__(2048 * t2 + t4) - 1; t6 += 1) {
        _t38 = (col_____(t6) - 0) / 2048;
        _t40 = (col_____(t6) - 0) % 2048;
        _P_DATA5 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
        _P_DATA5 -> next = _P1[1179 * t2 + 1 * _t38];
        _P1[1179 * t2 + 1 * _t38] = _P_DATA5;
        _P1[1179 * t2 + 1 * _t38] -> A = 0;
        _P1[1179 * t2 + 1 * _t38] -> col_[0] = t4;
        _P1[1179 * t2 + 1 * _t38] -> col_[1] = _t40;
        chill_count_1 += 1;
        _P_DATA1[1179 * t2 + 1 * _t38 + 1] += 1;
        _P1[1179 * t2 + 1 * _t38] -> A = A[t6];
      }
  for (t2 = 0; t2 <= 1176; t2 += 1) {
    if (t2 <= 0) {
      _P_DATA2 = ((unsigned short *)(malloc(sizeof(unsigned short ) * chill_count_1)));
      _P_DATA3 = ((unsigned short *)(malloc(sizeof(unsigned short ) * chill_count_1)));
      A_prime = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 1))));
    }
    for (t4 = 0; t4 <= 1178; t4 += 1) {
      _P_DATA5 = _P1[1179 * t2 + 1 * t4];
      for (newVariable0 = 1 - _P_DATA1[1179 * t2 + 1 * t4 + 1]; newVariable0 <= 0; newVariable0 += 1) {
        _P_DATA2[_P_DATA1[1179 * t2 + 1 * t4] - newVariable0] = _P_DATA5 -> col_[0];
        _P_DATA3[_P_DATA1[1179 * t2 + 1 * t4] - newVariable0] = _P_DATA5 -> col_[1];
        A_prime[(_P_DATA1[1179 * t2 + 1 * t4] - newVariable0) * 1] = _P_DATA5 -> A;
        _P_DATA5 = _P_DATA5 -> next;
      }
      _P_DATA1[1179 * t2 + 1 * t4 + 1] += _P_DATA1[1179 * t2 + 1 * t4];
    }
  }
#pragma omp parallel  private(t6,t8,t4)
{
    
#pragma omp for 
    for (t2 = 0; t2 <= 1176; t2 += 1) 
      for (t4 = 0; t4 <= 1178; t4 += 1) 
        for (t6 = _P_DATA1_(t2,t4); t6 <= _P_DATA1__(t2,t4) - 1; t6 += 1) {
          
#pragma simd
          for (t8 = 0; t8 <= 1; t8 += 1) 
            y[2048 * t2 + _P_DATA2[t6]][t8] += (A_prime[t6 * 1] * x[2048 * t4 + _P_DATA3[t6]][t8]);
        }
  }
#pragma omp parallel  private(t6,t8,t4)
{
    
#pragma omp for 
    for (t2 = 0; t2 <= 1178; t2 += 1) 
      for (t4 = 0; t4 <= 1176; t4 += 1) 
        for (t6 = _P_DATA1_(t4,t2); t6 <= _P_DATA1__(t4,t2) - 1; t6 += 1) {
          
#pragma simd
          for (t8 = 0; t8 <= 1; t8 += 1) 
            y[2048 * t2 + _P_DATA3[t6]][t8] += (A_prime[t6 * 1] * x[2048 * t4 + _P_DATA2[t6]][t8]);
        }
  }
  for (t2 = 2410496; t2 <= 2412468; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) 
      for (t6 = 0; t6 <= 1; t6 += 1) 
        y[col[t4]][t6] += (A[t4] * x[t2][t6]);
#pragma omp parallel  private(t6,t8,t4)
{
    
#pragma omp for 
    for (t2 = 2410496; t2 <= 2412468; t2 += 1) 
      for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
        
#pragma simd
        for (t6 = 0; t6 <= 1; t6 += 1) 
          y[t2][t6] += (A[t4] * x[col[t4]][t6]);
      }
  }
  for (_t37 = 0; _t37 <= 1176; _t37 += 1) 
    for (_t38 = 0; _t38 <= 1178; _t38 += 1) {
      _P_DATA5 = _P1[1179 * _t37 + 1 * _t38];
      for (newVariable0 = _P_DATA1[1179 * _t37 + 1 * _t38]; newVariable0 <= _P_DATA1[1179 * _t37 + 1 * _t38 + 1] - 1; newVariable0 += 1) {
        _P1[1179 * _t37 + 1 * _t38] = _P1[1179 * _t37 + 1 * _t38] -> next;
        free(_P_DATA5);
        _P_DATA5 = _P1[1179 * _t37 + 1 * _t38];
      }
    }
  free(_P_DATA1);
  free(_P_DATA2);
  free(_P_DATA3);
  free(A_prime);
}

int main(int argc,char **argv)
{
/////////////////////////////////////////////////////////////////////
  int num;
  int retval;
  struct timeval tv1;
  struct timeval tv2;
  struct timeval tv3;
  struct timeval tv4;
  struct timeval tv5;
//struct timezone tz;
  char filename[255UL];
  char NumV[255UL];
  struct sparse_matrix A;
  struct sparse_matrix M;
//	float **x, **b, **b2;
  float (*x)[2UL];
  float (*b)[2UL];
  float (*b2)[2UL];
//	float *x, *b, *b2;
  int num_threads;
  int j;
  int num_t = 0;
//			int NumVc=1;
  double time = 0;
  strcpy(filename,argv[1]);
//	strcpy(NumV, argv[2]);
//	NumVc = atoi(NumV);
  if (1) {
    fprintf(stderr,"main::Loading matrix %s\n",filename);
//fprintf(stderr, "R: %d C: %d\n", R, C);	
  }
  load_sparse_matrix(filename,&A);
//initialize b to 0 and x to 1
  int i;
  int ii;
  x = ((float (*)[2UL])(malloc((sizeof(float [2UL]) * A.ncols))));
  b = ((float (*)[2UL])(malloc((sizeof(float [2UL]) * A.ncols))));
  b2 = ((float (*)[2UL])(malloc((sizeof(float [2UL]) * A.ncols))));
  FILE *fp = (FILE *)((void *)0);
  FILE *fp1 = (FILE *)((void *)0);
  FILE *fp2 = (FILE *)((void *)0);
  FILE *fp3 = (FILE *)((void *)0);
/*	for(i = 0; i < A.ncols; i++){
            x[i] = (float*)malloc(sizeof(float)*NumVc);
            b[i] = (float*)malloc(sizeof(float)*NumVc);
            b2[i] = (float*)malloc(sizeof(float)*NumVc);
	}
*/
  for (i = 0; i < A.ncols; i++) 
    for (j = 0; j < 2; j++) {
      x[i][j] = 1.0;
      b[i][j] = 0;
      b2[i][j] = 0;
    }
/*
	//for(i = 0; i < A.ncols; i++){
            x = (float*)malloc(sizeof(float)*NumVc*A.ncols);
            b = (float*)malloc(sizeof(float)*NumVc*A.ncols);
            b2 = (float*)malloc(sizeof(float)*NumVc*A.ncols);
	//}
	for(i = 0; i < A.ncols; i++)
	 for(j=0; j < NumVc;j++){	
		x[i*NumVc + j] = 1.0;
		b[i*NumVc + j] = 0;
		b2[i*NumVc + j] = 0;
	 }	
*/
  int nrows;
  int nnz;
  int *rows;
  int *cols;
  REAL *vals;
//time = smv(&A,x,b);
//      	printf("Total SpMM time for %d vectors %.6lf seconds elapsed\n",NumVc ,time);  
  time = 0;
//for (num_t =0 ; num_t <10 ; num_t++)
  smv(&A,x,b);
//time = spmv_bcsr(A.max_row_len,A.nrows,A.rows,A.vals,b2,x,A.cols,A.nnz, A.ncols);
//time = //spmv_bcsr(A.ncols,A.nrows,A.rows,A.cols,x,b2,A.vals);
  csb(2,A.nrows,A.rows,A.cols,x,b2,A.vals);
{
//printf("Total SpMM CSB Executor time: %.6lf seconds elapsed for %d vectors and for the %dth iteration\n", time, NumVc, num_t); 
//time=0;
//}
    for (i = 0; i < A.nrows; i++) {
      char wrong = 0;
{
        for (j = 0; j < 2; j++) 
          if (fabs((b2[i][j] - b[i][j])) >= 0.1) {
            wrong = 1;
            printf("Values don\'t match at %d, expected %f obtained %f\n",i,b[i][j],b2[i][j]);
            break; 
          }
      }
      if (wrong == 1) 
        break; 
    }
  }
/*	for(i = 0; i < A.nrows; i++)
 		  for(j=0; j < NumVc; j++ )  
		  if(fabs(b2[i*NumVc+j] - b[i*NumVc+ j]) >= 0.1){	
  			printf("Values don't match at %d, expected %f obtained %f\n", i, b[i*NumVc + j], b2[i*NumVc + j]);
	  	        
		   }
	*/
//else	
//	printf("Values match at %d, expected %f obtained %f\n", i, b[i], b2[i]);
//}	
/*	for(i = 0; i < A.ncols; i++){
            free(x[i]); 
            free(b[i]);// = (float*)malloc(sizeof(float)*NumVc);
            free(b2[i]); //= (float*)malloc(sizeof(float)*NumVc);
	}
*/
  free(x);
  free(b);
  free(b2);
  free(A.rows);
  free(A.cols);
  free(A.vals);
//if( i != A.nrows)	
//printf("i  is %d\n", i) ;
//	exit(EXIT_FAILURE);
  return 0;
}
