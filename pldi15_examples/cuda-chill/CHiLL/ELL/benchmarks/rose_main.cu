#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define col_index__(_t12,_t13,_t14,i) col[3 * _t13 + 1 * _t14 + 1 * index_(_t12)]
#define col_index____(_t12,_t13,_t14,i) col[3 * _t13 + 1 * _t14 + 1 * index_(_t12) + 1]
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
__global__ void spmv_ell_GPU(int n,float *y,float *_P_DATA4,float *x,int *_P_DATA5);
//#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "smv.h"
#include "matrix_io.h"
#include "sparse_matrix.h"
//#include "spmv.h"
#include <sys/time.h>
#include <math.h>
//void spmv_bcsr(int n, int *index, float *a, float *y, float *x, int *col,int nnz);

void spmv(int n,int *index,float *a,float *y,float *x,int *col)
{
  int *devI3Ptr;
  float *devI2Ptr;
  float *devI1Ptr;
  float *devO1Ptr;
  int t8;
  int t6;
  int t4;
  float newVariable1;
  int _t36;
  int _t37;
  int _t39;
  int t2;
  int _t30;
  int _t29;
  int _t28;
  int *_P_DATA5;
  int _t27;
  int _t26;
  float *_P_DATA4;
  int _t21;
  int _t22;
  int _t17;
  int _t16;
  int _t15;
  int newVariable0;
  int *_P_DATA3;
  int *col_prime;
  float *a_prime;
  unsigned short *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t6;
  int _t5;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  a_prime = ((float *)(malloc(sizeof(float ) * ((n - 1 - 0 + 1) * 3))));
  col_prime = ((int *)(malloc(sizeof(int ) * ((n - 1 - 0 + 1) * 3))));
  _P_DATA3 = ((int *)(malloc(sizeof(int ) * 1)));
  chill_count_1 = 0;
  _P_DATA2 = ((unsigned short *)(malloc(sizeof(unsigned short ) * ((n - 1 - 0 + 1) * 1))));
  _P_DATA4 = ((float *)(malloc(sizeof(float ) * (3 * (n - 1 + 1)))));
  _P_DATA5 = ((int *)(malloc(sizeof(int ) * (3 * (n - 1 + 1)))));
  for (t2 = 0; t2 <= n - 1; t2 += 1) {
    for (t4 = 0; t4 <= (index__(t2) - index_(t2) - 1) / 3; t4 += 1) 
      for (t6 = 0; t6 <= __rose_lt(index__(t2) - index_(t2) - 3 * t4 - 1,2); t6 += 1) 
        _P_DATA3[t4] = -1;
    for (t4 = 0; t4 <= (index__(t2) - index_(t2) - 1) / 3; t4 += 1) 
      for (t6 = 0; t6 <= __rose_lt(index__(t2) - index_(t2) - 3 * t4 - 1,2); t6 += 1) {
        if (_P_DATA3[0] == -1) {
          _P_DATA3[t4] = chill_count_1;
          for (newVariable0 = 0; newVariable0 <= 2; newVariable0 += 1) {
            a_prime[_P_DATA3[t4] * 3 + 1 * newVariable0] = 0;
            col_prime[_P_DATA3[t4] * 3 + 1 * newVariable0] = 0;
          }
          _P_DATA2[_P_DATA3[t4]] = t4;
          chill_count_1 += 1;
        }
        a_prime[_P_DATA3[t4] * 3 + 1 * t6] = a[3 * t4 + t6 + index_(t2)];
        col_prime[_P_DATA3[t4] * 3 + 1 * t6] = col[3 * t4 + t6 + index_(t2)];
      }
  }
  for (t2 = 0; t2 <= 2; t2 += 1) 
    for (t4 = 0; t4 <= n - 1; t4 += 1) {
      _P_DATA4[t4 + t2 * (n - 1 + 1)] = a_prime[t4 * 3 + 1 * t2];
      _P_DATA5[t4 + t2 * (n - 1 + 1)] = col_prime[t4 * 3 + 1 * t2];
    }
  cudaMalloc(((void **)(&devO1Ptr)),1000000 * sizeof(float ));
  cudaMemcpy(devO1Ptr,y,1000000 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),3000000 * sizeof(float ));
  cudaMemcpy(devI1Ptr,_P_DATA4,3000000 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),1000000 * sizeof(float ));
  cudaMemcpy(devI2Ptr,x,1000000 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),3 * (n - 1 + 1) * sizeof(int ));
  cudaMemcpy(devI3Ptr,_P_DATA5,3 * (n - 1 + 1) * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid2 = dim3((n - 1) / 512 + 1,1);
  dim3 dimBlock2 = dim3(512,1);
  spmv_ell_GPU<<<dimGrid2,dimBlock2>>>(n,devO1Ptr,devI1Ptr,devI2Ptr,devI3Ptr);
  cudaMemcpy(y,devO1Ptr,1000000 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  free(_P_DATA2);
  free(a_prime);
  free(col_prime);
  free(_P_DATA3);
}

int main(int argc,char **argv)
{
/////////////////////////////////////////////////////////////////////
  int num;
  int retval;
  struct timeval tv1;
  struct timeval tv2;
  struct timeval tv3;
  struct timeval tv4;
  struct timeval tv5;
//struct timezone tz;
  char filename[255UL];
  struct sparse_matrix A;
  struct sparse_matrix M;
  float *x;
  float *b;
  float *b2;
  int num_threads;
//pthread_attr_t attr;
  strcpy(filename,argv[1]);
  if (1) {
    fprintf(stderr,"main::Loading matrix %s\n",filename);
//fprintf(stderr, "R: %d C: %d\n", R, C);	
  }
  load_sparse_matrix(filename,&A);
//initialize b to 0 and x to 1
  int i;
  int ii;
  x = ((float *)(malloc((sizeof(float ) * A.sparse_matrix::ncols))));
  b = ((float *)(malloc((sizeof(float ) * A.sparse_matrix::nrows))));
  b2 = ((float *)(malloc((sizeof(float ) * A.sparse_matrix::nrows))));
  FILE *fp = 0L;
  FILE *fp1 = 0L;
  FILE *fp2 = 0L;
  FILE *fp3 = 0L;
  for (i = 0; i < A.sparse_matrix::ncols; i++) 
    x[i] = 1.0;
  for (i = 0; i < A.sparse_matrix::nrows; i++) {
    b[i] = 0;
    b2[i] = 0;
  }
  int nrows;
  int nnz;
  int *rows;
  int *cols;
  REAL *vals;
  int m = 0;
  for (i = 0; i < A.sparse_matrix::nrows; i++) 
    if ((A.sparse_matrix::rows[i + 1] - A.sparse_matrix::rows[i]) > m) 
      m = (A.sparse_matrix::rows[i + 1] - A.sparse_matrix::rows[i]);
  printf("Max row len is %d\n",m);
  smv(&A,x,b);
//spmv(A.nrows,A.rows,A.vals,b2,x,A.cols);
  spmv(A.sparse_matrix::nrows,A.sparse_matrix::rows,A.sparse_matrix::vals,b2,x,A.sparse_matrix::cols);
  for (i = 0; i < A.sparse_matrix::nrows; i++) {
    if (fabs((b2[i] - b[i])) >= 0.1) {
      printf("Values don\'t match at %d, expected %f obtained %f\n",i,b[i],b2[i]);
      break; 
    }
//else	
//	printf("Values match at %d, expected %f obtained %f\n", i, b[i], b2[i]);
  }
  free(x);
  free(b);
  free(b2);
  free(A.sparse_matrix::rows);
  free(A.sparse_matrix::cols);
  free(A.sparse_matrix::vals);
  if (i != A.sparse_matrix::nrows) 
//printf("i  is %d\n", i) ;
    exit(1);
  return 0;
}

__global__ void spmv_ell_GPU(int n,float *y,float *_P_DATA4,float *x,int *_P_DATA5)
{
  int j;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int t2;
  float newVariable1;
  int t4;
  int t8;
  if (tx <= n - 512 * bx - 1) {
    newVariable1 = y[512 * bx + tx];
    for (j = 0; j <= 2; j += 1) 
      newVariable1 += (_P_DATA4[tx + 512 * bx + j * (n - 1 + 1)] * x[_P_DATA5[tx + 512 * bx + j * (n - 1 + 1)]]);
    y[512 * bx + tx] = newVariable1;
  }
}
