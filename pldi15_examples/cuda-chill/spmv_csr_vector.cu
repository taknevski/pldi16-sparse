/////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "smv.h"
#include "matrix_io.h"
#include "sparse_matrix.h"
//#include "cuPrintf.cu"
/////////////////////////////

#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define index_(i) index[i]
#define index__(i) index[i + 1]

__global__ void spmv_GPU(float *y,float *a,float *x,int *col,int *index);
__host__ void load_matrix(char *filename, int *rows, int *cols, float *vals);
//#define NZ 1666
//#define NUMROWS 494
//boneS01  127224 6715152
//bcsstk39  46772 2089294
//engine 143571 4706073
//gearbox 153746 9080404
//pkustk07  16860 2418804
//shipsec5 179860 10113096

#define NZ (10113096)
#define NUMROWS (179860)
#define TILE_FACTOR (1024)
#define NONZEROS_PER_BLOCK TILE_FACTOR

__device__ void segreduce_warp(float *y,float *val)
{
    float left = 0;
    int tx = threadIdx.x;
    float right;
   
    if(tx >= 1  &&  (tx && 0x1)) {right = *val; } *(val -  1)  +=right; right =0;  
    if(tx >= 2  && !(tx && 0x1)) {right = *val; } *(val -  2)  +=right; right =0;  
    if(tx >= 4  && !(tx && 0x3)) {right = *val; } *(val -  4)  +=right; right =0;  
    if(tx >= 8  && !(tx && 0x7)) {right = *val; } *(val -  8)  +=right; right =0;  
    if(tx >= 16 && !(tx && 0xF)) {right = *val; } *(val - 16) +=right; right =0;  
  



  if(tx ==0)
     *y = *val;
}  




void spmv(int n,int index[NUMROWS+1],float a[NZ],float y[NUMROWS],float x[NUMROWS],int col[NZ])
{

  cudaEvent_t start_event, stop_event;
  float cuda_elapsed_time, time_2;
  ( cudaEventCreate(&start_event));
  ( cudaEventCreate(&stop_event));

  int *devI4Ptr;
  int *devI3Ptr;
  float *devI2Ptr;
  float *devI1Ptr;
  float *devO1Ptr;
  int t12;
  int t10;
  int t8;
  int t4;
  int _t19;
  int _t14;
  int _t15;
  int t2;
  int t6;
  int i;
  int j;
  cudaMalloc(((void **)(&devO1Ptr)),NUMROWS* sizeof(float ));
  cudaMemcpy(devO1Ptr,y,NUMROWS* sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),NZ* sizeof(float ));
  cudaMemcpy(devI1Ptr,a,NZ* sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),NUMROWS* sizeof(float ));
  cudaMemcpy(devI2Ptr,x,NUMROWS* sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),NZ* sizeof(int ));
  cudaMemcpy(devI3Ptr,col,NZ* sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI4Ptr)),(NUMROWS+1)* sizeof(int ));
  cudaMemcpy(devI4Ptr,index,(NUMROWS+1)* sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid0 = dim3((NUMROWS - 1)/TILE_FACTOR + 1,1);
  dim3 dimBlock0 = dim3(32,TILE_FACTOR);
  cudaEventRecord(start_event, 0);
  spmv_GPU<<<dimGrid0,dimBlock0>>>(devO1Ptr,devI1Ptr,devI2Ptr,devI3Ptr,devI4Ptr);
  cudaThreadSynchronize();
  cudaEventRecord(stop_event, 0);
  cudaEventSynchronize(stop_event);
  cudaEventElapsedTime(&cuda_elapsed_time, start_event, stop_event);
 
  printf("first: Elapsed time %f milliseconds\n", cuda_elapsed_time);
  cudaMemcpy(y,devO1Ptr,NUMROWS* sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  cudaFree(devI4Ptr);






}

int main(int argc, char*argv[])
{
  int n = NUMROWS;
  static float a[NZ];
  static float y[NUMROWS];
  static float x[NUMROWS];
  static int index[NUMROWS+1];
  static int col[NZ];

  //////////////////////////////////////
    char filename[255];
    struct sparse_matrix A;

    if(argc!=2){
        fprintf(stderr, "use: program matrix\n");
        abort();
    }

    strcpy(filename, argv[1]);

    if (DEBUG) {
        fprintf(stderr, "main::Loading matrix\n");
        fprintf(stderr,"testing testing\n");    
    }

    load_matrix(filename, index, col, a);
    int i;
    for (i = 0; i < n; i++) {
        y[i] = 0.0;
        x[i] = 1.0;
    }
  //////////////////////////////////////
  


  spmv(n,index,a,y,x,col);

  //////////////////////////////
  for (i = 0; i < n; i++) {
    printf("%f\n", y[i]);
  }
  //////////////////////////////

  return 0;
}

////////////////////////////////////////////
__host__ void load_matrix(char *filename, int *rows, int *cols, float *vals)
{
    FILE *in;
    char data[NONZEROS_PER_BLOCK];
    int i;
    int n, nz;
    in = fopen(filename, "r");
    if(in==NULL){
        printf("something might be wrong with the file\n");
    }
    fgets(data, NONZEROS_PER_BLOCK, in);
    fprintf(stderr, "%s", data);
    fscanf(in, "%d %d\n", &n, &nz);
    if (DEBUG)
        fprintf(stderr, "load_sparse_matrix:: rows = %d, nnz = %d\n", n, nz);

    if (DEBUG)
                fprintf(stderr, "load_sparse_matrix::reading row index\n");

    for(i = 0; i <= n; i++){
        int temp;
        fscanf(in, "%d", &temp);
        temp--;
        rows[i] = temp;
    }

    if (DEBUG)
                fprintf(stderr, "load_sparse_matrix::reading column index\n");

    for(i = 0; i < nz; i++){
        int temp;
        fscanf(in, "%d", &temp);
        temp--;
        //fprintf(stderr, "%d\n", temp);
        cols[i] = temp;
        //fprintf(stderr, "%d\n", (*cols)[i]);
    }

    if (DEBUG)
                fprintf(stderr, "load_sparse_matrix::reading values\n");

    for(i = 0; i < nz; i++){
        char temp[20];
        REAL data;
        fscanf(in, "%s", temp);
        data = atof(temp);
        //fprintf(stderr, "%f\n", data);
        vals[i] = 1.0;
    }

    if (DEBUG)
                fprintf(stderr, "load_sparse_matrix::Loading sparse matrix done\n");
}

__global__ void spmv_GPU(float *y,float *a,float *x,int *col,int *index)
{
  int jj;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  __device__ __shared__ float _P1[64];
  int t2;
  int t4;
  int t8;
  int t10;
  if (ty <= NUMROWS - TILE_FACTOR * bx - 1) 
    for (jj = index_(bx); jj <= index__(bx) - 1; jj += 32) 
      if (tx <= index__(bx) - jj - 1) {
        _P1[tx] = (a[tx + jj] * x[col[tx + jj]]);
        segreduce_warp(&y[1024 * bx + ty],&_P1[tx]);
      }
}




