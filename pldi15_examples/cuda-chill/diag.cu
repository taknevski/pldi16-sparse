#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA1_(_t24) _P_DATA1[_t24]
#define _P_DATA1__(_t24) _P_DATA1[_t24 + 1]
#define col_(ip,jp) col[1 * jp]
#define col__(i,j) col[1 * j]
#define col___(ip,jp) col[1 * jp + 1]
#define col____(i,j) col[1 * j + 1]
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
__global__ void sgd_ut_mul_col_operator(float step_size,float (*fv)[16UL],unsigned short *_P_DATA2,float *new_ratings,int *_P_DATA1,int t2);
#define NZ 1666
#define NUMROWS 494
#define N 1000
#define SGD_LAMBDA 0.05f
#define SGD_FEATURE_SIZE 16

struct a_list 
{
  unsigned short col_;
  float ratings;
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void sgd_kernel_global_simd(int n,int *index,float *ratings,float *y,float *x,int *col,float (*fv)[16UL],float step_size)
{
  int *devI3Ptr;
  float *devI2Ptr;
  unsigned short *devI1Ptr;
  float *devO1Ptr;
  int t8;
  int t6;
  int t4;
  unsigned short newVariable1;
  int _t75;
  int _t74;
  int _t73;
  int _t72;
  int _t71;
  int _t70;
  int _t69;
  int _t68;
  int t2;
  float _P3[2 * 16];
  int _t58;
  int _t57;
  int _t56;
  int _t55;
  float _P2[2];
  int _t47;
  int _t46;
  int _t45;
  int _t44;
  int _t40;
  int _t39;
  int _t38;
  int _t37;
  int _t36;
  int _t35;
  int _t33;
  int _t32;
  int _t31;
  int _t30;
  int newVariable0;
  int In_1;
  int _t29;
  int _t28;
  struct a_list *_P_DATA4;
  struct mk *_P_DATA3;
  float *new_ratings;
  struct a_list *_P1[155997];
  unsigned short *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t27;
  int _t26;
  int _t25;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  int _t18;
  int _t17;
  int _t16;
  int _t15;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t6;
  int _t4;
  int _t5;
  int l;
  int _t3;
  int _t2;
  int _t1;
  int jp;
  int i;
  int j;
  int k;
  float err;
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * 155998)));
  _P_DATA1[0] = 0;
  chill_count_1 = 0;
  for (_t24 = 0; _t24 <= 155996; _t24 += 1) {
    _P1[1 * _t24] = 0;
    _P_DATA1[1 * _t24 + 1] = 0;
  }
  for (t2 = 0; t2 <= 46640; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
      _t24 = col_(t4) - t2 - 1;
      _P_DATA4 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
      _P_DATA4 -> next = _P1[1 * _t24];
      _P1[1 * _t24] = _P_DATA4;
      _P1[1 * _t24] -> ratings = -1;
      _P1[1 * _t24] -> col_ = t2;
      chill_count_1 += 1;
      _P_DATA1[1 * _t24 + 1] += 1;
      _P1[1 * _t24] -> ratings = ratings[t4];
    }
{
    _P_DATA2 = ((unsigned short *)(malloc(sizeof(unsigned short ) * chill_count_1)));
    new_ratings = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 1))));
    _P_DATA4 = _P1[1 * 0];
    for (newVariable0 = 1 - _P_DATA1[1 * 0 + 1]; newVariable0 <= 0; newVariable0 += 1) {
      _P_DATA2[_P_DATA1[1 * 0] - newVariable0] = _P_DATA4 -> col_[0];
      new_ratings[_P_DATA1[1 * 0] - newVariable0] = _P_DATA4 -> ratings;
      _P_DATA4 = _P_DATA4 -> next;
    }
    _P_DATA1[1 * 0 + 1] += _P_DATA1[1 * 0];
    for (t2 = 1; t2 <= 155996; t2 += 1) {
      _P_DATA4 = _P1[1 * t2];
      for (newVariable0 = 1 - _P_DATA1[1 * t2 + 1]; newVariable0 <= 0; newVariable0 += 1) {
        _P_DATA2[_P_DATA1[1 * t2] - newVariable0] = _P_DATA4 -> col_[0];
        new_ratings[_P_DATA1[1 * t2] - newVariable0] = _P_DATA4 -> ratings;
        _P_DATA4 = _P_DATA4 -> next;
      }
      _P_DATA1[1 * t2 + 1] += _P_DATA1[1 * t2];
    }
  }
  cudaMalloc(((void **)(&devO1Ptr)),16 * sizeof(float ));
  cudaMemcpy(devO1Ptr,fv,16 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),chill_count_1 * sizeof(unsigned short ));
  cudaMemcpy(devI1Ptr,_P_DATA2,chill_count_1 * sizeof(unsigned short ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),chill_count_1 * 1 * sizeof(float ));
  cudaMemcpy(devI2Ptr,new_ratings,chill_count_1 * 1 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),(155997 + 1) * sizeof(int ));
  cudaMemcpy(devI3Ptr,_P_DATA1,(155997 + 1) * sizeof(int ),cudaMemcpyHostToDevice);
  for (t2 = 0; t2 <= 155996; t2 += 1) {
    dim3 dimGrid2 = dim3((_P_DATA1__(t2) - _P_DATA1_(t2) - 1) / 2 + 1,1);
    dim3 dimBlock2 = dim3(16,2);
    sgd_ut_mul_col_operator<<<dimGrid2,dimBlock2>>>(step_size,((float (*)[16UL])devO1Ptr),devI1Ptr,devI2Ptr,devI3Ptr,t2);
  }
  cudaMemcpy(fv,devO1Ptr,16 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  for (_t24 = 0; _t24 <= 155996; _t24 += 1) {
    _P_DATA4 = _P1[1 * _t24];
    for (newVariable0 = _P_DATA1[1 * _t24]; newVariable0 <= _P_DATA1[1 * _t24 + 1] - 1; newVariable0 += 1) {
      _P1[1 * _t24] = _P1[1 * _t24] -> next;
      free(_P_DATA4);
      _P_DATA4 = _P1[1 * _t24];
    }
  }
  free(_P_DATA1);
  free(_P_DATA2);
  free(new_ratings);
}

int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[495UL];
  int col[1666UL];
  float (*fv)[16UL];
  float step_size = 0.09f;
  float err;;
  sgd_kernel_global_simd(n,index,a,y,x,col,fv,step_size);
  return 0;
}

__global__ void sgd_ut_mul_col_operator(float step_size,float (*fv)[16UL],unsigned short *_P_DATA2,float *new_ratings,int *_P_DATA1,int t2)
{
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  __device__ __shared__ float _P2[2];
  __device__ __shared__ float _P3[2 * 16];
  unsigned short newVariable1;
  int t4;
  int t6;
  int t8;
  if (ty <= _P_DATA1__(t2) - _P_DATA1_(t2) - 2 * bx - 1) 
    newVariable1 = _P_DATA2[_P_DATA1_(t2) + 2 * bx + ty];
  if (ty <= _P_DATA1__(t2) - _P_DATA1_(t2) - 2 * bx - 1) {
    _P2[ty] = -new_ratings[(2 * bx + _P_DATA1_(t2) + ty) * 1];
    _P3[tx + ty * 16] = (fv[newVariable1][tx] * fv[newVariable1 + (t2 + 1)][tx]);
    segreduce_warp(&_P2[ty],&_P3[0 + ty * 16]);
    fv[newVariable1][tx] -= (step_size * ((_P2[ty] * fv[newVariable1 + (t2 + 1)][tx]) + (0.05f * fv[newVariable1][tx])));
    fv[newVariable1 + (t2 + 1)][tx] -= (step_size * ((_P2[ty] * fv[newVariable1][tx]) + (0.05f * fv[newVariable1 + (t2 + 1)][tx])));
  }
}
