init("csr_vector.cpp", "spmv", 0)
dofile("cudaize.lua")
N = 4096
Ti = 1024
Tj=32
NNZ=5120
COPY_TO_SHARED=1
NO_PAD=0
ACCUMULATE_THEN_ASSIGN=0


tile_by_index(0,{"i"},{Ti},{l1_control="ii"},{"ii","i","j"})CU=1

tile_by_index(0,{"j"},{Tj},{l1_control="jj",l1_tile="j"},{"ii","i","j", "jj"},strided)CU=1
scalar_expand_by_index(0,{"i","j"},"RHS",COPY_TO_SHARED,NO_PAD,ACCUMULATE_THEN_ASSIGN)

known({"index__","index_"},{1,-1}, -1, 0)

cudaize(0,"spmv_GPU",{ a=NNZ,x=N,y=N,col=NNZ,index=NNZ},{block={"ii"}, thread={"j", "i"}},{"index"})

reduce_by_index(0,{"jj"}, "reduce_warp",{}, {"tx"})

