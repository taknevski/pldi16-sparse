	int diag_inspector(int movies, int users, int *index, float *a, int *col,
			int iter) {


		int t8;
		int t6;
		int t4;
		int t2;
		int newVariable4;
		int newVariable3;
		int newVariable2;
		int In_3;
		int In_2;
		int In_1;
		int _t39;
		int _t38;
		int _t37;
		struct a_list *_P_DATA4;
		int newVariable1;
		int newVariable0;
		struct mk *_P_DATA3;
		struct a_list **_P1;
		int *_P_DATA2;
		int chill_count_1;
		int *_P_DATA1;
		int chill_count_0;
		int _t36;
		int _t35;
		int _t34;
		int _t33;
		int _t32;
		int _t31;
		int _t30;
		int _t29;
		int _t28;
		int _t27;
		int _t25;
		int _t26;
		int _t24;
		int _t23;
		int _t22;
		int _t21;
		int _t20;
		int _t19;
		int _t18;
		int _t17;
		int _t16;
		int _t15;
		int _t14;
		int _t12;
		int _t11;
		int _t10;
		int _t9;
		int _t7;
		int _t6;
		int _t5;
		int _t4;
		int l;
		int _t3;
		int _t2;
		int _t1;
		int i;
		int j;
		int k;
		_P_DATA1 = (int *)malloc(sizeof(int ) * (users/C + movies/R));
		_P1 = (struct a_list **)malloc(sizeof(struct a_list *) * (users/C + movies/R - 1));
		_P_DATA1[0] = 0;
		_P_DATA3 = ((struct mk *)(malloc(sizeof(struct mk ) * (users/C + movies/R - 1))));
		chill_count_1 = 0;
		_P_DATA1[0] = 0;
		for (_t31 = 0; _t31 <= users/C + movies/R - 2; _t31 += 1) {
			_P1[1 * _t31] = 0;
			_P_DATA1[1 * _t31 + 1] = 0;
		}
		for (t2 = 0; t2 <= movies/R - 1; t2 += 1) {
			for (t4 = 0; t4 <= R-1; t4 += 1) 
				for (t6 = index_(R * t2 + t4); t6 <= index__(R * t2 + t4) - 1; t6 += 1) {
					_t31 =  (col_(t6)-movies+ R*(movies/R -1))/ C  - t2;
					_P_DATA3[_t31].ptr = 0;
				}
			for (t4 = 0; t4 <= R-1; t4 += 1) 
				for (t6 = index_(R* t2 + t4); t6 <= index__(R * t2 + t4) - 1; t6 += 1) {
					_t31 =  (col_(t6)-movies + R*(movies/R -1))/ C  - t2;
					_t34 =  (col_(t6)-movies + R*(movies/R -1)) % C;
					if (_P_DATA3[_t31].ptr == 0) {
						_P_DATA4 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
						_P_DATA4 -> next = _P1[_t31];
						_P1[_t31] = _P_DATA4;
						_P_DATA3[_t31].ptr = _P1[_t31];
						for (newVariable0 = 0; newVariable0 <= R-1; newVariable0 += 1) 
							for (newVariable1 = 0; newVariable1 <= C-1; newVariable1 += 1) 
								_P_DATA3[_t31].ptr -> ratings[C* newVariable0 + 1 * newVariable1] = -1;
						_P_DATA3[_t31].ptr -> col_[0] = t2;
						chill_count_1 += 1;
						_P_DATA1[_t31 + 1] += 1;
					}
					_P_DATA3[_t31].ptr -> ratings[C * t4 + 1 * _t34] = a[t6];
				}
		}

		new_col = new ArrayType(chill_count_1);  
		new_index = new ArrayType(users / C + movies / R);
		new_ratings = new FeatureArrayType(chill_count_1 * R * C);
		new_col->create_on_device();
		new_index->create_on_device();
		new_ratings->create_on_device();
		new_index->host_ptr()[0] = 0;
		for (t2 = 0; t2 <= users/C + movies/R - 2; t2 += 1) {
			for (newVariable2 = 1 - _P_DATA1[1 * t2 + 1]; newVariable2 <= 0; newVariable2 += 1) {
				new_col->host_ptr()[_P_DATA1[1 * t2] - newVariable2] = _P1[1 * t2] -> col_[0];
				for (newVariable3 = 0; newVariable3 <= R-1; newVariable3 += 1) 
					for (newVariable4 = 0; newVariable4 <= C-1; newVariable4 += 1) 
						new_ratings->host_ptr()[R*C * (_P_DATA1[1 * t2] - newVariable2) + C* newVariable3 + 1 * newVariable4] = _P1[1 * t2] -> ratings[C * newVariable3 + 1 * newVariable4];
				_P_DATA4 = _P1[1 * t2] -> next;
				free(_P1[1 * t2]);
				_P1[1 * t2] = _P_DATA4;
			}
			_P_DATA1[1 * t2 + 1] += _P_DATA1[1 * t2];
			new_index->host_ptr()[t2 + 1] = _P_DATA1[t2 + 1];
		}

		count_of_diagonals = users / C + movies / R - 1; 
		free(_P_DATA1);
		free(_P_DATA3);
		free(_P1);
		return chill_count_1;




	}
