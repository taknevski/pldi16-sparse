--init("datacopy_spmv.c", "spmv", 0)
--dofile("cudaize.lua")

init("spmv.cpp", "spmv", 0)
dofile("cudaize.lua")
NNZ=1666
N=494
Ti = 1024
Tj = 256
Tk = 32

print_dep()
--flatten loop levels 1 and 2 with NNZ being uninterpreted omega function name
flatten(0,{"new_index"},{1,2}, "NNZ")

--split flattened loop level to be a perfect multiple of warp size (32)
split_with_alignment(1,3,1024)

--print_code(1)

distribute({1,2},1)


--distribute remainder of splitted statement as it is not cudaized

--tile for number of non zeros per CUDA block (1024)
tile_by_index(1,{"new_index"},{Ti},{l1_control="kk"},{"i","j","kk","new_index"})CU=1



--tile for number of nonzeros per warp (256)
tile_by_index(1,{"new_index"},{Tj},{l1_control="bbb"},{"i", "j", "kk","bbb","new_index"})CU=1


--tile for warp size(32) to get(256/32= 8) iterations
tile_by_index(1,{"new_index"},{Tk},{l1_control="bbbb"},{"i", "j", "kk", "bbb","bbbb","new_index"})CU=2


--normalization of tiled bounds
shift_to(1,6,0)



--scalar expansion of product expression
scalar_expand(1,{4,6},"NNZ.i",1,64)
scalar_expand(1,{4,6},"RHS",1,64)




peel(1,5,-1)



distribute({5,6,7},6)


peel(5,6,-1)

fuse({5,6,7},5)
distribute({5,8},3)


scalar_expand(8,{3,4},"_P1")
scalar_expand(8,{3,4},"RHS")
distribute({8,9,10},3)
--distribute({1,8,9,10},1)
fuse({1,3,4,5,6,7,9,10},3)

distribute({8,9},1)
distribute({8,10},1)
--fuse({1,3,4,5,6,7,9,10},1)
fuse({1,3,4,5,6,7,9,10},1)
--print_code(1)

fuse({1,3,4,5,6,7,9,10},2)
fuse({1,3,4,5,6,7,9,10},3)




tile_by_index(8,{"kk"},{256},{l1_control="kkk"},{"i", "j", "kkk",  "kk","bbb","bbbb"})CU=1
shift_to(8,4,0)

scalar_expand(8,{4,5},"_P_DATA1",1)
scalar_expand(8,{4,5},"RHS",1)
distribute({8,11,12},5)

print_code(2)

tile_by_index(2,{"new_index"},{512},{l1_control="kkk"},{"i", "j", "kkk",  "new_index"},strided)CU=1
--tile(2,4,4)
--shift_to(2,4,0)



cudaize(1,"spmv_GPU",{ a=NNZ,x=N,y=N,col=NNZ,temp=NNZ, NNZ_j=NNZ, NNZ_i=NNZ },{block={"kk"}, thread={"new_index", "bbb"}},{"_P_DATA1", "_P_DATA2"})
cudaize(8,"spmv_second_level_GPU",{ a=NNZ,x=N,y=N,col=NNZ,temp=NNZ, NNZ_i=NNZ},{block={}, thread={"bbb","kk"}}, {"_P_DATA1", "_P_DATA2"})
--cudaize(2,"spmv_final_level_GPU",{ a=NNZ,x=N,y=N,col=NNZ, temp=NNZ,NNZ_j=NNZ, NNZ_i=NNZ},{block={}, thread={}}, {})

cudaize(2,"spmv_final_level_GPU",{ a=NNZ,x=N,y=N,col=NNZ, temp=NNZ,NNZ_j=NNZ, NNZ_i=NNZ},{block={}, thread={"new_index"}}, {})
reduce(1,{6},0,"segreduce_warp",{5})
reduce(5,{6},0,"segreduce_warp",{})
reduce(8,{4,5},0,"segreduce_block",{})
--add_sync(4,"tx")
--print_code(2)
add_sync(12,"tx")

