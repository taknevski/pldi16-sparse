#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define M 16

int main(double *A,double *X,double *Y)
{
  int i;
  int j;
  int k;
  int l;
  double d;
  double e;
  for (i = 0; i < 16; ++i) {
    d = A[i];
    for (j = 0; j < 16 * 16; ++j) {
      Y[((i * 16) * 16) + j] = (d * X[j]);
    }
    for (k = 1; k < 16; ++k) {
      d = A[i + (k * 16)];
      for (l = 0; l < 16 * 16; ++l) {
        e = (d * X[(16 * 16 * k) + l]);
        Y[((i * 16) * 16) + l] += e;
      }
    }
  }
  return 0;
}
