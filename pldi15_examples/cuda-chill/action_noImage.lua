init("simple_action_noImage.c","Action_NoImage_Center",0)
dofile("cudaize.lua") --defines custom tile_by_index, copy_to_registers,
                      --copy_to_shared methods
TI=32
TJ=64

N=1024



--Tile the i and j loop, introducing "ii" as the control loop for the "i"
--tile, "k" for the control loop fo the "j" tile, with the final order
--of {"ii", "k", "i", "j"}
--tile_by_index({"i","j"}, {TI,TJ}, {l1_control="ii", l2_control="k"}, {"ii", "k", "i", "j"})
--distribute({0,1}, 2)
print_code(0)
--tile_by_index(0,{"solventMol","solventAtom"}, {TI,TJ}, {l1_control="ii", l2_control="jj"}, {"ii", "jj", "solventMol", "solventAtom"})
tile_by_index(1,{"solventMol","solventAtom"}, {TI,TJ}, {l1_control="ii", l2_control="jj"}, {"ii", "jj", "solventMol", "solventAtom"})
print_code(0)
--tile_by_index({"i"}, {TI}, {l1_control="iii"}, {"ii", "k", "iii","i", "j"})
--tile_by_index({"j"}, {TI}, {l2_control="k"}, { "k", "i", "j"})
--tile_by_index({"i"}, {TI}, {l1_control="ii"}, {"ii", "i", "j"})
--print_code()
--Normalize indx will do a tile size of one over the loop level specified
--by the input index. This is useful to get a zero lower bound and hard
--upper bound on a loop instead of it being relative to previous loop
--levels.
--normalize_index("ii")
--normalize_index(0,"i")
--print_code(1)

--Cudaize now determines the grid dimentions from the loops themselves
--(the upper bounds of the block and thread loops). It also renames the
--given block and thread loops's indexes to the approviate values from
--the set {"bx","by","tx","ty","tz"}. The second parameter specifies the
--size of the arrays to be copied in the CUDA scaffolding.
--print_code(0)
distribute({0,1}, 2)

cudaize(1,"Action_No_image_GPU", {D_=N*3, SolventMols_=N*N*3},{block={"ii","jj"}, thread={"solventMol", "solventAtom"}},{})


print_code(0)
print_code(1)
