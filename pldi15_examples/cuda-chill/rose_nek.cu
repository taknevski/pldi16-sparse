#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
__global__ void local_grad3_GPU_2(double *ut,double *u2,double *Dt);
__global__ void local_grad3_GPU_1(double *us,double *u,double *Dt);
__global__ void local_grad3_GPU_0(double *ur,double *D,double *u1);
/*This code was generated using the Tensor-Contraction Autotuning tool,
developed by Axel Y. Rivera (University of Utah).
Code Generated Date and hour: 2014-05-19 15:57*/
#define nelt 100
#define lz 10
#define lx 10
#define ly 10

void local_grad3(double *ut,double *us,double *ur,double *D,double *u1,double *u,double *Dt,double *u2)
{
  int t14;
  int t12;
  double *devI2Ptr;
  double *devI1Ptr;
  double *devO1Ptr;
  int t10;
  int t8;
  int t6;
  int t4;
  int t2;
  double newVariable2;
  int _t41;
  int _t40;
  int _t39;
  int _t38;
  int _t37;
  double newVariable1;
  int _t34;
  int _t33;
  int _t31;
  int _t30;
  int _t29;
  double newVariable0;
  int _t27;
  int _t26;
  int _t25;
  int _t24;
  int _t23;
  int e;
  int j;
  int i;
  int k;
  int m;
  int k2;
  cudaMalloc(((void **)(&devO1Ptr)),100000 * sizeof(double ));
  cudaMemcpy(devO1Ptr,ur,100000 * sizeof(double ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),100 * sizeof(double ));
  cudaMemcpy(devI1Ptr,D,100 * sizeof(double ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),100000 * sizeof(double ));
  cudaMemcpy(devI2Ptr,u1,100000 * sizeof(double ),cudaMemcpyHostToDevice);
  dim3 dimGrid0 = dim3(100,1);
  dim3 dimBlock0 = dim3(10,100);
  local_grad3_GPU_0<<<dimGrid0,dimBlock0>>>(devO1Ptr,devI1Ptr,devI2Ptr);
  cudaMemcpy(ur,devO1Ptr,100000 * sizeof(double ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaMalloc(((void **)(&devO1Ptr)),100000 * sizeof(double ));
  cudaMemcpy(devO1Ptr,us,100000 * sizeof(double ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),100000 * sizeof(double ));
  cudaMemcpy(devI1Ptr,u,100000 * sizeof(double ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),100 * sizeof(double ));
  cudaMemcpy(devI2Ptr,Dt,100 * sizeof(double ),cudaMemcpyHostToDevice);
  dim3 dimGrid1 = dim3(100,10);
  dim3 dimBlock1 = dim3(10,10);
  local_grad3_GPU_1<<<dimGrid1,dimBlock1>>>(devO1Ptr,devI1Ptr,devI2Ptr);
  cudaMemcpy(us,devO1Ptr,100000 * sizeof(double ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaMalloc(((void **)(&devO1Ptr)),100000 * sizeof(double ));
  cudaMemcpy(devO1Ptr,ut,100000 * sizeof(double ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),100000 * sizeof(double ));
  cudaMemcpy(devI1Ptr,u2,100000 * sizeof(double ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),100 * sizeof(double ));
  cudaMemcpy(devI2Ptr,Dt,100 * sizeof(double ),cudaMemcpyHostToDevice);
  dim3 dimGrid2 = dim3(100,1);
  dim3 dimBlock2 = dim3(100,10);
  local_grad3_GPU_2<<<dimGrid2,dimBlock2>>>(devO1Ptr,devI1Ptr,devI2Ptr);
  cudaMemcpy(ut,devO1Ptr,100000 * sizeof(double ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
}

__global__ void local_grad3_GPU_0(double *ur,double *D,double *u1)
{
  int k;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  double newVariable0;
  int t2;
  int t4;
  int t6;
  int t8;
  newVariable0 = ur[1000 * bx + 10 * ty + tx];
  for (k = 0; k <= 9; k += 1) 
    newVariable0 = (newVariable0 + (D[(k * 10) + tx] * u1[((((bx * 10) * 10) * 10) + (ty * 10)) + k]));
  ur[1000 * bx + 10 * ty + tx] = newVariable0;
}

__global__ void local_grad3_GPU_1(double *us,double *u,double *Dt)
{
  int m;
  int bx;
  bx = blockIdx.x;
  int by;
  by = blockIdx.y;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  double newVariable1;
  int t2;
  int t4;
  int t6;
  int t8;
  int t10;
  newVariable1 = us[1000 * bx + 10 * ty + 100 * by + tx];
  for (m = 0; m <= 9; m += 1) 
    newVariable1 = (newVariable1 + (u[(((((bx * 10) * 10) * 10) + ((by * 10) * 10)) + (m * 10)) + tx] * Dt[(ty * 10) + m]));
  us[1000 * bx + 10 * ty + 100 * by + tx] = newVariable1;
}

__global__ void local_grad3_GPU_2(double *ut,double *u2,double *Dt)
{
  int k;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  double newVariable2;
  int t2;
  int t4;
  int t6;
  int t8;
  newVariable2 = ut[1000 * bx + 100 * ty + tx];
  for (k = 0; k <= 9; k += 1) 
    newVariable2 = (newVariable2 + (u2[((((bx * 10) * 10) * 10) + ((k * 10) * 10)) + tx] * Dt[(ty * 10) + k]));
  ut[1000 * bx + 100 * ty + tx] = newVariable2;
}
