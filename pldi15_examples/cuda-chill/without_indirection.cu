/////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "smv.h"
#include "matrix_io.h"
#include "sparse_matrix.h"
//#include "cuPrintf.cu"
/////////////////////////////

#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define NNZ(i,j) (*NNZ_count)
#define index_(i) index[i]
#define index__(i) index[i + 1]
__global__ void spmv_final_level_GPU(int *NNZ_count,float *y,int *NNZ_i,float *a,int *NNZ_j,float *x,int *col);
__global__ void spmv_second_level_GPU(int *NNZ_count,float *y,int *_P_DATA1,float *_P_DATA2);
__global__ void spmv_GPU(int *NNZ_count,float *y,int *_P_DATA1,float *_P_DATA2,int *NNZ_i,float *a,int *NNZ_j,float *x,int *col);
void spmv_inspector(int n,struct inspector &NNZ,int *index);
__host__ void load_matrix(char *filename, int *rows, int *cols, float *vals);
struct inspector 
{
  int *j;
  int *i;
  int count;
}
;

//#define NZ 1666
//#define NUMROWS 494
//boneS01  127224 6715152
//bcsstk39  46772 2089294
//engine 143571 4706073
//gearbox 153746 9080404
//pkustk07  16860 2418804
//shipsec5 179860 10113096

#define NZ (10113096)
#define NUMROWS (179860)
#define BLOCKDIM2 (1024)
#define NONZEROS_PER_BLOCK (2048)
#define WARPSIZE (32)
#define NONZEROS_PER_WARP (512)
#define DIM_X   (NONZEROS_PER_WARP/WARPSIZE)
#define DIM_Y   (NONZEROS_PER_BLOCK/NONZEROS_PER_WARP)
#define DIM_Z   (32)
#define DIM_X2  4
#define DIM_Y2  256


__device__ void segreduce_warp(float *y,int* idx, float *val, int iteration)
{
    float left = 0;
    int tx = threadIdx.x;
//    int ty = threadIdx.y;
    //static __shared__ float carry_val[4];    
    int id = idx[tx];
   
   if(tx > 0)
    idx[tx+32] = -1;
   
   if(iteration > 0)
    if(tx == 0)
     if(idx[32] == id)
        val[0] +=  val[32];
    
    if( id == idx[(tx + 63) & 63] ) { left = val[tx -   1]; }val[tx] += left; left = 0;
    if( id == idx[(tx + 62) & 63] ) { left = val[tx -   2]; }val[tx] += left; left = 0;
    if( id == idx[(tx + 60) & 63] ) { left = val[tx -   4]; }val[tx] += left; left = 0;
    if( id == idx[(tx + 56) & 63] ) { left = val[tx -   8]; }val[tx] += left; left = 0;
    if( id == idx[(tx + 48) & 63] ) { left = val[tx -  16]; }val[tx] += left; left = 0;
    




   if( tx < 31 && id != idx[tx + 1])
           y[id] += val[tx];
    if(tx == 31){
       if(iteration > 0)  
         if(idx[32] != idx[0])
           y[idx[32]] += val[32]; 


    
         idx[32] = id;
         val[32] = val[31]; 
   
  
 }

  
}  

__device__ void segreduce_warp(float *y,int* idx, float *val)
{
    float left = 0;
    int tx = threadIdx.x;
//    int ty = threadIdx.y;
    //static __shared__ float carry_val[4];    
    int id = idx[tx];
   
   if(tx > 0)
    idx[tx+32] = -1;
   
   
    if(tx == 0)
     if(idx[32] == id)
        val[0] +=  val[32];
    
    if( id == idx[(tx + 63) & 63] ) { left = val[tx -   1]; }val[tx] += left; left = 0;
    if( id == idx[(tx + 62) & 63] ) { left = val[tx -   2]; }val[tx] += left; left = 0;
    if( id == idx[(tx + 60) & 63] ) { left = val[tx -   4]; }val[tx] += left; left = 0;
    if( id == idx[(tx + 56) & 63] ) { left = val[tx -   8]; }val[tx] += left; left = 0;
    if( id == idx[(tx + 48) & 63] ) { left = val[tx -  16]; }val[tx] += left; left = 0;
    



   if( tx < 31 && id != idx[tx + 1])
           y[id] += val[tx];
    if(tx == 31){
         if(idx[32] != idx[0])
           y[idx[32]] += val[32]; 


    
 
 
  
 }

  
}  



/*__device__ void segreduce_warp(float *y,int* idx, float *val, float *carry_val)
{
    float left = 0;
    int tx = threadIdx.x;
//    int ty = threadIdx.y;
    //static __shared__ float carry_val[4];    
    int id = idx[tx]; 
   if(tx ==0)
      val[0] += carry_val[0];
    
    
    if( tx >=   1 && id == idx[tx -   1] ) { left = val[tx -   1]; }val[tx] += left; left = 0;
    if( tx >=   2 && id == idx[tx -   2] ) { left = val[tx -   2]; }val[tx] += left; left = 0;
    if( tx >=   4 && id == idx[tx -   4] ) { left = val[tx -   4]; }val[tx] += left; left = 0;
    if( tx >=   8 && id == idx[tx -   8] ) { left = val[tx -   8]; }val[tx] += left; left = 0;
    if( tx >=  16 && id == idx[tx -  16] ) { left = val[tx -  16]; }val[tx] += left; left = 0;
    


  
    if(id != idx[tx + 1])
           y[id] += val[tx];
}
__device__ void segreduce_warp2( float *y, int* idx, float *val, float *carry_val)
{
    float left = 0;
    int tx = threadIdx.x;
 //   int ty = threadIdx.y;
    //static __shared__ float carry_val[4];    
    int id = idx[tx]; 

   if(tx ==0)
      val[0] += carry_val[0];


    
    
   if( tx >=   1 && id == idx[tx -   1] ) { left = val[tx -   1]; }val[tx] += left; left = 0;
    if( tx >=   2 && id == idx[tx -   2] ) { left = val[tx -   2]; }val[tx] += left; left = 0;
    if( tx >=   4 && id == idx[tx -   4] ) { left = val[tx -   4]; }val[tx] += left; left = 0;
    if( tx >=   8 && id == idx[tx -   8] ) { left = val[tx -   8]; }val[tx] += left; left = 0;
    if( tx >=  16 && id == idx[tx -  16] ) { left = val[tx -  16]; }val[tx] += left; left = 0;


    if((tx < WARPSIZE -1) && idx[tx] != idx[tx + 1])
           y[id] += val[tx];

}
*/
__device__ void segreduce_block(float *y, int* idx, float *val, int kk, int count)
{
    float left = 0;

    int tx = threadIdx.x + DIM_X2*threadIdx.y; //+ DIM_Y2*DIM_X*threadIdx.z;
    int ty = threadIdx.y;
    int id = idx[tx];

    if( tx >=   1 && id  == idx[tx -   1] ) { left = val[tx -   1]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=   2 && id  == idx[tx -   2] ) { left = val[tx -   2]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=   4 && id == idx[tx -   4] ) { left = val[tx -   4]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=   8 && id == idx[tx -   8] ) { left = val[tx -   8]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=  16 && id == idx[tx -  16] ) { left = val[tx -  16]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=  32 && id == idx[tx -  32] ) { left = val[tx -  32]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=  64 && id == idx[tx -  64] ) { left = val[tx -  64]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >= 128 && id == idx[tx - 128] ) { left = val[tx - 128]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >= 256 && id == idx[tx - 256] ) { left = val[tx - 256]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >= 512 && id == idx[tx - 512] ) { left = val[tx - 512]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();


  if(tx < BLOCKDIM2 - 1 && (ty <  (count - DIM_Y2*NONZEROS_PER_BLOCK* kk - NONZEROS_PER_BLOCK) / NONZEROS_PER_BLOCK) ){
   if(id != idx[tx + 1])
         y[id] += val[tx];
   }
   else
     y[id] += val[tx];   

}

__device__ void segreduce_block2(float *y, int* idx, float *val, int kk, int count)
{
    float left = 0;

    int tx = threadIdx.x; //+ DIM_Y2*DIM_X*threadIdx.z;
   // int ty = threadIdx.y;
    int id = idx[tx];

    if( tx >=   1 && id  == idx[tx -   1] ) { left = val[tx -   1]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=   2 && id  == idx[tx -   2] ) { left = val[tx -   2]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=   4 && id == idx[tx -   4] ) { left = val[tx -   4]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=   8 && id == idx[tx -   8] ) { left = val[tx -   8]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=  16 && id == idx[tx -  16] ) { left = val[tx -  16]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=  32 && id == idx[tx -  32] ) { left = val[tx -  32]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >=  64 && id == idx[tx -  64] ) { left = val[tx -  64]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >= 128 && id == idx[tx - 128] ) { left = val[tx - 128]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >= 256 && id == idx[tx - 256] ) { left = val[tx - 256]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();
    if( tx >= 512 && id == idx[tx - 512] ) { left = val[tx - 512]; } __syncthreads(); val[tx] += left; left = 0; __syncthreads();


 if((tx < BLOCKDIM2 - 1) && (tx + kk < count) ){
  
   if(id != idx[tx + 1])
         y[id] += val[tx];
 }
 else
   y[id] += val[tx];

}


void spmv(int n,int index[NUMROWS+1],float a[NZ],float y[NUMROWS],float x[NUMROWS],int col[NZ])
{
  int t14;
  int *devI5Ptr;
  float *devI4Ptr;
  int *devI3Ptr;
  float *devI2Ptr;
  int *devI1Ptr;
  float *devO3Ptr;
  int *devO2Ptr;
  float *devO1Ptr;
  int *NNZ_count;
  int t12;
  int t10;
  int t8;
  int t6;
  int _t91;
  int _t90;
  int _t89;
  int _t88;
  int _t87;
  int _t86;
  int _t85;
  int _t84;
  int _t83;
  int _t82;
  int _t81;
  int _t80;
  int _t79;
  int t4;
  int t2;
  float _P4[256 * 4];
  int _t50;
  int _t49;
  int _t48;
  int _t47;
  int _t46;
  int _t45;
  int _t44;
  int _P3[256 * 4];
  int _t43;
  int _t42;
  int _t41;
  int _t40;
  int _t39;
  int _t38;
  int _t37;
  float *_P_DATA2;
  int _t36;
  int _t35;
  int _t34;
  int _t33;
  int _t32;
  int _t31;
  int *_P_DATA1;
  int _t30;
  int _t29;
  int _t28;
  int _t27;
  int _t26;
  int _t25;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  float _P2[4 * 64];
  int _t18;
  int _t17;
  int _t16;
  int _t15;
  int _t14;
  int _t13;
  int _P1[4 * 64];
  int _t12;
  int _t10;
  int _t9;
  int _t11;
  int _t8;
  int _t7;
  int _t6;
  int _t5;
  int _t4;
  int _t3;
  int new_index;
  int *NNZ_i;
  int *NNZ_j;
  struct inspector NNZ;
  int i;
  int j;
  NNZ.inspector::count = 0;
  cudaEvent_t start_event, stop_event;
  float cuda_elapsed_time, time_2;
  ( cudaEventCreate(&start_event));
  ( cudaEventCreate(&stop_event));

//Anand adding the following
  NNZ.i = (int *)malloc((NZ+1)*sizeof(int));
  NNZ.j = (int *)malloc((NZ)*sizeof(int));
  NNZ.i[NZ] = -1;
time_2 = 0;
//end
printf("reached\n");

  spmv_inspector(n,NNZ,index);
  NNZ_count = &NNZ.inspector::count;
//Anand adding the following
  _P_DATA1 = (int*)malloc(( (NNZ(_t26,_t28) - NONZEROS_PER_BLOCK) / NONZEROS_PER_BLOCK + 1) * (DIM_Y)*sizeof(int ));
  _P_DATA2 = (float*)malloc(((NNZ(_t26,_t28) - NONZEROS_PER_BLOCK) / NONZEROS_PER_BLOCK + 1) *(DIM_Y)*sizeof(float));
//end

  cudaMalloc(((void **)(&devO1Ptr)),NUMROWS * sizeof(float ));
  cudaMemcpy(devO1Ptr,y, NUMROWS* sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devO2Ptr)),((NNZ(_t26,_t28) - NONZEROS_PER_BLOCK) / NONZEROS_PER_BLOCK + 1) *(DIM_Y)* sizeof(int ));
  cudaMalloc(((void **)(&devO3Ptr)),((NNZ(_t32,_t34) - NONZEROS_PER_BLOCK) / NONZEROS_PER_BLOCK + 1) *(DIM_Y)* sizeof(float ));
  cudaMalloc(((void **)(&devI1Ptr)),NZ * sizeof(int ));
  cudaMemcpy(devI1Ptr,NNZ.inspector::i,NZ * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),NZ * sizeof(float ));
  cudaMemcpy(devI2Ptr,a,NZ * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),NZ * sizeof(int ));
  cudaMemcpy(devI3Ptr,NNZ.inspector::j,NZ * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI4Ptr)),NUMROWS * sizeof(float ));
  cudaMemcpy(devI4Ptr,x,NUMROWS * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI5Ptr)),NZ * sizeof(int ));
  cudaMemcpy(devI5Ptr,col,NZ * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid1 = dim3((NNZ(t2,t4) - NONZEROS_PER_BLOCK) / NONZEROS_PER_BLOCK + 1,1);
  dim3 dimBlock1 = dim3(WARPSIZE,DIM_Y);
  cudaMalloc(((void **)(&NNZ_count)),sizeof(int ));
  cudaMemcpy(NNZ_count,&NNZ.inspector::count,sizeof(int ),cudaMemcpyHostToDevice);
  cudaEventRecord(start_event, 0);


  spmv_GPU<<<dimGrid1,dimBlock1>>>(NNZ_count,devO1Ptr,devO2Ptr,devO3Ptr,devI1Ptr,devI2Ptr,devI3Ptr,devI4Ptr,devI5Ptr);
  //cudaThreadSynchronize();
  cudaEventRecord(stop_event, 0);
  cudaEventSynchronize(stop_event);
  cudaEventElapsedTime(&cuda_elapsed_time, start_event, stop_event);
  time_2 += cuda_elapsed_time;
  printf("first: Elapsed time %f milliseconds\n", cuda_elapsed_time);

  cudaMemcpy(y,devO1Ptr,NUMROWS * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
//Anand changed NNZ(i,j) to NNZ.count
  cudaMemcpy(_P_DATA1,devO2Ptr,((NNZ.inspector::count - NONZEROS_PER_BLOCK) / NONZEROS_PER_BLOCK + 1) *(DIM_Y)* sizeof(int ),cudaMemcpyDeviceToHost);
  cudaFree(devO2Ptr);
//Anand changed NNZ(i,j) to NNZ.count
  cudaMemcpy(_P_DATA2,devO3Ptr,((NNZ.inspector::count - NONZEROS_PER_BLOCK) / NONZEROS_PER_BLOCK + 1) *(DIM_Y)* sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO3Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  cudaFree(devI4Ptr);
  cudaFree(devI5Ptr);

  cudaFree(NNZ_count);

  NNZ_count = &NNZ.inspector::count;
  cudaMalloc(((void **)(&devO1Ptr)),NUMROWS * sizeof(float ));
  cudaMemcpy(devO1Ptr,y,NUMROWS * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),((NNZ(_t26,_t28) - NONZEROS_PER_BLOCK) / NONZEROS_PER_BLOCK + 1) *(DIM_Y)* sizeof(int ));
  cudaMemcpy(devI1Ptr,_P_DATA1,((NNZ(_t26,_t28) - NONZEROS_PER_BLOCK) / NONZEROS_PER_BLOCK + 1) *(DIM_Y)* sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),((NNZ(_t32,_t34) - NONZEROS_PER_BLOCK) / NONZEROS_PER_BLOCK + 1) *(DIM_Y)* sizeof(float ));
  cudaMemcpy(devI2Ptr,_P_DATA2,((NNZ(_t32,_t34) - NONZEROS_PER_BLOCK) / NONZEROS_PER_BLOCK + 1) *(DIM_Y)* sizeof(float ),cudaMemcpyHostToDevice);
  dim3 dimGrid2 = dim3(1);
  dim3 dimBlock2 = dim3(DIM_X2,DIM_Y2);
  cudaMalloc(((void **)(&NNZ_count)),sizeof(int ));
  cudaMemcpy(NNZ_count,&NNZ.inspector::count,sizeof(int ),cudaMemcpyHostToDevice);
 // cudaPrintfInit();
  cudaEventRecord(start_event, 0);

  spmv_second_level_GPU<<<dimGrid2,dimBlock2>>>(NNZ_count,devO1Ptr,devI1Ptr,devI2Ptr);

//  cudaThreadSynchronize();
  cudaEventRecord(stop_event, 0);
  cudaEventSynchronize(stop_event);
  cudaEventElapsedTime(&cuda_elapsed_time, start_event, stop_event);
  time_2 += cuda_elapsed_time;
  printf("second:Elapsed time %f milliseconds\n", cuda_elapsed_time);

// cudaPrintfDisplay(stdout, true);
// cudaPrintfEnd();

  cudaMemcpy(y,devO1Ptr,NUMROWS * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(NNZ_count);
  NNZ_count = &NNZ.inspector::count;
  cudaMalloc(((void **)(&devO1Ptr)),NUMROWS * sizeof(float ));
  cudaMemcpy(devO1Ptr,y,NUMROWS * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),NZ * sizeof(int ));
  cudaMemcpy(devI1Ptr,NNZ.inspector::i,NZ * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),NZ * sizeof(float ));
  cudaMemcpy(devI2Ptr,a,NZ * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),NZ * sizeof(int ));
  cudaMemcpy(devI3Ptr,NNZ.inspector::j,NZ * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI4Ptr)),NUMROWS * sizeof(float ));
  cudaMemcpy(devI4Ptr,x,NUMROWS * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI5Ptr)),NZ * sizeof(int ));
  cudaMemcpy(devI5Ptr,col,NZ * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid3 = dim3(1,1);
  dim3 dimBlock3 = dim3(1,1);
  cudaMalloc(((void **)(&NNZ_count)),sizeof(int ));
  cudaMemcpy(NNZ_count,&NNZ.inspector::count,sizeof(int ),cudaMemcpyHostToDevice);
  cudaEventRecord(start_event, 0);

  spmv_final_level_GPU<<<1,BLOCKDIM2>>>(NNZ_count,devO1Ptr,devI1Ptr,devI2Ptr,devI3Ptr,devI4Ptr,devI5Ptr);
  cudaThreadSynchronize();
  cudaEventRecord(stop_event, 0);
  cudaEventSynchronize(stop_event);
  cudaEventElapsedTime(&cuda_elapsed_time, start_event, stop_event);
  time_2 += cuda_elapsed_time;
  printf("third:Elapsed time %f milliseconds\n", cuda_elapsed_time);

  cudaMemcpy(y,devO1Ptr,NUMROWS * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  cudaFree(devI4Ptr);
  cudaFree(devI5Ptr);
  cudaFree(NNZ_count);

  printf("total:Elapsed time %f milliseconds\n", time_2);
}

int main(int argc, char*argv[])
{
  int n = NUMROWS;
  static float a[NZ];
  static float y[NUMROWS];
  static float x[NUMROWS];
  static int index[NUMROWS+1];
  static int col[NZ];

  //////////////////////////////////////
    char filename[255];
    struct sparse_matrix A;

    if(argc!=2){
        fprintf(stderr, "use: program matrix\n");
        abort();
    }

    strcpy(filename, argv[1]);

    if (DEBUG) {
        fprintf(stderr, "main::Loading matrix\n");
        fprintf(stderr,"testing testing\n");    
    }

    load_matrix(filename, index, col, a);
    int i;
    for (i = 0; i < n; i++) {
        y[i] = 0.0;
        x[i] = 1.0;
    }
  //////////////////////////////////////
  


  spmv(n,index,a,y,x,col);

  //////////////////////////////
  for (i = 0; i < n; i++) {
    printf("%f\n", y[i]);
  }
  //////////////////////////////

  return 0;
}

////////////////////////////////////////////
__host__ void load_matrix(char *filename, int *rows, int *cols, float *vals)
{
    FILE *in;
    char data[NONZEROS_PER_BLOCK];
    int i;
    int n, nz;
    in = fopen(filename, "r");
    if(in==NULL){
        printf("something might be wrong with the file\n");
    }
    fgets(data, NONZEROS_PER_BLOCK, in);
    fprintf(stderr, "%s", data);
    fscanf(in, "%d %d\n", &n, &nz);
    if (DEBUG)
        fprintf(stderr, "load_sparse_matrix:: rows = %d, nnz = %d\n", n, nz);

    if (DEBUG)
                fprintf(stderr, "load_sparse_matrix::reading row index\n");

    for(i = 0; i <= n; i++){
        int temp;
        fscanf(in, "%d", &temp);
        temp--;
        rows[i] = temp;
    }

    if (DEBUG)
                fprintf(stderr, "load_sparse_matrix::reading column index\n");

    for(i = 0; i < nz; i++){
        int temp;
        fscanf(in, "%d", &temp);
        temp--;
        //fprintf(stderr, "%d\n", temp);
        cols[i] = temp;
        //fprintf(stderr, "%d\n", (*cols)[i]);
    }

    if (DEBUG)
                fprintf(stderr, "load_sparse_matrix::reading values\n");

    for(i = 0; i < nz; i++){
        char temp[20];
        REAL data;
        fscanf(in, "%s", temp);
        data = atof(temp);
        //fprintf(stderr, "%f\n", data);
        vals[i] = 1.0;
    }

    if (DEBUG)
                fprintf(stderr, "load_sparse_matrix::Loading sparse matrix done\n");
}

void spmv_inspector(int n,struct inspector &NNZ,int *index)
{
  int t2;
  int t4;
  for (t2 = 0; t2 <= n - 1; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
      NNZ.inspector::j[NNZ.inspector::count] = t4;
      NNZ.inspector::i[NNZ.inspector::count] = t2;
      NNZ.inspector::count = NNZ.inspector::count + 1;
    }
}

__global__ void spmv_GPU(int *NNZ_count,float *y,int *_P_DATA1,float *_P_DATA2,int *NNZ_i,float *a,int *NNZ_j,float *x,int *col)
{
  int bbbb;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  struct inspector NNZ;
  __device__ __shared__ int _P1[4 * 64];
  __device__ __shared__ float _P2[4 * 64];
  int t2;
  int t4;
  int t6;
  int t8;
  int t10;
  int t12;
  for (bbbb = 0; bbbb < DIM_X - 1; bbbb += 1) {
    _P1[tx + ty * 64] = NNZ_i[32 * bbbb + NONZEROS_PER_BLOCK * bx + NONZEROS_PER_WARP* ty + tx];
    _P2[tx + ty * 64] = (a[32 * bbbb + NONZEROS_PER_BLOCK * bx + NONZEROS_PER_WARP* ty + tx] * x[col[32 * bbbb + NONZEROS_PER_BLOCK * bx + NONZEROS_PER_WARP* ty+ tx]]);
    segreduce_warp(&y[0],&_P1[0 + ty * 64],&_P2[0 + ty * 64],bbbb);
  }
  _P1[tx + ty * 64] = NNZ_i[32 *(DIM_X -1) + NONZEROS_PER_BLOCK * bx + NONZEROS_PER_WARP* ty+ tx];
  _P2[tx + ty * 64] = (a[32 * (DIM_X -1) + NONZEROS_PER_BLOCK * bx + NONZEROS_PER_WARP* ty+ tx] * x[col[32 * (DIM_X -1) + NONZEROS_PER_BLOCK * bx + NONZEROS_PER_WARP*ty + tx]]);
  segreduce_warp(&y[0],&_P1[0 + ty * 64],&_P2[0 + ty * 64]);
  _P_DATA1[ty + bx * 4] = _P1[31 + ty * 64];
  _P_DATA2[ty + bx * 4] = _P2[31 + ty * 64];
}



__global__ void spmv_second_level_GPU(int *NNZ_count,float *y,int *_P_DATA1,float *_P_DATA2)
{
  int kkk;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  int tz;
  tz = threadIdx.z;
  __device__ __shared__ int _P3[DIM_Y2*DIM_X2];
  __device__ __shared__ float _P4[DIM_Y2*DIM_X2];
  int t2;
  int t4;
  int t6;
  int t8;
  int t10;
  int t12;
  for (kkk = 0; kkk <= (NNZ(t2,t4) - NONZEROS_PER_BLOCK) /(DIM_Y2*NONZEROS_PER_BLOCK); kkk += 1)
    if (ty <= (NNZ(t2,t4) - (DIM_Y2*NONZEROS_PER_BLOCK) * kkk - NONZEROS_PER_BLOCK) / NONZEROS_PER_BLOCK) {
      _P3[tx + ty*DIM_X2] = _P_DATA1[tx + (DIM_Y2 * kkk + ty) *DIM_X2];
      _P4[tx + ty*DIM_X2] = _P_DATA2[tx + (DIM_Y2 * kkk + ty) *DIM_X2];
      __syncthreads();
      segreduce_block(&y[0],&_P3[0 + 0*DIM_X2],&_P4[0 + 0*DIM_X2],kkk,*NNZ_count);
      __syncthreads();
    }
}

__global__ void spmv_final_level_GPU(int *NNZ_count,float *y,int *NNZ_i,float *a,int *NNZ_j,float *x,int *col)
{
  int new_index;
  struct inspector NNZ;
  int t2;
  int t4;
  int t6;
  __device__ __shared__ int _P1[BLOCKDIM2];
  __device__ __shared__ float _P2[BLOCKDIM2];
  int tx = threadIdx.x; 

  for (new_index = (NNZ(t2,t4)/NONZEROS_PER_BLOCK)*NONZEROS_PER_BLOCK ; new_index <= NNZ(t2,t4) - 1; new_index += BLOCKDIM2)
     if(tx + new_index <=  NNZ(t2,t4) - 1){     
      _P1[tx] =  NNZ_i[new_index+tx]; 
      _P2[tx] =  a[new_index+tx]*x[col[new_index +tx]];
  
     __syncthreads();      
     segreduce_block2(&y[0],&_P1[0],&_P2[0],new_index,*NNZ_count);

     __syncthreads();
     };
}
