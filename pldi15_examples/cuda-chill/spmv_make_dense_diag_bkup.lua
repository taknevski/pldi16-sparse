init("spmv_bcsr.cpp", "spmv", 0)
dofile("cudaize.lua")

make_dense(0,2,"k")

known("lb",0)
known("ub",493)
known("n", 494)
skew({0},2,{-1,1})
permute(0,{"k","i","j"})
shift_to(0,1,0)
compact(0,1,"a_prime", 0, "a")
permute(2,{"i","k"})

N = 494
Ti = 20
tile_by_index(2,{"i"},{Ti},{l1_control="ii"},{"ii","i","k"})
cudaize(2,"spmv_diag_GPU",{x=N,y=N},{block={"ii"}, thread={"i"}}, {"a_prime", "_P_DATA2"})
