#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
__global__ void kernel_GPU(float *energy,float *atoms);
#define N 1
#define VOLSIZEY 512
#define VOLSIZEX 512
#define VOLSIZEZ 1
#define ATOMCOUNT 4000
#define GRIDSPACING 0.1
#define zDim 0
float sqrtf(float );

void cenergy_cpu(float atoms[4000 * 4],float *energy,float z)
{
  float *devI1Ptr;
  float *devO1Ptr;
  int t22;
  int t20;
  int t18;
  int t16;
  int t14;
  int t12;
  int t10;
  int t8;
  int t6;
  float newVariable0;
  int _t123;
  int _t122;
  int _t121;
  int _t120;
  int _t119;
  int _t118;
  int _t117;
  int _t116;
  int _t114;
  int _t113;
  int _t112;
  int _t111;
  int _t109;
  int _t108;
  int _t107;
  int _t106;
  int _t105;
  int _t104;
  int _t103;
  int _t102;
  int _t101;
  int _t100;
  int _t99;
  int _t98;
  int _t96;
  int _t95;
  int _t94;
  int _t93;
  int _t92;
  int _t91;
  int _t90;
  int _t89;
  int _t88;
  int _t87;
  int _t86;
  int _t85;
  int _t84;
  int _t83;
  int _t82;
  int _t80;
  int _t79;
  int _t74;
  int _t73;
  int _t71;
  int _t70;
  int _t69;
  int _t68;
  int _t67;
  int _t65;
  int _t64;
  int _t63;
  int _t61;
  int _t60;
  int _t56;
  int _t55;
  int _t54;
  int _t53;
  float _P1[4001];
  int _t52;
  int _t51;
  int _t50;
  int _t48;
  int _t47;
  int _t46;
  int _t45;
  int _t39;
  int _t38;
  int _t37;
  int _t36;
  int _t34;
  int _t33;
  int t4;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  int _t19;
  int t2;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t7;
  int _t5;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  int n;
  float dx;
  float dy;
  float dz;
  cudaMalloc(((void **)(&devO1Ptr)),262144 * sizeof(float ));
  cudaMemcpy(devO1Ptr,energy,262144 * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),4 * sizeof(float ));
  cudaMemcpy(devI1Ptr,atoms,4 * sizeof(float ),cudaMemcpyHostToDevice);
  dim3 dimGrid0 = dim3(16,32);
  dim3 dimBlock0 = dim3(32,16);
  kernel_GPU<<<dimGrid0,dimBlock0>>>(devO1Ptr,devI1Ptr);
  cudaMemcpy(energy,devO1Ptr,262144 * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
}

__global__ void kernel_GPU(float *energy,float *atoms)
{
  int n;
  int tmp_ty;
  int bx;
  bx = blockIdx.x;
  int by;
  by = blockIdx.y;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  float z;
  float dx;
  float dy;
  float dz;
  int t2;
  int t4;
  __device__ __shared__ float _P1[4001];
  float newVariable0;
  int t6;
  int t8;
  int t10;
  for (tmp_ty = 8 * ty; tmp_ty <= __rose_lt(8 * ty + 7,124); tmp_ty += 1) 
    _P1[32 * tmp_ty + tx - 0] = atoms[32 * tmp_ty + tx];
  __syncthreads();
  for (n = 0; n <= 3996; n += 4) {
    newVariable0 = energy[512 * tx + 16384 * bx + 16 * by + ty];
    dx = ((0.1 * (16 * by + ty)) - _P1[n - 0]);
    dy = ((0.1 * (32 * bx + tx)) - _P1[n + 1 - 0]);
    dz = (z - _P1[n + 2 - 0]);
    newVariable0 += (_P1[n + 3 - 0] / sqrtf((((dx * dx) + (dy * dy)) + (dz * dz))));
    __syncthreads();
    energy[512 * tx + 16384 * bx + 16 * by + ty] = newVariable0;
    __syncthreads();
  }
  __syncthreads();
}
