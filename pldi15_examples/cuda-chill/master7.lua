--init("datacopy_spmv.c", "spmv", 0)
--dofile("cudaize.lua")

init("spmv.cpp", "spmv", 0)
dofile("cudaize.lua")
NNZ=1666
N=494
Ti = 1024
Tj = 256
Tk = 32

--flatten loop levels 1 and 2 with NNZ being uninterpreted omega function name
flatten(0,{"new_index"},{1,2}, "NNZ")

--split flattened loop level to be a perfect multiple of warp size (32)
split_with_alignment(1,3,1024)


--distribute remainder of splitted statement as it is not cudaized
distribute({1,2},2)

--tile for number of non zeros per CUDA block (1024)
tile_by_index(1,{"new_index"},{Ti},{l1_control="kk"},{"i","j","kk","new_index"})CU=1



--tile for number of nonzeros per warp (256)
tile_by_index(1,{"new_index"},{Tj},{l1_control="bbb"},{"i", "j", "kk","bbb","new_index"})CU=1


--tile for warp size(32) to get(256/32= 8) iterations
tile_by_index(1,{"new_index"},{Tk},{l1_control="bbbb"},{"i", "j", "kk", "bbb","bbbb","new_index"})CU=2


--normalization of tiled bounds
shift_to(1,6,0)
print_code(1)
--tile(1,6,6)
print_code(1)

--scalar expansion of product expression
scalar_expand(1,{4,6},"NNZ.i",1)
scalar_expand(1,{4,6},"RHS",1)
distribute({1,4}, 3)

peel(1,6,-1)

fuse({1,3,4}, 3)
distribute({1,5},3)
scalar_expand(5,{3,4,5},"_P1")
scalar_expand(5,{3,4,5},"RHS")

print_code(5)


distribute({5,7},3)

fuse({1,3,4,6,7},3)
fuse({1,3,4,6,7},5)
distribute({1,2},1)
distribute({1,5},1)
tile_by_index(5,{"kk"},{16},{l1_control="kkk"},{"i", "j", "kkk",  "kk","bbb","bbbb"})CU=1
shift_to(5,4,0)
scalar_expand(5,{4,5,6},"_P_DATA1",1)
scalar_expand(5,{4,5,6},"RHS",1)


cudaize(3,"spmv_GPU",{ a=NNZ,x=N,y=N,col=NNZ,temp=NNZ, NNZ_j=NNZ, NNZ_i=NNZ },{block={"kk"}, thread={"new_index", "bbb"}},{"_P_DATA1", "_P_DATA2"})
cudaize(5,"spmv_second_level_GPU",{ a=NNZ,x=N,y=N,col=NNZ,temp=NNZ, NNZ_i=NNZ},{block={}, thread={"bbbb", "bbb", "kk"}}, {"_P_DATA1", "_P_DATA2"})
cudaize(2,"spmv_final_level_GPU",{ a=NNZ,x=N,y=N,col=NNZ, temp=NNZ,NNZ_j=NNZ, NNZ_i=NNZ},{block={}, thread={}}, {})
reduce(1,{6},0,"segreduce_warp")
reduce(5,{4,5,6},0,"segreduce_block")
--add_sync(4,"tx")
print_code(7)
add_sync(9,"tx")
	
