--init("datacopy_spmv.c", "spmv", 0)
--dofile("cudaize.lua")

init("spmv.cpp", "spmv", 0)
dofile("cudaize.lua")
NNZ=1666
N=494
Ti = 1024
Tj = 256
Tk = 32

--flatten loop levels 1 and 2 with NNZ being uninterpreted omega function name
coalesce(0,{"new_index"},{1,2}, "NNZ")

--split flattened loop level to be a perfect multiple of warp size (32)
split_with_alignment(1,3,1024)

--distribute remainder of splitted statement as it is not cudaized
distribute({1,2},1)



--tile for number of non zeros per CUDA block (1024)
tile_by_index(1,{"new_index"},{Ti},{l1_control="kk"},{"i","j","kk","new_index"})CU=1



--tile for number of nonzeros per warp (256)
tile_by_index(1,{"new_index"},{Tj},{l1_control="bbb"},{"i", "j", "kk","bbb","new_index"})CU=1


--tile for warp size(32) to get(256/32= 8) iterations
tile_by_index(1,{"new_index"},{Tk},{l1_control="bbbb"},{"i", "j", "kk", "bbb","bbbb","new_index"})CU=2


scalar_expand_for_segreduce(1, target_level="bbbb", privatized levels = {"new_index"})
function scalar_expand_for_segreduce(stmt_num, target_level,parallel_levels,segment_index,padding,shared_memory)
end


--1.Identify levels to peel(those levels that come after target level)
--2.Identify block level(first parallel dimension)
--3.If shared mem=true elide first block level and expand other parallel levels
--4.Peel last statement at target_level +1 and distribute other statements and fuse with scalar expanded, repeat peel-distribute-fuse cycle
--5.all parallel levels uptil target level appear in second level reduction
--6.scalar expand at target level and then distribute the scalar expanded statements from peeled statement
--7. fuse scalar expanded statemets with first stateement and fuse till block level

cudaize(1,"spmv_GPU",{ a=NNZ,x=N,y=N,col=NNZ,temp=NNZ, NNZ_j=NNZ, NNZ_i=NNZ },{block={"kk"}, thread={"new_index", "bbb"}},{"_P_DATA1", "_P_DATA2"})


tile_by_index(8,{"kk"},{256},{l1_control="kkk"},{"i", "j", "kkk",  "kk","bbb","bbbb"})CU=1
shift_to(8,4,0)
scalar_expand(8,{4,5},"_P_DATA1",1)
scalar_expand(8,{4,5},"RHS",1)
distribute({8,11,12},5)
cudaize(8,"spmv_second_level_GPU",{ a=NNZ,x=N,y=N,col=NNZ,temp=NNZ, NNZ_i=NNZ},{block={}, thread={"bbb","kk"}}, {"_P_DATA1", "_P_DATA2"})



tile_by_index(2,{"new_index"},{512},{l1_control="kkk"},{"i", "j", "kkk",  "new_index"},strided)CU=1
cudaize(2,"spmv_final_level_GPU",{ a=NNZ,x=N,y=N,col=NNZ, temp=NNZ,NNZ_j=NNZ, NNZ_i=NNZ},{block={}, thread={"new_index"}}, {})

reduce(1,{6},0,"segreduce_warp",{5})
reduce(5,{6},0,"segreduce_warp",{})
reduce(8,{4,5},0,"segreduce_block",{})
add_sync(12,"tx")

