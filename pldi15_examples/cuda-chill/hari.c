#define M 16

int main(double* A, double* X, double* Y){



  int i, j, k,l;
  double d, e;
  for (i = 0; i < M; ++i) { 
    d = A[i];
    for (j = 0; j < M * M; ++j) {
      Y[i * M * M + j] = d * X[j];
    }
    for (k = 1; k < M; ++k) {
      d = A[i + k * M];
      for (l = 0; l < M * M; ++l) {
        e = d * X[M * M * k + l];
        Y[i * M * M + l] += e;
      }
    }
  }


}

