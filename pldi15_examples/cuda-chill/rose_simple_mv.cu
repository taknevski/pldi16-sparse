#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
__global__ void mm_GPU(int *y,int (*a)[1024],int *x);
#define N 1024

void normalMM(int x[1024UL],int a[1024UL][1024UL],int y[1024UL])
{
  int *devI2Ptr;
  int *devI1Ptr;
  int *devO1Ptr;
  int t8;
  int t6;
  int t4;
  int newVariable0;
  int _t9;
  int _t6;
  int _t7;
  int t2;
  int i;
  int j;
  int k;
  cudaMalloc(((void **)(&devO1Ptr)),1024 * sizeof(int ));
  cudaMemcpy(devO1Ptr,y,1024 * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),1048576 * sizeof(int ));
  cudaMemcpy(devI1Ptr,a,1048576 * sizeof(int ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),1024 * sizeof(int ));
  cudaMemcpy(devI2Ptr,x,1024 * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid0 = dim3(16,1);
  dim3 dimBlock0 = dim3(64,1);
  mm_GPU<<<dimGrid0,dimBlock0>>>(devO1Ptr,((int (*)[1024])devI1Ptr),devI2Ptr);
  cudaMemcpy(y,devO1Ptr,1024 * sizeof(int ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
}

__global__ void mm_GPU(int *y,int (*a)[1024],int *x)
{
  int j;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int t2;
  int newVariable0;
  int t4;
  int t8;
  newVariable0 = y[tx + 64 * bx];
  for (j = 0; j <= 1023; j += 1) 
    newVariable0 += (a[tx + 64 * bx][j] * x[j]);
  y[tx + 64 * bx] = newVariable0;
}
