--init("datacopy_spmv.c", "spmv", 0)
--dofile("cudaize.lua")

init("spmv.cpp", "spmv", 0)
dofile("cudaize.lua")
NNZ=1666
N=494
Ti = 1024
Tj = 256
Tk = 32

--flatten loop levels 1 and 2 with NNZ being uninterpreted omega function name
flatten(0,{"new_index"},{1,2}, "NNZ")

--split flattened loop level to be a perfect multiple of warp size (32)
split_with_alignment(1,3,1024)


--distribute remainder of splitted statement as it is not cudaized
distribute({1,2},2)

--tile for number of non zeros per CUDA block (1024)
tile_by_index(1,{"new_index"},{Ti},{l1_control="kk"},{"i","j","kk","new_index"})CU=1



--tile for number of nonzeros per warp (256)
tile_by_index(1,{"new_index"},{Tj},{l1_control="bbb"},{"i", "j", "kk","bbb","new_index"})CU=1


--tile for warp size(32) to get(256/32= 8) iterations
tile_by_index(1,{"new_index"},{Tk},{l1_control="bbbb"},{"i", "j", "kk", "bbb","bbbb","new_index"})CU=2


--normalization of tiled bounds
shift_to(1,6,0)
print_code(1)
--tile(1,6,6)
print_code(1)

--scalar expansion of product expression
scalar_expand(1,{4,6},"RHS",1)



--separate copy to temp array from accumulation array)
distribute({1,3}, 3)


--peel last thread/iteration 31 from accumulation
peel(3,6,-1)


--fuse back the accumulation with the data copy statement
fuse({1,3}, 3)



--distribute cudaized statements from non-cudaized statements
--print_code(1)
distribute({1,4},3)

print_code(1)

scalar_expand(4,{3,4,5},"RHS")


distribute({4,5},3)

fuse({1,4},3)
fuse({1,4},5)


distribute({1,2},1)
distribute({1,5},1)
distribute({1,3}, 6)

print_code(3)


print_code(1)
print_idx()
cudaize(1,"spmv_GPU",{ a=NNZ,x=N,y=N,col=NNZ, temp=NNZ},{block={"kk"}, thread={"new_index", "bbb"}},{"_P_DATA1"})
add_sync(1,"tx")
print_code(4)
reduce(3,{6},0,"segreduce_warp")

--reduce(3,6,0,"segreduce_warp")

print_idx()
--tile(5,3,3)
tile_by_index(5,{"kk"},{16},{l1_control="kkk"},{"i", "j", "kkk",  "kk","bbb","bbbb"})CU=1

--shift_to(5,6,0)
--tile(5,6,6)
print_code(5)
shift_to(5,4,0)
scalar_expand(5,{4,5,6},"RHS",1)  
distribute({5,6},6)
cudaize(5,"spmv_second_level_GPU",{ a=NNZ,x=N,y=N,col=NNZ, temp=NNZ},{block={}, thread={"bbbb", "bbb", "kk"}}, {"_P_DATA1"})
reduce(6,{4,5,6},0,"segreduce_block")

add_sync(5,"tx")
add_sync(6,"tx")
print_code(5)
--cudaize(5,"spmv_second_level_GPU",{ a=NNZ,x=N,y=N,col=NNZ, temp=NNZ},{block={}, thread={"bbbb", "bbb", "kk"}}, {"_P_DATA1"})
cudaize(2,"spmv_final_level_GPU",{ a=NNZ,x=N,y=N,col=NNZ, temp=NNZ},{block={}, thread={}}, {})
--print_code(6)


