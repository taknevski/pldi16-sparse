__global__ void sgd_blk_diag_operator(float *fv,int *metadata, float *new_ratings,unsigned short*_P_DATA2,int *_P_DATA1,float step_size, int t2)
{
  int l;
  int i;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int ty;
  ty = threadIdx.y;
  __device__ __shared__ float _P2[2];
  __device__ __shared__ float _P3[2 * 16];
  unsigned short newVariable5;
  float _P4[2];
  float _P5[2];
  int t4;
  int t6;
  int t8;
  int t10;
  int t12;
  if (ty <= _P_DATA1__(t2) - _P_DATA1_(t2) - 2 * bx - 1) 
    newVariable5 = _P_DATA2[ty + 2 * bx + _P_DATA1_(t2)];
  if (ty <= _P_DATA1__(t2) - _P_DATA1_(t2) - 2 * bx - 1) {
    for (t10 = 0; t10 <= 1; t10 += 1) 
      _P5[2 * newVariable5 + t10 - 2 * newVariable5] = fv[(2 * newVariable5 + t10)*16 + tx];
    for (i = 0; i <= 1; i += 1) {
      for (t12 = 0; t12 <= 1; t12 += 1) 
        _P4[2 * newVariable5 + 2 * t2 + t12 + 1 - (2 * t2 + 2 * newVariable5 + 1)] = fv[(2 * newVariable5 + 2 * t2 + t12 + 1)*16 + tx];
      for (l = 0; l <= 1; l += 1) 
        if (0 <= new_ratings(t2,2 * bx + _P_DATA1_(t2) + ty,i,l)) {
          _P2[ty] = -new_ratings[(2 * bx + _P_DATA1_(t2) + ty) * 4 + 2 * i + 1 * l];
          _P3[tx + ty * 16] = (_P5[2 * newVariable5 + i - 2 * newVariable5] * _P4[2 * (t2 - 23320 + newVariable5) + l + 46641 - (2 * t2 + 2 * newVariable5 + 1)]);
          segreduce_warp(&_P2[ty],&_P3[0 + ty * 16]);
          _P5[2 * newVariable5 + i - 2 * newVariable5] -= (step_size * ((_P2[ty] * _P4[2 * (t2 - 23320 + newVariable5) + l + 46641 - (2 * t2 + 2 * newVariable5 + 1)]) + (0.05f * _P5[2 * newVariable5 + i - 2 * newVariable5])));
          _P4[2 * (t2 - 23320 + newVariable5) + l + 46641 - (2 * t2 + 2 * newVariable5 + 1)] -= (step_size * ((_P2[ty] * _P5[2 * newVariable5 + i - 2 * newVariable5]) + (0.05f * _P4[2 * (t2 - 23320 + newVariable5) + l + 46641 - (2 * t2 + 2 * newVariable5 + 1)])));
        }
        else if (new_ratings(t2,2 * bx + _P_DATA1_(t2) + ty,i,l) <= -2) {
          _P2[ty] = -new_ratings[(2 * bx + _P_DATA1_(t2) + ty) * 4 + 2 * i + 1 * l];
          _P3[tx + ty * 16] = (_P5[2 * newVariable5 + i - 2 * newVariable5] * _P4[2 * (t2 - 23320 + newVariable5) + l + 46641 - (2 * t2 + 2 * newVariable5 + 1)]);
          segreduce_warp(&_P2[ty],&_P3[0 + ty * 16]);
          _P5[2 * newVariable5 + i - 2 * newVariable5] -= (step_size * ((_P2[ty] * _P4[2 * (t2 - 23320 + newVariable5) + l + 46641 - (2 * t2 + 2 * newVariable5 + 1)]) + (0.05f * _P5[2 * newVariable5 + i - 2 * newVariable5])));
          _P4[2 * (t2 - 23320 + newVariable5) + l + 46641 - (2 * t2 + 2 * newVariable5 + 1)] -= (step_size * ((_P2[ty] * _P5[2 * newVariable5 + i - 2 * newVariable5]) + (0.05f * _P4[2 * (t2 - 23320 + newVariable5) + l + 46641 - (2 * t2 + 2 * newVariable5 + 1)])));
        }
      for (t12 = 0; t12 <= 1; t12 += 1) 
        fv[(2 * newVariable5 + 2 * t2 + t12 + 1)*16 + tx] = _P4[2 * newVariable5 + 2 * t2 + t12 + 1 - (2 * t2 + 2 * newVariable5 + 1)];
    }
    for (t10 = 0; t10 <= 1; t10 += 1) 
      fv[(2 * newVariable5 + t10)*16 + tx] = _P5[2 * newVariable5 + t10 - 2 * newVariable5];
  }
}
