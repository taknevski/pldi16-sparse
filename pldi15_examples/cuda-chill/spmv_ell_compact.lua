init("spmv_ell.cpp", "spmv", 0)
dofile("cudaize.lua")
Tj=100
N=2048
M=100
Ti=1024
normalize(0,2)

--tile(0,2,10,2,counted)
tile_by_index(0,{"j"},{Tj},{l1_control="jj"},{"i","jj","j"})


shift_to(0,2,0)
shift_to(0,3,0)

compact(0,{2},{"a_prime","col_prime"}, 0, {"a","col"}, {1,-1}, {"index_", "index__"}, 100, 1)

permute(0,{"j", "i"})


scalar_expand(0,{1,2}, "a_prime")
--scalar_expand_by_index(0,{"j", "i"}, "a_prime",0, NO_PAD, ASSIGN_THEN_ACCUMULATE)
--scalar_expand_by_index(0,{"j", "i"}, "col_prime",0, NO_PAD, ASSIGN_THEN_ACCUMULATE)

scalar_expand(0,{1,2}, "col_prime")

distribute({0,3,4}, 1)
fuse({3,4},1)

tile_by_index(0,{"i"},{Ti},{l1_control="ii"},{"ii","i","j"})
cudaize(0,"spmv_ell_GPU",{ _P_DATA4=N*M,x=N,y=N,_P_DATA3=N*M},{block={"ii"}, thread={"i"}}, {"_P_DATA4", "_P_DATA5", "n"})
copy_to_registers(0, "j", "y")


known({"index__","index_"},{1,-1}, -1, 0)
known({"n"},{1}, -1, 0)




