--CUBLAS 2 MM Multiply

--This function form intializes "CUDAIZE v2" versus "CUDAIZE v1" if you
--call init() and use global variables to specify procedure and loop

--Second parameter is procedure # and third is loop #
init("csrspmv.c", "spmv", 0) 

dofile("cudaize.lua") --defines custom tile_by_index, copy_to_registers,
	--copy_to_shared methods

TI=128
TJ=32

n=1024
NNZ=100000

--print_code(0)

tile_by_index(0,{"col_index"}, {TJ}, {l1_control="jj"}, {"row", "jj","col_index"},strided)
--print_code(0)
tile_by_index(0,{"row"}, {TI}, {l1_control="ii"}, {"ii", "row","jj", "col_index"})

--normalize_index("i")



cudaize(0,"spmv_GPU", {rows=n, x=n, vals=NNZ, y=n, cols=NNZ},
        {block={"ii"}, thread={"row"}})
--print_code(0)
