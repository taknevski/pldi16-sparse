#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA1_(_t31) _P_DATA1[_t31]
#define _P_DATA1__(_t31) _P_DATA1[_t31 + 1]
#define col_(_t35) col[_t35]
#define index_(i) index[i]
#define index__(i) index[i + 1]
#define NZ 1666
#define NUMROWS 494
#define N 1000
#define SGD_LAMBDA 0.05f
#define SGD_FEATURE_SIZE 16

struct a_list 
{
  int col_[1];
  float ratings[4];
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void sgd_kernel_global_simd(int n,int *index,float *ratings,float *y,float *x,int *col,float **fv,float step_size,float err)
{
  int t10;
  int t8;
  int t6;
  int t4;
  int t2;
  int _t45;
  int _t44;
  int _t43;
  int _t42;
  int _t41;
  int _t40;
  int newVariable4;
  int newVariable3;
  int newVariable2;
  int In_3;
  int In_2;
  int In_1;
  int _t39;
  int _t38;
  int _t37;
  struct a_list *_P_DATA4;
  int newVariable1;
  int newVariable0;
  struct mk *_P_DATA3;
  float *new_ratings;
  struct a_list *_P1[1249];
  int *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t36;
  int _t35;
  int _t34;
  int _t33;
  int _t32;
  int _t31;
  int _t30;
  int _t29;
  int _t28;
  int _t27;
  int _t25;
  int _t26;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  int _t18;
  int _t17;
  int _t16;
  int _t15;
  int _t14;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t7;
  int _t6;
  int _t5;
  int _t4;
  int l;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  int k;
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * 1250)));
  _P_DATA1[0] = 0;
  _P_DATA3 = ((struct mk *)(malloc(sizeof(struct mk ) * 1249)));
  chill_count_1 = 0;
  _P_DATA1[0] = 0;
  for (_t31 = 0; _t31 <= 1248; _t31 += 1) {
    _P1[1 * _t31] = 0;
    _P_DATA1[1 * _t31 + 1] = 0;
  }
  for (t2 = 0; t2 <= 499; t2 += 1) {
    for (t4 = 0; t4 <= 1; t4 += 1) 
      for (t6 = index_(2 * t2 + t4); t6 <= index__(2 * t2 + t4) - 1; t6 += 1) {
        _t31 = (col_(t6) + 998) / 2 - t2;
        _P_DATA3[_t31].ptr = 0;
      }
    for (t4 = 0; t4 <= 1; t4 += 1) 
      for (t6 = index_(2 * t2 + t4); t6 <= index__(2 * t2 + t4) - 1; t6 += 1) {
        _t31 = (col_(t6) + 998) / 2 - t2;
        _t34 = (col_(t6) + 998) % 2;
        if (_P_DATA3[_t31].ptr == 0) {
          _P_DATA4 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
          _P_DATA4 -> next = _P1[_t31];
          _P1[_t31] = _P_DATA4;
          _P_DATA3[_t31].ptr = _P1[_t31];
          for (newVariable0 = 0; newVariable0 <= 1; newVariable0 += 1) 
            for (newVariable1 = 0; newVariable1 <= 1; newVariable1 += 1) 
              _P_DATA3[_t31].ptr -> ratings[2 * newVariable0 + 1 * newVariable1] = 0;
          _P_DATA3[_t31].ptr -> col_[0] = t2;
          chill_count_1 += 1;
          _P_DATA1[_t31 + 1] += 1;
        }
        _P_DATA3[_t31].ptr -> ratings[2 * t4 + 1 * _t34] = ratings[t6];
      }
  }
  _P_DATA2 = ((int *)(malloc(sizeof(int ) * chill_count_1)));
  new_ratings = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 4))));
  for (t2 = 0; t2 <= 1248; t2 += 1) {
    for (newVariable2 = 1 - _P_DATA1[1 * t2 + 1]; newVariable2 <= 0; newVariable2 += 1) {
      _P_DATA2[_P_DATA1[1 * t2] - newVariable2] = _P1[1 * t2] -> col_[0];
      for (newVariable3 = 0; newVariable3 <= 1; newVariable3 += 1) 
        for (newVariable4 = 0; newVariable4 <= 1; newVariable4 += 1) 
          new_ratings[4 * (_P_DATA1[1 * t2] - newVariable2) + 2 * newVariable3 + 1 * newVariable4] = _P1[1 * t2] -> ratings[2 * newVariable3 + 1 * newVariable4];
      _P_DATA4 = _P1[1 * t2] -> next;
      free(_P1[1 * t2]);
      _P1[1 * t2] = _P_DATA4;
    }
    _P_DATA1[1 * t2 + 1] += _P_DATA1[1 * t2];
  }
  for (t2 = 0; t2 <= 1248; t2 += 1) 
    for (t4 = _P_DATA1_(t2); t4 <= _P_DATA1__(t2) - 1; t4 += 1) 
      for (t6 = 0; t6 <= 1; t6 += 1) 
        for (t8 = 0; t8 <= 1; t8 += 1) 
          for (t10 = 0; t10 <= 15; t10 += 1) 
            err += (new_ratings[t4 * 4 + 2 * t6 + 1 * t8] * fv[2 * (t2 - 499) + 2 * _P_DATA2[t4] + t8][_t36]);
  free(_P_DATA1);
  free(_P_DATA2);
  free(new_ratings);
  free(_P_DATA3);
}

int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[495UL];
  int col[1666UL];
  float **fv;
  float step_size = 0.09f;
  float err;;
  sgd_kernel_global_simd(n,index,a,y,x,col,fv,step_size,err);
  return 0;
}
