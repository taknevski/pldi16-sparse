#define ifhp 14
#define ofhp 12
#define ifwp 14
#define ofwp 12
#define nIfm 16
#define nOfm 16
#define KH 3
#define KW 3
#define ofw 12
#define ofh 12
void naive_conv_fp(int nImg,float *input[nIfm][ifhp][ifwp], float *output[nOfm][ofhp][ofwp], float *wt[nIfm][KH][KW])
{
   /*
   float (* __restrict input )[nIfm][ifhp][ifwp] = (float (*)[*][*][*])inp;
   float (* __restrict output)[nOfm][ofhp][ofwp] = (float (*)[*][*][*])outp;
   float (* __restrict wt    )[nIfm][KH][KW]     = (float (*)[*][KH][KW])wp;
   */
   int img, ofm, ifm, oj, oi, kj, ki;

   for(img=0; img < nImg; img++) {
      for(ofm=0; ofm < nOfm; ofm++) {
          //output[img][ofm][:][:] = 0.0f;
	 for(ifm=0; ifm < nIfm; ifm++) {
	    for(oj=0; oj < ofh; oj++) {
	       //int ij = oj * stride_h;
	       for(oi=0; oi < ofw; oi++) {
		  //int ii = oi * stride_w;
		  for(kj=0;kj < KH; kj++) {
		     for(ki=0;ki<KW;ki++) {
			output[img][ofm][oj][oi] += input[img][ifm][kj+oj][oi+ki] * wt[ofm][ifm][kj][ki];
		     }
		  }
	       }
	    }
	 }
      }
   }
}

