#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA1_(_t8) _P_DATA1[_t8]
#define _P_DATA1__(_t8) _P_DATA1[_t8 + 1]
#define col_(_t10) col[_t10]
#define index_(i) index[i]
#define index__(i) index[i + 1]
#define NZ 1666
#define NUMROWS 494
#define N 1000
#define SGD_LAMBDA 0.05f
#define SGD_FEATURE_SIZE 16
#include <stdio.h>
#include <stdlib.h>
struct a_list 
{
  int col_;
  float ratings[1];
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void sgd_kernel_global_simd(int n,int m, int *index,float *ratings,float *y,float *x,int *col,float **fv,float step_size,float err)
{
  int t6;
  int t4;
  int t2;
  int newVariable0;
  int In_1;
  struct a_list *_P_DATA4;
  struct mk *_P_DATA3;
  float *new_ratings;
  struct a_list **_P1;
  int *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t6;
  int _t4;
  int _t5;
  int l;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  int k;
  _P_DATA1 = (int *)malloc(sizeof(int ) * (m+n));
  _P1 = ((struct a_list **)(malloc(sizeof(struct a_list *) * (m+n-1)))) ; 
    printf("passed\n");
  chill_count_1 = 0;
  for (_t8 = 0; _t8 <= m+ n - 1; _t8 += 1) {
    _P1[1 * _t8] = NULL;
    _P_DATA1[1 * _t8] = 0;
  }
  for (t2 = 0; t2 <= n-1; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
      _t8 = col_(t4) - t2 + n-1;
       printf("passed2\n"); 
      _P_DATA4 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));

      _P_DATA4 -> next = _P1[_t8];
      _P1[_t8] = _P_DATA4;
      _P1[_t8] -> ratings[0] = 0;
      _P1[_t8] -> col_ = t2;
      chill_count_1 += 1;
      _P_DATA1[_t8] += 1;
      _P1[_t8] -> ratings[0] = ratings[t4];
    }
  _P_DATA2 = ((int *)(malloc(sizeof(int ) * chill_count_1)));

 new_ratings = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 1))));
printf("passed3\n");  
for (t2 = 0; t2 <= m +n - 2; t2 += 1) 
    for (newVariable0 = 1 - _P_DATA1[1 * t2 + 1]; newVariable0 <= 0; newVariable0 += 1) {
      _P_DATA2[_P_DATA1[1 * t2] - newVariable0] = _P1[t2] -> col_;
      new_ratings[1 * (_P_DATA1[1 * t2] - newVariable0)] = _P1[t2] -> ratings[0];
      _P_DATA4 = _P1[t2] -> next;
      _P1[t2] = _P_DATA4;
      free(_P1[t2]);
      _P_DATA1[1 * t2 + 1] += _P_DATA1[1 * t2];
    }
  // Note to rashid
  // After Inspection new_ratings, _P_DATA1 and _P_DATA2 need to be communicated to the GPU kernel

 
  free(_P_DATA1);
  free(_P_DATA2);
  free(new_ratings);
  free(_P_DATA3);
}

int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[495UL];
  int col[1666UL];
  float **fv;
  float step_size = 0.09f;
  float err;
  sgd_kernel_global_simd(n,n,index,a,y,x,col,fv,step_size,err);
  return 0;
}
