#define nelt 100
#define lz 10
#define lx 10
#define ly 10
void tensor(double *u, double *D, double *Dt, double *ur, double *us, double *ut){

	int e;
	int j;
	int i;
	int k;
	int m;

	for(e = 0; e < nelt; e++){
		for(j = 0; j < lx*ly; j++){
			for(i = 0; i < lz; i++){
				for(k = 0; k < lx; k++){
					ur[e*lx*ly*lz + j*lz + i] += D[k*ly + i]*u[e*lx*ly*lz + j*lz + k];
				}
			}
		}
	}

	for(e = 0; e < nelt; e++){
		for(j = 0; j < lx; j++){
			for(i = 0; i < ly; i++){
				for(k = 0; k < lx; k++){
					for(m = 0; m < ly; m++){
						us[e*lx*ly*lx + j*ly*lx + i*lx + k] += u[e*lx*ly*lx + j*ly*lx + m*lx + k]*Dt[i*ly + m];
					}
				}
			}
		}
	}

	for(e = 0; e < nelt; e++){
		for(j = 0; j < lx; j++){
			for(i = 0; i < ly*lz; i++){
				for(k = 0; k < lx; k++){
					ut[e*lx*ly*lz + j*ly*lz + i] += u[e*lx*ly*lz + k*ly*lz + i]*Dt[j*ly + k];
				}
			}
		}
	}

}

