#include <stdio.h>
#include <oski/oski_Tis.h> /*Get OSKI bindings */
//#define  DIM 3 /* matrix dimension */
#define  NUM_STORED_NZ 2 /* number of stored non-zero values */
//spmv(A.nrows,A.rows,A.vals,b2,x,A.cols)
void oski_bcsr(int dim,int *Aptr, float *Aval, float *y, float *x, int *Aind)
{
	
	/* Sparse matrix A, in compressed sparse row (CSR) format */
	//int Aptr[DIM+1] = { 0, 0, 1, 2 };
	//int Aind[NUM_STORED_NZ] = { 0, 0 };
        //float Aval[NUM_STORED_NZ] = {-2, 0.5};	
	/* Dense vectors, x, y, and scalar multipliers, α, β */
        //float x[DIM] = { .25, .45, .65 };
	//float y[DIM] = { 1, 1, 1 };
	float alpha = 1, beta = 0; 
	/* OSKI matrix/vector objects */
	oski_matrix_t A_tunable;
	oski_vecview_t x_view, y_view;
	
	/* Initialize OSKI and create matrix */
	oski_Init();
	
	A_tunable = oski_CreateMatCSR( Aptr, Aind, Aval, dim, dim, /* CSR arrays */
	COPY_INPUTMAT, /* ”copy mode” */
	/* remaining args specify how to interpret non−zero pattern */
 	1, INDEX_ZERO_BASED);

	/* Create wrappers around the dense vectors. */
	x_view = oski_CreateVecView( x, dim, STRIDE_UNIT );
	y_view = oski_CreateVecView( y, dim, STRIDE_UNIT );

	/* Perform matrix vector multiply, y ← α · A· x + β · y. */
	oski_MatMult( A_tunable, OP_NORMAL, alpha, x_view, beta, y_view);

	/* Clean−up interface objects and shut down OSKI */
	oski_DestroyMat( A_tunable );
	oski_DestroyVecView( x_view );
	oski_DestroyVecView( y_view );
	oski_Close();

	/* Print result , y. Should be ”[ .75 ; 1.05 ; .225 ]” */
	//printf ( "Answer: y = [ %f ; %f ; %f ]\n", y[0], y[1], y[2] );
	//return 0;
}
