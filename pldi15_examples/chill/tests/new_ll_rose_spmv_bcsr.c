/*
 * latest_inspector.c
 *
 *  Created on: Sep 4, 2014
 *      Author: anand
 */

#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define index_(i) index[i]
#define index__(i) index[i + 1]
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>

typedef struct float_list{
  float data[R*C]; 
  int col;	
  struct float_list *next;
}float_list;

typedef struct int_list{
  int pos; 
  struct int_list *next;
}int_list;

typedef struct marked{
  float_list *ptr;	
}marked;


float_list* insert_float(float_list *ptr){
    float_list *temp = (float_list*)malloc(sizeof(float_list));
    temp->next = ptr;	 			  
    //ptr  = temp;    
    return temp;			
}

int_list* insert_int(int_list *ptr){
    int_list *temp = (int_list*)malloc(sizeof(int_list));
    temp->next = ptr;	 			  

    return temp;			
}

float_list * copy_a_prime(int i, float *a, float_list *a_list, int *offset){
int k, j;
		for (k = 0; k < R; k++)
			for (j = 0; j < C; j++)
				a[i*R*C + k*C + j] = a_list->data[k* C + j];	

  offset[i] = a_list->col;	
  float_list *temp = a_list->next;
   
// a_list->next = NULL;			
  free(a_list);  	
  
  return temp;
  			

}
int_list *copy_offset_kk(int i, int *offset, int_list  *i_list){
int k, j;
		
		offset[i] = i_list->pos;	

  int_list *temp = i_list->next;	
  free(i_list);  	
  
  return temp;	
}


void spmv_bcsr(int m, int n, int *index, float *a, float *y, float *x, int *col,
		int nnz) {

	struct timeval t0, t1, t2;
        gettimeofday(&t0, NULL);
  	marked *mk;
  	float_list *a_list=NULL;
  	int_list *i_list=NULL;


	int *offset_index;
	int *offset_kk;
	
	float *a_prime;
	int ii, i, i_, kk, j, k, k_,count;
	float_list *temp;

	/* Inspector */



	


	//index-List = (int *) malloc(sizeof(index_list));
	
	mk = (marked*) malloc(n / C * sizeof(marked));
	offset_index = (int *) malloc((n/R + 1) * sizeof(int));

	offset_index[0] = 0;
	count = 0;
	for (ii = 0; ii < n / R; ii++) {

		//for (kk = 0; kk < n / C; kk++)
		//	marked[kk] = 0;

		//	memset (marked,0,n/C*sizeof(int));

		for (i = 0; i < R; i++)    // somehow show this is outdented
			for (j = index[ii * R + i]; j < index[ii * R + i + 1]; j++) {
				int kk = col[j] / C;

				mk[kk].ptr = NULL;

			}

		for (i = 0; i < R; i++)    // somehow show this is outdented
			for (j = index[ii * R + i]; j < index[ii * R + i + 1]; j++) {
				int kk = col[j] / C;  // using condition and bounds on k
				int k = col[j] - kk * C;

				//if (kk * C + offset == col[j]) {
				if (mk[kk].ptr == NULL) {
					//ptr = insert_float(a_list);
					
					temp = (float_list*)malloc(sizeof(float_list));
    					temp->next = a_list;
					a_list = temp;			
					mk[kk].ptr = a_list;
					for (i_ = 0; i_ < R; i_++)
						for (k_ = 0; k_ < C; k_++)
							mk[kk].ptr->data[i_ * C + k_] = 0;
					mk[kk].ptr->col = kk;	
					count++;
				}
			
						
				  mk[kk].ptr->data[i * C + k] = a[j];

				//}

			}
		offset_index[ii + 1] = count;
	}
  //gettimeofday(&t0, NULL);
	//
	offset_kk = (int *) malloc(count * sizeof(int));
	a_prime = (float *) malloc(count * R * C * sizeof(float));
	for(i = count-1; i >= 0; i--){

		for (k = 0; k < R; k++)
			for (j = 0; j < C; j++)
				a_prime[i*R*C + k*C + j] = a_list->data[k* C + j];	

  		offset_kk[i] = a_list->col;	
  		float_list *temp = a_list->next;
   		free(a_list);  	
  
  		a_list = temp;

												
	}

	gettimeofday(&t1, NULL);
	double elapsed = t1.tv_sec - t0.tv_sec
			+ (t1.tv_usec - t0.tv_usec) / 1000000.0;
	printf("Inspector Time: %.6lf seconds\n", elapsed);

	printf("Starting actual computation\n");
	gettimeofday(&t1, NULL);
	/* Executor */

	for (ii = 0; ii < n / R; ii++)
		for (kk = offset_index[ii]; kk < offset_index[ii + 1]; kk++)
			for (i = 0; i < R; i++)
				for (k = 0; k < C; k++)
					y[ii * R + i] += a_prime[kk * R * C + i * C + k]
							* x[C * offset_kk[kk] + k];

	gettimeofday(&t2, NULL);
	elapsed = t2.tv_sec - t1.tv_sec + (t2.tv_usec - t1.tv_usec) / 1000000.0;
	printf("%.6lf seconds elapsed\n", elapsed);
	double mflop_count = 1e-6 * (double) nnz * (double) 2.0;
	printf("Performance :%.6lf MFLOPS\n", mflop_count / elapsed);

	free(offset_index);
	free(offset_kk);
	free(mk);
	free(a_prime);
}


