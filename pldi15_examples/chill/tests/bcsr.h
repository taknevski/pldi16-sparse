
#ifndef __BCSR__
#define __BCSR__
void spmv_bcsr(int m, int n, int *index, float *a, float *y, float *x,
	       int *col, int nnz, int col_dim) ;
#endif

