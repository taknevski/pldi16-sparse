#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA2_(_t5) _P_DATA2[_t5]
#define _P_DATA2__(_t5) _P_DATA2[_t5 + 1]
#define col_(_t9) col[_t9]
#define index_(i) index[i]
#define index__(i) index[i + 1]
#define NZ 1666
#define NUMROWS 494
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>


struct a_list 
{
  float data[64];
  int col;
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void spmv_bcsr(int m, int n, int *index, float *a, float *y, float *x, int *col,int nnz)
{
  int t8;
  int t6;
  int t4;
  int t2;
  int In_3;
  int In_2;
  int In_1;
  int _t12;
  int _t11;
  int _t10;
  struct a_list *_P_DATA6;
  int newVariable2;
  int newVariable1;
  struct mk *_P_DATA5;
  float *a_prime;
  struct a_list *_P_DATA4;
  int *_P_DATA3;
  int chill_count_1;
  int *_P_DATA2;
  int chill_count_0;
  int newVariable0;
  float *_P_DATA1;
  int _t9;
  int _t8;
  int _t6;
  int _t7;
  int _t5;
  int k;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  _P_DATA1 = ((float *)(malloc(sizeof(float ) * 46840)));
  for (newVariable0 = 0; newVariable0 <= 46834; newVariable0 += 1) 
    _P_DATA1[newVariable0] = x[newVariable0];
  _P_DATA2 = ((int *)(malloc(sizeof(int ) * (5854 + 1))));
  _P_DATA4 = 0;
  _P_DATA5 = ((struct mk *)(malloc(sizeof(struct mk ) * 5855)));
  chill_count_1 = 0;
  _P_DATA2[0] = 0;
  for (t2 = 0; t2 <= 5853; t2 += 1) {
    for (t4 = 0; t4 <= 7; t4 += 1) 
      for (t6 = index_(8 * t2 + t4); t6 <= index__(8 * t2 + t4) - 1; t6 += 1) {
        _t6 = col_(t6) / 8;
        _P_DATA5[_t6].ptr = 0;
      }
    for (t4 = 0; t4 <= 7; t4 += 1) 
      for (t6 = index_(8 * t2 + t4); t6 <= index__(8 * t2 + t4) - 1; t6 += 1) {
        _t6 = col_(t6) / 8;
        _t8 = col_(t6) - 8 * _t6;
        if (_P_DATA5[_t6].ptr == 0) {
          _P_DATA6 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
          _P_DATA6 -> next = _P_DATA4;
          _P_DATA4 = _P_DATA6;
          _P_DATA5[_t6].ptr = _P_DATA4;
          for (newVariable1 = 0; newVariable1 <= 7; newVariable1 += 1) 
            for (newVariable2 = 0; newVariable2 <= 7; newVariable2 += 1) 
              _P_DATA5[_t6].ptr -> data[8 * newVariable1 + 1 * newVariable2] = 0;
          _P_DATA5[_t6].ptr -> col = _t6;//
          chill_count_1 += 1; 
        }
        _P_DATA5[_t6].ptr -> data[8 * t4 + 1 * _t8] = a[t6];
      }
    _P_DATA2[t2 + 1] = chill_count_1;
  }
  for (t2 = -chill_count_1 + 1; t2 <= 0; t2 += 1) {
    if (chill_count_1 + t2 <= 1) {
      _P_DATA3 = ((int *)(malloc(sizeof(int ) * chill_count_1)));
      a_prime = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 64))));
    }
    for (t4 = 0; t4 <= 7; t4 += 1) 
      for (t6 = 0; t6 <= 7; t6 += 1) 
        a_prime[-t2 * 64 + 8 * t4 + 1 * t6] = _P_DATA4 -> data[8 * t4 + 1 * t6];
    _P_DATA3[-t2] = _P_DATA4 -> col;
    _P_DATA6 = _P_DATA4 -> next;
    free(_P_DATA4);
    _P_DATA4 = _P_DATA6;
  }
  for (t2 = 0; t2 <= 5853; t2 += 1) 
    for (t4 = _P_DATA2_(t2); t4 <= _P_DATA2__(t2) - 1; t4 += 1) 
      for (t6 = 0; t6 <= 7; t6 += 1) 
        for (t8 = 0; t8 <= 7; t8 += 1) 
          y[8 * t2 + t6] += (a_prime[t4 * 64] * _P_DATA1[8 * _P_DATA3[t4] + t8]);
  for (t2 = 46832; t2 <= 46834; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) 
      y[t2] += (a[t4] * x[col[t4]]);
  free(_P_DATA1);
  free(_P_DATA2);
  free(_P_DATA3);
  free(a_prime);
  free(_P_DATA5);
}

/*int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[494 + 1];
  int col[1666UL];
  spmv(n,index,a,y,x,col);
  return 0;
}
*/
