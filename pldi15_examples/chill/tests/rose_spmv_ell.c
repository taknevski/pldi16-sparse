#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define index_(i) index[i]
#define index__(i) index[i + 1]
#include <stdlib.h>
void spmv_ell(int m, int n,int *index,float *a,float *y,float *x,int *col)
{
  int t4;
  int t2;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int *_P2;
  int _t8;
  int _t7;
  float *_P1;
  int _t6;
  int _t5;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;


  _P1 = malloc(n*m*sizeof(float));
  _P2 = malloc(n*m*sizeof(int));

  for (t2 = 0; t2 <= n-1; t2 += 1) {
    for (t4 = 0; t4 <= index__(t2) - index_(t2) - 1; t4 += 1) {
      _P1[t4 + t2 * m] = a[t4 + index_(t2)];
      _P2[t4 + t2 * m] = col[t4 + index_(t2)];
    }
    for (t4 = index__(t2) - index_(t2); t4 <= m-1; t4 += 1) {
      _P1[t4 + t2 * m] = 0;
      _P2[t4 + t2 * m] = 0;
    }
  }
  for (t2 = 0; t2 <= n-1; t2 += 1) 
    for (t4 = 0; t4 <= m-1; t4 += 1) 
      y[t2] += (_P1[t4 + t2 * m] * x[_P2[t4 + t2 * m]]);
}

/*int main()
{
  int n = 10;
  float val = 10.0;
  float *a;
  float *y;
  float *x;
  int index[10UL];
  int *col;
  a = (y = (x = &val));
  col = &n;
  spmv(n,index,a,y,x,col);
  return 0;
}
*/
