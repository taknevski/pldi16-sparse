#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define index_(i) index[i]
#define index__(i) index[i + 1]

void spmv(int n,int *index,float *a,float *y,float *x,int *col)
{
  int t4;
  int t2;
  float *_P_DATA4;
  int *_P_DATA3;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int *_P_DATA2;
  int _t8;
  int _t7;
  float *_P_DATA1;
  int _t6;
  int _t5;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;

   _P_DATA1= (float *)malloc(n*n*sizeof(float));
   _P_DATA2= (float *)malloc(n*n*sizeof(float));
   _P_DATA3= (float *)malloc(n*n*sizeof(float));
   _P_DATA4= (float *)malloc(n*n*sizeof(float));

  for (t2 = 0; t2 <= n - 1; t2 += 1) {
    for (t4 = 0; t4 <= index__(t2) - index_(t2) - 1; t4 += 1) {
      _P_DATA1[t4 + t2 * n] = a[t4 + index_(t2)];
      _P_DATA2[t4 + t2 * n] = col[t4 + index_(t2)];
    }
    for (t4 = index__(t2) - index_(t2); t4 <= n-1; t4 += 1) {
      _P_DATA1[t4 + t2 * n] = 0;
      _P_DATA2[t4 + t2 * n] = 0;
    }
  }
  for (t2 = 0; t2 <= n - 1; t2 += 1) 
    for (t4 = 0; t4 <= n-1; t4 += 1) {
      if (_P_DATA2[t2 * n + t4] != 0) 
        _P_DATA3[t2 * n + t4] = _P_DATA2[t2 * n + t4];
      else 
        _P_DATA3[t2 * n + t4] = t4;
      _P_DATA4[t2 + _P_DATA3[t2 * n + t4] * n] = _P_DATA1[t2 * n + t4];
    }
  for (t2 = 0; t2 <= n - 1; t2 += 1) 
    for (t4 = 0; t4 <= n - 1; t4 += 1) 
      y[t2] += (_P_DATA4[t2 * n + t4] * x[t4]);
}

