#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA2_(_t27) _P_DATA2[_t27]
#define _P_DATA2__(_t27) _P_DATA2[_t27 + 1]
#define col_(_t31) col[1 * _t31]
#define col___(_t31) col[1 * _t31 + 1]
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
#include <stdio.h>
#include <stdlib.h>

struct a_list 
{
  unsigned short col_;
  float a[64];
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void spmv(int n,int *index,float *a,float *y,float *x,int *col)
{
  int t8;
  int t6;
  int t4;
  int t2;
  int In_3;
  int In_2;
  int In_1;
  int _t34;
  int _t33;
  int _t32;
  struct a_list *_P_DATA6;
  int newVariable2;
  int newVariable1;
  struct mk *_P_DATA5;
  float *a_prime;
  struct a_list *_P_DATA4;
  unsigned short *_P_DATA3;
  int chill_count_1;
  int *_P_DATA2;
  int chill_count_0;
  int newVariable0;
  float *_P_DATA1;
  int _t31;
  int _t30;
  int _t29;
  int _t28;
  int _t27;
  int _t26;
  int _t25;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  int _t18;
  int _t17;
  int _t16;
  int _t15;
  int _t14;
  int _t13;
  int _t11;
  int _t10;
  int _t9;
  int _t7;
  int _t6;
  int _t5;
  int k;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  _P_DATA1 = ((float *)(malloc(sizeof(float ) * 62456)));
  for (newVariable0 = 0; newVariable0 <= 62450; newVariable0 += 1) 
    _P_DATA1[newVariable0] = x[newVariable0];
  _P_DATA2 = ((int *)(malloc(sizeof(int ) * 7807)));
  _P_DATA2[0] = 0;
  _P_DATA4 = 0;
  chill_count_1 = 0;
  _P_DATA2[0] = 0;
  for (t2 = 0; t2 <= 7805; t2 += 1) {
    for (t4 = 0; t4 <= 7; t4 += 1) 
      for (t6 = index_(8 * t2 + t4); t6 <= index__(8 * t2 + t4) - 1; t6 += 1) {
        _t28 = (col_(t6) - 0) / 8;
        _P_DATA5[_t28].mk::ptr = 0;
      }
    for (t4 = 0; t4 <= 7; t4 += 1) 
      for (t6 = index_(8 * t2 + t4); t6 <= index__(8 * t2 + t4) - 1; t6 += 1) {
        _t28 = (col_(t6) - 0) / 8;
        _t30 = (col_(t6) - 0) % 8;
        if (_P_DATA5[_t28].mk::ptr == 0) {
          _P_DATA6 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
          _P_DATA6 -> a_list::next = _P_DATA4;
          _P_DATA4 = _P_DATA6;
          _P_DATA5[_t28].mk::ptr = _P_DATA4;
          for (newVariable1 = 0; newVariable1 <= 7; newVariable1 += 1) 
            for (newVariable2 = 0; newVariable2 <= 7; newVariable2 += 1) 
              _P_DATA5[_t28].mk::ptr -> a_list::a[8 * newVariable1 + 1 * newVariable2] = 0;
          _P_DATA5[_t28].mk::ptr -> a_list::col_ = _t28;
          chill_count_1 += 1;
        }
        _P_DATA5[_t28].mk::ptr -> a_list::a[8 * t4 + 1 * _t30] = a[t6];
      }
    _P_DATA2[t2 + 1] = chill_count_1;
  }
  for (t2 = -chill_count_1 + 1; t2 <= 0; t2 += 1) {
    if (chill_count_1 + t2 <= 1) {
      _P_DATA3 = ((unsigned short *)(malloc(sizeof(unsigned short ) * chill_count_1)));
      a_prime = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 64))));
    }
    for (t4 = 0; t4 <= 7; t4 += 1) 
      for (t6 = 0; t6 <= 7; t6 += 1) 
        a_prime[-t2 * 64 + 8 * t4 + 1 * t6] = _P_DATA4 -> a_list::a[8 * t4 + 1 * t6];
    _P_DATA3[-t2] = _P_DATA4 -> a_list::col_;
    _P_DATA6 = _P_DATA4 -> a_list::next;
    free(_P_DATA4);
    _P_DATA4 = _P_DATA6;
  }
  for (t2 = 62448; t2 <= 62450; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) 
      y[t2] += (a[t4] * x[col[t4]]);
  for (t2 = 0; t2 <= 7805; t2 += 1) 
    for (t4 = _P_DATA2_(t2); t4 <= _P_DATA2__(t2) - 1; t4 += 1) 
      for (t6 = 0; t6 <= 7; t6 += 1) 
        for (t8 = 0; t8 <= 7; t8 += 1) 
          y[8 * t2 + t6] += (a_prime[t2 * 64 + 8 * t6 + 1 * t8] * _P_DATA1[8 * _P_DATA3[t4] + t8]);
  free(_P_DATA1);
  free(_P_DATA2);
  free(_P_DATA3);
  free(a_prime);
  free(_P_DATA5);
}

int main()
{
  int n;
  float *a;
  float *y;
  float *x;
  int *index;
  int *col;
  spmv(n,index,a,y,x,col);
  return 0;
}
