#include "smv.h"
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include<cstring>
#include <sys/time.h>

void smv(struct sparse_matrix *A, REAL *x, REAL *y)
{
	int row;
	int col_index;
	REAL temp;
struct timeval t1,t_2;        
        int i;
	if (DEBUG) 
		fprintf(stderr, "smv::Inside smv method\n");

       gettimeofday(&t1, NULL);  

        // Manu
	int *cols = A->cols;
	REAL *vals = A->vals;
	int *rows = A->rows; 

      	for(i=0; i < 100; i++){
	for(row = 0; row < A->nrows; row++){
		temp = 0;
		for(col_index = rows[row]; col_index < rows[row+1]; col_index++){
			temp+=vals[col_index] * x[cols[col_index]];
		}
		y[row]=temp;
	}
      }
  gettimeofday(&t_2, NULL);  
  double elapsed =t_2.tv_sec-t1.tv_sec + (t_2.tv_usec -t1.tv_usec)/1000000.0;  
  printf("%.6lf seconds elapsed\n", elapsed);  

	if (DEBUG)
		fprintf(stderr, "smv::Done with smv\n");

}
