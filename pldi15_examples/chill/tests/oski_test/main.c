//#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "smv.h"
#include "matrix_io.h"
#include "sparse_matrix.h"
#include "oski_bcsr.h"
#include <sys/time.h>
#include <math.h>


int main(int argc, char **argv)
{

/////////////////////////////////////////////////////////////////////
   int num, retval;

        struct timeval tv1, tv2,tv3,tv4,tv5;
        //struct timezone tz;
	
	char filename[255];
	struct sparse_matrix A;
	struct sparse_matrix M;
	float *x, *b, *b2;
	int num_threads;
	//pthread_attr_t attr;
	
	strcpy(filename, argv[1]);


	if (DEBUG){
		fprintf(stderr, "main::Loading matrix %s\n", filename);
	  	//fprintf(stderr, "R: %d C: %d\n", R, C);	
        }

	load_sparse_matrix(filename, &A);

	
	//initialize b to 0 and x to 1
	int i,ii;
	x = (float *) malloc(sizeof(float) * A.ncols);
	b = (float *) malloc(sizeof(float) * A.nrows);
	b2 = (float *) malloc(sizeof(float) * A.nrows);

        FILE *fp = NULL, *fp1 = NULL, *fp2 = NULL, *fp3=NULL;

	for(i = 0; i < A.ncols; i++)
		x[i] = 1.0;

	
	for(i = 0; i < A.nrows; i++){
		b[i] = 0;
 		b2[i]= 0;	
	}

        int nrows;
        int nnz;
        int *rows;
        int *cols;
        REAL *vals;
	smv(&A,x,b);
        //spmv(A.nrows,A.rows,A.vals,b2,x,A.cols);
        oski_bcsr(A.nrows,A.ncols, A.rows,A.vals,b2,x,A.cols,A.nnz);
        //oski_bcsr(int dim0, int dim,int *Aptr, float *Aval, float *y, float *x, int *Aind, int nnz)
	  	for(i = 0; i < A.nrows; i++){
 		     if(fabs(b2[i] - b[i]) >= 0.1){	
  			printf("Values don't match at %d, expected %f obtained %f\n", i, b[i], b2[i]);
	  	        break;
		     }
		    //else	
  		//	printf("Values match at %d, expected %f obtained %f\n", i, b[i], b2[i]);

 		}

        free(x);
        free(b);
        free(b2);
        free(A.rows);
        free(A.cols);        
        free(A.vals);
	if( i != A.nrows)	
        //printf("i  is %d\n", i) ;
		//   exit(EXIT_FAILURE);
  	return 0;
}

