#include <stdio.h>
#include <oski/oski_Tis.h> /*Get OSKI bindings */
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
//#define MAX_BUFFER 256
//#define  DIM 3 /* matrix dimension */
//#define  NUM_STORED_NZ 2 /* number of stored non-zero values */
//spmv(A.nrows,A.rows,A.vals,b2,x,A.cols)
void oski_bcsr(int dim0, int dim,int *Aptr, float *Aval, float *y, float *x, int *Aind, int nnz)
{
	
	/* Sparse matrix A, in compressed sparse row (CSR) format */
	//int Aptr[DIM+1] = { 0, 0, 1, 2 };
	//int Aind[NUM_STORED_NZ] = { 0, 0 };
        //float Aval[NUM_STORED_NZ] = {-2, 0.5};	
	/* Dense vectors, x, y, and scalar multipliers, α, β */
        //float x[DIM] = { .25, .45, .65 };
	//float y[DIM] = { 1, 1, 1 };
	float alpha = 1, beta = 0; 
        char *xforms;



        FILE* fp_saved_xforms = fopen( "./my_xform.txt", "rt" ); 

        // obtain file size:
        fseek (fp_saved_xforms, 0 , SEEK_END);
        int lSize = ftell (fp_saved_xforms);
        rewind (fp_saved_xforms);




        int num_chars_read;

        xforms = (char *)malloc(lSize+1);

       
        num_chars_read = fread(xforms, sizeof(char),lSize, fp_saved_xforms);
       printf("num chars is %d\n", lSize);
        xforms[num_chars_read] = NULL;

    


	struct timeval t0,t1,t2;        

	/* OSKI matrix/vector objects */
	oski_matrix_t A_tunable;
	oski_vecview_t x_view, y_view;
	
	/* Initialize OSKI and create matrix */
	oski_Init();
	
	A_tunable = oski_CreateMatCSR( Aptr, Aind, Aval, dim0, dim, /* CSR arrays */
	COPY_INPUTMAT, /* ”copy mode” */
	/* remaining args specify how to interpret non−zero pattern */
 	1, INDEX_ZERO_BASED);

	/* Create wrappers around the dense vectors. */
	x_view = oski_CreateVecView( x, dim, STRIDE_UNIT );
	y_view = oski_CreateVecView( y, dim0, STRIDE_UNIT );

	oski_MatMult( A_tunable, OP_NORMAL, alpha, x_view, beta, y_view);

        /* Tuning */
       // oski_SetHintMatMult( A_tunable, OP_NORMAL, 1.0, SYMBOLIC_VEC, 0.0, SYMBOLIC_VEC,ALWAYS_TUNE_AGGRESSIVELY);
       // oski_SetHint( A_tunable, HINT_SINGLE_BLOCKSIZE, ARGS_NONE ); // structural hint
       //  oski_TuneMat( A_tunable );

       /* Extract transformations */	
/*	xforms = oski_GetMatTransforms(A_tunable);
        printf ( "--- Matrix transformations ---\n%s\n--- end ---\n", xforms );
	fprintf ( fp_saved_xforms, "%s\n", xforms );
	fclose ( fp_saved_xforms );
*/

       /* Change matrix data structure. */
//       	gettimeofday(&t0, NULL);  

        oski_ApplyMatTransforms(A_tunable, xforms);



       	gettimeofday(&t1, NULL);  

	/* Perform matrix vector multiply, y ← α · A· x + β · y. */
	oski_MatMult( A_tunable, OP_NORMAL, alpha, x_view, beta, y_view);

	gettimeofday(&t2, NULL);  
  //	double elapsed_0 =t1.tv_sec-t0.tv_sec + (t1.tv_usec -t0.tv_usec)/1000000.0;  
  //	printf("OSKI BCSR Inspector:  %.6lf seconds elapsed\n", elapsed_0);  

  	double elapsed =t2.tv_sec-t1.tv_sec + (t2.tv_usec -t1.tv_usec)/1000000.0;  
  	printf("OSKI BCSR:  %.6lf seconds elapsed\n", elapsed);  
        double mflop_count = 1e-6 *(double)nnz * (double)2.0;
        printf("Performance :%.6lf MFLOPS\n", mflop_count/elapsed);
	/* Clean−up interface objects and shut down OSKI */
	oski_DestroyMat( A_tunable );
	oski_DestroyVecView( x_view );
	oski_DestroyVecView( y_view );
	oski_Close();

  	free(xforms);
	/* Print result , y. Should be ”[ .75 ; 1.05 ; .225 ]” */
	//printf ( "Answer: y = [ %f ; %f ; %f ]\n", y[0], y[1], y[2] );
	//return 0;
}
