#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define index_(i) index[i]
#define index__(i) index[i + 1]
#include <stdlib.h>
void spmv(int n,int *index,float *a,float *y,float *x,int *col)
{
  int t6;
  int t4;
  int t2;
  float *_P_DATA4;
  int *_P_DATA3;
  int _t18;
  int _t17;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int *_P_DATA2;
  int *_P_DATA5;
  int *_P_DATA7;
  int _t8;
  int _t7;
  float *_P_DATA1;
  float *_P_DATA6;
  int _t6;
  int _t5;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int t8;
  int i;
  int j;
  int notAllZero;
   _P_DATA1= malloc((unsigned long)n*(unsigned long)n*(unsigned long)sizeof(float));
   _P_DATA2=  malloc((unsigned long)n*(unsigned long)n*(unsigned long)sizeof(int));
   _P_DATA3= malloc((unsigned long)n*(unsigned long)n*(unsigned long)sizeof(int));
   _P_DATA4= malloc((unsigned long)n*(unsigned long)n*(unsigned long)sizeof(float));
   _P_DATA5= malloc(4*(unsigned long)(n/2)*(unsigned long)(n/2)*(unsigned long)sizeof(int));
   _P_DATA6= malloc(4*(unsigned long)(n/2)*(unsigned long)(n/2)*(unsigned long)sizeof(float));
   _P_DATA7= malloc(((unsigned long)(n/2)*(unsigned long)(n/2) +1)*(unsigned long)sizeof(int));

  for (t2 = 0; t2 <= n - 1; t2 += 1) {
    for (t4 = 0; t4 <= index__(t2) - index_(t2) - 1; t4 += 1) {
      _P_DATA1[t4 + t2 * n] = a[t4 + index_(t2)];
      _P_DATA2[t4 + t2 * n] = col[t4 + index_(t2)];
    }
    for (t4 = index__(t2) - index_(t2); t4 <= n-1; t4 += 1) {
      _P_DATA1[t4 + t2 * n] = 0;
      _P_DATA2[t4 + t2 * n] = 0;
    }
  }
  for (t2 = 0; t2 <= n - 1; t2 += 1) {
             
    for (t4 = 0; t4 <= n-1; t4 += 1) 
      _P_DATA3[t2 * n + t4] = 0;
     for (t4 = 0; t4 <= n-1; t4 += 1) 
      if (_P_DATA2[t2 * n + t4] !=0) {
        _P_DATA3[t2*n + _P_DATA2[t2*n + t4]] = 1;
        _P_DATA4[t2*n + _P_DATA2[t2 * n + t4]] = _P_DATA1[t2 * n + t4];
              
     } else 
        for (t6 = 0; t6 <= n-1; t6 += 1)
          if(_P_DATA3[t2 * n + t6] == 0){  
            _P_DATA3[t2 * n + t6] = 1;
            _P_DATA4[t2*n + t6] = _P_DATA1[t2 * n + t4];
            break;
         } 
  }

  _P_DATA7[0] = 0;
  for (t2 = 0; t2 <= n/2 - 1; t2 += 1) {
    _P_DATA7[0 + t2 + 1] = _P_DATA7[0 + t2];
    for (t4 = 0; t4 <= n/2 -1; t4 += 1) {
      notAllZero = 0;
      for (t6 = 0; t6 <= 1; t6 += 1) {
        for (t8 = 0; t8 <= 1; t8 += 1) 
          if (_P_DATA4[(2 * t2 + t6) * n + (2 * t4 + t8)] != 0) {
            notAllZero = 1;
            break; 
          }
        if (notAllZero == 1) 
          break; 
      }
      if (notAllZero == 1) {
        for (_t17 = 0; _t17 <= 1; _t17 += 1) 
          for (_t18 = 0; _t18 <= 1; _t18 += 1) {
            _P_DATA6[4 * _P_DATA7[0 + t2 + 1] + 2 * _t17 + _t18] = _P_DATA4[(2 * t2 + _t17) * n + (2 * t4 + _t18)];
            _P_DATA5[4 * _P_DATA7[0 + t2 + 1] + 2 * _t17 + _t18] = t4;
          }
        _P_DATA7[0 + t2 + 1] = _P_DATA7[0 + t2 + 1] + 1;
      }
    }
  }
  for (t2 = 0; t2 <= n/2 - 1; t2 += 1) 
    for (t4 = _P_DATA7[t2]; t4 <= _P_DATA7[t2+1]-1; t4 += 1) 
      for (t6 = 0; t6 <= 1; t6 += 1) 
        for (t8 = 0; t8 <= 1; t8 += 1) 
          y[2 * t2 + t6] += (_P_DATA6[4*t4 + 2* t6 + t8] * x[2* _P_DATA5[4*t4 + 2* t6 + t8] + t8]);


 
 free(_P_DATA1);   
 free(_P_DATA2);   
 free(_P_DATA3);   
 free(_P_DATA4);   
 free(_P_DATA5);   
 free(_P_DATA6);   
 free(_P_DATA7);   

}

