#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define index_(i) index[i]
#define index__(i) index[i + 1]
#include <cstdlib>
#include <cstdio>
#include <sys/time.h>  
#include <bitset> 
#include <map>
#include <vector>
#define R 2
#define C 2

#include "bcsr.h"

using namespace std;

/*
 typedef struct mirror{
 int j_start;
 int j_end;
 float val;
 //int count;
 }mirror;
 */



void spmv_bcsr(int m, int n, int *index, float *a, float *y, float *x, int *col, int nnz) {
	int t6;
	int t4;
	int t2;
	float *_P_DATA4;
	int *_P_DATA3;
	int _t18;
	int _t17;
	int _t14;
	int _t13;
	int _t12;
	int _t11;
	int _t10;
	int _t9;
	int *_P_DATA2;
	int *_P_DATA5;
	int *_P_DATA7;
	int _t8;
	int _t7;
	float *_P_DATA1;
	float *_P_DATA6;
	int _t6;
	int _t5;
	int _t4;
	int _t3;
	int _t2;
	int _t1;
	int t10[R];
	int t8;
	int i;
	int j;
	int notAllZero;
        float reg[R*C];

//   _P_DATA1= malloc((unsigned long)n*(unsigned long)n*(unsigned long)sizeof(float));
//   _P_DATA2=  malloc((unsigned long)n*(unsigned long)n*(unsigned long)sizeof(int));
//   _P_DATA3= malloc((unsigned long)n*(unsigned long)n*(unsigned long)sizeof(int));
//   _P_DATA4= malloc((unsigned long)n*(unsigned long)n*(unsigned long)sizeof(float));
//   _P_DATA5= malloc(4*(unsigned long)(n/2)*(unsigned long)(n/2)*(unsigned long)sizeof(int));
//   _P_DATA6= malloc(4*(unsigned long)(n/2)*(unsigned long)(n/2)*(unsigned long)sizeof(float));
//   _P_DATA7= malloc(((unsigned long)(n/2)*(unsigned long)(n/2) +1)*(unsigned long)sizeof(int));
//      bitset<(unsigned int) 4000000000> *_P_DATA4_MIRROR = new bitset<(unsigned int) 4000000000>;

	_P_DATA5 = (int *)malloc((2 * m + 1) * n * sizeof(int));
	_P_DATA6 = (float *)malloc((2 * m + 1) * n * sizeof(float));
	_P_DATA7 = (int *)malloc((2 * m + 1) * n * sizeof(int));
//	map<int, int> sigma_inv[n];
//	for (t2 = 0; t2 <= n - 1; t2 += 1) {
		//int start = 0;
//		int count = 0;
//		for (t4 = index_(t2); t4 <= index__(t2)- 1; t4 += 1) {

//			_P_DATA4_MIRROR->set(t2*n + col[t4]);
//			sigma_inv[t2].insert( pair<int,int>(col[t4], t4) );

			// sigma_inv[t2][col[t4]] = t4;

			/*    if(col[t4] > start)
			 {
			 _P_DATA4_MIRROR[t2*(2*m+1) +count].j_start = start;
			 _P_DATA4_MIRROR[t2*(2*m+1) +count].j_end = col[t4] - 1;
			 _P_DATA4_MIRROR[t2*(2*m+1) +count].val = 0;

			 _P_DATA4_MIRROR[t2*(2*m+1) +count+1].j_start = col[t4];
			 _P_DATA4_MIRROR[t2*(2*m+1) +count+1].j_end = col[t4];
			 _P_DATA4_MIRROR[t2*(2*m+1) +count+1].val = a[t4];
			 count+=2;
			 }
			 else if(col[t4] == start){
			 _P_DATA4_MIRROR[t2*(2*m+1) +count].j_start = col[t4];
			 _P_DATA4_MIRROR[t2*(2*m+1) +count].j_end = col[t4];
			 _P_DATA4_MIRROR[t2*(2*m+1) +count].val = a[t4];
			 count++;

			 }
			 else
			 printf("wrong!\n");


			 start = col[t4]+1;
			 _P_DATA4_MIRROR[t2*(2*m+1) +count].j_start = start;
			 _P_DATA4_MIRROR[t2*(2*m+1) +count].j_end = n-1;
			 */

//		}
//	}

	_P_DATA7[0] = 0;
	for (t2 = 0; t2 <= n / R - 1; t2 += 1) {
//    printf("t2 is %d\n", t2);
		_P_DATA7[0 + t2 + 1] = _P_DATA7[0 + t2];
                 //   int count[2] = {0,0};
                 

		for (t4 = 0; t4 <= n / C - 1; t4 += 1) {
			notAllZero = 0;

			for (t6 = 0; t6 <= R-1; t6 += 1) {
		                t10[t6] = index[t2*R + t6];
   				for (t8 = 0; t8 <= C-1; t8 += 1) {
                                   for( ; t10[t6] < index[t2*R + t6 +1];t10[t6]++){
                                          if(t4*C + t8 == col[t10[t6]]){
 						notAllZero = 1;  	
						reg[t6*C +t8]= a[t10[t6]];
				                break;
                                          }
                                          else if(col[t10[t6]] > t4*C  + t8){
   					        reg[t6*C + t8] = 0;
  				         	break;	
 					  }	
				   }	
			           if(t10[t6] >= index[t2*R + t6 +1])	    
                                       reg[t6*C + t8] = 0;

 					//    for(t10=0; t10 < 2*m+1; t10++)
					//      while(_P_DATA4_MIRROR[(2*t2 + t6)*(2*m + 1) + count[t6]].j_start >  2 * t4 + t8 ||  2*t4 +t8 > _P_DATA4_MIRROR[(2*t2 + t6)*(2*m + 1) + count[t6]].j_end)
					//       count[t6]++;

					//if (_P_DATA4_MIRROR[(2*t2 + t6)*(2*m + 1) + count[t6]].val != 0) {

					//if (_P_DATA4[(2 * t2 + t6) * n + (2 * t4 + t8)] != 0) {
					/*if (_P_DATA4_MIRROR->test(
							(R * t2 + t6) * n + (C * t4 + t8))) {
						notAllZero = 1;
						break;
					}
  					*/
				//if (notAllZero == 1)
				//	break;

				}
				//if (notAllZero == 1)
				//	break;
			}
			if (notAllZero == 1) {
                                  
                                 
				for (_t17 = 0; _t17 <= R-1; _t17 += 1) 
					//    while(_P_DATA4_MIRROR[(2*t2 + _t17)*(2*m + 1) + count[_t17]].j_start >  2 * t4  ||  2*t4 > _P_DATA4_MIRROR[(2*t2 + _t17)*(2*m + 1) + count[_t17]].j_end)
					//        count[_t17]--;
					//t10[_t17] = index[t2*R + _t17];
					for (_t18 = 0; _t18 <= C-1; _t18 += 1)
						_P_DATA6[R*C* _P_DATA7[0 + t2 + 1] + C * _t17 + _t18] = reg[_t17*C + _t18];								 
						/*for(t10=0; t10 < 2*m+1; t10++)
						 if(_P_DATA4_MIRROR[(2*t2 + _t17)*(2*m + 1) + t10].j_start  <= 2 * t4 + _t18 &&  2*t4 +_t18 <= _P_DATA4_MIRROR[(2*t2 + _t17)*(2*m + 1) + t10].j_end)
						 break;
						 */
						//while(_P_DATA4_MIRROR[(2*t2 + _t17)*(2*m + 1) + count[_t17]].j_start >  2 * t4 + _t18  ||  2*t4 + _t18 > _P_DATA4_MIRROR[(2*t2 + _t17)*(2*m + 1) + count[_t17]].j_end)
						//       count[_t17]++;
 				/*	bool found = false;
	
                                       for(; t10[_t17] < index[t2*R + _t17 +1];t10[_t17]++){
                                          if(t4*C + _t18 == col[t10[_t17]]){
 						found = true;
 						//notAllZero = 1;  	
						_P_DATA6[R*C* _P_DATA7[0 + t2 + 1] + C * _t17 + _t18] = a[t10[_t17]];
                                                break;
                                          }
			                  else if(col[t10[_t17]] > t4*C  + _t18)
   						break;	
				          }
				*/		
				//		if (!found)
				//			_P_DATA6[R*C* _P_DATA7[0 + t2 + 1] + C * _t17 + _t18] =	0; //_P_DATA4_MIRROR[(2*t2 + _t17)*(2*m + 1) + count[_t17]].val;
						/*else {
							map<int, int>::iterator entry;
							entry = sigma_inv[(R* t2 + _t17)].find(
									C* t4 + _t18);
							if (entry != sigma_inv[(R* t2 + _t17)].end())
								_P_DATA6[R*C* _P_DATA7[0 + t2 + 1] + C * _t17
										+ _t18] = a[entry->second]; //0;       //_P_DATA4_MIRROR[(2*t2 + _t17)*(2*m + 1) + count[_t17]].val;
							else {
								printf("error in sigma_inv\n");
								free(_P_DATA5);
								free(_P_DATA6);
								free(_P_DATA7);
								free(_P_DATA4_MIRROR);
								exit(0);

							}
						}
					       */	
						_P_DATA5[_P_DATA7[0 + t2 + 1] ] = t4;
				//	}
				//}
				_P_DATA7[0 + t2 + 1] = _P_DATA7[0 + t2 + 1] + 1;
				if (_P_DATA7[t2 + 1] >= R*C * (2 * m + 1) * n - 1) {
					printf("error overflow\n");
					/* free(_P_DATA1);
					 free(_P_DATA2);
					 free(_P_DATA3);
					 free(_P_DATA4);
					 */
					free(_P_DATA5);
					free(_P_DATA6);
					free(_P_DATA7);
					//free(_P_DATA4_MIRROR);					
					exit(0);
				}
                            
			}
		}
	}

	struct timeval t1, t_2;

	printf("Starting actual computation\n");
	gettimeofday(&t1, NULL);

	for (t2 = 0; t2 <= n / R - 1; t2 += 1)
		for (t4 = _P_DATA7[t2]; t4 <= _P_DATA7[t2 + 1] - 1; t4 += 1)
			for (t6 = 0; t6 <= R-1; t6 += 1)
				for (t8 = 0; t8 <= C-1; t8 += 1)
					y[R* t2 + t6] += (_P_DATA6[R*C* t4 + C* t6 + t8]
							* x[C* _P_DATA5[t4] + t8]);

	/*  for (t2 = 0; t2 <= n/2 - 1; t2 += 1)
	 for (t4 = _P_DATA7[t2]; t4 <= _P_DATA7[t2+1]-1; t4 += 1) {
	 int col = 2*_P_DATA5[t4];
	 float val_0 = x[col];
	 float val_1 = x[col+1];
	 float reg_0 = y[2*t2];
	 float reg_1 = y[2*t2+1];

	 reg_0 =  _P_DATA6[4 *t4]*val_0 + _P_DATA6[4 *t4+ 1]*val_1;
	 reg_1 =  _P_DATA6[4 *t4+ 2*t2]*val_0 +  _P_DATA6[4 *t4+2*t2+1]*val_1;
	 y[2*t2] += reg_0;
	 y[2*t2+1] += reg_1;
	 }

	 */
	gettimeofday(&t_2, NULL);
	double elapsed = t_2.tv_sec - t1.tv_sec
			+ (t_2.tv_usec - t1.tv_usec) / 1000000.0;
	printf("%.6lf seconds elapsed\n", elapsed);
        double mflop_count = 1e-6 *(double)nnz * (double)2.0;
	printf("Performance :%.6lf MFLOPS\n", mflop_count/elapsed);

	/* free(_P_DATA1);
	 free(_P_DATA2);
	 free(_P_DATA3);
	 free(_P_DATA4);
	 */

	free(_P_DATA5);
	free(_P_DATA6);
	free(_P_DATA7);
	//free(_P_DATA4_MIRROR);

}

