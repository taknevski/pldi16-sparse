#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define index_(i) index[i]
#define index__(i) index[i + 1]
__global__ void spmv_ell_GPU(int m, int n, float *y,float *_P_DATA3,float *x,int *_P_DATA4);
extern "C" void spmv_ell_gpu_host(int m, int n,int *index,float *a,float *y,float *x,int *col);
#include <stdio.h>
void spmv_ell_gpu_host(int m, int n,int *index,float *a,float *y,float *x,int *col)
{
  int *devI3Ptr;
  float *devI2Ptr;
  float *devI1Ptr;
  float *devO1Ptr;
  int t6;
  int t2;
  int t4;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  int *_P_DATA4;
  int _t18;
  int _t17;
  float *_P_DATA3;
  int _t15;
  int _t16;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int *_P_DATA2;
  int _t8;
  int _t7;
  float *_P_DATA1;
  int _t6;
  int _t5;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  _P_DATA1 = (float*)malloc(sizeof(float ) * ((n - 1 + 1) * m));
  _P_DATA2 = (int*)malloc(sizeof(int ) * ((n - 1 + 1) * m));
  _P_DATA3 = (float*)malloc(sizeof(float ) * (m* (n - 1 + 1)));
  _P_DATA4 = (int *)malloc(sizeof(int ) * (m* (n - 1 + 1)));
  printf("m is %d\n", m);
  for (t2 = 0; t2 <= n - 1; t2 += 1) {
    for (t4 = 0; t4 <= index__(t2) - index_(t2) - 1; t4 += 1) {
      _P_DATA1[t4 + t2 * m] = a[t4 + index_(t2)];
      _P_DATA2[t4 + t2 * m] = col[t4 + index_(t2)];
    }
    for (t4 = index__(t2) - index_(t2); t4 <= m - 1; t4 += 1) {
      _P_DATA1[t4 + t2 * m] = 0;
      _P_DATA2[t4 + t2 * m] = 0;
    }
  }
  if (1 <= n) 
    for (t2 = 0; t2 <= m-1; t2 += 1) 
      for (t4 = 0; t4 <= n - 1; t4 += 1) {
        _P_DATA3[t4 + t2 * (n - 1 + 1)] = _P_DATA1[t2 + t4 * m];
        _P_DATA4[t4 + t2 * (n - 1 + 1)] = _P_DATA2[t2 + t4 * m];
      }
  cudaMalloc(((void **)(&devO1Ptr)),n * sizeof(float ));
  cudaMemcpy(devO1Ptr,y, n * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),n * m * sizeof(float ));
  cudaMemcpy(devI1Ptr,_P_DATA3,n * m * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),n * sizeof(float ));
  cudaMemcpy(devI2Ptr,x,n * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),n * m * sizeof(int ));
  cudaMemcpy(devI3Ptr,_P_DATA4,n * m * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid2 = dim3((n - 1) / 512 + 1,1);
  dim3 dimBlock2 = dim3(512,1);
  spmv_ell_GPU<<<dimGrid2,dimBlock2>>>(m,n, devO1Ptr,devI1Ptr,devI2Ptr,devI3Ptr);
  cudaMemcpy(y,devO1Ptr,n * sizeof(float ),cudaMemcpyDeviceToHost);
  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
}

/*int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[494 + 1];
  int col[1666UL];
  spmv(n,index,a,y,x,col);
  return 0;
}
*/
__global__ void spmv_ell_GPU(int m, int n,float *y,float *_P_DATA3,float *x,int *_P_DATA4)
{
  int j;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  int t4;
  int t2;
  int t6;
  if (1 <= n) 
    for (j = 0; j <= m - 1; j += 1) 
      if (tx <= n - 512* bx - 1) 
        y[512* bx + tx] += (_P_DATA3[512* bx + tx + j * (n - 1 + 1)] * x[_P_DATA4[512* bx + tx + j * (n - 1 + 1)]]);
}
