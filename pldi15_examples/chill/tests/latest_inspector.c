/*
 * latest_inspector.c
 *
 *  Created on: Sep 4, 2014
 *      Author: anand
 */

#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define index_(i) index[i]
#define index__(i) index[i + 1]
#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>

void spmv_bcsr(int m, int n, int *index, float *a, float *y, float *x, int *col,
		int nnz) {

	struct timeval t0, t1, t2;
	int *offset_index;
	int *offset_kk;
	int *marked;
	float *a_prime;
	int ii, i, i_, kk, j, k, k_,count;

	/* Inspector */



	gettimeofday(&t0, NULL);
	count =0;
	for (ii = 0; ii < n / R; ii++)
		for (i = 0; i < R; i++)
			count += index[ii * R + i + 1] - index[ii * R + i];

	offset_index = (int *) malloc((count + 1) * sizeof(int));
	offset_kk = (int *) malloc(count * sizeof(int));
	a_prime = (float *) malloc(count * R * C * sizeof(float));
	marked = (int *) malloc(n / C * sizeof(int));


	offset_index[0] = 0;
	count = 0;
	for (ii = 0; ii < n / R; ii++) {

		//for (kk = 0; kk < n / C; kk++)
		//	marked[kk] = 0;

		//	memset (marked,0,n/C*sizeof(int));

		for (i = 0; i < R; i++)    // somehow show this is outdented
			for (j = index[ii * R + i]; j < index[ii * R + i + 1]; j++) {
				int kk = col[j] / C;

				marked[kk] = 0;

			}

		for (i = 0; i < R; i++)    // somehow show this is outdented
			for (j = index[ii * R + i]; j < index[ii * R + i + 1]; j++) {
				int kk = col[j] / C;  // using condition and bounds on k
				int k = col[j] - kk * C;

				//if (kk * C + offset == col[j]) {
				if (marked[kk] == 0) {
					marked[kk] = count;	// FIXME!
					for (i_ = 0; i_ < R; i_++)
						for (k_ = 0; k_ < C; k_++)
							a_prime[marked[kk] * R * C + i_ * C + k_] = 0;
					count++;

				}
				offset_kk[marked[kk]] = kk;
				a_prime[marked[kk] * R * C + i * C + k] = a[j];

				//}

			}
		offset_index[ii + 1] = count;
	}
	gettimeofday(&t1, NULL);
	double elapsed = t1.tv_sec - t0.tv_sec
			+ (t1.tv_usec - t0.tv_usec) / 1000000.0;
	printf("Inspector Time: %.6lf seconds\n", elapsed);

	printf("Starting actual computation\n");
	gettimeofday(&t1, NULL);
	/* Executor */

	for (ii = 0; ii < n / R; ii++)
		for (kk = offset_index[ii]; kk < offset_index[ii + 1]; kk++)
			for (i = 0; i < R; i++)
				for (k = 0; k < C; k++)
					y[ii * R + i] += a_prime[kk * R * C + i * C + k]
							* x[C * offset_kk[kk] + k];

	gettimeofday(&t2, NULL);
	elapsed = t2.tv_sec - t1.tv_sec + (t2.tv_usec - t1.tv_usec) / 1000000.0;
	printf("%.6lf seconds elapsed\n", elapsed);
	double mflop_count = 1e-6 * (double) nnz * (double) 2.0;
	printf("Performance :%.6lf MFLOPS\n", mflop_count / elapsed);

	free(offset_index);
	free(offset_kk);
	free(marked);
	free(a_prime);
}
