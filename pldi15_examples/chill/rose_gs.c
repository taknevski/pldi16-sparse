#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define col_(t) col[t]
#define idx_(i) idx[i]
#define idx__(i) idx[i + 1]
#define N 512

int main()
{
  int t4;
  int t2;
  int i;
  int t;
  float a[512UL];
  int col[512UL];
  int idx[513UL];
  for (t2 = 0; t2 <= 511; t2 += 1) 
    for (t4 = idx_(t2); t4 <= idx__(t2) - 1; t4 += 1) 
      a[t2] = a[col[t4]];
  return 0;
}
