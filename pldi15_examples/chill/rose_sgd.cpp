#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define index_(i) index[i]
#define index__(i) index[i + 1]
#define NZ 1666
#define NUMROWS 494
#define N 1000
#define SGD_LAMBDA 0.05f
#define SGD_FEATURE_SIZE 16

void sgd_kernel_global_simd(int n,int *index,float *ratings,float *y,float *x,int *col,float **fv,float step_size,float *err)
{
  int t8;
  int t6;
  int t4;
  int t2;
  int _t7;
  int _t6;
  int _t4;
  int _t5;
  int l;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  int k;
  for (t2 = 0; t2 <= 1998; t2 += 1) 
    for (t4 = __rose_gt(0,-t2 + 999); t4 <= __rose_lt(999,-t2 + 1998); t4 += 1) 
      for (t6 = index_(t4); t6 <= index__(t4) - 1; t6 += 1) {
        if (t4 + (t2 - 999) == col[t6]) 
          err[t6] = -ratings[t6];
        if (t4 + t2 - 999 == col[t6]) 
          err[t6] += (fv[t4][0] * fv[t4 + t2 - 999][0]);
        if (t4 + t2 - 999 == col[t6]) {
          fv[t4][0] -= (step_size * ((err[t6] * fv[t4 + t2 - 999][0]) + (0.05f * fv[t4][0])));
          fv[t4 + t2 - 999][0] -= (step_size * ((err[t6] * fv[t4][0]) + (0.05f * fv[t4 + t2 - 999][0])));
        }
        for (t8 = 1; t8 <= 15; t8 += 1) {
          if (t4 + t2 - 999 == col[t6]) 
            err[t6] += (fv[t4][t8] * fv[t4 + t2 - 999][t8]);
          if (t4 + t2 - 999 == col[t6]) {
            fv[t4][t8] -= (step_size * ((err[t6] * fv[t4 + t2 - 999][t8]) + (0.05f * fv[t4][t8])));
            fv[t4 + t2 - 999][t8] -= (step_size * ((err[t6] * fv[t4][t8]) + (0.05f * fv[t4 + t2 - 999][t8])));
          }
        }
      }
}

int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[494 + 1];
  int col[1666UL];
  float **fv;
  float step_size = 0.09f;
  float err[1666UL];;
  sgd_kernel_global_simd(n,index,a,y,x,col,fv,step_size,err);
  return 0;
}
