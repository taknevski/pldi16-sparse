#define N 5

int main() {
    // c[1] could be optimized away as a scalar, which isn't supported yet
    typedef float array[N];
    array a,b,c,d,e,f,g,h,l;
    int i, j;
    int temp1,temp2;

    // The outer-loop is needed, otherwise only the first loop's dependency vector prints out
    for (j=0; j<1; ++j) {
        // Non-reduction 1:
        //      They reduce the elements of some vector or array dimension
        //      down to one element. [Allen & Kennedy]
	
	//int temp;
	temp1 = 10;
	temp2 = temp1*10;
        for (i = 0; i < N; i++) {
            a[i] = a[0] + b[i];
        }

        // Non-reduction 2:
        //      Only the final result of the reduction is used later; use of
        //      an intermediate result voids the reduction. [Allen & Kennedy]
        for (i = 0; i < N; i++) {
            c[0] = c[0] + d[i];
            e[i] = c[0];
        }

        // Non-reduction 3:
        //      There is no variation inside the intermediate accumulation;
        //      that is, the reduction operates on the vector and nothing
        //      else. [Allen & Kennedy]
        for (i = 0; i < N; i++) {
            //f[0] = f[0] + i*g[i];
	    f[0] = f[0] + i*a[i];
        }

        // If I uncomment the assignment below,
        //h[0] = 0;
        // I get the following strange dependence debug message:
        //      4->5: h:_quasioutput(0, -5~-1) h:_quasiflow(0, -5~-1)

        // True reduction:
        //      DEP_R2W: Read  before Write / anti-dependence / WAR
        //      DEP_W2R: Write before Read  / true dependence / RAW
        //      DEP_W2W: Write before Write / output dependence (flow) / WAW
        //      All symbols are the same
        //      All dependences to and from same statement
        //      No dependences to or from other statements (for statements in loop)
        for (i = 0; i < N; i++)
            h[0] = h[0] + l[i];
    }

    //return h[0];
    return 0;
}
