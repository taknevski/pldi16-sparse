
#ifndef __SMV__
#define __SMV__
#include "sparse_matrix.h"
#define NumVc 2
double smv(struct sparse_matrix *A, REAL (*x)[NumVc], REAL (*y)[NumVc]);

#endif

