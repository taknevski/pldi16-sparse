#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA1_(_t58,_t59) _P_DATA1[590 * _t58 + _t59]
#define _P_DATA1__(_t58,_t59) _P_DATA1[590 * _t58 + _t59 + 1]
#define col_(ip,jp) col[1 * jp]
#define col__(i,j) col[1 * j]
#define col___(ip,jp) col[1 * jp + 1]
#define col____(i,j) col[1 * j + 1]
#define col_____(_t62) col[1 * _t62]
#define col_______(_t62) col[1 * _t62 + 1]
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
#define N 1000
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#define NumVc 2

struct a_list 
{
  unsigned short col_[2];
  float A;
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void csb(int m,int n,int *index,int *col,float (*x)[4UL],float (*y)[4UL],float *A)
{
  int t8;
  int t6;
  int t4;
  int t2;
  int newVariable0;
  int In_1;
  int _t66;
  int _t65;
  int _t64;
  struct a_list *_P_DATA5;
  struct mk *_P_DATA4;
  float *A_prime;
  struct a_list *_P1[346920];
  unsigned short *_P_DATA3;
  unsigned short *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t63;
  int _t62;
  int _t61;
  int _t60;
  int _t59;
  int _t58;
  int _t57;
  int _t56;
  int _t55;
  int _t54;
  int _t53;
  int _t52;
  int _t51;
  int _t50;
  int _t49;
  int _t48;
  int _t47;
  int _t46;
  int _t45;
  int _t44;
  int _t43;
  int _t42;
  int _t41;
  int _t40;
  int _t39;
  int _t38;
  int _t37;
  int _t36;
  int _t35;
  int _t34;
  int _t33;
  int _t32;
  int _t31;
  int _t30;
  int _t29;
  int _t28;
  int _t27;
  int _t26;
  int _t25;
  int _t24;
  int _t23;
  int _t21;
  int _t20;
  int _t19;
  int _t18;
  int _t16;
  int _t15;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t6;
  int _t5;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int jp;
  int i;
  int j;
  int k;
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * 346921)));
  _P_DATA1[0] = 0;
  chill_count_1 = 0;
  for (_t58 = 0; _t58 <= 587; _t58 += 1) 
    for (_t59 = 0; _t59 <= 589; _t59 += 1) {
      _P1[590 * _t58 + 1 * _t59] = 0;
      _P_DATA1[590 * _t58 + 1 * _t59 + 1] = 0;
    }
  for (t2 = 0; t2 <= 587; t2 += 1) 
    for (t4 = 0; t4 <= 4095; t4 += 1) 
      for (t6 = index_(4096 * t2 + t4); t6 <= index__(4096 * t2 + t4) - 1; t6 += 1) {
        _t59 = (col_____(t6) - 0) / 4096;
        _t61 = (col_____(t6) - 0) % 4096;
        _P_DATA5 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
        _P_DATA5 -> next = _P1[590 * t2 + 1 * _t59];
        _P1[590 * t2 + 1 * _t59] = _P_DATA5;
        _P1[590 * t2 + 1 * _t59] -> A = 0;
        _P1[590 * t2 + 1 * _t59] -> col_[0] = t4;
        _P1[590 * t2 + 1 * _t59] -> col_[1] = _t61;
        chill_count_1 += 1;
        _P_DATA1[590 * t2 + 1 * _t59 + 1] += 1;
        _P1[590 * t2 + 1 * _t59] -> A = A[t6];
      }
  for (t2 = 0; t2 <= 587; t2 += 1) {
    if (t2 <= 0) {
      _P_DATA2 = ((unsigned short *)(malloc(sizeof(unsigned short ) * chill_count_1)));
      _P_DATA3 = ((unsigned short *)(malloc(sizeof(unsigned short ) * chill_count_1)));
      A_prime = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 1))));
    }
    for (t4 = 0; t4 <= 589; t4 += 1) {
      _P_DATA5 = _P1[590 * t2 + 1 * t4];
      for (newVariable0 = 1 - _P_DATA1[590 * t2 + 1 * t4 + 1]; newVariable0 <= 0; newVariable0 += 1) {
        _P_DATA2[_P_DATA1[590 * t2 + 1 * t4] - newVariable0] = _P_DATA5 -> col_[0];
        _P_DATA3[_P_DATA1[590 * t2 + 1 * t4] - newVariable0] = _P_DATA5 -> col_[1];
        A_prime[(_P_DATA1[590 * t2 + 1 * t4] - newVariable0) * 1] = _P_DATA5 -> A;
        _P_DATA5 = _P_DATA5 -> next;
      }
      _P_DATA1[590 * t2 + 1 * t4 + 1] += _P_DATA1[590 * t2 + 1 * t4];
    }
  }
#pragma omp parallel  private(t8,t6,t4)
{
    
#pragma omp for 
    for (t2 = 0; t2 <= 587; t2 += 1) 
      for (t4 = 0; t4 <= 589; t4 += 1) 
        for (t6 = _P_DATA1_(t2,t4); t6 <= _P_DATA1__(t2,t4) - 1; t6 += 1) {
          
#pragma simd
          for (t8 = 0; t8 <= 1; t8 += 1) 
            y[4096 * t2 + _P_DATA2[t6]][t8] += (A_prime[t6 * 1] * x[4096 * t4 + _P_DATA3[t6]][t8]);
        }
  }
#pragma omp parallel  private(t8,t6,t4)
{
    
#pragma omp for 
    for (t2 = 0; t2 <= 589; t2 += 1) 
      for (t4 = 0; t4 <= 587; t4 += 1) 
        for (t6 = _P_DATA1_(t4,t2); t6 <= _P_DATA1__(t4,t2) - 1; t6 += 1) {
          
#pragma simd
          for (t8 = 0; t8 <= 1; t8 += 1) 
            y[4096 * t2 + _P_DATA3[t6]][t8] += (A_prime[t6 * 1] * x[4096 * t4 + _P_DATA2[t6]][t8]);
        }
  }
  for (t2 = 2408448; t2 <= 2412468; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) 
      for (t6 = 0; t6 <= 1; t6 += 1) 
        y[col[t4]][t6] += (A[t4] * x[t2][t6]);
#pragma omp parallel  private(t8,t6,t4)
{
    
#pragma omp for 
    for (t2 = 2408448; t2 <= 2412468; t2 += 1) 
      for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
        
#pragma simd
        for (t6 = 0; t6 <= 1; t6 += 1) 
          y[t2][t6] += (A[t4] * x[col[t4]][t6]);
      }
  }
  for (_t58 = 0; _t58 <= 587; _t58 += 1) 
    for (_t59 = 0; _t59 <= 589; _t59 += 1) {
      _P_DATA5 = _P1[590 * _t58 + 1 * _t59];
      for (newVariable0 = _P_DATA1[590 * _t58 + 1 * _t59]; newVariable0 <= _P_DATA1[590 * _t58 + 1 * _t59 + 1] - 1; newVariable0 += 1) {
        _P1[590 * _t58 + 1 * _t59] = _P1[590 * _t58 + 1 * _t59] -> next;
        free(_P_DATA5);
        _P_DATA5 = _P1[590 * _t58 + 1 * _t59];
      }
    }
  free(_P_DATA1);
  free(_P_DATA2);
  free(_P_DATA3);
  free(A_prime);
}

int main()
{
  int m = 4;
  int n = 1000;
  int *index;
  float (*x)[4UL];
  float (*y)[4UL];
  int *col;
  float *A;
  csb(m,n,index,col,x,y,A);
  return 0;
}
