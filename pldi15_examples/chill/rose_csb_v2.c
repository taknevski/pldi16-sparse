#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA1_(_t36,_t37) _P_DATA1[590 * _t36 + _t37]
#define _P_DATA1_(_t46,_t47) _P_DATA1[590 * _t46 + _t47]
#define _P_DATA1__(_t36,_t37) _P_DATA1[590 * _t36 + _t37 + 1]
#define _P_DATA1__(_t46,_t47) _P_DATA1[590 * _t46 + _t47 + 1]
#define col_(_t40) col[_t40]
#define col_(j) col[j]
#define index_(i) index[i]
#define index__(i) index[i + 1]
#define N 1000

struct a_list 
{
  int col_[2];
  float A;
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void csb(int m,int n,int *index,int *col,float *x,float *y,float *A)
{
  int t6;
  int t4;
  int t2;
  int _t50;
  int _t49;
  int _t48;
  int _t47;
  int _t46;
  int _t45;
  int _t44;
  int _t43;
  int _t42;
  int _t41;
  int newVariable0;
  int In_1;
  struct a_list *_P_DATA5;
  struct mk *_P_DATA4;
  float *A_prime;
  struct a_list *_P1[346920];
  int *_P_DATA3;
  int *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t40;
  int _t39;
  int _t38;
  int _t37;
  int _t36;
  int _t35;
  int _t34;
  int _t33;
  int _t32;
  int _t31;
  int _t30;
  int _t29;
  int _t28;
  int _t27;
  int _t26;
  int _t25;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  int _t18;
  int _t17;
  int _t15;
  int _t14;
  int _t13;
  int _t11;
  int _t10;
  int _t9;
  int k;
  int _t8;
  int _t7;
  int _t6;
  int _t5;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * 346921)));
  _P_DATA1[0] = 0;
  chill_count_1 = 0;
  for (_t36 = 0; _t36 <= 587; _t36 += 1) 
    for (_t37 = 0; _t37 <= 589; _t37 += 1) {
      _P1[590 * _t36 + 1 * _t37] = 0;
      _P_DATA1[590 * _t36 + 1 * _t37 + 1] = 0;
    }
  for (t2 = 0; t2 <= 587; t2 += 1) 
    for (t4 = 0; t4 <= 4095; t4 += 1) 
      for (t6 = index_(4096 * t2 + t4); t6 <= index__(4096 * t2 + t4) - 1; t6 += 1) {
        _t37 = (col_(t6) - 0) / 4096;
        _t39 = (col_(t6) - 0) % 4096;
        _P_DATA5 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
        _P_DATA5 -> next = _P1[590 * t2 + 1 * _t37];
        _P1[590 * t2 + 1 * _t37] = _P_DATA5;
        _P1[590 * t2 + 1 * _t37] -> A = 0;
        _P1[590 * t2 + 1 * _t37] -> col_[0] = t4;
        _P1[590 * t2 + 1 * _t37] -> col_[1] = _t39;
        chill_count_1 += 1;
        _P_DATA1[590 * t2 + 1 * _t37 + 1] += 1;
        _P1[590 * t2 + 1 * _t37] -> A = A[t6];
      }
  _P_DATA2 = ((int *)(malloc(sizeof(int ) * chill_count_1)));
  _P_DATA3 = ((int *)(malloc(sizeof(int ) * chill_count_1)));
  A_prime = ((float *)(malloc(sizeof(float ) * chill_count_1)));
  for (t2 = 0; t2 <= 587; t2 += 1) 
    for (t4 = 0; t4 <= 589; t4 += 1) {
      for (newVariable0 = 1 - _P_DATA1[590 * t2 + 1 * t4 + 1]; newVariable0 <= 0; newVariable0 += 1) {
        _P_DATA2[_P_DATA1[590 * t2 + 1 * t4] - newVariable0] = _P1[590 * t2 + 1 * t4] -> col_[0];
        _P_DATA3[_P_DATA1[590 * t2 + 1 * t4] - newVariable0] = _P1[590 * t2 + 1 * t4] -> col_[1];
        A_prime[_P_DATA1[590 * t2 + 1 * t4] - newVariable0] = _P1[590 * t2 + 1 * t4] -> A;
        _P_DATA5 = _P1[590 * t2 + 1 * t4] -> next;
        free(_P1[590 * t2 + 1 * t4]);
        _P1[590 * t2 + 1 * t4] = _P_DATA5;
      }
      _P_DATA1[590 * t2 + 1 * t4 + 1] += _P_DATA1[590 * t2 + 1 * t4];
    }
  for (t2 = 0; t2 <= 587; t2 += 1) 
    for (t4 = 0; t4 <= 589; t4 += 1) 
      for (t6 = _P_DATA1_(t2,t4); t6 <= _P_DATA1__(t2,t4) - 1; t6 += 1) 
        y[4096 * t2 + _P_DATA2[t6]] += (A_prime[t6] * x[4096 * t4 + _P_DATA3[t6]]);
  for (t2 = 2408448; t2 <= 2412468; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
      y[t2] += (A[t4] * x[col[t4]]);
      y[col[t4]] += (A[t4] * x[t2]);
    }
  for (t2 = 0; t2 <= 589; t2 += 1) 
    for (t4 = 0; t4 <= 587; t4 += 1) 
      for (t6 = _P_DATA1_(t4,t2); t6 <= _P_DATA1__(t4,t2) - 1; t6 += 1) 
        y[4096 * t2 + _P_DATA3[t6]] += (A_prime[t6] * x[4096 * t4 + _P_DATA2[t6]]);
  free(_P_DATA1);
  free(_P_DATA2);
  free(_P_DATA3);
  free(A_prime);
  free(_P_DATA4);
}

int main()
{
  int m = 4;
  int n = 1000;
  int *index;
  float *x;
  float *y;
  int *col;
  float *A;
  csb(m,n,index,col,x,y,A);
  return 0;
}
