#include <omp.h>
#include "LevelSchedule.hpp"
#include <stdio.h>
#include <string.h>
//CSR A;

void ilu0RefCHiLL(int m,int *rowptr, int *colidx, int *diagptr, double*values ) 
{ 

  int i,k,j1,j2,c;
  double tmp;
  for(i=0; i < m; i++) 
    { 
       for(k= rowptr[i]; k < diagptr[i]; k++)
        { 
          values[k] = values[k]/values[diagptr[colidx[k]]];
	
          for(j1= k+1; j1 < rowptr[i+1]; j1++) 	 
            for(j2 = diagptr[colidx[k]] + 1;  j2 <rowptr[colidx[k] + 1]; j2++) {
               	if(colidx[j2] > colidx[j1])
		    break;	
		if(colidx[j2] == colidx[j1]){	
                   values[j1] -= values[k]*values[j2]; 
		   break;	
		}
            } 
        } 
     }
}



int main(){

  int rowptr[501];
    int diagptr[500];
  int colidx[5000];
  double values[5000];
  ilu0RefCHiLL(500, rowptr, colidx, diagptr, values);

  return 0;
}
