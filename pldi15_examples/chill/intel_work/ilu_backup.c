void ilu0RefCHiLL(int m,int *rowptr, int *colidx, int *diagptr, double*values ) 
{ 

  int i,k,j1,j2,c;
  double tmp;
  for(i=0; i < m; i++) 
    { 
       for(k= rowptr[i]; k < diagptr[i]; k++)
        { 
          c = colidx[k];
          tmp = values[k] /= values[diagptr[c]]; // a_ik /= a_kk                                                                                                                                                            
 
          j1 = k + 1;
	  j2 = diagptr[c] + 1;
 
          while (j1 < rowptr[i + 1] && j2 <rowptr[c + 1]) {
            if (colidx[j1] < colidx[j2]) ++j1;
            else if (colidx[j2] < colidx[j1]) ++j2;
            else { 
              values[j1] -= tmp*values[j2]; // a_ij -= a_ik*a_kj                                                                                                                                                                      
              ++j1; ++j2; 
            }
          } 
        } 
    } 
}



int main(){

  int rowptr[501];
    int diagptr[500];
  int colidx[5000];
  double values[5000];
  ilu0RefCHiLL(500, rowptr, colidx, diagptr, values);

  return 0;
}
