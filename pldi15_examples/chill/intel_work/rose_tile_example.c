#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))

void ilu0RefCHiLL(int m,int *rowptr,int *colidx,int *diagptr,double *values)
{
  int t2;
  int t6;
  int t4;
  int _t3;
  int _t1;
  int i;
  for (t2 = 0; t2 <= __rose_lt(7,m - 1); t2 += 1) 
    for (t4 = 0; t4 <= (m - t2 - 1) / 8; t4 += 1) 
      values[t2 + 8 * t4] = 1.0;
}

int main()
{
  int rowptr[501UL];
  int diagptr[500UL];
  int colidx[5000UL];
  double values[5000UL];
  ilu0RefCHiLL(500,rowptr,colidx,diagptr,values);
  return 0;
}
