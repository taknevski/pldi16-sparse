#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define colidx_(i,k) colidx[1 * k]
#define colidx__(ip,kp) colidx[1 * kp]
#define colidx___(i,k) colidx[1 * k + 1]
#define colidx____(ip,kp) colidx[1 * kp + 1]
#define colidx_p(i,k,ip,kp) colidx[1 * kp]
#define diagptr_(i) diagptr[1 * i]
#define diagptr___(i) diagptr[1 * i + 1]
#define diagptr___p(i,k,ip) diagptr[1 * ip + 1]
#define diagptr_colidx__(i,k) diagptr[1 * colidx_(i,k)]
#define diagptr_colidx___(ip,kp) diagptr[1 * colidx__(i,k)]
#define diagptr_colidx____(i,k) diagptr[1 * colidx_(i,k) + 1]
#define diagptr_colidx_____(ip,kp) diagptr[1 * colidx__(i,k) + 1]
#define diagptr_colidx____p(i,k,ip,kp) diagptr[1 * colidx_(i,kp) + 1]
#define diagptr_colidx__p(i,k,ip,kp) diagptr[1 * colidx_(i,kp)]
#define diagptr_p(i,k,ip) diagptr[1 * ip]
#define rowptr_(i) rowptr[1 * i]
#define rowptr__(i) rowptr[1 * i + 1]
#define rowptr___(i) rowptr[1 * i + 1]
#define rowptr____(i) rowptr[1 * i]
#define rowptr__p(i,k,ip) rowptr[1 * ip + 1]
#define rowptr_colidx__(i,k) rowptr[1 * colidx_(i,k) + 1]
#define rowptr_colidx____(i,k) rowptr[1 * colidx_(i,k)]
#define rowptr_colidx____p(i,k,ip,kp) rowptr[1 * colidx_(i,kp)]
#define rowptr_colidx__p(i,k,ip,kp) rowptr[1 * colidx_(i,kp) + 1]
#define rowptr_p(i,k,ip) rowptr[1 * ip]
#define taskBoundaries_(i) schedule -> taskBoundaries[i]
#define taskBoundaries__(i) schedule -> taskBoundaries[i + 1]
#define threadBoundaries_(i) schedule -> threadBoundaries[i]
#define threadBoundaries__(i) schedule -> threadBoundaries[i + 1]
#include <omp.h>
#include "LevelSchedule.hpp"
#include <stdio.h>
#include <string.h>
//CSR A;

void ilu0RefCHiLL(int m,int *rowptr,int *colidx,int *diagptr,double *values)
{
  int idx;
  volatile int zplanes[256];
  int right;
  int left;
  int num_threads;
  int tid;
  int ub;
  int t8;
  int t6;
  int t4;
  int t2;
  int _t5;
  int _t4;
  int _t3;
  int In_1;
  int _t2;
  int _t1;
  int *perm_inv;
  int *perm;
  class SpMP::LevelSchedule *schedule;
  class SpMP::CSR *A;
  int ip;
  int kp;
  int i;
  int k;
  int j1;
  int j2;
  double tmp;
  A = (new SpMP::CSR );
  schedule = (new SpMP::LevelSchedule );
  for (t2 = 0; t2 <= m - 1; t2 += 1) 
    for (t4 = rowptr_(t2); t4 <= diagptr_(t2) - 1; t4 += 1) 
      if (colidx_(t2,t4) + 1 <= t2) {
        t6 = colidx_(t2,t4);
        for (t8 = rowptr_p(t2,t4,t6); t8 <= diagptr_p(t2,t4,t6) - 1; t8 += 1) 
          A ->  connect_local (t6,t2);
      }
      else if (t2 + 1 <= colidx_(t2,t4)) {
        t6 = colidx_(t2,t4);
        for (t8 = rowptr_p(t2,t4,t6); t8 <= diagptr_colidx__(t2,t4) - 1; t8 += 1) 
          A ->  connect_remote (t2,t6);
      }
  schedule ->  constructTaskGraph ( *A);
  perm = schedule -> SpMP::LevelSchedule::origToThreadContPerm;
  perm_inv = schedule -> SpMP::LevelSchedule::threadContToOrigPerm;
#pragma omp parallel  private(tid,t6,t8,t4)
{
    tid = omp_get_thread_num();
    num_threads = omp_get_num_threads();
{
      for (t4 = threadBoundaries_(tid); t4 <= threadBoundaries__(tid) - 1; t4 += 1) {
        for (t6 = taskBoundaries_(t4); t6 <= taskBoundaries__(t4) - 1; t6 += 1) 
          for (t8 = rowptr_(perm[t6]); t8 <= diagptr_(perm[t6]) - 1; t8 += 1) {
//c = colidx[k];
// a_ik /= a_kk
            values[t8] = (values[t8] / values[diagptr[colidx[t8]]]);
            j1 = (t8 + 1);
            j2 = (diagptr[colidx[t8]] + 1);
            while((j1 < rowptr[perm[t6] + 1]) && (j2 < rowptr[colidx[t8] + 1])){
              if (colidx[j1] == colidx[j2]) {
// a_ij -= a_ik*a_kj
                values[j1] -= (values[t8] * values[j2]);
                ++j1;
                ++j2;
              }
              else if (colidx[j1] < colidx[j2]) 
                ++j1;
              else 
                ++j2;
            }
          }
#pragma omp barrier 
      }
    }
  }
}

int main()
{
  int rowptr[501UL];
  int diagptr[500UL];
  int colidx[5000UL];
  double values[5000UL];
  ilu0RefCHiLL(500,rowptr,colidx,diagptr,values);
  return 0;
}
