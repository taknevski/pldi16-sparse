#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define diagptr_(i) diagptr[i]
#define diagptr_) diagptr[c]
#define rowptr_(i) rowptr[i]

void ilu0RefCHiLL(int m,int *rowptr,int *colidx,int *diagptr,double *values)
{
  int idx;
  volatile int zplanes[256];
  int right;
  int left;
  int num_threads;
  int tid;
  int ub;
  int t8;
  int t6;
  int t4;
  int t2;
  int _t5;
  int _t4;
  int _t3;
  int In_1;
  int _t2;
  int _t1;
  int i_;
  int A;
  int i;
  int k;
  int j1;
  int j2;
  int c;
  double tmp;
  for (t2 = 0; t2 <= m - 1; t2 += 1) 
    for (t4 = rowptr_(t2); t4 <= diagptr_(t2) - 1; t4 += 1) 
      for (t6 = 0; t6 <= m - 1; t6 += 1) 
        connect(A,t2,t6);
  constructTaskGraph(A);
#pragma omp parallel  private(tid,num_threads,sc_temp,t6,t2,t4,t8)
{
    tid = omp_get_thread_num();
    num_threads = omp_get_num_threads();
{
      for (t4 = threadBoundaries_(tid); t4 <= threadBoundaries__(tid) - 1; t4 += 1) 
        for (t6 = taskBoundaries_(t4); t6 <= taskBoundaries__(t4) - 1; t6 += 1) 
          for (t8 = rowptr_(t6); t8 <= diagptr_(t6) - 1; t8 += 1) {
            c = colidx[t8];
// a_ik /= a_kk                                                                                                                                                            
            tmp = (values[t8] /= values[diagptr[c]]);
            j1 = (t8 + 1);
            j2 = (diagptr[c] + 1);
            while((j1 < rowptr[t6 + 1]) && (j2 < rowptr[c + 1])){
              if (colidx[j1] < colidx[j2]) 
                ++j1;
              else if (colidx[j2] < colidx[j1]) 
                ++j2;
              else {
// a_ij -= a_ik*a_kj                                                                                                                                                                      
                values[j1] -= (tmp * values[j2]);
                ++j1;
                ++j2;
              }
            }
          }
#pragma omp barrier 
    }
  }
}

int main()
{
  int rowptr[501UL];
  int diagptr[500UL];
  int colidx[5000UL];
  double values[5000UL];
  ilu0RefCHiLL(500,rowptr,colidx,diagptr,values);
  return 0;
}
