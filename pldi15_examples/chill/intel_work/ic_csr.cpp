extern double sqrt(int);
void ic_csr(int n, double *val, int * row, int *col){

int i, k,l, m;
for (i = 0; i < n - 1; i++) {

  val[row[i]] = sqrt(val[row[i]]);//S1

  for (m = row[i] + 1; m < row[i+1]; m++){ 
     val[m] = val[m] / val[row[i]];//S2
  }

  for (m = row[i] + 1; m < row[i+1]; m++) {
    for (k = row[col[m]] ; k < row[col[m]+1]; k++){
      for ( l = m; l < row[i+1] && col[l+1] <= col[k]; l++){
        if (col[l] == col[k]){
          val[k] -= val[m]* val[l]; //S3
        }
      }
    }
  }
}


}
