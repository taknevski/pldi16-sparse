#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define colidx_(i,k) colidx[1 * k]
#define colidx___(i,k) colidx[1 * k + 1]
#define rowptr_(i) rowptr[1 * i]
#define rowptr__(i) rowptr[1 * i + 1]
#define rowptr___(i) rowptr[1 * i + 1]
#define rowptr____(i) rowptr[1 * i]
#define rowptr__p(i,k,ip) rowptr[1 * ip + 1]
#define rowptr_p(i,k,ip) rowptr[1 * ip]
#define taskBoundaries_(i) schedule -> taskBoundaries[i]
#define taskBoundaries__(i) schedule -> taskBoundaries[i + 1]
#define threadBoundaries_(i) schedule -> threadBoundaries[i]
#define threadBoundaries__(i) schedule -> threadBoundaries[i + 1]
#include "LevelSchedule.hpp"
#include <algorithm>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
using namespace std;

void backwardSolveRef(int n,int *rowptr,int *colidx,int *idiag,double *values,double *y,const double *b)
{
  int idx;
  volatile int zplanes[256];
  int right;
  int left;
  int num_threads;
  int tid;
  int ub;
  int t8;
  int t4;
  int t2;
  int _t15;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t6;
  int _t5;
  int _t4;
  int _t3;
  int In_1;
  int _t2;
  int _t1;
  int *perm_inv;
  int *perm;
  int cnt;
  int tmp;
  int j;
  int c;
  int source_offset;
  int acc;
  int t6;
  int found;
  class SpMP::LevelSchedule *schedule;
  class SpMP::CSR *A;
  int *_P_DATA4;
  int *_P_DATA3;
  int *_P_DATA2;
  int *_P_DATA1;
  int ip;
  int i;
  int k;
  double sum;
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * n)));
  _P_DATA2 = ((int *)(malloc(sizeof(int ) * n)));
  A = (new SpMP::CSR );
  schedule = (new SpMP::LevelSchedule );
#pragma omp parallel 
{
    
#pragma omp for 
    for (t2 = 0; t2 <= n - 1; t2 += 1) {
      A -> ::SpMP::CSR::rowptr[t2] = 0;
      _P_DATA1[t2] = 0;
    }
  }
  A -> SpMP::CSR::rowptr[n] = 0;
{
    if (2 <= n) 
#pragma omp parallel  private(n,t6,found,t4)
{
        
#pragma omp for 
        for (t2 = -n + 1; t2 <= 0; t2 += 1) 
          for (t4 = rowptr_(-t2); t4 <= rowptr__(-t2) - 1; t4 += 1) {
            if (colidx_(t2,t4) + t2 <= -1) {
              found = 0;
              found = 0;
            }
            t6 = colidx_(t2,t4);
            if (colidx_(t2,t4) + t2 <= -1) 
              found = 1;
            if (colidx_(t2,t4) + t2 <= -1) 
              if (found == 1) 
                _P_DATA1[-t2] = _P_DATA1[-t2] + 1;
            if (1 <= colidx_(t2,t4) + t2) {
              found = 0;
              found = 0;
            }
            t6 = colidx_(t2,t4);
            if (1 <= colidx_(t2,t4) + t2) 
              found = 1;
            if (1 <= colidx_(t2,t4) + t2) 
              if (found == 1) 
                _P_DATA1[-t2] = _P_DATA1[-t2] + 1;
          }
      }
  }
  for (t2 = 0; t2 <= n - 1; t2 += 1) 
    A -> SpMP::CSR::rowptr[t2 + 1] = A -> SpMP::CSR::rowptr[t2] + _P_DATA1[t2];
#pragma omp parallel  private(n,t6,found,t4)
{
    
#pragma omp for 
    for (t2 = 0; t2 <= n - 1; t2 += 1) 
      _P_DATA1[t2] = 0;
  }
  A -> SpMP::CSR::colidx = ((int *)(malloc(sizeof(int ) * A -> SpMP::CSR::rowptr[n])));
  memset(A -> SpMP::CSR::colidx,-1,A -> SpMP::CSR::rowptr[n]);
{
    if (2 <= n) 
#pragma omp parallel  private(n,t6,found,t4)
{
        
#pragma omp for 
        for (t2 = -n + 1; t2 <= 0; t2 += 1) 
          for (t4 = rowptr_(-t2); t4 <= rowptr__(-t2) - 1; t4 += 1) {
            if (colidx_(t2,t4) + t2 <= -1) 
              found = 0;
            t6 = colidx_(t2,t4);
            if (colidx_(t2,t4) + t2 <= -1) 
              found = 1;
            if (colidx_(t2,t4) + t2 <= -1) 
              if (found == 1) 
                if (find(A -> ::SpMP::CSR::colidx + A -> ::SpMP::CSR::rowptr[-t2],A -> ::SpMP::CSR::colidx + (A -> ::SpMP::CSR::rowptr[-t2] + _P_DATA1[-t2]),t6) == A -> ::SpMP::CSR::colidx + A -> ::SpMP::CSR::rowptr[-t2] + _P_DATA1[-t2]) {
                  A -> ::SpMP::CSR::colidx[A -> ::SpMP::CSR::rowptr[-t2] + _P_DATA1[-t2]] = t6;
                  _P_DATA1[-t2] = _P_DATA1[-t2] + 1;
                }
            if (1 <= colidx_(t2,t4) + t2) 
              found = 0;
            t6 = colidx_(t2,t4);
            if (1 <= colidx_(t2,t4) + t2) 
              found = 1;
            if (1 <= colidx_(t2,t4) + t2) 
              if (found == 1) 
                if (find(A -> ::SpMP::CSR::colidx + A -> ::SpMP::CSR::rowptr[-t2],A -> ::SpMP::CSR::colidx + (A -> ::SpMP::CSR::rowptr[-t2] + _P_DATA1[-t2]),t6) == A -> ::SpMP::CSR::colidx + A -> ::SpMP::CSR::rowptr[-t2] + _P_DATA1[-t2]) {
                  A -> ::SpMP::CSR::colidx[A -> ::SpMP::CSR::rowptr[-t2] + _P_DATA1[-t2]] = t6;
                  _P_DATA1[-t2] = _P_DATA1[-t2] + 1;
                }
          }
      }
  }
  acc = 0;
  for (t2 = 0; t2 <= n - 1; t2 += 1) 
    acc += _P_DATA1[t2];
  _P_DATA3 = ((int *)(malloc(sizeof(int ) * acc)));
  source_offset = 0;
  for (t2 = 0; t2 <= n - 1; t2 += 1) {
    memcpy(_P_DATA3 + A -> SpMP::CSR::rowptr[t2],A -> SpMP::CSR::colidx + source_offset,_P_DATA1[t2] * sizeof(int ));
    source_offset = A -> SpMP::CSR::rowptr[t2 + 1];
    A -> SpMP::CSR::rowptr[t2 + 1] = A -> SpMP::CSR::rowptr[t2] + _P_DATA1[t2];
  }
  free(A -> SpMP::CSR::colidx);
  A -> SpMP::CSR::colidx = _P_DATA3;
#pragma omp parallel  private(n,t6,found,t4)
{
    
#pragma omp for 
    for (t2 = 0; t2 <= n - 1; t2 += 1) 
      sort(A -> ::SpMP::CSR::colidx + A -> ::SpMP::CSR::rowptr[t2],A -> ::SpMP::CSR::colidx + A -> ::SpMP::CSR::rowptr[t2 + 1]);
  }
#pragma omp parallel  private(n,t6,found,t4)
{
    
#pragma omp for 
    for (t2 = 0; t2 <= n - 1; t2 += 1) 
      _P_DATA2[t2] = 0;
  }
#pragma omp parallel  private(n,t6,found,c,j,t4)
{
    
#pragma omp for 
    for (t2 = 0; t2 <= n - 1; t2 += 1) {
      if (!binary_search(A -> ::SpMP::CSR::colidx + A -> ::SpMP::CSR::rowptr[t2],A -> ::SpMP::CSR::colidx + A -> ::SpMP::CSR::rowptr[t2 + 1],t2)) 
        __sync_fetch_and_add(_P_DATA2 + t2,1);
      for (j = A -> ::SpMP::CSR::rowptr[t2]; j <= A -> ::SpMP::CSR::rowptr[t2 + 1] - 1; j += 1) {
        c = A -> ::SpMP::CSR::colidx[j];
        if (!binary_search(A -> ::SpMP::CSR::colidx + A -> ::SpMP::CSR::rowptr[c],A -> ::SpMP::CSR::colidx + A -> ::SpMP::CSR::rowptr[c + 1],t2)) 
          __sync_fetch_and_add(_P_DATA2 + c,1);
      }
    }
  }
  acc = 0;
  for (t2 = 0; t2 <= n - 1; t2 += 1) 
    acc += _P_DATA2[t2];
  _P_DATA4 = ((int *)(malloc(sizeof(int ) * (A -> SpMP::CSR::rowptr[n] + acc))));
  source_offset = 0;
  for (t2 = 0; t2 <= n - 1; t2 += 1) {
    memcpy(_P_DATA4 + A -> SpMP::CSR::rowptr[t2],A -> SpMP::CSR::colidx + source_offset,(_P_DATA1[t2] + _P_DATA2[t2]) * sizeof(int ));
    tmp = _P_DATA2[t2];
    _P_DATA2[t2] = A -> SpMP::CSR::rowptr[t2 + 1] - source_offset;
    source_offset = A -> SpMP::CSR::rowptr[t2 + 1];
    A -> SpMP::CSR::rowptr[t2 + 1] = A -> SpMP::CSR::rowptr[t2] + _P_DATA1[t2] + tmp;
  }
  free(A -> SpMP::CSR::colidx);
  A -> SpMP::CSR::colidx = _P_DATA4;
#pragma omp parallel  private(n,t6,found,c,j,cnt,t4)
{
    
#pragma omp for 
    for (t2 = 0; t2 <= n - 1; t2 += 1) {
      if (!binary_search(A -> ::SpMP::CSR::colidx + A -> ::SpMP::CSR::rowptr[t2],A -> ::SpMP::CSR::colidx + A -> ::SpMP::CSR::rowptr[t2] + _P_DATA2[t2],t2)) {
        cnt = __sync_fetch_and_add(_P_DATA1 + t2,1);
        A -> ::SpMP::CSR::colidx[A -> ::SpMP::CSR::rowptr[t2] + cnt] = t2;
      }
      for (j = A -> ::SpMP::CSR::rowptr[t2]; j <= A -> ::SpMP::CSR::rowptr[t2] + _P_DATA2[t2] - 1; j += 1) {
        c = A -> ::SpMP::CSR::colidx[j];
        if (!binary_search(A -> ::SpMP::CSR::colidx + A -> ::SpMP::CSR::rowptr[c],A -> ::SpMP::CSR::colidx + A -> ::SpMP::CSR::rowptr[c] + _P_DATA2[c],t2)) {
          cnt = __sync_fetch_and_add(_P_DATA1 + c,1);
          A -> ::SpMP::CSR::colidx[A -> ::SpMP::CSR::rowptr[c] + cnt] = t2;
        }
      }
    }
  }
#pragma omp parallel  private(n,t6,found,c,j,cnt,t4)
{
    
#pragma omp for 
    for (t2 = 0; t2 <= n - 1; t2 += 1) {
      sort(A -> ::SpMP::CSR::colidx + A -> ::SpMP::CSR::rowptr[t2],A -> ::SpMP::CSR::colidx + A -> ::SpMP::CSR::rowptr[t2 + 1]);
      for (j = A -> ::SpMP::CSR::rowptr[t2]; j <= A -> ::SpMP::CSR::rowptr[t2 + 1] - 1; j += 1) 
        if (A -> ::SpMP::CSR::colidx[j] == t2) 
          A -> ::SpMP::CSR::diagptr[t2] = j;
    }
  }
  schedule ->  constructTaskGraph ( *A);
  perm = schedule -> SpMP::LevelSchedule::origToThreadContPerm;
  perm_inv = schedule -> SpMP::LevelSchedule::threadContToOrigPerm;
#pragma omp parallel  private(tid,t6,t4,t8) num_threads(12)
{
    tid = omp_get_thread_num();
    num_threads = omp_get_num_threads();
{
      for (t4 = -threadBoundaries__(tid) + 1; t4 <= -threadBoundaries_(tid); t4 += 1) {
        for (t6 = -taskBoundaries__(-t4) + 1; t6 <= -taskBoundaries_(-t4); t6 += 1) {
          t8 = rowptr_(-t6);
          sum = b[-t6];
          for (t8 = rowptr_(-(-(-t6))); t8 <= rowptr__(-(-(-t6))) - 1; t8 += 1) 
            sum -= (values[t8] * y[colidx[t8]]);
          t8 = rowptr__(-t6);
          y[-t6] = (sum * idiag[-t6]);
        }
#pragma omp barrier 
      }
    }
  }
}

int main()
{
  int n = 500;
  int rowptr[501UL];
  int diagptr[500UL];
  int colidx[5000UL];
  double values[5000UL];
  double b[5000UL];
  double y[5000UL];
  backwardSolveRef(n,rowptr,colidx,diagptr,values,y,b);
  return 0;
}
