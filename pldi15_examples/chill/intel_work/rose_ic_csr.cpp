#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define col_(i,k,l,m) col[1 * l]
#define col__(i,k,m) col[1 * k]
#define col___(i,k,l,m) col[1 * l + 1]
#define col____(i,k,m) col[1 * k + 1]
#define col_____(i,k,l,m) col[1 * l]
#define col______(i,m) col[1 * m]
#define col________(i,m) col[1 * m + 1]
#define row_(i) row[1 * i + 1]
#define row__(i) row[1 * i]
#define row___(i) row[1 * i]
#define row____(i) row[1 * i + 1]
#define row_____(ip) row[1 * ip]
#define row_______(ip) row[1 * ip + 1]
#define row_col_______(i,m) row[1 * col______(i,m)]
#define row_col________(i,m) row[1 * col______(i,m) + 1]
#define row_col_________(i,m) row[1 * col______(i,m) + 1]
#define row_col__________(i,m) row[1 * col______(i,m)]
double sqrt(int );

void ic_csr(int n,double *val,int *row,int *col)
{
  int t8;
  int t6;
  int t4;
  int t2;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int ip;
  int i;
  int k;
  int l;
  int m;
  for (t2 = 0; t2 <= n - 2; t2 += 1) {
    t4 = row__(t2) + 1;
    t6 = row_col_______(t2,t4);
    t8 = row__(t2) + 1;
//S1
    val[row[t2]] = sqrt(val[row[t2]]);
    for (t4 = row__(t2) + 1; t4 <= row_(t2) - 1; t4 += 1) {
      t6 = row_col_______(t2,t4);
//S2
      val[t4] = (val[t4] / val[row[t2]]);
    }
    for (t4 = row__(t2) + 1; t4 <= row_(t2) - 1; t4 += 1) 
      if (row_col_______(t2,t4) + 1 <= row_col________(t2,t4)) 
        for (t6 = row_col_______(t2,t4); t6 <= row_col________(t2,t4) - 1; t6 += 1) 
          for (t8 = t4; t8 <= row_(t2) - 1; t8 += 1) 
            if (col_(t2,t6,t8,t4) == col__(t2,t6,t4) && col___(t2,t6,t8,t4) <= col_(t2,t6,t8,t4)) 
//S3
              val[t6] -= (val[t4] * val[t8]);
  }
}
