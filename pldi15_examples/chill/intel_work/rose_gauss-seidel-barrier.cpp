#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define colidx_(i,k) colidx[1 * k]
#define rowptr_(i) rowptr[1 * i]
#define rowptr__(i) rowptr[1 + 1 * i]
#define taskBoundaries_(i) schedule -> taskBoundaries[i]
#define taskBoundaries__(i) schedule -> taskBoundaries[i + 1]
#define threadBoundaries_(i) schedule -> threadBoundaries[i]
#define threadBoundaries__(i) schedule -> threadBoundaries[i + 1]
#include "LevelSchedule.hpp"

void forwardSolveRef(int n,int *rowptr,int *colidx,int *idiag,double *values,double *y,const double *b)
{
  int idx;
  volatile int zplanes[256];
  int right;
  int left;
  int num_threads;
  int tid;
  int ub;
  int t8;
  int t6;
  int t4;
  int t2;
  int _t15;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t6;
  int _t5;
  int _t4;
  int _t3;
  int In_1;
  int _t2;
  int _t1;
  int *perm_inv;
  int *perm;
  int i';
  class SpMP::LevelSchedule *schedule;
  class SpMP::CSR *A;
  int i;
  int k;
  double sum;
  A = (new SpMP::CSR );
  schedule = (new SpMP::LevelSchedule );
  if (2 <= n) 
    for (t2 = 0; t2 <= n - 1; t2 += 1) 
      for (t4 = rowptr_(t2); t4 <= rowptr__(t2) - 1; t4 += 1) {
        t6 = colidx_(t2,t4);
        if (t6 + 1 <= t2) 
          if (0 <= t6) 
            A ->  connect_local (-t6,-t2); else {}
        else if (t2 + 1 <= t6 && t6 + 1 <= n) 
          A ->  connect_remote (-t2,-t6);
      }
  schedule ->  constructTaskGraph ( *A);
  perm = schedule -> SpMP::LevelSchedule::origToThreadContPerm;
  perm_inv = schedule -> SpMP::LevelSchedule::threadContToOrigPerm;
#pragma omp parallel  private(tid,num_threads,sc_temp,t6,t4,t8)
{
    tid = omp_get_thread_num();
    num_threads = omp_get_num_threads();
{
      for (t4 = threadBoundaries_(tid); t4 <= threadBoundaries__(tid) - 1; t4 += 1) 
        for (t6 = taskBoundaries_(t4); t6 <= taskBoundaries__(t4) - 1; t6 += 1) {{
            t8 = rowptr_(t6);
            sum = b[t6];
            for (t8 = rowptr_(perm[t6]); t8 <= rowptr__(perm[t6]) - 1; t8 += 1) 
              sum -= (values[t8] * y[colidx[t8]]);
            t8 = rowptr__(t6);
            y[t6] = (sum * idiag[t6]);
          }
#pragma omp barrier 
        }
    }
  }
}

int main()
{
  int n = 500;
  int rowptr[501UL];
  int diagptr[500UL];
  int colidx[5000UL];
  double values[5000UL];
  double b[5000UL];
  double y[5000UL];
  forwardSolveRef(n,rowptr,colidx,diagptr,values,y,b);
  return 0;
}
