#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define colidx_(i,j1,j2,k) colidx[1 * j2]
#define colidx__(i,j1,k) colidx[1 * j1]
#define colidx___(i,k) colidx[1 * k]
#define diagptr_(i) diagptr[1 * i]
#define diagptr_colidx____(i,k) diagptr[1 * colidx___(i,k)]
#define rowptr_(i) rowptr[1 + 1 * i]
#define rowptr__(i) rowptr[1 * i]
#define rowptr_colidx____(i,k) rowptr[1 + 1 * colidx___(i,k)]
#define taskBoundaries_(i) schedule -> taskBoundaries[i]
#define taskBoundaries__(i) schedule -> taskBoundaries[i + 1]
#define threadBoundaries_(i) schedule -> threadBoundaries[i]
#define threadBoundaries__(i) schedule -> threadBoundaries[i + 1]
#include <omp.h>
#include "LevelSchedule.hpp"
#include <stdio.h>
#include <string.h>
//CSR A;

void ilu0RefCHiLL(int m,int *rowptr,int *colidx,int *diagptr,double *values)
{
  int idx;
  volatile int zplanes[256];
  int right;
  int left;
  int num_threads;
  int tid;
  int ub;
  int t12;
  int t10;
  int t8;
  int t6;
  int t4;
  int t2;
  int _t27;
  int _t26;
  int _t25;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  int _t18;
  int _t17;
  int _t16;
  int _t15;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t6;
  int _t5;
  int In_1;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int *perm_inv;
  int *perm;
  int i_;
  class SpMP::LevelSchedule *schedule;
  class SpMP::CSR *A;
  int i;
  int k;
  int j1;
  int j2;
  int c;
  double tmp;
  A = (new SpMP::CSR );
  schedule = (new SpMP::LevelSchedule );
  for (t2 = 0; t2 <= m - 1; t2 += 1) 
    for (t4 = rowptr__(t2); t4 <= diagptr_(t2) - 1; t4 += 1) 
      for (t6 = 0; t6 <= m - 1; t6 += 1) 
        A ->  connect_local (t2,t6);
  schedule ->  constructTaskGraph ( *A);
  perm = schedule -> SpMP::LevelSchedule::origToThreadContPerm;
  perm_inv = schedule -> SpMP::LevelSchedule::threadContToOrigPerm;
#pragma omp parallel  private(tid,num_threads,sc_temp,t4,t8,t12,t6,t10)
{
    tid = omp_get_thread_num();
    num_threads = omp_get_num_threads();
{
      for (t4 = threadBoundaries_(tid); t4 <= threadBoundaries__(tid) - 1; t4 += 1) 
        for (t6 = taskBoundaries_(t4); t6 <= taskBoundaries__(t4) - 1; t6 += 1) {
          for (t8 = rowptr__(t6); t8 <= diagptr_(t6) - 1; t8 += 1) 
            for (t10 = t8 + 1; t10 <= rowptr_(t6) - 1; t10 += 1) {
              if (t10 - 1 <= t8) 
                for (t12 = diagptr_colidx____(t6,t8) + 1; t12 <= rowptr_colidx____(t6,t8) - 1; t12 += 1) 
                  if (t12 - 1 <= diagptr_colidx____(t6,t8)) {
                    values[t8] = (values[t8] / values[diagptr[colidx[t8]]]);
                    break; 
                    values[t10] -= (values[t8] * values[t12]);
                    break; 
                  }
                  else {
                    break; 
                    values[t10] -= (values[t8] * values[t12]);
                    break; 
                  }
              else 
                for (t12 = diagptr_colidx____(t6,t8) + 1; t12 <= rowptr_colidx____(t6,t8) - 1; t12 += 1) {
                  break; 
                  values[t10] -= (values[t8] * values[t12]);
                  break; 
                }
              if (rowptr_colidx____(tid,t4) - 1 <= diagptr_colidx____(t6,t8) && t10 - 1 <= t8) {
                t12 = rowptr_colidx____(tid,t4);
                values[t8] = (values[t8] / values[diagptr[colidx[t8]]]);
              }
            }
#pragma omp barrier 
        }
    }
  }
}

int main()
{
  int rowptr[501UL];
  int diagptr[500UL];
  int colidx[5000UL];
  double values[5000UL];
  ilu0RefCHiLL(500,rowptr,colidx,diagptr,values);
  return 0;
}
