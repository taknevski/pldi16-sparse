#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define colidx_(i,j) colidx[1 * j]
#define colidx___(i,j) colidx[1 * j + 1]
#define rowptr_(i) rowptr[1 * i]
#define rowptr__(i) rowptr[1 * i + 1]
#define rowptr___(i) rowptr[1 * i + 1]
#define rowptr____(i) rowptr[1 * i]
#define BS 2

void forwardSolveRef_(double values[][2UL][2UL],double y[][2UL],const double b[][2UL],double idiag[][2UL][2UL],int *rowptr,int *colidx,int n,double sum[][2UL])
{
  int t8;
  int t6;
  int t4;
  int t2;
  int i;
  int j;
  int ii;
  int jj;
  for (t2 = 0; t2 <= n - 1; t2 += 1) {
    for (t4 = 0; t4 <= 1; t4 += 1) 
      sum[t2][t4] = b[t2][t4];
    for (t4 = rowptr_(t2); t4 <= rowptr__(t2) - 1; t4 += 1) 
      for (t6 = 0; t6 <= 1; t6 += 1) 
        for (t8 = 0; t8 <= 1; t8 += 1) 
//s0
          sum[t2][t8] = (sum[t2][t8] - (values[t4][t6][t8] * y[colidx[t4]][t6]));
    for (t4 = 0; t4 <= 1; t4 += 1) 
//s1
      y[t2][t4] = 0;
    for (t4 = 0; t4 <= 1; t4 += 1) 
      for (t6 = 0; t6 <= 1; t6 += 1) 
//s2
        y[t2][t6] += (idiag[t2][t4][t6] * sum[t2][t4]);
  }
}
