#define BS 2
void forwardSolveRef_(double values[][BS][BS], double y[][BS], const double b[][BS], double idiag[][BS][BS], int *rowptr, int *colidx, int n, double sum[][BS])
{
  int i, j, ii, jj;
  
  for (i = 0; i < n; ++i) {
    for (j = 0; j < BS; ++j) {
      sum[i][j] = b[i][j];
    }
    for (j = rowptr[i]; j < rowptr[i + 1]; ++j) {
      for (jj = 0; jj < BS; ++jj) {
        for (ii = 0; ii < BS; ++ii) {
          sum[i][ii] = sum[i][ii] -  values[j][jj][ii]*y[colidx[j]][jj];//s0
        }
      }
    }

    for (j = 0; j < BS; ++j) {
      y[i][j] = 0;  //s1
    }
    for (j = 0; j < BS; ++j) {
      for (jj = 0; jj < BS; ++jj) {
        y[i][jj] += idiag[i][j][jj]*sum[i][j];//s2
      }
    }
  } // for each row
}

