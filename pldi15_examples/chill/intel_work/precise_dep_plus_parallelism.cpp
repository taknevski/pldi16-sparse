
#include <algorithm>
#include <cmath>
#include <cstring>

#include "../Vector.hpp"
#include "../LevelSchedule.hpp"
#include "../synk/barrier.hpp"

#include "test.hpp"

using namespace SpMP;
//Added by Anand 11/02/2015
#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define colidx_(i,k) colidx[1 * k]

#define rowptr_(i) rowptr[1 * i]
#define rowptr__(i) rowptr[1+1 * i]
#define diagptr_(i) diagptr[1 * i]
#define taskBoundaries_(i) schedule -> taskBoundaries[i]
#define taskBoundaries__(i) schedule -> taskBoundaries[i + 1]
#define threadBoundaries_(i) schedule -> threadBoundaries[i]
#define threadBoundaries__(i) schedule -> threadBoundaries[i + 1]


CSR * test_ilu_dep_precise(CSR *A, double *time) {


	double tBegin = omp_get_wtime();

	//ADJACENCY_LIST *graph = new ADJACENCY_LIST(A->m);

	CSR *gr = new CSR();
	gr->m = A->m;
	gr->rowptr = MALLOC(int, A->m + 1);
	gr->diagptr = MALLOC(int, A->m);
	gr->extptr = NULL;


	//graph->rowptr =  MALLOC(int, A->m + 1);
	//graph->diagptr =  MALLOC(int, A->m);
	//graph->extptr = NULL;
	int m = A->m;
	int *rowptr = A->rowptr;
	int *diagptr = A->diagptr;
	int *count =  MALLOC(int, A->m);
	int *colidx = A->colidx;
	int t2,t4,t6;

	int * col;	
#pragma omp parallel for
	for(int i=0; i < m;i++){
		gr->rowptr[i] =0;
		count[i]=0;
	}
	gr->rowptr[m] = 0;
	if (2 <= m) 
#pragma omp parallel for 
		for(int i_ = 0; i_ < m; i_++){
			for(int k_ = rowptr[i_]; k_ < diagptr[i_]; k_++){  
				bool found = false;
				int i = colidx[k_];
				for(int j1_ = k_ + 1; j1_ < rowptr[i_+1];j1_++){
					for(int j2_ = diagptr[i] + 1; j2_ < rowptr[i +1];j2_++){

						if(colidx[j1_] == colidx[j2_]){            
							for(int k = rowptr[i]; k < diagptr[i]; k++){ 
								for(int j2 = diagptr[colidx[k]] + 1; j2 < rowptr[colidx[k] +1];j2++){     
									int j1 = diagptr[i]; 
									if( rowptr[i] + 1 <= j1 && j1 < rowptr[i+1]) 
										if(colidx[j1] == colidx[j2]){
											found = true;

											count[i_]++;
											break;		 


										}	

									if(found)
										break;								 
									j1 = j2_;                     

									if( rowptr[i] + 1 <= j1 && j1 < rowptr[i+1]) 
										if(colidx[j1] == colidx[j2]){
											found = true;		  
											count[i_]++;
											if(found)
												break;
										}
									if(found)
										break;
								}
								if(found)
									break;

							}//end k
							if(found)
								break;

						}//end if
						if(found)
							break;

					}//end j2_
					if(found)
						break;

				}//end j1_

			}//end k_
		}//end i_
	/*		for (t2 = 0; t2 <= m - 1; t2 += 1) 
			for (t4 = rowptr_(t2); t4 <= diagptr_(t2) - 1; t4 += 1) 
			if (colidx_(t2,t4) + 1<= t2) 
			if (0 <= colidx_(t2,t4)) {
			t6 = colidx_(t2,t4);
			count[t2]++;
	//graph->connect(t6, t2);
	//graph->connect(t2, t6);
	} else {}
	else if (t2 + 1 <= colidx_(t2,t4) && colidx_(t2,t4) + 1 <= m) {
	t6 = colidx_(t2,t4);
//gr->rowptr[t2 + 1]++;	 
count[t2]++; 	
//graph->connect(t2, t6);
//graph->connect(t6, t2);
}
	 */

//#pragma omp single
//{
// FIXME - parallel prefix sum
//for (int i = 0; i < A->m ; ++i) 
//	count[i+1] += count[i];

//#pragma omp parallel for	  
for (int i = 0; i < A->m ; ++i) {

	gr->rowptr[i+1] = gr->rowptr[i] + count[i];
}

#pragma omp parallel for	  
for (int i = 0; i < A->m ; ++i) {

	count[i] = 0;	
}

gr->colidx = MALLOC(int, gr->rowptr[m]);	
//		col = MALLOC(int, gr->rowptr[m]);	

/*		for(int i=0; i < m+ 1; i++)
		if(gr->rowptr[i]!= A->rowptr[i])
		{
		printf("Mismatch at %d gr is %d and A is %d\n", i, gr->rowptr[i], A->rowptr[i]);
//break;
} 
printf("done\n");
 */
/*	if (2 <= m) 
#pragma omp parallel for private(t4,t6)
for (t2 = 0; t2 <= m - 1; t2 += 1){ 
//int count1=0;
for (t4 = rowptr_(t2); t4 <= diagptr_(t2) - 1; t4 += 1) 
if (colidx_(t2,t4) + 1 <= t2) 
if (0 <= colidx_(t2,t4)) {
t6 = colidx_(t2,t4);
//int a[1];
//a[0] = t6;	
if(find(gr->colidx + gr->rowptr[t2], gr->colidx+ gr->rowptr[t2] + count[t2], t6) == gr->colidx + gr->rowptr[t2] + count[t2])	
gr->colidx[gr->rowptr[t2] + count[t2]++] = t6;
//else
//	count[t2]--;	


//gr->rowptr[t2+1]++;
//graph->connect(t6, t2);
//graph->connect(t2, t6);
} else {}
else if (t2 + 1 <= colidx_(t2,t4) && colidx_(t2,t4) + 1 <= m) {
t6 = colidx_(t2,t4);
//int a[1];
//a[0] = t6;	

if(find(gr->colidx + gr->rowptr[t2], gr->colidx + gr->rowptr[t2] + count[t2], t6) == gr->colidx + gr->rowptr[t2] + count[t2])	
gr->colidx[gr->rowptr[t2] + count[t2]++] = t6;
//else
//		count[t2]--;	


//gr->rowptr[t2 + 1]++;   

//graph->connect(t2, t6);
//graph->connect(t6, t2);
}

}
 */

if(2 <= m)
#pragma omp parallel for 
	for(int i_ = 0; i_ < m; i_++){
		for(int k_ = rowptr[i_]; k_ < diagptr[i_]; k_++){
			bool found = false;  
			int i = colidx[k_];
			for(int j1_ = k_ + 1; j1_ < rowptr[i_+1];j1_++){
				for(int j2_ = diagptr[i] + 1; j2_ < rowptr[i +1];j2_++){
					if(colidx[j1_] == colidx[j2_]){            
						for(int k = rowptr[i]; k < diagptr[i]; k++){ 
							for(int j2 = diagptr[colidx[k]] + 1; j2 < rowptr[colidx[k] +1];j2++){     
								int j1 = diagptr[i]; 
								if( rowptr[i] + 1 <= j1 && j1 < rowptr[i+1]) 
									if(colidx[j1] == colidx[j2])
										if(find(gr->colidx + gr->rowptr[i_], gr->colidx+ gr->rowptr[i_] + count[i_], i) == gr->colidx + gr->rowptr[i_] + count[i_]){	
											found = true;	
											gr->colidx[gr->rowptr[i_] + count[i_]++] = i;
											break;
										}
								if(found)
									break;
								j1 = j2_;                                                                                                           
								if( rowptr[i] + 1 <= j1 && j1 < rowptr[i+1]) 
									if(colidx[j1] == colidx[j2])
										if(find(gr->colidx + gr->rowptr[i_], gr->colidx+ gr->rowptr[i_] + count[i_], i) == gr->colidx + gr->rowptr[i_] + count[i_]){	
											found = true;	
											gr->colidx[gr->rowptr[i_] + count[i_]++] = i;
											break;	
										}
								if(found)
									break;

							}//end j2
							if(found)
								break;

						}//end k
						if(found)
							break;

					}//end if
					if(found)
						break;

				}//end j2_
				if(found)
					break;

			}//end j1_
		}//end k_
	}//end i_



int sum = 0;

for(int i=0; i < m; i++)
sum += count[i];


col = MALLOC(int, sum);	

// forward direction
int source_offset=0;
for (int i = 0; i < m; ++i) {
	memcpy(
			col + gr->rowptr[i], gr->colidx + source_offset,
			count[i]*sizeof(int));
	source_offset = gr->rowptr[i+1];
	gr->rowptr[i+1] = gr->rowptr[i] + count[i]; 

}

/*		for(int i=0; i < m+ 1; i++)
		if(gr->rowptr[i]!= A->rowptr[i])
		{
		printf("Mismatch at %d gr is %d and A is %d\n", i, gr->rowptr[i], A->rowptr[i]);
		break;
		} 
		printf("done\n");
 */

FREE(gr->colidx);
gr->colidx = col;		

#pragma omp parallel for
for (int i = 0; i < m; ++i) 
sort(gr->colidx + gr->rowptr[i], gr->colidx + gr->rowptr[i+1]);

/*for(int i=0; i < 10; i++){
  if(gr->rowptr[i] != A->rowptr[i]){
  printf("error at %d gr->rowptr[%d] is %d and A->rowptr%d] is %d \n",i, i, gr->rowptr[i], i ,A->rowptr[i] );
  break;		
  }
  int j;
  for(j = gr->rowptr[i]; j < gr->rowptr[i+1]; j++)
  {
//if(gr->colidx[j] != A->colidx[j])
printf("At %d and j is %d gr->colidx[%d] is %d\n", i, j, j , gr->colidx[j]);
//break;	
}		
//if(j != gr->rowptr[i+1])
//break;	
//if(i==0)
//break;	
}
//if(gr->rowptr[m] != A->rowptr[m])
// printf("error at %d\n", m);

y	
//printf("done\n");
 */


int *count2 = MALLOC(int, m);
#pragma omp parallel for
for(int i=0; i < m ; i++)
count2[i] = 0;	

#pragma omp parallel for
for (int i = 0; i < m; i++) {
	if (!binary_search(gr->colidx + gr->rowptr[i], gr->colidx + gr->rowptr[i+1], i)) {
		// for each (i, c), add (c, i)
		__sync_fetch_and_add(count2 + i, 1);
	}


	for (int j = gr->rowptr[i]; j < gr->rowptr[i+1]; ++j) {
		int c = gr->colidx[j];
		// assume colidx is sorted
		if (!binary_search(gr->colidx + gr->rowptr[c], gr->colidx + gr->rowptr[c+1], i)) {
			// for each (i, c), add (c, i)
			__sync_fetch_and_add(count2 + c, 1);
		}
	}
}

// FIXME - parallel prefix sum
//  for (int i = 0; i < A->m ; ++i) {
//   rowptr[i + 1] = rowptr[i] + count[i] + count2[i];
// }


//values = new double[rowptr[A->m]];            
//diagptr = MALLOC(int, A->m);
sum =0;
for(int i=0; i < m; i++)
sum += count2[i];

int *col2  = MALLOC(int, gr->rowptr[m] + sum);             


// construct symColIdx

// forward direction
source_offset=0;	
for (int i = 0; i < m; ++i) {
	//count[i] = A->rowptr[i+1] - A->rowptr[i];
	memcpy(
			col2 + gr->rowptr[i], gr->colidx + source_offset,
			(count[i] + count2[i])*sizeof(int));
	int tmp =  count2[i];
	count2[i] =  gr->rowptr[i+1] - source_offset;   
	source_offset = gr->rowptr[i+1];
	gr->rowptr[i+1] = gr->rowptr[i] + count[i] + tmp;
	//count2[i] = 0;
}
FREE(gr->colidx);
gr->colidx = col2;
#pragma omp parallel for
for (int i = 0; i < m; i++) {

	if (!binary_search(gr->colidx + gr->rowptr[i], gr->colidx + gr->rowptr[i] +  count2[i], i)) {
		// for each (i, c), add (c, i)
		int cnt = __sync_fetch_and_add(count + i, 1);
		gr->colidx[gr->rowptr[i] + cnt] = i;

		// for each (i, c), add (c, i)
		//__sync_fetch_and_add(count2 + c, 1);
	}
	for (int j = gr->rowptr[i]; j < gr->rowptr[i] + count2[i]; ++j) {
		int c = gr->colidx[j];
		// assume colidx is sorted
		if (!binary_search(gr->colidx + gr->rowptr[c], gr->colidx + gr->rowptr[c] +  count2[c], i)) {
			// for each (i, c), add (c, i)
			int cnt = __sync_fetch_and_add(count + c, 1);
			gr->colidx[gr->rowptr[c] + cnt] = i;

			// for each (i, c), add (c, i)
			//__sync_fetch_and_add(count2 + c, 1);
		}
	}
}

#pragma omp parallel for
for (int i = 0; i < m; ++i) {
	sort(gr->colidx + gr->rowptr[i], gr->colidx + gr->rowptr[i+1] );
	for (int j = gr->rowptr[i]; j < gr->rowptr[i + 1]; ++j) {
		if (gr->colidx[j] == i) {gr->diagptr[i] = j;}
	}
}


FREE(count);
FREE(count2);

double tEnd = omp_get_wtime();
//graph->construct_CSR();
//CSR *b = new CSR(m,m, graph->rowptr, graph->colidx, graph->values);

//b->diagptr = graph->diagptr;

*time = tEnd - tBegin;

/*	for(int i=0; i < m; i++){
	if(gr->rowptr[i] != A->rowptr[i]){
	printf("error at %d gr->rowptr[%d] is %d and A->rowptr%d] is %d \n",i, i, gr->rowptr[i], i ,A->rowptr[i] );
	break;		
	}
	int j;
	for(j = gr->rowptr[i]; j < gr->rowptr[i+1]; j++)
	{
	if(gr->colidx[j] != A->colidx[j])
	printf("error at %d and j is %d gr->colidx[%d] is %d and A->colidx[%d] is %d\n", i, j, j , gr->colidx[j],j,  A->colidx[j]);
	break;	
	}		
//if(j != gr->rowptr[i+1])
//break;	
//if(i==0)
//break;	
}
if(gr->rowptr[m] != A->rowptr[m])
printf("error at %d\n", m);


printf("done\n");
 */


return gr;
}


















CSR * test_gs_dep(CSR *A, double *time) {


	double tBegin = omp_get_wtime();

//	ADJACENCY_LIST *graph = new ADJACENCY_LIST(A->m);

	CSR *gr = new CSR();
	gr->m = A->m;
	gr->rowptr = MALLOC(int, A->m + 1);
	gr->diagptr = MALLOC(int, A->m);
	gr->extptr = NULL;


	//graph->rowptr =  MALLOC(int, A->m + 1);
	//graph->diagptr =  MALLOC(int, A->m);
	//graph->extptr = NULL;
	int m = A->m;
	int *rowptr = A->rowptr;
	int *diagptr = A->diagptr;
	int *count =  MALLOC(int, A->m);
	int *colidx = A->colidx;
	int t2,t4,t6;

	int * col;	
#pragma omp parallel for
	for(int i=0; i < m;i++){
		gr->rowptr[i] =0;
		count[i]=0;
	}
	gr->rowptr[m] = 0;
	if (2 <= m) 
#pragma omp parallel for private(t4,t6)
		for (t2 = 0; t2 <= m - 1; t2 += 1) 
			for (t4 = rowptr_(t2); t4 <= rowptr__(t2) - 1; t4 += 1) 
				if (colidx_(t2,t4) + 1 <= t2) 
					if (0 <= colidx_(t2,t4)) {
						t6 = colidx_(t2,t4);
						count[t2]++;
						//graph->connect(t6, t2);
						//graph->connect(t2, t6);
					} else {}
					else if (t2 + 1 <= colidx_(t2,t4) && colidx_(t2,t4) + 1 <= m) {
						t6 = colidx_(t2,t4);
						//gr->rowptr[t2 + 1]++;	 
						count[t2]++; 	
						//graph->connect(t2, t6);
						//graph->connect(t6, t2);
					}


	//#pragma omp single
	//{
	// FIXME - parallel prefix sum
	//for (int i = 0; i < A->m ; ++i) 
	//	count[i+1] += count[i];

	//#pragma omp parallel for	  
	for (int i = 0; i < A->m ; ++i) {

		gr->rowptr[i+1] = gr->rowptr[i] + count[i];
	}

#pragma omp parallel for	  
	for (int i = 0; i < A->m ; ++i) {

		count[i] = 0;	
	}

	gr->colidx = MALLOC(int, gr->rowptr[m]);	
	//		col = MALLOC(int, gr->rowptr[m]);	

	/*		for(int i=0; i < m+ 1; i++)
			if(gr->rowptr[i]!= A->rowptr[i])
			{
			printf("Mismatch at %d gr is %d and A is %d\n", i, gr->rowptr[i], A->rowptr[i]);
			break;
			} 
			printf("done\n");
	 */
	if (2 <= m) 
#pragma omp parallel for private(t4,t6)
		for (t2 = 0; t2 <= m - 1; t2 += 1){ 
			//int count1=0;
			for (t4 = rowptr_(t2); t4 <= rowptr__(t2) - 1; t4 += 1) 
				if (colidx_(t2,t4) + 1 <= t2) 
					if (0 <= colidx_(t2,t4)) {
						t6 = colidx_(t2,t4);
						//int a[1];
						//a[0] = t6;	
						if(find(gr->colidx + gr->rowptr[t2], gr->colidx+ gr->rowptr[t2] + count[t2], t6) == gr->colidx + gr->rowptr[t2] + count[t2])	
							gr->colidx[gr->rowptr[t2] + count[t2]++] = t6;
						//else
						//	count[t2]--;	


						//gr->rowptr[t2+1]++;
						//graph->connect(t6, t2);
						//graph->connect(t2, t6);
					} else {}
					else if (t2 + 1 <= colidx_(t2,t4) && colidx_(t2,t4) + 1 <= m) {
						t6 = colidx_(t2,t4);
						//int a[1];
						//a[0] = t6;	

						if(find(gr->colidx + gr->rowptr[t2], gr->colidx + gr->rowptr[t2] + count[t2], t6) == gr->colidx + gr->rowptr[t2] + count[t2])	
							gr->colidx[gr->rowptr[t2] + count[t2]++] = t6;
						//else
						//		count[t2]--;	


						//gr->rowptr[t2 + 1]++;   

						//graph->connect(t2, t6);
						//graph->connect(t6, t2);
					}

		}



	int sum = 0;

	for(int i=0; i < m; i++)
		sum += count[i];


	col = MALLOC(int, sum);	

	// forward direction
	int source_offset=0;
	for (int i = 0; i < m; ++i) {
		memcpy(
				col + gr->rowptr[i], gr->colidx + source_offset,
				count[i]*sizeof(int));
		source_offset = gr->rowptr[i+1];
		gr->rowptr[i+1] = gr->rowptr[i] + count[i]; 

	}

	/*		for(int i=0; i < m+ 1; i++)
			if(gr->rowptr[i]!= A->rowptr[i])
			{
			printf("Mismatch at %d gr is %d and A is %d\n", i, gr->rowptr[i], A->rowptr[i]);
			break;
			} 
			printf("done\n");
	 */

	FREE(gr->colidx);
	gr->colidx = col;		
         
       #pragma omp parallel for
	for (int i = 0; i < m; ++i) 
		sort(gr->colidx + gr->rowptr[i], gr->colidx + gr->rowptr[i] + count[i]);




	int *count2 = MALLOC(int, m);
#pragma omp parallel for
	for(int i=0; i < m ; i++)
		count2[i] = 0;	

#pragma omp parallel for
	for (int i = 0; i < m; i++) {
			if (!binary_search(gr->colidx + gr->rowptr[i], gr->colidx + gr->rowptr[i+1], i)) {
				// for each (i, c), add (c, i)
				__sync_fetch_and_add(count2 + i, 1);
			}

		for (int j = gr->rowptr[i]; j < gr->rowptr[i+1]; ++j) {
			int c = gr->colidx[j];
			// assume colidx is sorted
			if (!binary_search(gr->colidx + gr->rowptr[c], gr->colidx + gr->rowptr[c+1], i)) {
				// for each (i, c), add (c, i)
				__sync_fetch_and_add(count2 + c, 1);
			}
		}
	}

	// FIXME - parallel prefix sum
	//  for (int i = 0; i < A->m ; ++i) {
	//   rowptr[i + 1] = rowptr[i] + count[i] + count2[i];
	// }


	//values = new double[rowptr[A->m]];            
	//diagptr = MALLOC(int, A->m);
	sum =0;
	for(int i=0; i < m; i++)
		sum += count2[i];

	int *col2  = MALLOC(int, gr->rowptr[m] + sum);             


	// construct symColIdx

	source_offset=0;	

        for (int i = 0; i < m; ++i) {
                //count[i] = A->rowptr[i+1] - A->rowptr[i];
                memcpy(
                                col2 + gr->rowptr[i], gr->colidx + source_offset,
                                (count[i] + count2[i])*sizeof(int));
                int tmp =  count2[i];
                count2[i] =  gr->rowptr[i+1] - source_offset;   
                source_offset = gr->rowptr[i+1];
                gr->rowptr[i+1] = gr->rowptr[i] + count[i] + tmp;
                //count2[i] = 0;
        }
        FREE(gr->colidx);
        gr->colidx = col2;
#pragma omp parallel for
        for (int i = 0; i < m; i++) {

          if (!binary_search(gr->colidx + gr->rowptr[i], gr->colidx + gr->rowptr[i] +  count2[i], i)) {
                                // for each (i, c), add (c, i)
                                int cnt = __sync_fetch_and_add(count + i, 1);
                                gr->colidx[gr->rowptr[i] + cnt] = i;

                                // for each (i, c), add (c, i)
                                //__sync_fetch_and_add(count2 + c, 1);
                        }
                for (int j = gr->rowptr[i]; j < gr->rowptr[i] + count2[i]; ++j) {
                        int c = gr->colidx[j];
                        // assume colidx is sorted
                        if (!binary_search(gr->colidx + gr->rowptr[c], gr->colidx + gr->rowptr[c] +  count2[c], i)) {
                                // for each (i, c), add (c, i)
                                int cnt = __sync_fetch_and_add(count + c, 1);
                                gr->colidx[gr->rowptr[c] + cnt] = i;

                                // for each (i, c), add (c, i)
                                //__sync_fetch_and_add(count2 + c, 1);
                        }
                }
        }

#pragma omp parallel for
	for (int i = 0; i < m; ++i) {
		sort(gr->colidx + gr->rowptr[i], gr->colidx + gr->rowptr[i+1]);
		for (int j = gr->rowptr[i]; j < gr->rowptr[i + 1]; ++j) {
			if (gr->colidx[j] == i) {gr->diagptr[i] = j;}
		}
	}


	FREE(count);
	FREE(count2);

	double tEnd = omp_get_wtime();
	//graph->construct_CSR();
	//CSR *b = new CSR(m,m, graph->rowptr, graph->colidx, graph->values);

	//b->diagptr = graph->diagptr;

	*time = tEnd - tBegin;

			for(int i=0; i < m; i++){
			if(gr->rowptr[i] != A->rowptr[i]){
			   printf("error \n");
			   break;		
			}
			int j;
			for(j = gr->rowptr[i]; j < gr->rowptr[i+1]; j++)
			{
			if(gr->colidx[j] != A->colidx[j])
			printf("error \n");
			break;	
			}		
			if(j != gr->rowptr[i+1])
			break;		
			}
			if(gr->rowptr[m] != A->rowptr[m])
			 printf("error \n");

	
			//printf("done\n");
	 


	return gr;
}
CSR * test_ilu_dep(CSR *A, double *time) {


	double tBegin = omp_get_wtime();

	//ADJACENCY_LIST *graph = new ADJACENCY_LIST(A->m);

	CSR *gr = new CSR();
	gr->m = A->m;
	gr->rowptr = MALLOC(int, A->m + 1);
	gr->diagptr = MALLOC(int, A->m);
	gr->extptr = NULL;


	//graph->rowptr =  MALLOC(int, A->m + 1);
	//graph->diagptr =  MALLOC(int, A->m);
	//graph->extptr = NULL;
	int m = A->m;
	int *rowptr = A->rowptr;
	int *diagptr = A->diagptr;
	int *count =  MALLOC(int, A->m);
	int *colidx = A->colidx;
	int t2,t4,t6;

	int * col;	
#pragma omp parallel for
	for(int i=0; i < m;i++){
		gr->rowptr[i] =0;
		count[i]=0;
	}
	gr->rowptr[m] = 0;
	if (2 <= m) 
#pragma omp parallel for private(t4,t6)
		for (t2 = 0; t2 <= m - 1; t2 += 1) 
			for (t4 = rowptr_(t2); t4 <= diagptr_(t2) - 1; t4 += 1) 
				if (colidx_(t2,t4) + 1<= t2) 
					if (0 <= colidx_(t2,t4)) {
						t6 = colidx_(t2,t4);
						count[t2]++;
						//graph->connect(t6, t2);
						//graph->connect(t2, t6);
					} else {}
					else if (t2 + 1 <= colidx_(t2,t4) && colidx_(t2,t4) + 1 <= m) {
						t6 = colidx_(t2,t4);
						//gr->rowptr[t2 + 1]++;	 
						count[t2]++; 	
						//graph->connect(t2, t6);
						//graph->connect(t6, t2);
					}


	//#pragma omp single
	//{
	// FIXME - parallel prefix sum
	//for (int i = 0; i < A->m ; ++i) 
	//	count[i+1] += count[i];

	//#pragma omp parallel for	  
	for (int i = 0; i < A->m ; ++i) {

		gr->rowptr[i+1] = gr->rowptr[i] + count[i];
	}

#pragma omp parallel for	  
	for (int i = 0; i < A->m ; ++i) {

		count[i] = 0;	
	}

	gr->colidx = MALLOC(int, gr->rowptr[m]);	
	//		col = MALLOC(int, gr->rowptr[m]);	

	/*		for(int i=0; i < m+ 1; i++)
			if(gr->rowptr[i]!= A->rowptr[i])
			{
			printf("Mismatch at %d gr is %d and A is %d\n", i, gr->rowptr[i], A->rowptr[i]);
			break;
			} 
			printf("done\n");
	 */
	if (2 <= m) 
#pragma omp parallel for private(t4,t6)
		for (t2 = 0; t2 <= m - 1; t2 += 1){ 
			//int count1=0;
			for (t4 = rowptr_(t2); t4 <= diagptr_(t2) - 1; t4 += 1) 
				if (colidx_(t2,t4) + 1 <= t2) 
					if (0 <= colidx_(t2,t4)) {
						t6 = colidx_(t2,t4);
						//int a[1];
						//a[0] = t6;	
						if(find(gr->colidx + gr->rowptr[t2], gr->colidx+ gr->rowptr[t2] + count[t2], t6) == gr->colidx + gr->rowptr[t2] + count[t2])	
							gr->colidx[gr->rowptr[t2] + count[t2]++] = t6;
						//else
						//	count[t2]--;	


						//gr->rowptr[t2+1]++;
						//graph->connect(t6, t2);
						//graph->connect(t2, t6);
					} else {}
					else if (t2 + 1 <= colidx_(t2,t4) && colidx_(t2,t4) + 1 <= m) {
						t6 = colidx_(t2,t4);
						//int a[1];
						//a[0] = t6;	

						if(find(gr->colidx + gr->rowptr[t2], gr->colidx + gr->rowptr[t2] + count[t2], t6) == gr->colidx + gr->rowptr[t2] + count[t2])	
							gr->colidx[gr->rowptr[t2] + count[t2]++] = t6;
						//else
						//		count[t2]--;	


						//gr->rowptr[t2 + 1]++;   

						//graph->connect(t2, t6);
						//graph->connect(t6, t2);
					}

		}



	int sum = 0;

	for(int i=0; i < m; i++)
		sum += count[i];


	col = MALLOC(int, sum);	

	// forward direction
	int source_offset=0;
	for (int i = 0; i < m; ++i) {
		memcpy(
				col + gr->rowptr[i], gr->colidx + source_offset,
				count[i]*sizeof(int));
		source_offset = gr->rowptr[i+1];
		gr->rowptr[i+1] = gr->rowptr[i] + count[i]; 

	}

	/*		for(int i=0; i < m+ 1; i++)
			if(gr->rowptr[i]!= A->rowptr[i])
			{
			printf("Mismatch at %d gr is %d and A is %d\n", i, gr->rowptr[i], A->rowptr[i]);
			break;
			} 
			printf("done\n");
	 */

	FREE(gr->colidx);
	gr->colidx = col;		
         
       #pragma omp parallel for
	for (int i = 0; i < m; ++i) 
		sort(gr->colidx + gr->rowptr[i], gr->colidx + gr->rowptr[i+1]);

			/*for(int i=0; i < 10; i++){
			if(gr->rowptr[i] != A->rowptr[i]){
			   printf("error at %d gr->rowptr[%d] is %d and A->rowptr%d] is %d \n",i, i, gr->rowptr[i], i ,A->rowptr[i] );
			   break;		
			}
			int j;
			for(j = gr->rowptr[i]; j < gr->rowptr[i+1]; j++)
			{
			//if(gr->colidx[j] != A->colidx[j])
			printf("At %d and j is %d gr->colidx[%d] is %d\n", i, j, j , gr->colidx[j]);
			//break;	
			}		
			//if(j != gr->rowptr[i+1])
			//break;	
			//if(i==0)
			//break;	
			}
			//if(gr->rowptr[m] != A->rowptr[m])
			// printf("error at %d\n", m);

	
			//printf("done\n");
			*/


	int *count2 = MALLOC(int, m);
#pragma omp parallel for
	for(int i=0; i < m ; i++)
		count2[i] = 0;	

#pragma omp parallel for
	for (int i = 0; i < m; i++) {
	        if (!binary_search(gr->colidx + gr->rowptr[i], gr->colidx + gr->rowptr[i+1], i)) {
				// for each (i, c), add (c, i)
				__sync_fetch_and_add(count2 + i, 1);
			}


		for (int j = gr->rowptr[i]; j < gr->rowptr[i+1]; ++j) {
			int c = gr->colidx[j];
			// assume colidx is sorted
			if (!binary_search(gr->colidx + gr->rowptr[c], gr->colidx + gr->rowptr[c+1], i)) {
				// for each (i, c), add (c, i)
				__sync_fetch_and_add(count2 + c, 1);
			}
		}
	}

	// FIXME - parallel prefix sum
	//  for (int i = 0; i < A->m ; ++i) {
	//   rowptr[i + 1] = rowptr[i] + count[i] + count2[i];
	// }


	//values = new double[rowptr[A->m]];            
	//diagptr = MALLOC(int, A->m);
	sum =0;
	for(int i=0; i < m; i++)
		sum += count2[i];

	int *col2  = MALLOC(int, gr->rowptr[m] + sum);             


	// construct symColIdx

	// forward direction
	source_offset=0;	
	for (int i = 0; i < m; ++i) {
		//count[i] = A->rowptr[i+1] - A->rowptr[i];
		memcpy(
				col2 + gr->rowptr[i], gr->colidx + source_offset,
				(count[i] + count2[i])*sizeof(int));
		int tmp =  count2[i];
		count2[i] =  gr->rowptr[i+1] - source_offset;   
                source_offset = gr->rowptr[i+1];
		gr->rowptr[i+1] = gr->rowptr[i] + count[i] + tmp;
		//count2[i] = 0;
	}
	FREE(gr->colidx);
	gr->colidx = col2;
#pragma omp parallel for
	for (int i = 0; i < m; i++) {

          if (!binary_search(gr->colidx + gr->rowptr[i], gr->colidx + gr->rowptr[i] +  count2[i], i)) {
                                // for each (i, c), add (c, i)
                                int cnt = __sync_fetch_and_add(count + i, 1);
                                gr->colidx[gr->rowptr[i] + cnt] = i;

                                // for each (i, c), add (c, i)
                                //__sync_fetch_and_add(count2 + c, 1);
                        }
		for (int j = gr->rowptr[i]; j < gr->rowptr[i] + count2[i]; ++j) {
			int c = gr->colidx[j];
			// assume colidx is sorted
			if (!binary_search(gr->colidx + gr->rowptr[c], gr->colidx + gr->rowptr[c] +  count2[c], i)) {
				// for each (i, c), add (c, i)
				int cnt = __sync_fetch_and_add(count + c, 1);
				gr->colidx[gr->rowptr[c] + cnt] = i;

				// for each (i, c), add (c, i)
				//__sync_fetch_and_add(count2 + c, 1);
			}
		}
	}

#pragma omp parallel for
	for (int i = 0; i < m; ++i) {
		sort(gr->colidx + gr->rowptr[i], gr->colidx + gr->rowptr[i+1] );
		for (int j = gr->rowptr[i]; j < gr->rowptr[i + 1]; ++j) {
			if (gr->colidx[j] == i) {gr->diagptr[i] = j;}
		}
	}


	FREE(count);
	FREE(count2);

	double tEnd = omp_get_wtime();
	//graph->construct_CSR();
	//CSR *b = new CSR(m,m, graph->rowptr, graph->colidx, graph->values);

	//b->diagptr = graph->diagptr;

	*time = tEnd - tBegin;

		/*	for(int i=0; i < m; i++){
			if(gr->rowptr[i] != A->rowptr[i]){
			   printf("error at %d gr->rowptr[%d] is %d and A->rowptr%d] is %d \n",i, i, gr->rowptr[i], i ,A->rowptr[i] );
			   break;		
			}
			int j;
			for(j = gr->rowptr[i]; j < gr->rowptr[i+1]; j++)
			{
			if(gr->colidx[j] != A->colidx[j])
			printf("error at %d and j is %d gr->colidx[%d] is %d and A->colidx[%d] is %d\n", i, j, j , gr->colidx[j],j,  A->colidx[j]);
			  break;	
			}		
			//if(j != gr->rowptr[i+1])
			//break;	
			//if(i==0)
			//break;	
			}
			if(gr->rowptr[m] != A->rowptr[m])
			 printf("error at %d\n", m);

	
			printf("done\n");
	 	*/


	return gr;
}





CSR * test_ilu0_dep_opt(CSR *A, double *time) {

        // j1 == j2                                                                                                                                                    

double tBegin = omp_get_wtime();

//int **symRowPtr, **symDiagPtr, **symExtPtr, **symColIdx;





//getSymmetricNnzPattern(A, symRowPtr, symDiagPtr, symExtPtr, symColIdx);



//#define PRINT_TIME_BREAKDOWN


//double tBegin = omp_get_wtime();

        //cout<<B->m<<endl;
int *count = new int[A->m + 1];
int *rowptr = MALLOC(int, A->m + 1);
int *colidx;
int *diagptr;
double *values;
rowptr[0] = 0;
#pragma omp parallel
  {
    int tid = omp_get_thread_num();

    int iBegin, iEnd;
    getSimpleThreadPartition(&iBegin, &iEnd, A->m);
    // construct symRowPtr
    for (int i = iBegin; i < iEnd; ++i) {
      rowptr[i + 1] = A->rowptr[i + 1] - A->rowptr[i];
    }
#pragma omp barrier

    for (int i = iBegin; i < iEnd; ++i) {
      for (int j = A->rowptr[i]; j < A->rowptr[i+1]; ++j) {
        int c = A->colidx[j];
        // assume colidx is sorted
        if (c > i  && !binary_search(A->colidx + A->rowptr[c], A->colidx + A->rowptr[c+1], i)) {
          // for each (i, c), add (c, i)
          __sync_fetch_and_add(rowptr + c + 1, 1);
        }
      }
    }

#pragma omp single
      {
        // FIXME - parallel prefix sum
        for (int i = 0; i < A->m ; ++i) {
          rowptr[i + 1] += rowptr[i];
        }

	//values = new double[rowptr[A->m]];	        
	colidx  = MALLOC(int,rowptr[A->m]);	        
	diagptr = MALLOC(int, A->m);

      }

      // construct symColIdx

      // forward direction
      for (int i = iBegin; i < iEnd; ++i) {
        count[i] = A->rowptr[i+1] - A->rowptr[i];
        memcpy(
          colidx + rowptr[i], A->colidx + A->rowptr[i],
          count[i]*sizeof(int));
      }
#pragma omp barrier

      // backward direction
      for (int i = iBegin; i < iEnd; ++i) {
        for (int j = A->rowptr[i]; j < A->rowptr[i+1]; ++j) {
          int c = colidx[j];
          if (c > i  && !binary_search(A->colidx + A->rowptr[c], A->colidx + A->rowptr[c+1], i)) {
            // for each (i, c), add (c, i)
            int cnt = __sync_fetch_and_add(count + c, 1);
            colidx[rowptr[c] + cnt] = i;
          }
        }
      }
#pragma omp barrier


      // sort colidx and copy remote
      for (int i = iBegin; i < iEnd; ++i) {
        sort(colidx + rowptr[i], colidx + rowptr[i] + count[i]);
        for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
          if (colidx[j] <= i) {diagptr[i] = j;}
        }
      }
  }

CSR *B = new CSR(A->m, A->n,rowptr, colidx, NULL);

B->diagptr = diagptr;
delete[] count;
//return B;

        

//initialize rowptrs
/*
//#pragma omp parallel for 
        for (int i = 0; i <= A->m; i++) {
                rowptr[i] = 0;
                count[i] = 0;
        }

////first record number of nonzeros per row
//#pragma omp parallel for 
        for (int i = 0; i < A->m; i++)
                for (int k = A->rowptr[i]; k < A->rowptr[i + 1]; k++)
                        if (0 <= A->colidx[k] && A->colidx[k] < A->m){

                        	//if(A->colidx[k] <=  i)		
			          rowptr[i+1]++;
				if(A->colidx[k] > i && !binary_search(A->colidx + A->rowptr[A->colidx[k]], A->colidx + A->rowptr[A->colidx[k] + 1], i) )
				  rowptr[A->colidx[k]+1]++; 					
		        }

        for (int i = 1; i <= A->m; i++)
                rowptr[i] += rowptr[i - 1];

        double tEnd = omp_get_wtime();
     
        CSR *B = new CSR();
        B->alloc(A->m, rowptr[A->m]);

        for (int i = 0; i <= A->m; i++)
                B->rowptr[i] =  rowptr[i];

        delete[] rowptr;
        //#pragma omp parallel for 

        double tBegin2 = omp_get_wtime();

        for (int i2 = 0; i2 < A->m; i2++){
                for (int k2 = A->rowptr[i2]; k2 < A->rowptr[i2 + 1]; k2++){
                        if (0 <= A->colidx[k2] && A->colidx[k2] < A->m) {

                        	//if(A->colidx[k] <=  i){		
				 B->values[B->rowptr[i2] + count[i2]] = 1.0;
				 B->colidx[B->rowptr[i2] + count[i2]++] = A->colidx[k2];
			///	}else{
				if(A->colidx[k2] > i2 && !binary_search(A->colidx + A->rowptr[A->colidx[k2]], A->colidx + A->rowptr[A->colidx[k2] + 1], i2)){ 
				 B->values[B->rowptr[A->colidx[k2]] + count[A->colidx[k2]]] = 1.0;
				 B->colidx[B->rowptr[A->colidx[k2]] + count[A->colidx[k2]]++] = i2;
				}  
                        }
		}
	       sort(B->colidx +  B->rowptr[i2], B->colidx + B->rowptr[i2] + count[i2]);
	}	

        double tEnd2 = omp_get_wtime();
        //printf("Time taken for optimized inspector %lfs\n",tEnd2 - tBegin2 + tEnd - tBegin);                  
        *time = tEnd2 - tBegin2 + tEnd - tBegin;
        delete[] count;
        
      for (int i = 0; i < B->m; i++)
                for (int j = B->rowptr[i]; j < B->rowptr[i + 1]; j++) {
                        if (B->colidx[j] >= i) {
                                B->diagptr[i] = j;
                                break;
                        }
                }


 CSR *B = new CSR(A->m, A->n, *symRowPtr, *symColIdx, NULL);
 B->diagptr = *symDiagPtr;	
 B->extptr = *symExtPtr;
 */
 double tEnd = omp_get_wtime();
 *time = tEnd - tBegin;
 return B;
}

/**
 * Reference sequential sparse triangular solver
 */

void forwardSolveRef(const CSR& A, double y[], const double b[]) {
	ADJUST_FOR_BASE;

	for (int i = base; i < A.m + base; ++i) {
		double sum = b[i];
		for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
			sum -= values[j] * y[colidx[j]];
		}
		y[i] = sum * idiag[i];
	} // for each row
}

void backwardSolveRef(const CSR& A, double y[], const double b[]) {
	ADJUST_FOR_BASE;

	for (int i = A.m - 1 + base; i >= base; --i) {
		double sum = b[i];
		for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
			sum -= values[j] * y[colidx[j]];
		}
		y[i] = sum * idiag[i];
	} // for each row
}

/**
 * Forward sparse triangular solver parallelized with level scheduling
 * and point-to-point synchronization
 */
void forwardSolveWithReorderedMatrix(const CSR& A, double y[], const double b[],
		const LevelSchedule *schedule) {
	ADJUST_FOR_BASE;

/*
#pragma omp parallel
	{
		int nthreads = omp_get_num_threads();
		int tid = omp_get_thread_num();

		const int ntasks = schedule.ntasks;
		const short *nparents = schedule.nparentsForward;
		const vector<int>& threadBoundaries = schedule.threadBoundaries;
		const vector<int>& taskBoundaries = schedule.taskBoundaries;

		int nPerThread = (ntasks + nthreads - 1) / nthreads;
		int nBegin = min(nPerThread * tid, ntasks);
		int nEnd = min(nBegin + nPerThread, ntasks);

		volatile int *taskFinished = schedule.taskFinished;
		int **parents = schedule.parentsForward;

		memset((char *) (taskFinished + nBegin), 0,
				(nEnd - nBegin) * sizeof(int));

		synk::Barrier::getInstance()->wait(tid);

		for (int task = threadBoundaries[tid]; task < threadBoundaries[tid + 1];
				++task) {
			SPMP_LEVEL_SCHEDULE_WAIT;

			for (int i = taskBoundaries[task] + base;
					i < taskBoundaries[task + 1] + base; ++i) {
				double sum = b[i];
				for (int j = rowptr[i]; j < rowptr[i + 1]; ++j) {
					sum -= values[j] * y[colidx[j]];
				}
				y[i] = sum * idiag[i];
			}

			SPMP_LEVEL_SCHEDULE_NOTIFY;
		} // for each task
	} // omp parallel
*/
int t4,t6,t8;
double sum;
#pragma omp parallel  private(t4,t8,t6, sum)  //pragma needs some fixing
{
    //fix declaration to be here in this scope
    int tid = omp_get_thread_num();
    int nthreads = omp_get_num_threads();
    const int ntasks = schedule -> ::SpMP::LevelSchedule::ntasks;
    const short *nparents = schedule -> ::SpMP::LevelSchedule::nparentsForward;
    int nPerThread = (ntasks + nthreads - 1) / nthreads;
    int nBegin = __rose_lt(nPerThread * tid,ntasks);
    int nEnd = __rose_lt(nBegin + nPerThread,ntasks);
    volatile int *taskFinished = schedule -> ::SpMP::LevelSchedule::taskFinished;
    int **parents = schedule -> ::SpMP::LevelSchedule::parentsForward;
    memset(((char *)(taskFinished + nBegin)),0,(nEnd - nBegin) * sizeof(int ));
   #pragma omp barrier 
   {
//change t4 to task for SPMP_LVEL_SCHEDULE
      for (int task = threadBoundaries_(tid); task <= threadBoundaries__(tid) - 1; task += 1) 
      {
	{
          SPMP_LEVEL_SCHEDULE_WAIT;
          for (t6 = taskBoundaries_(task); t6 <= taskBoundaries__(task) - 1; t6 += 1) {
            t8 = rowptr_(t6);
            sum = b[t6];
            for (t8 = rowptr_(t6); t8 <= rowptr__(t6) - 1; t8 += 1) 
              sum -= (values[t8] * y[colidx[t8]]);
            t8 = rowptr__(t6);
            y[t6] = (sum * idiag[t6]);
          }
        }
        SPMP_LEVEL_SCHEDULE_NOTIFY;
      }
   }
 }













}

/**
 * Backward sparse triangular solver parallelized with level scheduling
 * and point-to-point synchronization
 */
void backwardSolveWithReorderedMatrix(const CSR& A, double y[],
		const double b[], const LevelSchedule *schedule) {



	ADJUST_FOR_BASE;
/*
#pragma omp parallel
	{
		int nthreads = omp_get_num_threads();
		int tid = omp_get_thread_num();

		const int ntasks = schedule.ntasks;
		const short *nparents = schedule.nparentsBackward;
		const vector<int>& threadBoundaries = schedule.threadBoundaries;
		const vector<int>& taskBoundaries = schedule.taskBoundaries;

		int nPerThread = (ntasks + nthreads - 1) / nthreads;
		int nBegin = min(nPerThread * tid, ntasks);
		int nEnd = min(nBegin + nPerThread, ntasks);

		volatile int *taskFinished = schedule.taskFinished;
		int **parents = schedule.parentsBackward;

		memset((char *) (taskFinished + nBegin), 0,
				(nEnd - nBegin) * sizeof(int));

		synk::Barrier::getInstance()->wait(tid);

		for (int task = threadBoundaries[tid + 1] - 1;
				task >= threadBoundaries[tid]; --task) {
			SPMP_LEVEL_SCHEDULE_WAIT;

			for (int i = taskBoundaries[task + 1] - 1 + base;
					i >= taskBoundaries[task] + base; --i) {
				double sum = b[i];
				for (int j = rowptr[i + 1] - 1; j >= rowptr[i]; --j) {
					sum -= values[j] * y[colidx[j]];
				}
				y[i] = sum * idiag[i];
			}

			SPMP_LEVEL_SCHEDULE_NOTIFY;
		} // for each task
	} // omp parallel
*/
int  t4,t6,t8;
double sum;

#pragma omp parallel  private(t4,t6,t8,sum)
{
    int tid = omp_get_thread_num(); //need to change this to negative
    int nthreads = omp_get_num_threads();
    const int ntasks = schedule -> ::SpMP::LevelSchedule::ntasks;
    const short *nparents = schedule -> ::SpMP::LevelSchedule::nparentsBackward;//changed
    int nPerThread = (ntasks + nthreads - 1) / nthreads;
    int nBegin = __rose_lt(nPerThread * tid,ntasks);
    int nEnd = __rose_lt(nBegin + nPerThread,ntasks);
    volatile int * taskFinished = schedule -> ::SpMP::LevelSchedule::taskFinished;
    int **parents = schedule -> ::SpMP::LevelSchedule::parentsBackward;//Anand:changed
    memset(((char *)(taskFinished + nBegin)),0,(nEnd - nBegin) * sizeof(int ));
    #pragma omp barrier 
{
 	//change lb from threadBoundaries__ -1 to threadBoundaries__ +1	
      
          
     //change -tid to tid
      for (int t4 = -threadBoundaries__(tid) + 1; t4 <= -threadBoundaries_(tid); t4 += 1) {
	  int task = -t4; //added
          {
          SPMP_LEVEL_SCHEDULE_WAIT;
 	  //change lb from taskBoundaries__ -1 to taskBoundaries__ +1	
          for (t6 = -taskBoundaries__(-t4) + 1; t6 <= -taskBoundaries_(-t4); t6 += 1) {
            t8 = rowptr_(-t6);
            sum = b[-t6];
            for (t8 = rowptr_(-(-(-t6))); t8 <= rowptr__(-(-(-t6))) - 1; t8 += 1) 
              sum -= (values[t8] * y[colidx[t8]]);
            t8 = rowptr__(-t6);
            y[-t6] = (sum * idiag[-t6]);
          }
        }
        SPMP_LEVEL_SCHEDULE_NOTIFY;
      }
    }
}






}


// serial ilu0
void ilu0_ref(double *lu, const CSR& A) {
	int base = A.getBase();

	const int *rowptr = A.rowptr - base;
	const int *colidx = A.colidx - base;
	const int *diagptr = A.diagptr - base;
	const double *values = A.values - base;

	lu -= base;

	int tid = omp_get_thread_num();

#pragma omp for
	for (int i = base; i < A.getNnz() + base; i++) {
		lu[i] = values[i];
	}

	for (int i = 0; i < A.m; ++i) {
		for (int j = rowptr[i]; j < diagptr[i]; ++j) {
			int c = colidx[j];
			double tmp = lu[j] /= lu[diagptr[c]];

			int k1 = j + 1, k2 = diagptr[c] + 1;

			while (k1 < rowptr[i + 1] && k2 < rowptr[c + 1]) {
				if (colidx[k1] < colidx[k2])
					++k1;
				else if (colidx[k1] > colidx[k2])
					++k2;
				else {
					lu[k1] -= tmp * lu[k2];
					++k1;
					++k2;
				}
			}
		}
	} // for each row
}

// parallel ilu0
void ilu0_p2p_reordered(double *lu, const CSR& A, const LevelSchedule *schedule) {
	int base = A.getBase();

	const int *rowptr = A.rowptr - base;
	const int *colidx = A.colidx - base;
	const int *diagptr = A.diagptr - base;
	const double *values = A.values - base;

	lu -= base;
	int t4,t6,t8;
#pragma omp parallel private(t4,t6, t8)
	{
		int tid = omp_get_thread_num();

#pragma omp for
		for (int i = base; i < A.getNnz() + base; i++) {
			lu[i] = values[i];
		}

		/*const int ntasks = schedule.ntasks;
		const short *nparents = schedule.nparentsForward;
		const vector<int>& threadBoundaries = schedule.threadBoundaries;
		const vector<int>& taskBoundaries = schedule.taskBoundaries;

		const int *perm = schedule.threadContToOrigPerm;

		int nBegin, nEnd;
		getSimpleThreadPartition(&nBegin, &nEnd, ntasks);

		volatile int *taskFinished = schedule.taskFinished;
		int **parents = schedule.parentsForward;

		memset((char *) (taskFinished + nBegin), 0,
				(nEnd - nBegin) * sizeof(int));

		synk::Barrier::getInstance()->wait(tid);

		for (int task = threadBoundaries[tid]; task < threadBoundaries[tid + 1];
				++task) {
			SPMP_LEVEL_SCHEDULE_WAIT;

			for (int i = taskBoundaries[task]; i < taskBoundaries[task + 1];
					++i) {
				int row = perm[i] + base;

				for (int j = rowptr[row]; j < diagptr[row]; ++j) {
					int c = colidx[j];
					double tmp = lu[j] /= lu[diagptr[c]];

					int k1 = j + 1, k2 = diagptr[c] + 1;

					while (k1 < rowptr[row + 1] && k2 < rowptr[c + 1]) {
						if (colidx[k1] < colidx[k2])
							++k1;
						else if (colidx[k1] > colidx[k2])
							++k2;
						else {
							lu[k1] -= tmp * lu[k2];
							++k1;
							++k2;
						}
					}
				}
			} // for each row

			SPMP_LEVEL_SCHEDULE_NOTIFY;
		} // for each level*/

   int *perm = schedule -> SpMP::LevelSchedule::origToThreadContPerm;
   int *perm_inv = schedule -> SpMP::LevelSchedule::threadContToOrigPerm;
//#pragma omp parallel  private(tid,num_threads,sc_temp,t8,t6,t4)
//{
    //tid = omp_get_thread_num();
    int nthreads = omp_get_num_threads();
    const int ntasks = schedule -> ::SpMP::LevelSchedule::ntasks;
    const short *nparents = schedule -> ::SpMP::LevelSchedule::nparentsForward;
    int nPerThread = (ntasks + nthreads - 1) / nthreads;
    int nBegin = __rose_lt(nPerThread * tid,ntasks);
    int nEnd = __rose_lt(nBegin + nPerThread,ntasks);
    volatile int *taskFinished = schedule -> ::SpMP::LevelSchedule::taskFinished;
    int **parents = schedule -> ::SpMP::LevelSchedule::parentsForward;
    memset(((char *)(taskFinished + nBegin)),0,(nEnd - nBegin) * sizeof(int ));
#pragma omp barrier 
{
      for (int task = threadBoundaries_(tid); task <= threadBoundaries__(tid) - 1; task += 1) {{
          SPMP_LEVEL_SCHEDULE_WAIT;
          for (t6 = taskBoundaries_(task); t6 <= taskBoundaries__(task) - 1; t6 += 1) 
            for (t8 = rowptr_(t6); t8 <= diagptr_(t6) - 1; t8 += 1) {
//c = colidx[k];
// a_ik /= a_kk
              lu[t8] = (lu[t8] / lu[perm[diagptr[colidx[t8]]]]);
              int j1 = (t8 + 1);
              int j2 = (diagptr[perm[colidx[t8]]] + 1);
              while((j1 < rowptr[perm[perm_inv[t6] + 1]]) && (j2 < rowptr[perm[colidx[t8] + 1]])){
                if (colidx[j1] == colidx[j2]) {
// a_ij -= a_ik*a_kj
                  lu[j1] -= (lu[t8] * lu[j2]);
                  ++j1;
                  ++j2;
                }
                else if (colidx[j1] < colidx[j2]) 
                  ++j1;
                else 
                  ++j2;
              }
            }
        }
        SPMP_LEVEL_SCHEDULE_NOTIFY;
      }
    }
   } // omp parallel
}


void ilu0(double *lu, const CSR& A, const LevelSchedule *schedule) {
	int base = A.getBase();

	const int *rowptr = A.rowptr - base;
	const int *colidx = A.colidx - base;
	const int *diagptr = A.diagptr - base;
	const double *values = A.values - base;

	lu -= base;
	int t4,t6,t8;
#pragma omp parallel private(t4,t6, t8)
	{
		int tid = omp_get_thread_num();

#pragma omp for
		for (int i = base; i < A.getNnz() + base; i++) {
			lu[i] = values[i];
		}

		/*const int ntasks = schedule.ntasks;
		const short *nparents = schedule.nparentsForward;
		const vector<int>& threadBoundaries = schedule.threadBoundaries;
		const vector<int>& taskBoundaries = schedule.taskBoundaries;

		const int *perm = schedule.threadContToOrigPerm;

		int nBegin, nEnd;
		getSimpleThreadPartition(&nBegin, &nEnd, ntasks);

		volatile int *taskFinished = schedule.taskFinished;
		int **parents = schedule.parentsForward;

		memset((char *) (taskFinished + nBegin), 0,
				(nEnd - nBegin) * sizeof(int));

		synk::Barrier::getInstance()->wait(tid);

		for (int task = threadBoundaries[tid]; task < threadBoundaries[tid + 1];
				++task) {
			SPMP_LEVEL_SCHEDULE_WAIT;

			for (int i = taskBoundaries[task]; i < taskBoundaries[task + 1];
					++i) {
				int row = perm[i] + base;

				for (int j = rowptr[row]; j < diagptr[row]; ++j) {
					int c = colidx[j];
					double tmp = lu[j] /= lu[diagptr[c]];

					int k1 = j + 1, k2 = diagptr[c] + 1;

					while (k1 < rowptr[row + 1] && k2 < rowptr[c + 1]) {
						if (colidx[k1] < colidx[k2])
							++k1;
						else if (colidx[k1] > colidx[k2])
							++k2;
						else {
							lu[k1] -= tmp * lu[k2];
							++k1;
							++k2;
						}
					}
				}
			} // for each row

			SPMP_LEVEL_SCHEDULE_NOTIFY;
		} // for each level*/

   //perm = schedule -> SpMP::LevelSchedule::origToThreadContPerm;
   int *perm_inv = schedule -> SpMP::LevelSchedule::threadContToOrigPerm;
//#pragma omp parallel  private(tid,num_threads,sc_temp,t8,t6,t4)
//{
    //tid = omp_get_thread_num();
    int nthreads = omp_get_num_threads();
    const int ntasks = schedule -> ::SpMP::LevelSchedule::ntasks;
    const short *nparents = schedule -> ::SpMP::LevelSchedule::nparentsForward;
    int nPerThread = (ntasks + nthreads - 1) / nthreads;
    int nBegin = __rose_lt(nPerThread * tid,ntasks);
    int nEnd = __rose_lt(nBegin + nPerThread,ntasks);
    volatile int *taskFinished = schedule -> ::SpMP::LevelSchedule::taskFinished;
    int **parents = schedule -> ::SpMP::LevelSchedule::parentsForward;
    memset(((char *)(taskFinished + nBegin)),0,(nEnd - nBegin) * sizeof(int ));
#pragma omp barrier 
{
      for (int task = threadBoundaries_(tid); task <= threadBoundaries__(tid) - 1; task += 1) {{
          SPMP_LEVEL_SCHEDULE_WAIT;
          for (t6 = taskBoundaries_(task); t6 <= taskBoundaries__(task) - 1; t6 += 1) 
            for (t8 = rowptr_(perm_inv[t6]); t8 <= diagptr_(perm_inv[t6]) - 1; t8 += 1) {
//c = colidx[k];
// a_ik /= a_kk
              lu[t8] = (lu[t8] / lu[diagptr[colidx[t8]]]);
              int j1 = (t8 + 1);
              int j2 = (diagptr[colidx[t8]] + 1);
              while((j1 < rowptr[perm_inv[t6] + 1]) && (j2 < rowptr[colidx[t8] + 1])){
                if (colidx[j1] == colidx[j2]) {
// a_ij -= a_ik*a_kj
                  lu[j1] -= (lu[t8] * lu[j2]);
                  ++j1;
                  ++j2;
                }
                else if (colidx[j1] < colidx[j2]) 
                  ++j1;
                else 
                  ++j2;
              }
            }
        }
        SPMP_LEVEL_SCHEDULE_NOTIFY;
      }
    }
   } // omp parallel
}



double dot(const double x[], const double y[], int len) {
	double sum = 0;
#pragma omp parallel for reduction(+:sum)
	for (int i = 0; i < len; ++i) {
		sum += x[i] * y[i];
	}
	return sum;
}

double norm(const double x[], int len) {
	return sqrt(dot(x, x, len));
}

// w = a*x + b*y
void waxpby(int n, double w[], double alpha, const double x[], double beta,
		const double y[]) {
	if (1 == alpha) {
		if (1 == beta) {
#pragma omp parallel for
			for (int i = 0; i < n; ++i) {
				w[i] = x[i] + y[i];
			}
		} else if (-1 == beta) {
#pragma omp parallel for
			for (int i = 0; i < n; ++i) {
				w[i] = x[i] - y[i];
			}
		} else {
#pragma omp parallel for
			for (int i = 0; i < n; ++i) {
				w[i] = x[i] + beta * y[i];
			}
		}
	} else if (1 == beta) {
#pragma omp parallel for
		for (int i = 0; i < n; ++i) {
			w[i] = alpha * x[i] + y[i];
		}
	} else if (-1 == alpha) {
		if (0 == beta) {
#pragma omp parallel for
			for (int i = 0; i < n; ++i) {
				w[i] = -x[i];
			}
		} else {
#pragma omp parallel for
			for (int i = 0; i < n; ++i) {
				w[i] = beta * y[i] - x[i];
			}
		}
	} else if (0 == beta) {
#pragma omp parallel for
		for (int i = 0; i < n; ++i) {
			w[i] = alpha * x[i];
		}
	} else {
#pragma omp parallel for
		for (int i = 0; i < n; ++i) {
			w[i] = alpha * x[i] + beta * y[i];
		}
	}
}

void pcg_symgs(CSR *A, double *x, const double *b, double tol, int maxiter) {
	double spmv_time = 0;
	double trsv_time = 0;

	// Construct SymGS pre-conditioner
	CSR *L = new CSR, *U = new CSR;
	splitLU(*A, L, U);
#pragma omp parallel for
	for (int i = 0; i < U->m; ++i) {
		for (int j = U->rowptr[i]; j < U->rowptr[i + 1]; ++j) {
			U->values[j] *= U->idiag[i];
		}
		U->idiag[i] = 1;
	}

	double *r = MALLOC(double, A->m);
	spmv_time -= omp_get_wtime();
	A->multiplyWithVector(r, -1, x, 1, b, 0); // r = b - A*x
	spmv_time += omp_get_wtime();
	double normr0 = norm(r, A->m);
	double rel_err = 1.;

	double *z = MALLOC(double, A->m);
	double *y = MALLOC(double, A->m);
	// z = M\r, where M is pre-conditioner
	trsv_time -= omp_get_wtime();
	forwardSolveRef(*L, y, r);
	backwardSolveRef(*U, z, y);
	trsv_time += omp_get_wtime();

	double *p = MALLOC(double, A->m);
	copyVector(p, z, A->m);
	double rz = dot(r, z, A->m);
	int k = 1;

	double *Ap = MALLOC(double, A->m);

	while (k <= maxiter) {
		double old_rz = rz;

		spmv_time -= omp_get_wtime();
		A->multiplyWithVector(Ap, p); // Ap = A*p
		spmv_time += omp_get_wtime();

		double alpha = old_rz / dot(p, Ap, A->m);
		waxpby(A->m, x, 1, x, alpha, p); // x += alpha*p
		waxpby(A->m, r, 1, r, -alpha, Ap); // r -= alpha*Ap
		rel_err = norm(r, A->m) / normr0;
		if (rel_err < tol)
			break;

		trsv_time -= omp_get_wtime();
		forwardSolveRef(*L, y, r);
		backwardSolveRef(*U, z, y);
		trsv_time += omp_get_wtime();

		rz = dot(r, z, A->m);
		double beta = rz / old_rz;
		waxpby(A->m, p, 1, z, beta, p); // p = z + beta*p
		++k;
	}

	printf("iter = %d rel_err = %g\n", k, rel_err);

	double spmv_bytes = (k + 1) * (12. * A->getNnz() + (4. + 2 * 8) * A->m);
	double trsv_bytes = k
			* (12. * L->getNnz() + 12. * U->getNnz()
					+ (8. + 2 * 4 + 2 * 2 * 8) * L->m);
	printf("spmv_perf = %g gbps trsv_perf = %g gbps\n",
			spmv_bytes / spmv_time / 1e9, trsv_bytes / trsv_time / 1e9);

	delete L;
	delete U;

	FREE (r);
	FREE (z);
	FREE (y);
	FREE (p);
	FREE (Ap);
}

void pcg_symgs_opt(CSR *A, double *x, const double *b, double tol,
		int maxiter) {
	double spmv_time = 0;
	double trsv_time = 0;

	// Construct SymGS pre-conditioner
	CSR *L = new CSR, *U = new CSR;
	splitLU(*A, L, U);
#pragma omp parallel for
	for (int i = 0; i < U->m; ++i) {
		for (int j = U->rowptr[i]; j < U->rowptr[i + 1]; ++j) {
			U->values[j] *= U->idiag[i];
		}
		U->idiag[i] = 1;
	}

	double time[9];	
        CSR *B;
          
        for(int i = 0; i < 8; i++){
          B  =  test_ilu0_dep_opt(A, &time[i]);

          FREE(B->rowptr); 
//          FREE(B->diagptr); 
          FREE(B->colidx); 
 //         FREE(B->extptr);  
  
          delete B;

      }
          
        B  =  test_ilu0_dep_opt(A, &time[8]);
          
        sort(time, time + 9);
	printf("median OPTIMIZED-INSPECTOR time %f\n", time[4]);                                                                                                            
	LevelSchedule schedule;
	schedule.constructTaskGraph(*B);
	printf("parallelism = %g\n",
			(double) A->m / (schedule.levIndices.size() - 1));

	const int *perm = schedule.origToThreadContPerm;
	const int *invPerm = schedule.threadContToOrigPerm;

	CSR *APerm = A->permute(perm, invPerm);
	CSR *LPerm = L->permute(perm, invPerm);
	CSR *UPerm = U->permute(perm, invPerm);

	delete L;
	delete U;
        FREE(B->rowptr); 
        //FREE(B->diagptr); 
        FREE(B->colidx); 
        //FREE(B->extptr);  

	delete B;
	double *bPerm = getReorderVectorWithInversePerm(b, invPerm, A->m);
	double *xPerm = getReorderVectorWithInversePerm(x, invPerm, A->m);

	double *r = MALLOC(double, A->m);
	spmv_time -= omp_get_wtime();
	APerm->multiplyWithVector(r, -1, xPerm, 1, bPerm, 0); // r = b - A*x
	spmv_time += omp_get_wtime();
	double normr0 = norm(r, A->m);
	double rel_err = 1.;

	double *z = MALLOC(double, A->m);
	double *y = MALLOC(double, A->m);
	// z = M\r, where M is pre-conditioner
	trsv_time -= omp_get_wtime();
	forwardSolveWithReorderedMatrix(*LPerm, y, r, &schedule);
	backwardSolveWithReorderedMatrix(*UPerm, z, y,&schedule);
	trsv_time += omp_get_wtime();

	double *p = MALLOC(double, A->m);
	copyVector(p, z, A->m);
	double rz = dot(r, z, A->m);
	int k = 1;

	double *Ap = MALLOC(double, A->m);

	while (k <= maxiter) {
		double old_rz = rz;

		spmv_time -= omp_get_wtime();
		APerm->multiplyWithVector(Ap, p); // Ap = A*p
		spmv_time += omp_get_wtime();

		double alpha = old_rz / dot(p, Ap, A->m);
		waxpby(A->m, xPerm, 1, xPerm, alpha, p); // x += alpha*p
		waxpby(A->m, r, 1, r, -alpha, Ap); // r -= alpha*Ap
		rel_err = norm(r, A->m) / normr0;
		if (rel_err < tol)
			break;

		trsv_time -= omp_get_wtime();
		forwardSolveWithReorderedMatrix(*LPerm, y, r, &schedule);
	        backwardSolveWithReorderedMatrix(*UPerm, z, y,&schedule);
		trsv_time += omp_get_wtime();

		rz = dot(r, z, A->m);
		double beta = rz / old_rz;
		waxpby(A->m, p, 1, z, beta, p); // p = z + beta*p
		++k;
	}

	reorderVectorOutOfPlaceWithInversePerm(x, xPerm, perm, A->m);

	printf("iter = %d, rel_err = %g\n", k, rel_err);

	double spmv_bytes = (k + 1) * (12. * A->getNnz() + (4. + 2 * 8) * A->m);
	double trsv_bytes = k
			* (12. * LPerm->getNnz() + 12. * UPerm->getNnz()
					+ (8. + 2 * 4 + 2 * 2 * 8) * LPerm->m);
	printf("spmv_perf = %g gbps trsv_perf = %g gbps\n",
			spmv_bytes / spmv_time / 1e9, trsv_bytes / trsv_time / 1e9);
        //                            + (8. + 2 * 4 + 2 * 2 * 8) * LPerm->m);
        printf("spmv_time = %g s trsv_tme = %g s\n",
                        spmv_time , trsv_time);
	delete APerm;
	delete LPerm;
	delete UPerm;

	FREE (r);
	FREE (z);
	FREE (y);
	FREE (p);
	FREE (Ap);

	FREE(bPerm);
	FREE(xPerm);
}

void pcg_ilu0(CSR *A, double *x, const double *b, double tol, int maxiter) {
	double spmv_time = 0;
	double trsv_time = 0;
	double ilu0_time = 0;
	double ilu0_time1 = 0;
	double ilu0_time2 = 0;
	double ilu_times[10];
	double pcg_time = 0;
	// Construct ILU0 pre-conditioner


	
	double *lu = MALLOC(double, A->getNnz());

	for(int num = 0; num < 10; num++){
	
		ilu0_time1 = omp_get_wtime();
		ilu0_ref(lu, *A);
		ilu0_time2 = omp_get_wtime();
 		ilu_times[num] = ilu0_time2 - ilu0_time1;
 		ilu0_time += ilu_times[num];	       
        
        }
        
          
        sort(ilu_times, ilu_times + 10);
	printf("median ilu0 ref time %f\n", ilu_times[5]);                                                                                                            
        //ilu0_time /= 10;       
        
	CSR LU(A->m, A->n, A->rowptr, A->colidx, lu);
	LU.computeInverseDiag();

	CSR *L = new CSR, *U = new CSR;
	splitLU(LU, L, U);
#pragma omp parallel for
	for (int i = 0; i < L->m; ++i) {
		L->idiag[i] = 1;
	}

	double *r = MALLOC(double, A->m);
	spmv_time -= omp_get_wtime();
	A->multiplyWithVector(r, -1, x, 1, b, 0); // r = b - A*x
	spmv_time += omp_get_wtime();
	double normr0 = norm(r, A->m);
	double rel_err = 1.;

	double *z = MALLOC(double, A->m);
	double *y = MALLOC(double, A->m);
	// z = M\r, where M is pre-conditioner
	trsv_time -= omp_get_wtime();
	forwardSolveRef(*L, y, r);
	backwardSolveRef(*U, z, y);
	trsv_time += omp_get_wtime();

	double *p = MALLOC(double, A->m);
	copyVector(p, z, A->m);
	double rz = dot(r, z, A->m);
	int k = 1;

	double *Ap = MALLOC(double, A->m);

	while (k <= maxiter) {
		pcg_time -= omp_get_wtime();
		double old_rz = rz;

		spmv_time -= omp_get_wtime();
		A->multiplyWithVector(Ap, p); // Ap = A*p
		spmv_time += omp_get_wtime();

		double alpha = old_rz / dot(p, Ap, A->m);
		waxpby(A->m, x, 1, x, alpha, p); // x += alpha*p
		waxpby(A->m, r, 1, r, -alpha, Ap); // r -= alpha*Ap
		rel_err = norm(r, A->m) / normr0;
		if (rel_err < tol){
			pcg_time += omp_get_wtime();
			break;
		}

		trsv_time -= omp_get_wtime();
		forwardSolveRef(*L, y, r);
		backwardSolveRef(*U, z, y);
		trsv_time += omp_get_wtime();

		rz = dot(r, z, A->m);
		double beta = rz / old_rz;
		waxpby(A->m, p, 1, z, beta, p); // p = z + beta*p
		pcg_time += omp_get_wtime();
		++k;
	}

	printf("iter = %d rel_err = %g\n", k, rel_err);
        printf("Total pcg time: %f, Average time per iteration  %f\n", pcg_time, pcg_time/k);                                                                                                            

	double spmv_bytes = (k + 1) * (12. * A->getNnz() + (4. + 2 * 8) * A->m);
	double trsv_bytes = k
			* (12. * L->getNnz() + 12. * U->getNnz()
					+ (8. + 2 * 4 + 2 * 2 * 8) * L->m);
	double ilu0_bytes = 10*(16.*A->getNnz() + 8.*A->m + 4.*A->getNnz()); 
	printf("spmv_perf = %g gbps trsv_perf = %g gbps ilu0_perf = %g gbps\n",
			spmv_bytes / spmv_time / 1e9, trsv_bytes / trsv_time / 1e9, ilu0_bytes / ilu0_time / 1e9);
        printf("spmv_time = %g s trsv_tme = %g s\n",
                        spmv_time , trsv_time);
	delete L;
	delete U;

	FREE (r);
	FREE (z);
	FREE (y);
	FREE (p);
	FREE (Ap);
}

void pcg_ilu0_opt(CSR *A, double *x, const double *b, double tol, int maxiter) {
	double spmv_time = 0;
	double trsv_time = 0;
	double ilu0_time = 0;
	double pcg_time  = 0;


        double time[9];
        CSR *B, *C;
                
	for(int i = 0; i < 8; i++){
		B  =  test_gs_dep(A, &time[i]);
		FREE(B->rowptr);
		//FREE(B->diagptr);
		FREE(B->colidx);
		//FREE(B->extptr);
	
	        delete B;
        }
        B  =  test_gs_dep(A, &time[8]);
                                                            
	sort(time, time + 9);
	printf("median GS-PRECISE-DEP-TEST time %f\n", time[4]);
	time[4] -= omp_get_wtime(); 
	LevelSchedule schedule;
	schedule.constructTaskGraph(*B);
        time[4] += omp_get_wtime(); 
	printf("median GS-TOTAL-INSPECTOR time %f\n", time[4]);
	printf("parallelism = %g\n",
			(double) A->m / (schedule.levIndices.size() - 1));


         for(int i = 0; i < 8; i++){
                C  =  test_ilu_dep_precise(A, &time[i]);
                FREE(C->rowptr);
                //FREE(B->diagptr);
                FREE(C->colidx);
                //FREE(B->extptr);

                delete C;
        }
        C  =  test_ilu_dep_precise(A, &time[8]);
        printf("median ILU-PRECISE-DEP-TEST time %f\n", time[4]);
        sort(time, time + 9);
        time[4] -= omp_get_wtime();
        LevelSchedule schedule2;
        schedule2.constructTaskGraph(*C);
        time[4] += omp_get_wtime();
        printf("median ILU-INSPECTOR time %f\n", time[4]);
        printf("precise parallelism = %g\n",
                        (double) A->m / (schedule2.levIndices.size() - 1));

	// Construct ILU0 pre-conditioner
	double *lu = MALLOC(double, A->getNnz());

        for(int num = 0; num < 10; num++){
        	ilu0_time -= omp_get_wtime();
	        ilu0(lu, *A, &schedule2);
		ilu0_time += omp_get_wtime();
        }                                                        
  	//ilu0_time /= 10;                                                              
	printf("median ilu0 p2p time %f\n", ilu0_time/10);
	CSR LU(A->m, A->n, A->rowptr, A->colidx, lu);
	LU.computeInverseDiag();

	CSR *L = new CSR, *U = new CSR;
	splitLU(LU, L, U);
#pragma omp parallel for
	for (int i = 0; i < L->m; ++i) {
		L->idiag[i] = 1;
	}

	const int *perm = schedule.origToThreadContPerm;
	const int *invPerm = schedule.threadContToOrigPerm;

	CSR *APerm = A->permute(perm, invPerm);
	CSR *LPerm = L->permute(perm, invPerm);
	CSR *UPerm = U->permute(perm, invPerm);

	delete L;
	delete U;
	FREE(B->rowptr);
	FREE(C->rowptr);
	FREE(B->colidx);
	FREE(C->rowptr);
	
	delete B;
	delete C;
	double *bPerm = getReorderVectorWithInversePerm(b, invPerm, A->m);
	double *xPerm = getReorderVectorWithInversePerm(x, invPerm, A->m);

	double *r = MALLOC(double, A->m);
	spmv_time -= omp_get_wtime();
	APerm->multiplyWithVector(r, -1, xPerm, 1, bPerm, 0); // r = b - A*x
	spmv_time += omp_get_wtime();
	double normr0 = norm(r, A->m);
	double rel_err = 1.;

	double *z = MALLOC(double, A->m);
	double *y = MALLOC(double, A->m);
	// z = M\r, where M is pre-conditioner
	trsv_time -= omp_get_wtime();
	forwardSolveWithReorderedMatrix(*LPerm, y, r, &schedule);
	backwardSolveWithReorderedMatrix(*UPerm, z, y, &schedule);
	trsv_time += omp_get_wtime();

	double *p = MALLOC(double, A->m);
	copyVector(p, z, A->m);
	double rz = dot(r, z, A->m);
	int k = 1;

	double *Ap = MALLOC(double, A->m);

	while (k <= maxiter) {
		pcg_time -= omp_get_wtime();
		double old_rz = rz;

		spmv_time -= omp_get_wtime();
		APerm->multiplyWithVector(Ap, p); // Ap = A*p
		spmv_time += omp_get_wtime();

		double alpha = old_rz / dot(p, Ap, A->m);
		waxpby(A->m, xPerm, 1, xPerm, alpha, p); // x += alpha*p
		waxpby(A->m, r, 1, r, -alpha, Ap); // r -= alpha*Ap
		rel_err = norm(r, A->m) / normr0;
		if (rel_err < tol){
			pcg_time += omp_get_wtime();
			break;
		}

		trsv_time -= omp_get_wtime();
		forwardSolveWithReorderedMatrix(*LPerm, y, r, &schedule);
		backwardSolveWithReorderedMatrix(*UPerm, z, y, &schedule);
		trsv_time += omp_get_wtime();

		rz = dot(r, z, A->m);
		double beta = rz / old_rz;
		waxpby(A->m, p, 1, z, beta, p); // p = z + beta*p
		
		pcg_time += omp_get_wtime();
		++k;
	}

	reorderVectorOutOfPlaceWithInversePerm(x, xPerm, perm, A->m);

        printf("Total pcg time: %f, Average time per iteration  %f\n", pcg_time, pcg_time/k);                                                                                                            
	printf("iter = %d, rel_err = %g\n", k, rel_err);

	double spmv_bytes = (k + 1) * (12. * A->getNnz() + (4. + 2 * 8) * A->m);
	double trsv_bytes = k
			* (12. * LPerm->getNnz() + 12. * UPerm->getNnz()
					+ (8. + 2 * 4 + 2 * 2 * 8) * LPerm->m);
        double ilu0_bytes = 10*(16.*A->getNnz() + 8.*A->m + 4.*A->getNnz());   
	printf("spmv_perf = %g gbps trsv_perf = %g gbps ilu0_perf = %g gbps\n",
                                        spmv_bytes / spmv_time / 1e9, trsv_bytes / trsv_time / 1e9, ilu0_bytes / ilu0_time / 1e9);
        printf("spmv_time = %g s trsv_tme = %g s\n",
                        spmv_time , trsv_time);                                        

	delete APerm;
	delete LPerm;
	delete UPerm;

	FREE (r);
	FREE (z);
	FREE (y);
	FREE (p);
	FREE (Ap);

	FREE(bPerm);
	FREE(xPerm);
}

void ilu0BarrierReordered(double *lu, const CSR& A, const LevelSchedule  *schedule, const int *perm) {
	int base = A.getBase();

	const int *rowptr = A.rowptr - base;
	const int *colidx = A.colidx - base;
	const int *diagptr = A.diagptr - base;
	const double *values = A.values - base;

	lu -= base;
	int t4, t6,t8;



#pragma omp parallel  private(t4,t6,t8)
        {
                //int t2 = omp_get_thread_num();
#pragma omp for
                for (int i = base; i < A.getNnz() + base; i++) {
                        lu[i] = values[i];
                }


    //int *perm = schedule -> SpMP::LevelSchedule::origToThreadContPerm;
    int *perm_inv = schedule -> SpMP::LevelSchedule::threadContToOrigPerm;
//#pragma omp parallel  private(t4,t6,t8)
//{
    int tid = omp_get_thread_num();
    int   num_threads = omp_get_num_threads();
   {
      for (t4 = threadBoundaries_(tid); t4 <= threadBoundaries__(tid) - 1; t4 += 1){ 
        for (t6 = taskBoundaries_(t4); t6 <= taskBoundaries__(t4) - 1; t6 += 1) {
          for (t8 = rowptr_(perm_inv[t6]); t8 <= diagptr_(perm_inv[t6]) - 1; t8 += 1) {
//c = colidx[k];
// a_ik /= a_kk
            lu[t8] = (lu[t8] / lu[diagptr[colidx[t8]]]);
            int j1 = (t8 + 1);
            int j2 = (diagptr[colidx[t8]] + 1);
            while((j1 < rowptr[perm_inv[t6] + 1]) && (j2 < rowptr[colidx[t8] + 1])){
              if (colidx[j1] == colidx[j2]) {
// a_ij -= a_ik*a_kj
                lu[j1] -= (lu[t8] * lu[j2]);
                ++j1;
                ++j2;
              }
              else if (colidx[j1] < colidx[j2]) 
                ++j1;
              else 
                ++j2;
            }
          }
        }
      #pragma omp barrier 
     }    
   }
 }

/*#pragma omp parallel
	{
		int t2 = omp_get_thread_num();

#pragma omp for
		for (int i = base; i < A.getNnz() + base; i++) {
			lu[i] = values[i];
		}
		//for (t2 = 0; t2 <= ub - 1; t2 += 1)
		 synk::Barrier::getInstance()->wait(t2);

		for (int t4 = threadBoundaries_(t2); t4 <= threadBoundaries__(t2)- 1; t4 += 1) {
			for (int t6 = taskBoundaries_(t4); t6 <= taskBoundaries__(t4) - 1; t6 += 1) {
				int row = perm[t6] + base;

				for (int t8 = rowptr_(row); t8 <= diagptr_(row) - 1; t8 += 1) {
					int c = colidx[t8];
					// a_ik /= a_kk
					double tmp = (lu[t8] /= lu[diagptr[c]]);
					int j1 = (t8 + 1);
					int j2 = (diagptr[c] + 1);
					while((j1 < rowptr[row + 1]) && (j2 < rowptr[c + 1])) {
						if (colidx[j1] < colidx[j2])
						++j1;
						else if (colidx[j2] < colidx[j1])
						++j2;
						else {
							// a_ij -= a_ik*a_kj
							lu[j1] -= (tmp * lu[j2]);
							++j1;
							++j2;
						}
					}
				}
			}
			synk::Barrier::getInstance()->wait(t2);
		}
	}
*/
	/*
	 #pragma omp parallel
	 {
	 int tid = omp_get_thread_num();

	 #pragma omp for
	 for (int i = base; i < A.getNnz() + base; i++) {
	 lu[i] = values[i];
	 }




	 const int ntasks = schedule.ntasks;
	 const short *nparents = schedule.nparentsForward;
	 const vector<int>& threadBoundaries = schedule.threadBoundaries;
	 const vector<int>& taskBoundaries = schedule.taskBoundaries;

	 const int *perm = schedule.threadContToOrigPerm;

	 int nBegin, nEnd;
	 getSimpleThreadPartition(&nBegin, &nEnd, ntasks);

	 volatile int *taskFinished = schedule.taskFinished;
	 int **parents = schedule.parentsForward;

	 memset((char *) (taskFinished + nBegin), 0,
	 (nEnd - nBegin) * sizeof(int));

	 synk::Barrier::getInstance()->wait(tid);

	 for (int task = threadBoundaries[tid]; task < threadBoundaries[tid + 1];
	 ++task) {
	 SPMP_LEVEL_SCHEDULE_WAIT
	 ;

	 for (int i = taskBoundaries[task]; i < taskBoundaries[task + 1];
	 ++i) {
	 int row = perm[i] + base;

	 for (int j = rowptr[row]; j < diagptr[row]; ++j) {
	 int c = colidx[j];
	 double tmp = lu[j] /= lu[diagptr[c]];

	 int k1 = j + 1, k2 = diagptr[c] + 1;

	 while (k1 < rowptr[row + 1] && k2 < rowptr[c + 1]) {
	 if (colidx[k1] < colidx[k2])
	 ++k1;
	 else if (colidx[k1] > colidx[k2])
	 ++k2;
	 else {
	 lu[k1] -= tmp * lu[k2];
	 ++k1;
	 ++k2;
	 }
	 }
	 }
	 } // for each row

	 SPMP_LEVEL_SCHEDULE_NOTIFY
	 ;
	 } // for each level
	 } // omp parallel
	 */
}





void ilu0Barrier(double *lu, const CSR& A, const LevelSchedule *schedule,
		const int *perm) {
	int base = A.getBase();

	const int *rowptr = A.rowptr - base;
	const int *colidx = A.colidx - base;
	const int *diagptr = A.diagptr - base;
	const double *values = A.values - base;
	int t4,t6,t8;
	lu -= base;
#pragma omp parallel  private(t4,t6,t8)
	{
		//int t2 = omp_get_thread_num();
#pragma omp for
		for (int i = base; i < A.getNnz() + base; i++) {
			lu[i] = values[i];
		}


    //perm = schedule -> SpMP::LevelSchedule::origToThreadContPerm;
    int *perm_inv = schedule -> SpMP::LevelSchedule::threadContToOrigPerm;
//#pragma omp parallel  private(t4,t6,t8)
//{
    int tid = omp_get_thread_num();
    int   num_threads = omp_get_num_threads();
   {
      for (t4 = threadBoundaries_(tid); t4 <= threadBoundaries__(tid) - 1; t4 += 1){ 
        for (t6 = taskBoundaries_(t4); t6 <= taskBoundaries__(t4) - 1; t6 += 1) {
          for (t8 = rowptr_(perm_inv[t6]); t8 <= diagptr_(perm_inv[t6]) - 1; t8 += 1) {
//c = colidx[k];
// a_ik /= a_kk
            lu[t8] = (lu[t8] / lu[diagptr[colidx[t8]]]);
            int j1 = (t8 + 1);
            int j2 = (diagptr[colidx[t8]] + 1);
            while((j1 < rowptr[perm_inv[t6] + 1]) && (j2 < rowptr[colidx[t8] + 1])){
              if (colidx[j1] == colidx[j2]) {
// a_ij -= a_ik*a_kj
                lu[j1] -= (lu[t8] * lu[j2]);
                ++j1;
                ++j2;
              }
              else if (colidx[j1] < colidx[j2]) 
                ++j1;
              else 
                ++j2;
            }
          }
        }
      #pragma omp barrier 
     }    
   }
 }









/*
#pragma omp for
		for (int i = base; i < A.getNnz() + base; i++) {
			lu[i] = values[i];
		}
		//for (t2 = 0; t2 <= ub - 1; t2 += 1)
		 synk::Barrier::getInstance()->wait(t2);

		for (int t4 = threadBoundaries_(t2); t4 <= threadBoundaries__(t2)- 1; t4 += 1) {
			for (int t6 = taskBoundaries_(t4); t6 <= taskBoundaries__(t4) - 1; t6 += 1) {
				int row = perm[t6] + base;

				for (int t8 = rowptr_(row); t8 <= diagptr_(row) - 1; t8 += 1) {
					int c = colidx[t8];
					// a_ik /= a_kk
					double tmp = (lu[t8] /= lu[diagptr[c]]);
					int j1 = (t8 + 1);
					int j2 = (diagptr[c] + 1);
					while((j1 < rowptr[row + 1]) && (j2 < rowptr[c + 1])) {
						if (colidx[j1] < colidx[j2])
						++j1;
						else if (colidx[j2] < colidx[j1])
						++j2;
						else {
							// a_ij -= a_ik*a_kj
							lu[j1] -= (tmp * lu[j2]);
							++j1;
							++j2;
						}
					}
				}
			}
			synk::Barrier::getInstance()->wait(t2);
		}
	}
*/
	/*
	 #pragma omp parallel
	 {
	 int tid = omp_get_thread_num();

	 #pragma omp for
	 for (int i = base; i < A.getNnz() + base; i++) {
	 lu[i] = values[i];
	 }




	 const int ntasks = schedule.ntasks;
	 const short *nparents = schedule.nparentsForward;
	 const vector<int>& threadBoundaries = schedule.threadBoundaries;
	 const vector<int>& taskBoundaries = schedule.taskBoundaries;

	 const int *perm = schedule.threadContToOrigPerm;

	 int nBegin, nEnd;
	 getSimpleThreadPartition(&nBegin, &nEnd, ntasks);

	 volatile int *taskFinished = schedule.taskFinished;
	 int **parents = schedule.parentsForward;

	 memset((char *) (taskFinished + nBegin), 0,
	 (nEnd - nBegin) * sizeof(int));

	 synk::Barrier::getInstance()->wait(tid);

	 for (int task = threadBoundaries[tid]; task < threadBoundaries[tid + 1];
	 ++task) {
	 SPMP_LEVEL_SCHEDULE_WAIT
	 ;

	 for (int i = taskBoundaries[task]; i < taskBoundaries[task + 1];
	 ++i) {
	 int row = perm[i] + base;

	 for (int j = rowptr[row]; j < diagptr[row]; ++j) {
	 int c = colidx[j];
	 double tmp = lu[j] /= lu[diagptr[c]];

	 int k1 = j + 1, k2 = diagptr[c] + 1;

	 while (k1 < rowptr[row + 1] && k2 < rowptr[c + 1]) {
	 if (colidx[k1] < colidx[k2])
	 ++k1;
	 else if (colidx[k1] > colidx[k2])
	 ++k2;
	 else {
	 lu[k1] -= tmp * lu[k2];
	 ++k1;
	 ++k2;
	 }
	 }
	 }
	 } // for each row

	 SPMP_LEVEL_SCHEDULE_NOTIFY
	 ;
	 } // for each level
	 } // omp parallel
	 */
}

void forwardGSWithBarrier(const CSR& A, double y[], const double b[],
		const LevelSchedule *schedule, const int *perm) {
	ADJUST_FOR_BASE;

int t4,t6,t8;
double sum;
//  perm = schedule -> SpMP::LevelSchedule::origToThreadContPerm;
    int *perm_inv = schedule -> SpMP::LevelSchedule::threadContToOrigPerm;//needs to be perm_inv
#pragma omp parallel  private(t6,t4,t8,sum)
{
    int tid = omp_get_thread_num();
    int num_threads = omp_get_num_threads();
{
      for (t4 = threadBoundaries_(tid); t4 <= threadBoundaries__(tid) - 1; t4 += 1){ 
        for (t6 = taskBoundaries_(t4); t6 <= taskBoundaries__(t4) - 1; t6 += 1) {
            t8 = rowptr_(perm_inv[t6]);//Anand:change perm[t6]
            sum = b[perm_inv[t6]];//change
            for (t8 = rowptr_(perm_inv[t6]); t8 <= rowptr__(perm_inv[t6]) - 1; t8 += 1) 
              sum -= (values[t8] * y[colidx[t8]]);
            t8 = rowptr__(perm_inv[t6]);//Anand:change perm[t6]
            y[perm_inv[t6]] = (sum * idiag[perm_inv[t6]]);//change
          }
#pragma omp barrier //need to change level to insert omp barrier 
        }
    }
  }


/*
//CHiLL code target(partially generated) //Anand 11/02/2015
#pragma omp parallel
	{
          	int t2 = omp_get_thread_num();

		for (int t4 = threadBoundaries_(t2); t4 <= threadBoundaries__(t2)- 1; t4 += 1) {
			for (int t6 = taskBoundaries_(t4); t6 <= taskBoundaries__(t4) - 1; t6 += 1) {
				//int t8 = rowptr_(t6);
				//perm support to be added //Anand 11/02/2015
				int row = perm[t6] + base;
				double sum = b[row];
				for (int t8 = rowptr_(row); t8 <= rowptr__(row) - 1; t8 += 1)
				sum -= (values[t8] * y[colidx[t8]]);
				//t8 = rowptr__(t6);
				y[row] = (sum * idiag[row]);
			}
			synk::Barrier::getInstance()->wait(t2);
		}
	}
*/
	/*#pragma omp parallel
	 {
	 int tid = omp_get_thread_num();

	 const vector<int>& threadBoundaries = schedule.threadBoundaries;
	 const vector<int>& taskBoundaries = schedule.taskBoundaries;

	 for (int task = threadBoundaries[tid]; task < threadBoundaries[tid + 1]; ++task) {
	 for (int i = taskBoundaries[task]; i < taskBoundaries[task + 1]; ++i) {
	 int row = perm[i] + base;
	 double sum = b[row];
	 for (int j = rowptr[row]; j < rowptr[row + 1]; ++j) {
	 sum -= values[j]*y[colidx[j]];
	 }
	 y[row] += sum*idiag[row];
	 } // for each row

	 synk::Barrier::getInstance()->wait(tid);
	 } // for each level
	 } // omp parallel
	 */

}

/**
 * Backward Gauss-Seidel smoother parallelized with level scheduling
 * and barrier synchronization
 */
void backwardGSWithBarrier(const CSR& A, double y[], const double b[],
		const LevelSchedule *schedule, const int *perm) {
	ADJUST_FOR_BASE;

  int t4,t6,t8;
  double sum;	
  int *perm_inv = schedule -> SpMP::LevelSchedule::threadContToOrigPerm;
#pragma omp parallel  private(t4,t6,t8)
{
    int tid = omp_get_thread_num(); //added definitions here in this scope
    int  num_threads = omp_get_num_threads();
{
      for (t4 = -threadBoundaries__(tid) + 1; t4 <= -threadBoundaries_(tid); t4 += 1){ 
        for (t6 = -taskBoundaries__(-t4) + 1; t6 <= -taskBoundaries_(-t4); t6 += 1) {
            t8 = rowptr_(-(-perm_inv[-t6]));
            sum = b[-(-perm_inv[-t6])];
            for (t8 = rowptr_(-(-perm_inv[-t6])); t8 <= rowptr__(-(-perm_inv[-t6])) - 1; t8 += 1) 
              sum -= (values[t8] * y[colidx[t8]]);
            t8 = rowptr__(-(-perm_inv[-t6]));
            y[-(-perm_inv[-t6])] = (sum * idiag[-(-perm_inv[-t6])]);
          }
#pragma omp barrier 
        }
    }
  }


//CHiLL code target(partially generated) //Anand 11/02/2015
/*#pragma omp parallel
	{
		int t2 = omp_get_thread_num();

		for (int t4 = threadBoundaries__(t2)- 1; t4 >= threadBoundaries_(t2); t4--) {
			for (int t6 = taskBoundaries__(t4) - 1; t6 >= taskBoundaries_(t4); t6--) {
				int t8 = rowptr__(t6);
				//perm support to be added //Anand 11/02/2015
				int row = perm[t6] + base;
				double sum = b[row];
				for (int t8 = rowptr__(row)-1; t8 >= rowptr_(row); t8--)
				sum -= (values[t8] * y[colidx[t8]]);
				t8 = rowptr_(t6);
				y[row] = (sum * idiag[row]);
			}
			synk::Barrier::getInstance()->wait(t2);
		}
	}
*/
	/*

	 #pragma omp parallel
	 {
	 int tid = omp_get_thread_num();

	 const vector<int>& threadBoundaries = schedule.threadBoundaries;
	 const vector<int>& taskBoundaries = schedule.taskBoundaries;

	 for (int task = threadBoundaries[tid + 1] - 1;
	 task >= threadBoundaries[tid]; --task) {
	 for (int i = taskBoundaries[task + 1] - 1;
	 i >= taskBoundaries[task]; --i) {
	 int row = perm[i] + base;
	 double sum = b[row];
	 for (int j = rowptr[row + 1] - 1; j >= rowptr[row]; --j) {
	 sum -= values[j] * y[colidx[j]];
	 }
	 y[row] += sum * idiag[row];
	 } // for each row
	 synk::Barrier::getInstance()->wait(tid);
	 } // for each level
	 } // omp parallel
	 */

}



void forwardSolveWithReorderedMatrixBarrier(const CSR& A, double y[], const double b[],
		const LevelSchedule *schedule) {
	ADJUST_FOR_BASE;

/*
//CHiLL code target(partially generated) //Anand 11/02/2015
#pragma omp parallel
	{
          	int t2 = omp_get_thread_num();

		for (int t4 = threadBoundaries_(t2); t4 <= threadBoundaries__(t2)- 1; t4 += 1) {
			for (int t6 = taskBoundaries_(t4); t6 <= taskBoundaries__(t4) - 1; t6 += 1) {
				int t8 = rowptr_(t6);
				//perm support to be added //Anand 11/02/2015
				//int row = perm[t6] + base;
				double sum = b[t6];
				for (int t8 = rowptr_(t6); t8 <= rowptr__(t6) - 1; t8 += 1)
				sum -= (values[t8] * y[colidx[t8]]);
				t8 = rowptr__(t6);
				y[t6] = (sum * idiag[t6]);
			}
			synk::Barrier::getInstance()->wait(t2);
		}
	}
*/

int t4,t6,t8;
double sum;

#pragma omp parallel  private(sum,t6,t4,t8)
{
    int tid = omp_get_thread_num();
    int num_threads = omp_get_num_threads();
{
      for (t4 = threadBoundaries_(tid); t4 <= threadBoundaries__(tid) - 1; t4 += 1){ 
        for (t6 = taskBoundaries_(t4); t6 <= taskBoundaries__(t4) - 1; t6 += 1) {
            t8 = rowptr_(t6);
            sum = b[t6];
            for (t8 = rowptr_(t6); t8 <= rowptr__(t6) - 1; t8 += 1) 
              sum -= (values[t8] * y[colidx[t8]]);
            t8 = rowptr__(t6);
            y[t6] = (sum * idiag[t6]);
          }
#pragma omp barrier 
        }
    }
  }








	/*#pragma omp parallel
	 {
	 int tid = omp_get_thread_num();

	 const vector<int>& threadBoundaries = schedule.threadBoundaries;
	 const vector<int>& taskBoundaries = schedule.taskBoundaries;

	 for (int task = threadBoundaries[tid]; task < threadBoundaries[tid + 1]; ++task) {
	 for (int i = taskBoundaries[task]; i < taskBoundaries[task + 1]; ++i) {
	 int row = perm[i] + base;
	 double sum = b[row];
	 for (int j = rowptr[row]; j < rowptr[row + 1]; ++j) {
	 sum -= values[j]*y[colidx[j]];
	 }
	 y[row] += sum*idiag[row];
	 } // for each row

	 synk::Barrier::getInstance()->wait(tid);
	 } // for each level
	 } // omp parallel
	 */

}

/**
 * Backward Gauss-Seidel smoother parallelized with level scheduling
 * and barrier synchronization
 */
void backwardSolveWithReorderedMatrixBarrier(const CSR& A, double y[], const double b[],
		const LevelSchedule *schedule) {
	ADJUST_FOR_BASE;


//CHiLL code target(partially generated) //Anand 11/02/2015
/*#pragma omp parallel
	{
		int t2 = omp_get_thread_num();

		for (int t4 = threadBoundaries__(t2)- 1; t4 >= threadBoundaries_(t2); t4--) {
			for (int t6 = taskBoundaries__(t4) - 1; t6 >= taskBoundaries_(t4); t6--) {
				int t8 = rowptr__(t6);
				//perm support to be added //Anand 11/02/2015
				//int row = perm[t6] + base;
				double sum = b[t6];
				for (int t8 = rowptr__(t6)-1; t8 >= rowptr_(t6); t8--)
				sum -= (values[t8] * y[colidx[t8]]);
				t8 = rowptr_(t6);
				y[t6] = (sum * idiag[t6]);
			}
			synk::Barrier::getInstance()->wait(t2);
		}
	}
*/
int t6,t4,t8;
double sum;
#pragma omp parallel  private(sum,t6,t4,t8)
{
    int tid = omp_get_thread_num();
    int num_threads = omp_get_num_threads();
{
      for (t4 = -threadBoundaries__(tid) +1; t4 <= -threadBoundaries_(tid); t4 += 1) {
        for (t6 = -taskBoundaries__(-t4) + 1; t6 <= -taskBoundaries_(-t4); t6 += 1) {
            t8 = rowptr_(-t6);
            sum = b[-t6];
            for (t8 = rowptr_(-(-(-t6))); t8 <= rowptr__(-(-(-t6))) - 1; t8 += 1) 
              sum -= (values[t8] * y[colidx[t8]]);
            t8 = rowptr__(-t6);
            y[-t6] = (sum * idiag[-t6]);
          }
#pragma omp barrier 
        }
    }
  }








	/*

	 #pragma omp parallel
	 {
	 int tid = omp_get_thread_num();

	 const vector<int>& threadBoundaries = schedule.threadBoundaries;
	 const vector<int>& taskBoundaries = schedule.taskBoundaries;

	 for (int task = threadBoundaries[tid + 1] - 1;
	 task >= threadBoundaries[tid]; --task) {
	 for (int i = taskBoundaries[task + 1] - 1;
	 i >= taskBoundaries[task]; --i) {
	 int row = perm[i] + base;
	 double sum = b[row];
	 for (int j = rowptr[row + 1] - 1; j >= rowptr[row]; --j) {
	 sum -= values[j] * y[colidx[j]];
	 }
	 y[row] += sum * idiag[row];
	 } // for each row
	 synk::Barrier::getInstance()->wait(tid);
	 } // for each level
	 } // omp parallel
	 */

}

void pcg_symgs_barrier(CSR *A, double *x, const double *b, double tol,
		int maxiter) {
	double spmv_time = 0;
	double trsv_time = 0;

	// Construct SymGS pre-conditioner
	CSR *L = new CSR, *U = new CSR;
	splitLU(*A, L, U);
#pragma omp parallel for
	for (int i = 0; i < U->m; ++i) {
		for (int j = U->rowptr[i]; j < U->rowptr[i + 1]; ++j) {
			U->values[j] *= U->idiag[i];
		}
		U->idiag[i] = 1;
	}


        double time[9];
        CSR *B;
                
	for(int i = 0; i < 8; i++){
		B  =  test_ilu0_dep_opt(A, &time[i]);
		FREE(B->rowptr);
		//FREE(B->diagptr);
		FREE(B->colidx);
		//FREE(B->extptr);

	        delete B;
        }
        B  =  test_ilu0_dep_opt(A, &time[8]);
                                                            
	sort(time, time + 9);
	printf("median OPTIMIZED-INSPECTOR time %f\n", time[4]);



	LevelSchedule *barrierSchedule = new LevelSchedule;
	barrierSchedule->useBarrier = true;
	barrierSchedule->transitiveReduction = false;
	barrierSchedule->constructTaskGraph(*B);

	printf("parallelism = %g\n",
			(double) A->m / (barrierSchedule->levIndices.size() - 1));

	const int *perm = barrierSchedule->origToThreadContPerm;
	const int *invPerm = barrierSchedule->threadContToOrigPerm;

	//CSR *APerm = A->permute(perm, invPerm);
	//CSR *LPerm = L->permute(perm, invPerm);
	//CSR *UPerm = U->permute(perm, invPerm);


	//double *bPerm = getReorderVectorWithInversePerm(b, invPerm, A->m);
	//double *xPerm = getReorderVectorWithInversePerm(x, invPerm, A->m);

	double *r = MALLOC(double, A->m);
	spmv_time -= omp_get_wtime();
	A->multiplyWithVector(r, -1, x, 1, b, 0); // r = b - A*x
	spmv_time += omp_get_wtime();
	double normr0 = norm(r, A->m);
	double rel_err = 1.;

	double *z = MALLOC(double, A->m);
	double *y = MALLOC(double, A->m);
	// z = M\r, where M is pre-conditioner
	trsv_time -= omp_get_wtime();
	forwardGSWithBarrier(*L, y, r, barrierSchedule, invPerm);
	backwardGSWithBarrier(*U, z, y, barrierSchedule, invPerm);
	//forwardSolveWithReorderedMatrix(*LPerm, y, r, schedule);
	//backwardSolveWithReorderedMatrix(*UPerm, z, y, schedule);
	trsv_time += omp_get_wtime();

	double *p = MALLOC(double, A->m);
	copyVector(p, z, A->m);
	double rz = dot(r, z, A->m);
	int k = 1;

	double *Ap = MALLOC(double, A->m);

	while (k <= maxiter) {
		double old_rz = rz;

		spmv_time -= omp_get_wtime();
		A->multiplyWithVector(Ap, p); // Ap = A*p
		spmv_time += omp_get_wtime();

		double alpha = old_rz / dot(p, Ap, A->m);
		waxpby(A->m, x, 1, x, alpha, p); // x += alpha*p
		waxpby(A->m, r, 1, r, -alpha, Ap); // r -= alpha*Ap
		rel_err = norm(r, A->m) / normr0;
		if (rel_err < tol)
			break;

		trsv_time -= omp_get_wtime();
		forwardGSWithBarrier(*L, y, r, barrierSchedule, invPerm);
		backwardGSWithBarrier(*U, z, y, barrierSchedule, invPerm);
		//forwardSolveWithReorderedMatrix(*LPerm, y, r, schedule);
		//backwardSolveWithReorderedMatrix(*UPerm, z, y, schedule);
		trsv_time += omp_get_wtime();

		rz = dot(r, z, A->m);
		double beta = rz / old_rz;
		waxpby(A->m, p, 1, z, beta, p); // p = z + beta*p
		++k;
	}

	//reorderVectorOutOfPlaceWithInversePerm(x, xPerm, perm, A->m);

	printf("iter = %d, rel_err = %g\n", k, rel_err);

	double spmv_bytes = (k + 1) * (12. * A->getNnz() + (4. + 2 * 8) * A->m);
	double trsv_bytes = k
			* (12. * L->getNnz() + 12. * U->getNnz()
					+ (8. + 2 * 4 + 2 * 2 * 8) * L->m);
	printf("spmv_perf = %g gbps trsv_perf = %g gbps\n",
			spmv_bytes / spmv_time / 1e9, trsv_bytes / trsv_time / 1e9);

	//delete APerm;
	//delete LPerm;
	//delete UPerm;
	delete L;
	delete U;
	
	FREE(B->rowptr);
	//FREE(B->diagptr);
	FREE(B->colidx);
	//FREE(B->extptr);
	
			
	delete B;
	
	FREE (r);
	FREE (z);
	FREE (y);
	FREE (p);
	FREE (Ap);

	// FREE(bPerm);
	// FREE(xPerm);
}

void pcg_ilu0_barrier(CSR *A, double *x, const double *b, double tol,
		int maxiter) {
	double spmv_time = 0;
	double trsv_time = 0;
	double ilu0_time = 0;
	
        double time[9];
        CSR *B;
                
	for(int i = 0; i < 8; i++){
		B  =  test_ilu0_dep_opt(A, &time[i]);
		FREE(B->rowptr);
		//FREE(B->diagptr);
		FREE(B->colidx);
		//FREE(B->extptr);

	        delete B;
        }
        B  =  test_ilu0_dep_opt(A, &time[8]);
                                                            
	sort(time, time + 9);
	printf("median OPTIMIZED-INSPECTOR time %f\n", time[4]);
	
	
	
	LevelSchedule *barrierSchedule = new LevelSchedule;
	barrierSchedule->useBarrier = true;
	barrierSchedule->transitiveReduction = false;
	barrierSchedule->constructTaskGraph(*B);

	const int *perm = barrierSchedule->origToThreadContPerm;
	const int *invPerm = barrierSchedule->threadContToOrigPerm;
	//LevelSchedule schedule;
	//schedule.constructTaskGraph(*A);
	printf("parallelism = %g\n",
			(double) A->m / (barrierSchedule->levIndices.size() - 1));

	// Construct ILU0 pre-conditioner
	double *lu = MALLOC(double, A->getNnz());

        for(int num = 0; num < 10; num++){   
	ilu0_time -= omp_get_wtime();
	ilu0Barrier(lu, *A, barrierSchedule, invPerm);
	ilu0_time += omp_get_wtime();
	}  

	CSR LU(A->m, A->n, A->rowptr, A->colidx, lu);
	LU.computeInverseDiag();
	CSR *L = new CSR, *U = new CSR;
	splitLU(LU, L, U);
#pragma omp parallel for
	for (int i = 0; i < L->m; ++i) {
		L->idiag[i] = 1;
	}

	//CSR *APerm = A->permute(perm, invPerm);
	//CSR *LPerm = L->permute(perm, invPerm);
	//CSR *UPerm = U->permute(perm, invPerm);


	//double *bPerm = getReorderVectorWithInversePerm(b, invPerm, A->m);
	//double *xPerm = getReorderVectorWithInversePerm(x, invPerm, A->m);

	double *r = MALLOC(double, A->m);
	spmv_time -= omp_get_wtime();
	A->multiplyWithVector(r, -1, x, 1, b, 0); // r = b - A*x
	spmv_time += omp_get_wtime();
	double normr0 = norm(r, A->m);
	double rel_err = 1.;

	double *z = MALLOC(double, A->m);
	double *y = MALLOC(double, A->m);
	// z = M\r, where M is pre-conditioner
	trsv_time -= omp_get_wtime();

	forwardGSWithBarrier(*L, y, r, barrierSchedule, invPerm);
	backwardGSWithBarrier(*U, z, y, barrierSchedule, invPerm);

	//forwardSolveWithReorderedMatrix(*LPerm, y, r, schedule);
	//backwardSolveWithReorderedMatrix(*UPerm, z, y, schedule);
	trsv_time += omp_get_wtime();

	double *p = MALLOC(double, A->m);
	copyVector(p, z, A->m);
	double rz = dot(r, z, A->m);
	int k = 1;

	double *Ap = MALLOC(double, A->m);

	while (k <= maxiter) {
		double old_rz = rz;

		spmv_time -= omp_get_wtime();
		A->multiplyWithVector(Ap, p); // Ap = A*p
		spmv_time += omp_get_wtime();

		double alpha = old_rz / dot(p, Ap, A->m);
		waxpby(A->m, x, 1, x, alpha, p); // x += alpha*p
		waxpby(A->m, r, 1, r, -alpha, Ap); // r -= alpha*Ap
		rel_err = norm(r, A->m) / normr0;
		if (rel_err < tol)
			break;

		trsv_time -= omp_get_wtime();
		forwardGSWithBarrier(*L, y, r, barrierSchedule, invPerm);
		backwardGSWithBarrier(*U, z, y, barrierSchedule, invPerm);
		//forwardSolveWithReorderedMatrix(*LPerm, y, r, schedule);
		//backwardSolveWithReorderedMatrix(*UPerm, z, y, schedule);
		trsv_time += omp_get_wtime();

		rz = dot(r, z, A->m);
		double beta = rz / old_rz;
		waxpby(A->m, p, 1, z, beta, p); // p = z + beta*p
		++k;
	}

	//reorderVectorOutOfPlaceWithInversePerm(x, xPerm, perm, A->m);

	printf("iter = %d, rel_err = %g\n", k, rel_err);

	double spmv_bytes = (k + 1) * (12. * A->getNnz() + (4. + 2 * 8) * A->m);
	double trsv_bytes = k
			* (12. * L->getNnz() + 12. * U->getNnz()
					+ (8. + 2 * 4 + 2 * 2 * 8) * L->m);
	
	double ilu0_bytes = 10*(16.*A->getNnz() + 8.*A->m + 4.*A->getNnz());
	printf("spmv_perf = %g gbps trsv_perf = %g gbps ilu0_perf = %g gbps\n",
	                                                        spmv_bytes / spmv_time / 1e9, trsv_bytes / trsv_time / 1e9, ilu0_bytes / ilu0_time / 1e9);
	                                                        
	
	//printf("spmv_perf = %g gbps trsv_perf = %g gbps\n",
	//		spmv_bytes / spmv_time / 1e9, trsv_bytes / trsv_time / 1e9);
			
	//delete APerm;
	//delete LPerm;
	//delete UPerm;
	delete L;
	delete U;
	FREE(B->rowptr);
	//FREE(B->diagptr);
	FREE(B->colidx);
	//FREE(B->extptr);
	
	delete B;

	FREE (r);
	FREE (z);
	FREE (y);
	FREE (p);
	FREE (Ap);

	//FREE(bPerm);
	//FREE(xPerm);
}


void pcg_symgs_barrier_reordered(CSR *A, double *x, const double *b, double tol,
		int maxiter) {
	double spmv_time = 0;
	double trsv_time = 0;

	// Construct SymGS pre-conditioner
	CSR *L = new CSR, *U = new CSR;
	splitLU(*A, L, U);
#pragma omp parallel for
	for (int i = 0; i < U->m; ++i) {
		for (int j = U->rowptr[i]; j < U->rowptr[i + 1]; ++j) {
			U->values[j] *= U->idiag[i];
		}
		U->idiag[i] = 1;
	}

	LevelSchedule schedule;

	LevelSchedule *barrierSchedule = new LevelSchedule;
	schedule.useBarrier = true;
	schedule.transitiveReduction = false;

        double time[9];
        CSR *B;
                
	for(int i = 0; i < 8; i++){
		B  =  test_ilu0_dep_opt(A, &time[i]);
		FREE(B->rowptr);
		//FREE(B->diagptr);
		FREE(B->colidx);
		//FREE(B->extptr);

	        delete B;
        }
        B  =  test_ilu0_dep_opt(A, &time[8]);
                                                            
	sort(time, time + 9);
	printf("median OPTIMIZED-INSPECTOR time %f\n", time[4]);
	

	schedule.constructTaskGraph(*B);
	printf("parallelism = %g\n",
			(double) A->m / (schedule.levIndices.size() - 1));

	const int *perm = schedule.origToThreadContPerm;
	const int *invPerm = schedule.threadContToOrigPerm;

	CSR *APerm = A->permute(perm, invPerm);
	CSR *LPerm = L->permute(perm, invPerm);
	CSR *UPerm = U->permute(perm, invPerm);

	delete L;
	delete U;
	FREE(B->rowptr);
//	FREE(B->diagptr);
	FREE(B->colidx);
//	FREE(B->extptr);

					
	delete B;

	double *bPerm = getReorderVectorWithInversePerm(b, invPerm, A->m);
	double *xPerm = getReorderVectorWithInversePerm(x, invPerm, A->m);

	double *r = MALLOC(double, A->m);
	spmv_time -= omp_get_wtime();
	APerm->multiplyWithVector(r, -1, xPerm, 1, bPerm, 0); // r = b - A*x
	spmv_time += omp_get_wtime();
	double normr0 = norm(r, A->m);
	double rel_err = 1.;

	double *z = MALLOC(double, A->m);
	double *y = MALLOC(double, A->m);
	// z = M\r, where M is pre-conditioner
	trsv_time -= omp_get_wtime();
	forwardSolveWithReorderedMatrixBarrier(*LPerm, y, r, &schedule);
	backwardSolveWithReorderedMatrixBarrier(*UPerm, z, y, &schedule);
	trsv_time += omp_get_wtime();

	double *p = MALLOC(double, A->m);
	copyVector(p, z, A->m);
	double rz = dot(r, z, A->m);
	int k = 1;

	double *Ap = MALLOC(double, A->m);

	while (k <= maxiter) {
		double old_rz = rz;

		spmv_time -= omp_get_wtime();
		APerm->multiplyWithVector(Ap, p); // Ap = A*p
		spmv_time += omp_get_wtime();

		double alpha = old_rz / dot(p, Ap, A->m);
		waxpby(A->m, xPerm, 1, xPerm, alpha, p); // x += alpha*p
		waxpby(A->m, r, 1, r, -alpha, Ap); // r -= alpha*Ap
		rel_err = norm(r, A->m) / normr0;
		if (rel_err < tol)
			break;

		trsv_time -= omp_get_wtime();
		forwardSolveWithReorderedMatrixBarrier(*LPerm, y, r, &schedule);
		backwardSolveWithReorderedMatrixBarrier(*UPerm, z, y, &schedule);
		trsv_time += omp_get_wtime();

		rz = dot(r, z, A->m);
		double beta = rz / old_rz;
		waxpby(A->m, p, 1, z, beta, p); // p = z + beta*p
		++k;
	}

	reorderVectorOutOfPlaceWithInversePerm(x, xPerm, perm, A->m);

	printf("iter = %d, rel_err = %g\n", k, rel_err);

	double spmv_bytes = (k + 1) * (12. * A->getNnz() + (4. + 2 * 8) * A->m);
	double trsv_bytes = k
			* (12. * LPerm->getNnz() + 12. * UPerm->getNnz()
					+ (8. + 2 * 4 + 2 * 2 * 8) * LPerm->m);
	printf("spmv_perf = %g gbps trsv_perf = %g gbps\n",
			spmv_bytes / spmv_time / 1e9, trsv_bytes / trsv_time / 1e9);

	delete APerm;
	delete LPerm;
	delete UPerm;

	FREE (r);
	FREE (z);
	FREE (y);
	FREE (p);
	FREE (Ap);

	FREE(bPerm);
	FREE(xPerm);
}

void pcg_ilu0_barrier_reordered(CSR *A, double *x, const double *b, double tol, int maxiter) {
	double spmv_time = 0;
	double trsv_time = 0;
	double ilu0_time = 0;
	double pcg_time  = 0;
	LevelSchedule schedule;
	schedule.useBarrier = true;
	schedule.transitiveReduction = false;

        double time[9];
        CSR *B, *C;

        for(int i = 0; i < 8; i++){
                B  =  test_gs_dep(A, &time[i]);
                FREE(B->rowptr);
                //FREE(B->diagptr);
                FREE(B->colidx);
                //FREE(B->extptr);

                delete B;
        }
        B  =  test_gs_dep(A, &time[8]);

        sort(time, time + 9);
        printf("median GS-PRECISE-DEP-TEST time %f\n", time[4]);
	time[4] -= omp_get_wtime();
        //LevelSchedule schedule;
        schedule.constructTaskGraph(*B);
        time[4] += omp_get_wtime();
        printf("median GS-TOTAL-INSPECTOR time %f\n", time[4]);
        printf("parallelism = %g\n",
                        (double) A->m / (schedule.levIndices.size() - 1));


         for(int i = 0; i < 8; i++){
                C  =  test_ilu_dep_precise(A, &time[i]);
                FREE(C->rowptr);
                //FREE(B->diagptr);
                FREE(C->colidx);
                //FREE(B->extptr);

                delete C;
        }
        C  =  test_ilu_dep_precise(A, &time[8]);

        sort(time, time + 9);
        printf("median ILU-PRECISE-DEP-TEST time %f\n", time[4]);
	time[4] -= omp_get_wtime();
        LevelSchedule schedule2;
        schedule2.useBarrier = true;
        schedule2.transitiveReduction = false;
	schedule2.constructTaskGraph(*C);
        time[4] += omp_get_wtime();
        printf("median ILU-INSPECTOR time %f\n", time[4]);
        printf("precise parallelism = %g\n",
                        (double) A->m / (schedule2.levIndices.size() - 1));

        // Construct ILU0 pre-conditioner
        double *lu = MALLOC(double, A->getNnz());

        for(int num = 0; num < 10; num++){
                ilu0_time -= omp_get_wtime();
                ilu0BarrierReordered(lu, *A, &schedule2, schedule2.threadContToOrigPerm);
		ilu0_time += omp_get_wtime();
        }
        //ilu0_time /= 10;
        printf("median ilu0 barrier time %f\n", ilu0_time/10);        
	



	const int *perm = schedule.origToThreadContPerm;
	const int *invPerm = schedule.threadContToOrigPerm;
	CSR *APerm = A->permute(perm, invPerm);

	// Construct ILU0 pre-conditioner
	//double *lu = MALLOC(double, A->getNnz());
	                                     
	
	CSR LU(A->m, A->n, A->rowptr, A->colidx, lu);
	LU.computeInverseDiag();

	CSR *L = new CSR, *U = new CSR;
	splitLU(LU, L, U);
#pragma omp parallel for
	for (int i = 0; i < L->m; ++i) {
		L->idiag[i] = 1;
	}



	//CSR *APerm = A->permute(perm, invPerm);
	CSR *LPerm = L->permute(perm, invPerm);
	CSR *UPerm = U->permute(perm, invPerm);

	delete L;
	delete U;
	FREE(B->rowptr);
	FREE(C->rowptr);
	FREE(B->colidx);
	FREE(C->colidx);

	delete B;
	delete C;

	double *bPerm = getReorderVectorWithInversePerm(b, invPerm, A->m);
	double *xPerm = getReorderVectorWithInversePerm(x, invPerm, A->m);

	double *r = MALLOC(double, A->m);
	spmv_time -= omp_get_wtime();
	APerm->multiplyWithVector(r, -1, xPerm, 1, bPerm, 0); // r = b - A*x
	spmv_time += omp_get_wtime();
	double normr0 = norm(r, A->m);
	double rel_err = 1.;

	double *z = MALLOC(double, A->m);
	double *y = MALLOC(double, A->m);
	// z = M\r, where M is pre-conditioner
	trsv_time -= omp_get_wtime();
	forwardSolveWithReorderedMatrixBarrier(*LPerm, y, r, &schedule);
	backwardSolveWithReorderedMatrixBarrier(*UPerm, z, y, &schedule);
	trsv_time += omp_get_wtime();

	double *p = MALLOC(double, A->m);
	copyVector(p, z, A->m);
	double rz = dot(r, z, A->m);
	int k = 1;

	double *Ap = MALLOC(double, A->m);

	while (k <= maxiter) {
		pcg_time -= omp_get_wtime();
		double old_rz = rz;

		spmv_time -= omp_get_wtime();
		APerm->multiplyWithVector(Ap, p); // Ap = A*p
		spmv_time += omp_get_wtime();

		double alpha = old_rz / dot(p, Ap, A->m);
		waxpby(A->m, xPerm, 1, xPerm, alpha, p); // x += alpha*p
		waxpby(A->m, r, 1, r, -alpha, Ap); // r -= alpha*Ap
		rel_err = norm(r, A->m) / normr0;
		if (rel_err < tol){
			pcg_time += omp_get_wtime();
			break;
		}
		trsv_time -= omp_get_wtime();
		forwardSolveWithReorderedMatrixBarrier(*LPerm, y, r, &schedule);
		backwardSolveWithReorderedMatrixBarrier(*UPerm, z, y, &schedule);
		trsv_time += omp_get_wtime();

		rz = dot(r, z, A->m);
		double beta = rz / old_rz;
		waxpby(A->m, p, 1, z, beta, p); // p = z + beta*p
		pcg_time += omp_get_wtime();
		++k;
	}

	reorderVectorOutOfPlaceWithInversePerm(x, xPerm, perm, A->m);

	printf("Total pcg time: %f, Average time per iteration  %f\n", pcg_time, pcg_time/k);
	printf("iter = %d, rel_err = %g\n", k, rel_err);

	double spmv_bytes = (k + 1) * (12. * A->getNnz() + (4. + 2 * 8) * A->m);
	double trsv_bytes = k
			* (12. * LPerm->getNnz() + 12. * UPerm->getNnz()
					+ (8. + 2 * 4 + 2 * 2 * 8) * LPerm->m);

        double ilu0_bytes = 10*(16.*A->getNnz() + 8.*A->m + 4.*A->getNnz());   
	printf("spmv_perf = %g gbps trsv_perf = %g gbps ilu0_perf = %g gbps\n",
                                                                                spmv_bytes / spmv_time / 1e9, trsv_bytes / trsv_time / 1e9, ilu0_bytes / ilu0_time / 1e9);
                                                                                
        printf("spmv_time = %g s trsv_tme = %g s\n",
                        spmv_time , trsv_time);
//	printf("spmv_perf = %g gbps trsv_perf = %g gbps\n",
//			spmv_bytes / spmv_time / 1e9, trsv_bytes / trsv_time / 1e9);

	delete APerm;
	delete LPerm;
	delete UPerm;

	FREE (r);
	FREE (z);
	FREE (y);
	FREE (p);
	FREE (Ap);

	FREE(bPerm);
	FREE(xPerm);
}




int main(int argc, char *argv[]) {
	int m = argc > 1 ? atoi(argv[1]) : 64; // default input is 64^3 27-pt 3D Lap.
	if (argc < 2) {
		fprintf(stderr,
				"Using default 64^3 27-pt 3D Laplacian matrix\n"
						"-- Usage examples --\n"
						"  %s 128 : 128^3 27-pt 3D Laplacian matrix\n"
						"  %s inline_1: run with inline_1.mtx matrix in matrix market format\n\n",
				argv[0], argv[0]);
	}

	char buf[1024];
	sprintf(buf, "%d", m);
	printf("input=%s\n", argc > 1 ? argv[1] : buf);

	CSR A(argc > 1 ? argv[1] : buf);
	double *x = MALLOC(double, A.m);
	double *y = MALLOC(double, A.m);
	double *x_ref = MALLOC(double, A.m);
	double *x_ref2 = MALLOC(double, A.m);
	double *b = NULL;
	if (argc > 1 && strlen(argv[1]) >= 4) {
		// load rhs file if it exists
		char buf2[1024];
		strncpy(buf2, argv[1], strlen(argv[1]) - 4);
		int m, n;
		loadVectorMatrixMarket((string(buf2) + "_b.mtx").c_str(), &b, &m, &n);
	}
	if (!b) {
		b = MALLOC(double, A.m);
		for (int i = 0; i < A.m; ++i)
			b[i] = 1;
	}

	double tol = 1e-7;
	int maxiter = 20000;

/*	for (int i = 0; i < A.m; ++i)
		x_ref[i] = 0;
	pcg_symgs(&A, x_ref, b, tol, maxiter); //changed by Anand reference values

	for (int i = 0; i < A.m; ++i)
		x[i] = 0;
	pcg_symgs_opt(&A, x, b, tol, maxiter);

	correctnessCheck<double>(x_ref, x, A.m, 1e-7);
*/
	for (int i = 0; i < A.m; ++i)
		x_ref2[i] = 0;
	pcg_ilu0(&A, x_ref2, b, tol, maxiter);


	for (int i = 0; i < A.m; ++i)
		x[i] = 0;
	pcg_ilu0_opt(&A, x, b, tol, maxiter);

//	correctnessCheck<double>(x_ref2, x, A.m); //changed by Anand reference values

/*	for (int i = 0; i < A.m; ++i)
		y[i] = 0;
	pcg_symgs_barrier(&A, y, b, tol, maxiter);

	correctnessCheck<double>(x_ref, y, A.m);


	for (int i = 0; i < A.m; ++i)
		y[i] = 0;
	pcg_ilu0_barrier(&A, y, b, tol, maxiter);

	correctnessCheck<double>(x_ref2, y, A.m); //changed by Anand reference values


	for (int i = 0; i < A.m; ++i)
		y[i] = 0;
	pcg_symgs_barrier_reordered(&A, y, b, tol, maxiter);

	correctnessCheck<double>(x_ref, y, A.m);
*/



	for (int i = 0; i < A.m; ++i)
		y[i] = 0;
	pcg_ilu0_barrier_reordered(&A, y, b, tol, maxiter);

//	correctnessCheck<double>(x_ref2, y, A.m); //changed by Anand reference values

	FREE(x);
	FREE(b);
	FREE(x_ref);
	FREE(y);
  	synk::Barrier::deleteInstance();
	return 0;
}

