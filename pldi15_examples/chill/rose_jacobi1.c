#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define N 512

int main()
{
  int t2;
  int t6;
  int t4;
  int _t2;
  int _t1;
  int i;
  int t;
  float a[512UL][512UL];
  for (t2 = 4; t2 <= 580; t2 += 64) 
    for (t4 = __rose_gt(2,t2 - 511); t4 <= __rose_lt(100,t2 + 61); t4 += 1) 
      for (t6 = __rose_gt(t4 + 2,t2); t6 <= __rose_lt(t2 + 63,t4 + 511); t6 += 1) 
        a[t4][t6 - t4] = ((a[t4 - 1][t6 - t4 - 1] + a[t4 - 1][t6 - t4]) + a[t4 - 1][t6 - t4 + 1]);
  return 0;
}
