#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA1_(_t20,_t21) _P_DATA1[31225 * _t20 + _t21]
#define _P_DATA1__(_t20,_t21) _P_DATA1[31225 * _t20 + _t21 + 1]
#define col_(_t24) col[_t24]
#define index_(i) index[i]
#define index__(i) index[i + 1]
#define N 1000
#include <stdio.h>
#include <stdlib.h>
struct a_list 
{
  int col_[2];
  float A[1];
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void csb(int m,int n,int *index,int *col,float *x,float *y,float *A)
{
  int t6;
  int t4;
  int t2;
  int newVariable0;
  int In_1;
  struct a_list *_P_DATA5;
  struct mk *_P_DATA4;
  float *A_prime;
  struct a_list *_P1[31225];
  int *_P_DATA3;
  int *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  int _t18;
  int _t17;
  int _t16;
  int _t15;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t9;
  int _t8;
  int _t7;
  int _t5;
  int _t4;
  int _t3;
  int k;
  int _t2;
  int _t1;
  int i;
  int j;
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * 975000626)));
  _P_DATA1[0] = 0;
  chill_count_1 = 0;
  for (_t20 = 0; _t20 <= 31224; _t20 += 1) 
    for (_t21 = 0; _t21 <= 31224; _t21 += 1) {
      _P1[31225 * _t20 + 1 * _t21] = 0;
      _P_DATA1[31225 * _t20 + 1 * _t21 + 1] = 0;
    }
  for (t2 = 0; t2 <= 31224; t2 += 1) 
    for (t4 = 0; t4 <= 1; t4 += 1) 
      for (t6 = index_(2 * t2 + t4); t6 <= index__(2 * t2 + t4) - 1; t6 += 1) {
        _t21 = col_(t6) / 2;
        _t23 = col_(t6) - 2 * _t21;
        _P_DATA5 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
        _P_DATA5 -> next = _P1[31225 * t2 + 1 * _t21];
        _P1[31225 * t2 + 1 * _t21] = _P_DATA5;
        _P1[31225 * t2 + 1 * _t21] -> A[0] = 0;
        _P1[31225 * t2 + 1 * _t21] -> col_[0] = t4;
        _P1[31225 * t2 + 1 * _t21] -> col_[1] = _t23;
        chill_count_1 += 1;
        _P_DATA1[31225 * t2 + 1 * _t21 + 1] += 1;
        _P1[31225 * t2 + 1 * _t21] -> A[0] = A[t6];
      }
  _P_DATA3 = ((int *)(malloc(sizeof(int ) * chill_count_1)));
  A_prime = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 1))));
  for (t2 = 0; t2 <= 31224; t2 += 1) 
    for (t4 = 0; t4 <= 31224; t4 += 1) {
      for (newVariable0 = 1 - _P_DATA1[31225 * t2 + 1 * t4 + 1]; newVariable0 <= 0; newVariable0 += 1) {
        _P_DATA2[_P_DATA1[31225 * t2 + 1 * t4] - newVariable0] = _P1[31225 * t2 + 1 * t4] -> col_[0];
        _P_DATA3[_P_DATA1[31225 * t2 + 1 * t4] - newVariable0] = _P1[31225 * t2 + 1 * t4] -> col_[1];
        A_prime[1 * (_P_DATA1[31225 * t2 + 1 * t4] - newVariable0)] = _P1[31225 * t2 + 1 * t4] -> A[0];
        _P_DATA5 = _P1[31225 * t2 + 1 * t4] -> next;
        free(_P1[31225 * t2 + 1 * t4]);
        _P1[31225 * t2 + 1 * t4] = _P_DATA5;
      }
      _P_DATA1[31225 * t2 + 1 * t4 + 1] += _P_DATA1[31225 * t2 + 1 * t4];
    }
  for (t2 = 0; t2 <= 31224; t2 += 1) 
    for (t4 = 0; t4 <= 31224; t4 += 1) 
      for (t6 = _P_DATA1_(t2,t4); t6 <= _P_DATA1__(t2,t4) - 1; t6 += 1) 
        y[2 * t2 + _P_DATA2[t6]] += (A_prime[t6 * 1] * x[2 * t4 + _P_DATA3[t6]]);
  free(_P_DATA1);
  free(_P_DATA2);
  free(_P_DATA3);
  free(A_prime);
  free(_P_DATA4);
}

int main()
{
  int m = 4;
  int n = 1000;
  int *index;
  float *x;
  float *y;
  int *col;
  float *A;
  csb(m,n,index,col,x,y,A);
  return 0;
}
