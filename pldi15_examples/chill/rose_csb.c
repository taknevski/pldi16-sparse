#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define col_i_(k) col_i[_t11]
#define index_(i,j) index[i][j]
#define index__(i,j) index[i][j + 1]
#define N 1000

void csb(int m,int n,int **index,int *col_i,int *col_j,float **y,float **x,float *A)
{
  int t8;
  int t6;
  int t4;
  int t2;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t6;
  int _t5;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  int k;
  int l;
  for (t2 = 0; t2 <= n - 1; t2 += 1) 
    for (t4 = 0; t4 <= n - 1; t4 += 1) 
      for (t6 = index_(t2,t4); t6 <= index__(t2,t4) - 1; t6 += 1) {
        y[(2 * t2) + col_i[t6]][0] += (A[t6] * x[(2 * t4) + col_j[t6]][0]);
        y[(2 * t2) + col_i[t6]][0 + 1] += (A[t6] * x[(2 * t4) + col_j[t6]][0 + 1]);
        y[(2 * t2) + col_i[t6]][0 + 2] += (A[t6] * x[(2 * t4) + col_j[t6]][0 + 2]);
        y[(2 * t2) + col_i[t6]][0 + 3] += (A[t6] * x[(2 * t4) + col_j[t6]][0 + 3]);
      }
}

int main()
{
  int m = 4;
  int n = 1000;
  int **index;
  float **x;
  float **y;
  int *col_i;
  int *col_j;
  float *A;
  csb(m,n,index,col_i,col_j,y,x,A);
  return 0;
}
