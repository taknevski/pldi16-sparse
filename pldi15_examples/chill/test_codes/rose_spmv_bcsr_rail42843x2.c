#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA1_(_t18) _P_DATA1[_t18]
#define _P_DATA1__(_t18) _P_DATA1[_t18 + 1]
#define _P_DATA2_(_t26,_t27) _P_DATA2[1 * _t27]
#define _P_DATA2___(_t26,_t27) _P_DATA2[1 * _t27 + 1]
#define col_(_t22) col[1 * _t22]
#define col___(_t22) col[1 * _t22 + 1]
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
#include <stdio.h>
#include <stdlib.h>

struct a_list 
{
  int col_;
  float a[6];
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void spmv_bcsr(int m, int n, int *index, float *a, float *y, float *x, int *col,int nnz)
{
  int t8;
  int t6;
  int t4;
  int t2;
  int _t89;
  int _t88;
  int _t87;
  int _t86;
  int _t85;
  int _t84;
  int _t83;
  int _t82;
  int _t81;
  int _t80;
  int _t79;
  int _t78;
  int _t77;
  int _t76;
  int _t75;
  int _t74;
  int _t73;
  int _t72;
  int _t71;
  int _t70;
  int _t69;
  int _t68;
  int _t67;
  int _t66;
  int _t65;
  int _t64;
  int _t63;
  int _t62;
  int _t61;
  int _t60;
  int _t59;
  int _t58;
  int _t57;
  int _t56;
  int _t55;
  int _t54;
  int _t53;
  int _t52;
  int _t51;
  int _t50;
  int _t49;
  int _t48;
  int _t47;
  int _t46;
  int _t45;
  int _t44;
  int _t43;
  int _t42;
  int _t41;
  int _t40;
  int _t39;
  int _t38;
  int _t37;
  int _t36;
  int _t35;
  float _P2[4];
  int _t34;
  int _t33;
  int _t32;
  int _t31;
  int _t30;
  float _P1[4];
  int _t29;
  int _t28;
  int _t27;
  int _t26;
  int In_3;
  int In_2;
  int In_1;
  int _t25;
  int _t24;
  int _t23;
  struct a_list *_P_DATA5;
  int newVariable1;
  int newVariable0;
  struct mk *_P_DATA4;
  float *a_prime;
  struct a_list *_P_DATA3;
  int *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  int _t18;
  int _t17;
  int _t16;
  int _t15;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t6;
  int _t4;
  int _t5;
  int k;
  int _t2;
  int _t1;
  int i;
  int j;
struct timeval t0, t1, t_2;
gettimeofday(&t0, NULL);
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * 1429)));
  _P_DATA1[0] = 0;
  _P_DATA3 = 0;
  _P_DATA4 = ((struct mk *)(malloc(sizeof(struct mk ) * 546305)));
  chill_count_1 = 0;
  _P_DATA1[0] = 0;
  for (t2 = 0; t2 <= 1427; t2 += 1) {
    for (t4 = 0; t4 <= 2; t4 += 1) 
      for (t6 = index_(3 * t2 + t4); t6 <= index__(3 * t2 + t4) - 1; t6 += 1) {
        _t19 = (col_(t6) - 0) / 2;
        _P_DATA4[_t19] . ptr = 0;
      }
    for (t4 = 0; t4 <= 2; t4 += 1) 
      for (t6 = index_(3 * t2 + t4); t6 <= index__(3 * t2 + t4) - 1; t6 += 1) {
        _t19 = (col_(t6) - 0) / 2;
        _t21 = (col_(t6) - 0) % 2;
        if (_P_DATA4[_t19] . ptr == 0) {
          _P_DATA5 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
          _P_DATA5 -> next = _P_DATA3;
          _P_DATA3 = _P_DATA5;
          _P_DATA4[_t19] . ptr = _P_DATA3;
          for (newVariable0 = 0; newVariable0 <= 2; newVariable0 += 1) 
            for (newVariable1 = 0; newVariable1 <= 1; newVariable1 += 1) 
              _P_DATA4[_t19] . ptr -> a[2 * newVariable0 + 1 * newVariable1] = 0;
          _P_DATA4[_t19] . ptr -> col_ = _t19;
          chill_count_1 += 1;
        }
        _P_DATA4[_t19] . ptr -> a[2 * t4 + 1 * _t21] = a[t6];
      }
    _P_DATA1[t2 + 1] = chill_count_1;
  }
  for (t2 = -chill_count_1 + 1; t2 <= 0; t2 += 1) {
    if (chill_count_1 + t2 <= 1) {
      _P_DATA2 = ((int *)(malloc(sizeof(int ) * chill_count_1)));
      a_prime = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 6))));
    }
    for (t4 = 0; t4 <= 2; t4 += 1) 
      for (t6 = 0; t6 <= 1; t6 += 1) 
        a_prime[-t2 * 6 + 2 * t4 + 1 * t6] = _P_DATA3 -> a[2 * t4 + 1 * t6];
    _P_DATA2[-t2] = _P_DATA3 -> col_;
    _P_DATA5 = _P_DATA3 -> next;
    free(_P_DATA3);
    _P_DATA3 = _P_DATA5;
  }
gettimeofday(&t1, NULL);
double elapsed = t1.tv_sec - t0.tv_sec  + (t1.tv_usec - t0.tv_usec) / 1000000.0;
printf("Inspector Time: %.6lf seconds\n", elapsed);
printf("Starting actual computation\n");
gettimeofday(&t1, NULL);
  for (t2 = 0; t2 <= 1427; t2 += 1) 
    for (t4 = _P_DATA1_(t2); t4 <= _P_DATA1__(t2) - 1; t4 += 1) {
      _P2[3 * t2 - 3 * t2] = y[3 * t2];
      _P2[3 * t2 + 1 - 3 * t2] = y[3 * t2 + 1];
      _P2[3 * t2 + 2 - 3 * t2] = y[3 * t2 + 2];
      t8 = 2 * _P_DATA2_(t2,t4);
      _P1[t8 - 2 * _P_DATA2_(t2,t4)] = x[t8];
      _P1[t8 + 1 - 2 * _P_DATA2_(t2,t4)] = x[t8 + 1];
      _P1[t8 - 2 * _P_DATA2_(t2,t4)] = x[t8];
      _P1[t8 + 1 - 2 * _P_DATA2_(t2,t4)] = x[t8 + 1];
      _P1[t8 - 2 * _P_DATA2_(t2,t4)] = x[t8];
      _P1[t8 + 1 - 2 * _P_DATA2_(t2,t4)] = x[t8 + 1];
      _P2[3 * t2 + 0 - 3 * t2] += a_prime[t4 * 6 + 2 * 0 + 1 * 0] * _P1[2 * _P_DATA2[t4] + 0 - 2 * _P_DATA2_(t2,t4)];
      _P2[3 * t2 + 0 - 3 * t2] += a_prime[t4 * 6 + 2 * 0 + 1 * (0 + 1)] * _P1[2 * _P_DATA2[t4] + (0 + 1) - 2 * _P_DATA2_(t2,t4)];
      _P2[3 * t2 + (0 + 1) - 3 * t2] += a_prime[t4 * 6 + 2 * (0 + 1) + 1 * 0] * _P1[2 * _P_DATA2[t4] + 0 - 2 * _P_DATA2_(t2,t4)];
      _P2[3 * t2 + (0 + 1) - 3 * t2] += a_prime[t4 * 6 + 2 * (0 + 1) + 1 * (0 + 1)] * _P1[2 * _P_DATA2[t4] + (0 + 1) - 2 * _P_DATA2_(t2,t4)];
      _P2[3 * t2 + (0 + 2) - 3 * t2] += a_prime[t4 * 6 + 2 * (0 + 2) + 1 * 0] * _P1[2 * _P_DATA2[t4] + 0 - 2 * _P_DATA2_(t2,t4)];
      _P2[3 * t2 + (0 + 2) - 3 * t2] += a_prime[t4 * 6 + 2 * (0 + 2) + 1 * (0 + 1)] * _P1[2 * _P_DATA2[t4] + (0 + 1) - 2 * _P_DATA2_(t2,t4)];
      y[3 * t2] = _P2[3 * t2 - 3 * t2];
      y[3 * t2 + 1] = _P2[3 * t2 + 1 - 3 * t2];
      y[3 * t2 + 2] = _P2[3 * t2 + 2 - 3 * t2];
    }
gettimeofday(&t_2, NULL);
elapsed = t_2.tv_sec - t1.tv_sec + (t_2.tv_usec - t1.tv_usec) / 1000000.0;
printf("%.6lf seconds elapsed\n", elapsed);
double mflop_count = 1e-6 * (double) nnz * (double) 2.0;
printf("Performance :%.6lf MFLOPS\n", mflop_count / elapsed);
  free(_P_DATA1);
  free(_P_DATA2);
  free(a_prime);
  free(_P_DATA4);
}

