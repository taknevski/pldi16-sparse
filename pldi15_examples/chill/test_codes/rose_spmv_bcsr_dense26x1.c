#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA1_(_t13) _P_DATA1[_t13]
#define _P_DATA1__(_t13) _P_DATA1[_t13 + 1]
#define col_(_t16) col[1 * _t16]
#define col___(_t16) col[1 * _t16 + 1]
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
#include <stdio.h>
#include <stdlib.h>

struct a_list 
{
  unsigned short col_;
  float a[6];
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void spmv_bcsr(int m, int n, int *index, float *a, float *y, float *x, int *col,int nnz)
{
  int t6;
  int t4;
  int t2;
  int _t50;
  int _t49;
  int _t48;
  int _t47;
  int _t46;
  int _t45;
  int _t44;
  int _t43;
  int _t42;
  int _t41;
  int _t40;
  int _t39;
  int _t38;
  int _t37;
  int _t36;
  int _t35;
  int _t34;
  int _t33;
  int _t32;
  int _t31;
  int _t30;
  int _t29;
  int _t28;
  int _t27;
  int _t26;
  int _t25;
  int _t24;
  int _t23;
  float _P1[8];
  int _t22;
  int _t21;
  int _t20;
  int In_2;
  int In_1;
  int _t19;
  int _t18;
  int _t17;
  struct a_list *_P_DATA5;
  int newVariable0;
  struct mk *_P_DATA4;
  float *a_prime;
  struct a_list *_P_DATA3;
  unsigned short *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t16;
  int _t15;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t6;
  int _t7;
  int _t5;
  int k;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
struct timeval t0, t1, t_2;
gettimeofday(&t0, NULL);
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * 334)));
  _P_DATA1[0] = 0;
  _P_DATA3 = 0;
  _P_DATA4 = ((struct mk *)(malloc(sizeof(struct mk ) * 2000)));
  chill_count_1 = 0;
  _P_DATA1[0] = 0;
  for (t2 = 0; t2 <= 332; t2 += 1) {
    for (t4 = 0; t4 <= 5; t4 += 1) 
      for (t6 = index_(6 * t2 + t4); t6 <= index__(6 * t2 + t4) - 1; t6 += 1) {
        _t14 = col_(t6);
        _P_DATA4[_t14] . ptr = 0;
      }
    for (t4 = 0; t4 <= 5; t4 += 1) 
      for (t6 = index_(6 * t2 + t4); t6 <= index__(6 * t2 + t4) - 1; t6 += 1) {
        _t14 = col_(t6);
        if (_P_DATA4[_t14] . ptr == 0) {
          _P_DATA5 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
          _P_DATA5 -> next = _P_DATA3;
          _P_DATA3 = _P_DATA5;
          _P_DATA4[_t14] . ptr = _P_DATA3;
          for (newVariable0 = 0; newVariable0 <= 5; newVariable0 += 1) 
            _P_DATA4[_t14] . ptr -> a[1 * newVariable0] = 0;
          _P_DATA4[_t14] . ptr -> col_ = _t14;
          chill_count_1 += 1;
        }
        _P_DATA4[_t14] . ptr -> a[1 * t4] = a[t6];
      }
    _P_DATA1[t2 + 1] = chill_count_1;
  }
  for (t2 = -chill_count_1 + 1; t2 <= 0; t2 += 1) {
    if (chill_count_1 + t2 <= 1) {
      _P_DATA2 = ((unsigned short *)(malloc(sizeof(unsigned short ) * chill_count_1)));
      a_prime = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 6))));
    }
    for (t4 = 0; t4 <= 5; t4 += 1) 
      a_prime[-t2 * 6 + 1 * t4] = _P_DATA3 -> a[1 * t4];
    _P_DATA2[-t2] = _P_DATA3 -> col_;
    _P_DATA5 = _P_DATA3 -> next;
    free(_P_DATA3);
    _P_DATA3 = _P_DATA5;
  }
gettimeofday(&t1, NULL);
double elapsed = t1.tv_sec - t0.tv_sec  + (t1.tv_usec - t0.tv_usec) / 1000000.0;
printf("Inspector Time: %.6lf seconds\n", elapsed);
printf("Starting actual computation\n");
gettimeofday(&t1, NULL);
  for (t2 = 0; t2 <= 332; t2 += 1) 
    for (t4 = _P_DATA1_(t2); t4 <= _P_DATA1__(t2) - 1; t4 += 1) {
      _P1[6 * t2 - 6 * t2] = y[6 * t2];
      _P1[6 * t2 + 1 - 6 * t2] = y[6 * t2 + 1];
      _P1[6 * t2 + 2 - 6 * t2] = y[6 * t2 + 2];
      _P1[6 * t2 + 3 - 6 * t2] = y[6 * t2 + 3];
      _P1[6 * t2 + 4 - 6 * t2] = y[6 * t2 + 4];
      _P1[6 * t2 + 5 - 6 * t2] = y[6 * t2 + 5];
      _P1[6 * t2 + 0 - 6 * t2] += a_prime[t4 * 6 + 1 * 0] * x[_P_DATA2[t4]];
      _P1[6 * t2 + (0 + 1) - 6 * t2] += a_prime[t4 * 6 + 1 * (0 + 1)] * x[_P_DATA2[t4]];
      _P1[6 * t2 + (0 + 2) - 6 * t2] += a_prime[t4 * 6 + 1 * (0 + 2)] * x[_P_DATA2[t4]];
      _P1[6 * t2 + (0 + 3) - 6 * t2] += a_prime[t4 * 6 + 1 * (0 + 3)] * x[_P_DATA2[t4]];
      _P1[6 * t2 + (0 + 4) - 6 * t2] += a_prime[t4 * 6 + 1 * (0 + 4)] * x[_P_DATA2[t4]];
      _P1[6 * t2 + (0 + 5) - 6 * t2] += a_prime[t4 * 6 + 1 * (0 + 5)] * x[_P_DATA2[t4]];
      y[6 * t2] = _P1[6 * t2 - 6 * t2];
      y[6 * t2 + 1] = _P1[6 * t2 + 1 - 6 * t2];
      y[6 * t2 + 2] = _P1[6 * t2 + 2 - 6 * t2];
      y[6 * t2 + 3] = _P1[6 * t2 + 3 - 6 * t2];
      y[6 * t2 + 4] = _P1[6 * t2 + 4 - 6 * t2];
      y[6 * t2 + 5] = _P1[6 * t2 + 5 - 6 * t2];
    }
  for (t2 = 1998; t2 <= 1999; t2 += 1) 
    if (index_(t2) + 1 <= index__(t2)) 
      for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) 
        y[t2] += a[t4] * x[col[t4]];
gettimeofday(&t_2, NULL);
elapsed = t_2.tv_sec - t1.tv_sec + (t_2.tv_usec - t1.tv_usec) / 1000000.0;
printf("%.6lf seconds elapsed\n", elapsed);
double mflop_count = 1e-6 * (double) nnz * (double) 2.0;
printf("Performance :%.6lf MFLOPS\n", mflop_count / elapsed);
  free(_P_DATA1);
  free(_P_DATA2);
  free(a_prime);
  free(_P_DATA4);
}

