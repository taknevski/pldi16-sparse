#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA1_(_t6) _P_DATA1[_t6]
#define _P_DATA1__(_t6) _P_DATA1[_t6 + 1]
#define col_(_t8) col[1 * _t8]
#define col___(_t8) col[1 * _t8 + 1]
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
#include <stdio.h>
#include <stdlib.h>

struct a_list 
{
  int col_;
  float a;
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void spmv_bcsr(int m, int n, int *index, float *a, float *y, float *x, int *col,int nnz)
{
  int t4;
  int t2;
  int In_1;
  int _t10;
  int _t9;
  struct a_list *_P_DATA5;
  struct mk *_P_DATA4;
  float *a_prime;
  struct a_list *_P_DATA3;
  int *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t8;
  int _t7;
  int _t6;
  int _t5;
  int _t4;
  int _t3;
  int k;
  int _t2;
  int _t1;
  int i;
  int j;
struct timeval t0, t1, t_2;
gettimeofday(&t0, NULL);
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * 83335)));
  _P_DATA1[0] = 0;
  _P_DATA3 = 0;
  chill_count_1 = 0;
  for (t2 = 0; t2 <= 83333; t2 += 1) {
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) {
      _t7 = col_(t4);
      _P_DATA5 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
      _P_DATA5 -> next = _P_DATA3;
      _P_DATA3 = _P_DATA5;
      _P_DATA3 -> a = 0;
      _P_DATA3 -> col_ = _t7;
      chill_count_1 += 1;
      _P_DATA3 -> a = a[t4];
    }
    _P_DATA1[t2 + 1] = chill_count_1;
  }
  _P_DATA2 = ((int *)(malloc(sizeof(int ) * chill_count_1)));
  a_prime = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 1))));
  a_prime[-(-chill_count_1 + 1) * 1] = _P_DATA3 -> a;
  _P_DATA2[-(-chill_count_1 + 1)] = _P_DATA3 -> col_;
  _P_DATA5 = _P_DATA3 -> next;
  free(_P_DATA3);
  _P_DATA3 = _P_DATA5;
  for (t2 = -chill_count_1 + 2; t2 <= 0; t2 += 1) {
    a_prime[-t2 * 1] = _P_DATA3 -> a;
    _P_DATA2[-t2] = _P_DATA3 -> col_;
    _P_DATA5 = _P_DATA3 -> next;
    free(_P_DATA3);
    _P_DATA3 = _P_DATA5;
  }
gettimeofday(&t1, NULL);
double elapsed = t1.tv_sec - t0.tv_sec  + (t1.tv_usec - t0.tv_usec) / 1000000.0;
printf("Inspector Time: %.6lf seconds\n", elapsed);
printf("Starting actual computation\n");
gettimeofday(&t1, NULL);
  for (t2 = 0; t2 <= 83333; t2 += 1) 
    for (t4 = _P_DATA1_(t2); t4 <= _P_DATA1__(t2) - 1; t4 += 1) 
      y[t2] += a_prime[t4 * 1] * x[_P_DATA2[t4]];
gettimeofday(&t_2, NULL);
elapsed = t_2.tv_sec - t1.tv_sec + (t_2.tv_usec - t1.tv_usec) / 1000000.0;
printf("%.6lf seconds elapsed\n", elapsed);
double mflop_count = 1e-6 * (double) nnz * (double) 2.0;
printf("Performance :%.6lf MFLOPS\n", mflop_count / elapsed);
  free(_P_DATA1);
  free(_P_DATA2);
  free(a_prime);
}

