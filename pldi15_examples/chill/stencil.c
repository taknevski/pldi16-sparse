#define SIZE 64
#define GHOSTS 1
#define PR_SIZE 64

#define ALPHA 0.9
#define BETA 0.9
#define GAMMA 0.9
#define DELTA 0.9

#define PENCIL (SIZE + 2 * GHOSTS)
#define PLANE  ( PENCIL * PENCIL )


void TwentySevenPointStencil_Hull_Chill(double *input, double *temp)
{
	int i,j,k;
	int ii,jj,kk;

	double * __restrict__ in    = input + GHOSTS * PLANE + GHOSTS * PENCIL + GHOSTS; // i.e. [0] = first non ghost zone point
	double * __restrict__ out   = temp + GHOSTS * PLANE + GHOSTS * PENCIL + GHOSTS;


	double (*_in)[PR_SIZE+2*GHOSTS][PR_SIZE+2*GHOSTS];
	double (*_out)[PR_SIZE+2*GHOSTS][PR_SIZE+2*GHOSTS];

	_in = (double (*)[PR_SIZE+2*GHOSTS][PR_SIZE+2*GHOSTS])(in);
	_out = (double (*)[PR_SIZE+2*GHOSTS][PR_SIZE+2*GHOSTS])(out);


	for (k=0; k<SIZE+GHOSTS-1; k++){
		for (j=0; j<SIZE+GHOSTS-1; j++){
			for (i=0; i<SIZE+GHOSTS-1; i++){

				_out[k][j][i] = ALPHA * _in[k][j][i] 

						+  BETA * (  	_in[k-1][j][i] + _in[k][j-1][i] 
					        		+ _in[k][j+1][i] + _in[k+1][j][i]
								+ _in[k][j][i-1] + _in[k][j][i+1]
							  )

						+ GAMMA * (  	_in[k-1][j][i-1] + _in[k][j-1][i-1] 
					        		+ _in[k][j+1][i-1] + _in[k+1][j][i-1]
								+ _in[k-1][j-1][i] + _in[k-1][j+1][i]
								+ _in[k+1][j-1][i] + _in[k+1][j+1][i]
					        		+ _in[k-1][j][i+1] + _in[k][j-1][i+1]
								+ _in[k][j+1][i+1] + _in[k+1][j][i+1]
							  )

						+ DELTA * (  	_in[k-1][j-1][i-1] + _in[k-1][j+1][i-1] 
								+ _in[k+1][j-1][i-1] + _in[k+1][j+1][i-1]
					        		+ _in[k-1][j-1][i+1] + _in[k-1][j+1][i+1]
								+ _in[k+1][j-1][i+1] + _in[k+1][j+1][i+1]
							  );

			
			}}}		
}

