
cd ../
for i in `ls ./matrices/bellgarland/*.csr` 
do
	echo $i
	n=$(head -2 $i | tail -1 | awk '{print $1}')
	nnz=$(head -2 $i | tail -1 | awk '{print $2}')
	echo $n, $nnz 

	line=$(grep "#define NUMROWS" spmv_csr_scalar.cu)
	echo $line
	newline=$(echo "#define NUMROWS ("$n")")

	echo $newline

	sed "s|$line|$newline|g" spmv_csr_scalar.cu > temp1.cu

        line=$(grep "#define NZ" spmv_csr_scalar.cu)
        echo $line
        newline=$(echo "#define NZ ("$nnz")")
        echo $newline

	sed "s|$line|$newline|g" temp1.cu > spmv_csr_scalar.cu

	rm temp1.cu

	make -f CUDACHILLMakefile

	./rosecudasmv $i > $i.scalar1
        ./rosecudasmv $i > $i.scalar2
        ./rosecudasmv $i > $i.scalar3
        ./rosecudasmv $i > $i.scalar4
        ./rosecudasmv $i > $i.scalar5
	

done
