# This file contains utility function related to sparse matrices


# reading csr file in the following format:
# line1 - header information
# line2 - n,nnz
# line3 - row index vector
# line4 - column index vector
# line5 - value vector
def readcsr(csrfile):

        # read header
        temp = csrfile.readline()
        #num_rows and nnz
        temp = csrfile.readline()

        tlist = temp.split()
        num_rows = int(tlist[0])
        nnz = int(tlist[1])

        print num_rows, nnz

        #read the row index vector
        tlist = csrfile.readline()
        rowid = tlist.split()

        #read the column index vector
        tlist = csrfile.readline()
        colid = tlist.split()

        #read the value vector
        tlist = csrfile.readline()
        vals = tlist.split()

        #read the permutation vector
        tlist = csrfile.readline()
        perm = tlist.split()

        print 'Row index length = %d' % (len(rowid))
        print 'Column index length = %d' % (len(colid))
        print 'Value vector length = %d' % (len(vals))
        print 'Permutation vector length = %d' % (len(perm))

        edgelist = []
        vallist = []
        for i in range(0,num_rows):
                edgelist.append([])
                vallist.append([])
                for j in range(int(rowid[i]),int(rowid[i+1])):
                        edgelist[i].append(int(colid[j-1])-1)
                        vallist[i].append(float(vals[j-1]))

        csrfile.close()

        return num_rows, nnz, edgelist, vallist

# reading csr file in the following format:
# line1 - header information
# line2 - n,nnz
# one line per element:row index vector,column index vector,value vector
def readcsr1(csrfile):

        # read header
        temp = csrfile.readline()
        #num_rows and nnz
        temp = csrfile.readline()

        tlist = temp.split()
        num_rows = int(tlist[0])
        nnz = int(tlist[1])

        print num_rows, nnz

        #read the row index vector
	rowid = []
	for i in range(0,num_rows+1):
        	tlist = csrfile.readline()
		rowid.append(int(tlist))	

        #read the column index vector
	colid = []
	for i in range(0,nnz):
        	tlist = csrfile.readline()
        	colid.append(int(tlist))

        #read the value vector
	vals = []
	for i in range(0,nnz):
        	tlist = csrfile.readline()
        	vals.append(int(tlist))

        #read the permutation vector
        tlist = csrfile.readline()
        perm = tlist.split()

        print 'Row index length = %d' % (len(rowid))
        print 'Column index length = %d' % (len(colid))
        print 'Value vector length = %d' % (len(vals))
        print 'Permutation vector length = %d' % (len(perm))

        edgelist = []
        vallist = []
        for i in range(0,num_rows):
                edgelist.append([])
                vallist.append([])
                for j in range(int(rowid[i]),int(rowid[i+1])):
                        edgelist[i].append(int(colid[j-1])-1)
                        vallist[i].append(float(vals[j-1]))

        csrfile.close()

        return num_rows, nnz, edgelist, vallist




# Read file in mtx format
# Input: open input file, symmetry (0-non, 1-sym)
def readmtx(infile, sym):

        #read the header information
        line = infile.readline()
        while (line.startswith('%')):
                line = infile.readline()

        # read the matrix propeties
        tline = line.split()
        m = int(tline[0])
        n = int(tline[1])
        nnz = int(tline[2])
        origindlist = []
        origvallist = []
        for i in range(0,m):
                origindlist.append([])
                origvallist.append([])

        if (sym):
                nnz = 2*nnz - n

        print "%d, %d, %d" % (m,n,nnz)
        line = infile.readline()
        tl =[]

        if (sym):
                print 'sysm'
                while line:
                        tl = line.split()
                        i = int(tl[0])
                        j = int(tl[1])
                        if (len(tl) == 3):
                                val = float(tl[2])
                        else:
                                val = 1.0
                        origindlist[i-1].append(j-1)
                        if (i != j):
                                origindlist[j-1].append(i-1)
                                origvallist[j-1].append(val)
                        origvallist[i-1].append(val)
                        line = infile.readline()
        else:
                print 'non-sym'
                while line:
                        tl = line.split()
                        i = int(tl[0])
                        j = int(tl[1])
                        if (len(tl) == 3):
                                val = float(tl[2])
                        else:
                                val = 1.0
                        origindlist[i-1].append(j-1)
                        origvallist[i-1].append(val)
                        line = infile.readline()
	infile.close()

        return m, nnz, origindlist, origvallist


# Write file in csr format
def writecsr(outfile,edgelist,vallist, n,nnz):
        outfile.write("Manu_generated_file\n")

        s = '%d %d\n' % (n, nnz)
        outfile.write(s)
        s = '%d\n' % 1
        outfile.write(s)
        sum = 1
        for i in range(0,len(edgelist)):
                sum += len(edgelist[i])
                s = '%d\n' % sum
                outfile.write(s)
        for i in range(0,len(edgelist)):
                for j in range(0,len(edgelist[i])):
                        s = '%d\n' % (edgelist[i][j]+1)
                        outfile.write(s)

        for i in range(0,len(vallist)):
                for j in range(0,len(vallist[i])):
                        s = '%f\n' % vallist[i][j]
                        outfile.write(s)

        outfile.close()


# Write file in mtx format
def writemtx(outfile,edgelist,vallist,n,nnz):
        outfile.write("Manu_generated_file\n")

        s = '%d %d %d\n' % (n,n, nnz)
        outfile.write(s)

        for i in range(0,len(edgelist)):
                for j in range(0,len(edgelist[i])):
                        s = '%d %d %f\n' % (edgelist[i][j]+1,i+1,vallist[i][j])
                        outfile.write(s)

        outfile.close()


# Converting file from csr to blocked csr format
# Inputs: n,nnz,edgelist,value list, row length to be blocked, column length to be blocked, file
# NOTE: works for r=1 only!!
def csr2blkcsr(n,nnz,edgelist,vallist,r,c,outfile,flag):
        bedgelist = []
        bvallist = []
        for i in range(0,len(edgelist)):
                bedgelist.append([])
                bvallist.append([])
                l = len(edgelist[i])
                t = l % c
                j = 0
                while (j < l):
                        id = edgelist[i][j]
                        q = id/c
                        r = id % c
                        t = id - r
                        bedgelist[i].append(q)

                        # fill-in elements before element 'id'
                        for k in range(t,id):
                                bvallist[i].append(0)

                        # add element 'id'
                        bvallist[i].append(vallist[i][j])

                        # fill-in / add elements from id+1 to id + (c-id%c) 
                        count = 0
                        s = id+1
                        e = id + (c - r)
                        for k in range(1,e-s+1):
                           if ((j+k) < l):
                                if (edgelist[i][j+k] != (id+k)):
                                        bvallist[i].append(0)
                                else:
                                        bvallist[i].append(vallist[i][j+k])
                                        count += 1
                           else:
                                bvallist[i].append(0)
                        j += count + 1

        sum0 = 0
        for i in range(0,len(bedgelist)):
                sum0 += len(bedgelist[i])
        sum1 = 0
        for i in range(0,len(bvallist)):
                sum1 = sum1 + len(bvallist[i])

	if (flag == 0):
        	writecsr(outfile,bedgelist,bvallist, len(bedgelist),sum0,sum1)
	elif (flag == 1):
		ratio = getloads2floatratio(len(bedgelist),sum0,sum1)
		return ratio

#Input: nnz is the total number of nzs with fillins
def getloads2floatratio(num_rows,num_colids,nnz):

#	print num_rows,num_colids,nnz
	float_c = 2*nnz
	load_c = (num_rows+1) + num_colids + 2*nnz
	ratio = float(((num_rows+1) + num_colids + 2*nnz))/ (2*nnz)

	return load_c,float_c,ratio


