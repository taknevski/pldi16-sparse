
cd ../
for i in `ls ./matrices/bellgarland/*.csr` 
do
	echo $i
	n=$(head -2 $i | tail -1 | awk '{print $1}')
	nnz=$(head -2 $i | tail -1 | awk '{print $2}')
	echo $n, $nnz 

	line=$(grep "#define NUMROWS" spmv_csr_vector.cu)
	echo $line
	newline=$(echo "#define NUMROWS ("$n")")

	echo $newline

	sed "s|$line|$newline|g" spmv_csr_vector.cu > temp1.cu

        line=$(grep "#define NZ" spmv_csr_vector.cu)
        echo $line
        newline=$(echo "#define NZ ("$nnz")")
        echo $newline

	sed "s|$line|$newline|g" temp1.cu > spmv_csr_vector.cu

	rm temp1.cu

	make -f CUDACHILLMakefile

	./rosecudasmv $i > $i.vector1
        ./rosecudasmv $i > $i.vector2
        ./rosecudasmv $i > $i.vector3
        ./rosecudasmv $i > $i.vector4
        ./rosecudasmv $i > $i.vector5
	

done
