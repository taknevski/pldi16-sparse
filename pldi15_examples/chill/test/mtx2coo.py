import utils
import sys


if (len(sys.argv) != 3):
        sys.exit("Should provide compressed input file\n")

fname = sys.argv[1]
coofile =  fname+'.coo'
infile = fname+'.mtx'
sym = int(sys.argv[2])

mtxfile = open(infile,'r');
outfile = open(coofile,'w');

n, nnz, origindlist, origvallist = utils.readmtx(mtxfile,sym);

utils.writemtx(outfile,origindlist,origvallist, n,nnz);


