
cd ../
for i in `ls ./matrices/bellgarland/*.csr` 
do
	echo $i
	n=$(head -2 $i | tail -1 | awk '{print $1}')
	nnz=$(head -2 $i | tail -1 | awk '{print $2}')
	echo $n, $nnz 

	line=$(grep "#define NUMROWS" with_indirection.cu)
	echo $line
	newline=$(echo "#define NUMROWS ("$n")")

	echo $newline

	sed "s|$line|$newline|g" with_indirection.cu > temp1.cu

        line=$(grep "#define NZ" with_indirection.cu)
        echo $line
        newline=$(echo "#define NZ ("$nnz")")
        echo $newline

	sed "s|$line|$newline|g" temp1.cu > with_indirection.cu

	rm temp1.cu

	make -f CUDACHILLMakefile

	./rosecudasmv $i > $i.out1
        ./rosecudasmv $i > $i.out2
        ./rosecudasmv $i > $i.out3
        ./rosecudasmv $i > $i.out4
        ./rosecudasmv $i > $i.out5
	

done
