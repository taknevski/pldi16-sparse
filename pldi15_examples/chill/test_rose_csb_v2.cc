#include  <cstdio>
#include  <cstdlib>
#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA1_(_t21) _P_DATA1[_t21]
#define _P_DATA1__(_t21) _P_DATA1[_t21 + 1]
#define col_(_t24) col[_t24]
#define index_(i) index[i]
#define index__(i) index[i + 1]
//#define N 1000
#define R 2
#define C 2
struct a_list 
{
  int col_;
  float A[2];
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void csb(int m,int n,int *index,int *col,float *x,float *y,float *A)
{
  int t8;
  int t6;
  int t4;
  int t2;
  int In_2;
  int In_1;
  struct a_list *_P_DATA5;
  int newVariable0;
  struct mk *_P_DATA4;
  float *A_prime;
  struct a_list *_P1;
  int *_P_DATA3;
  int chill_count_1;
  int *_P_DATA2;
  int *_P_DATA1;
  int chill_count_0;
  int _t24;
  int _t23;
  int _t22;
  int _t21;
  int _t20;
  int _t19;
  int _t18;
  int _t17;
  int _t16;
  int _t15;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t9;
  int _t8;
  int _t7;
  int _t5;
  int _t4;
  int _t3;
  int k;
  int _t2;
  int _t1;
  int i;
  int j;
  int t10;	
  _P1 = NULL; 
  _P_DATA1 = (int *)malloc((sizeof(int ) * (n/C+1))*(n/R + 1) );
  chill_count_1 = 0;
  _P_DATA1[0] = 0;
  for (t2 = 0; t2 < n/R;t2++) {
    for (t4 = 0; t4 <n/C; t4++) {
       for (t6 = 0; t6 < R; t4++) 
	for (t8 = 0; t8 < C; t8++) 
          for (t10 = index_(R* t2 + t6); t10 < index__(R* t2 + t6); t10++) 
	    if(C*t4 + t8 == col[t10]){
              _P_DATA5 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
              _P_DATA5 -> next = _P1;
              _P1 = _P_DATA5;
              for (newVariable0 = 0; newVariable0 < C; newVariable0++) 
                _P1-> A[newVariable0] = 0;
              _P1-> col_ = t4;
              chill_count_1 += 1;
              

	    } 
	_P_DATA1[t2*(n/C + 1) + t4+1] = chill_count_1;	
	}
      _P_DATA1[(t2+1)*(n/C+1) + 0] = chill_count_1;	
    }		
    
  for (t2 = -chill_count_1 + 1; t2 <= 0; t2 += 1) {
    if (chill_count_1 + t2 <= 1) {
      _P_DATA3 = ((int *)(malloc(sizeof(int ) * chill_count_1)));
      A_prime = ((float *)(malloc(sizeof(float ) * (chill_count_1 * C))));
    }
    for (t4 = 0; t4 <  C; t4 ++) 
      A_prime[-t2 * C + t4] = _P1 -> A[t4];
    _P_DATA3[-t2] = _P1-> col_;
    _P_DATA5 = _P1 -> next;
    free(_P1);
    _P1  = _P_DATA5;
  }
  for (t2 = 0; t2 < n/R; t2 ++) 
    for (t4 = 0; t4 < n/C  ; t4 ++) 
      for (t6 = _P_DATA1[t2*(n/C + 1) + t4]; t6 < _P_DATA1[t2*(n/C+ 1) + t4+1] ; t6++) 
        for (t8 = 0; t8 < C; t8 ++) 
          y[R* t2 + _P_DATA3[t6]] += (A_prime[t6 * C + 1 * t8] * x[C* t4 + t8]);
  free(_P_DATA1);
  free(_P_DATA3);
  free(A_prime);
}

int main()
{
  int m = 4;
  int n = 1000;
  int *index;
  float *x;
  float *y;
  int *col;
  float *A;
  csb(m,n,index,col,x,y,A);
  return 0;
}
