#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define index_(i) index[1 * i]
#define index__(i) index[1 * i + 1]
#define index___(i) index[1 * i + 1]
#define index____(i) index[1 * i]
#define NZ 1666
#define NUMROWS 494

void spmv(int n,int index[494UL],float a[1666UL],float y[494UL],float x[494UL],int col[1666UL])
{
  int t8;
  int t6;
  int t4;
  int t2;
  float *_P_DATA1;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t6;
  int _t5;
  int _t4;
  int _t2;
  int _t1;
  int i;
  int j;
  _P_DATA1 = ((float *)(malloc(sizeof(float ) * (((n - 1) / 1024 + 1) * 1024))));
  for (t2 = 0; t2 <= (n - 1) / 1024; t2 += 1) 
    for (t4 = 0; t4 <= __rose_lt(1023,n - 1024 * t2 - 1); t4 += 1) 
      if (index_(1024 * t2 + t4) + 1 <= index__(1024 * t2 + t4)) {
        for (t6 = index_(1024 * t2 + t4); t6 <= index__(1024 * t2 + t4) - 1; t6 += 1) 
          for (t8 = t6 - 31 + (index_(1024 * t2 + t4) - (t6 - 31)) % 32; t8 <= t6; t8 += 32) 
            _P_DATA1[t4 + t2 * 1024] = (a[t6] * x[col[t6]]);
        for (t6 = index_(1024 * t2 + t4); t6 <= index__(1024 * t2 + t4) - 1; t6 += 1) 
          for (t8 = t6 - 31 + (index_(1024 * t2 + t4) - (t6 - 31)) % 32; t8 <= t6; t8 += 32) 
            y[1024 * t2 + t4] += _P_DATA1[t4 + t2 * 1024];
      }
}

int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[494 + 1];
  int col[1666UL];
  spmv(n,index,a,y,x,col);
  return 0;
}
