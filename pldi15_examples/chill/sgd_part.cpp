#define NZ 1666
#define NUMROWS 494
#define N 1000
#define SGD_LAMBDA 0.05f
#define SGD_FEATURE_SIZE 16
void sgd_kernel_global_simd(int n, int *index, float *ratings, float *y, float *x, int *col, float **fv, float step_size, float *err) {
int i,j,k;


      for(i=0; i < n; i++){
	for(j=index[i]; j < index[i+1]; j++){
		//err[j] = -ratings[j];

		for(k=0; k < SGD_FEATURE_SIZE; k++)
			  err[j] += ratings[j]*fv[col[j]][k];
		
		
/*	        for(k=0; k < SGD_FEATURE_SIZE; k++){
			fv[i][k]       -= step_size *(err[j] * fv[col[j]][k] + SGD_LAMBDA*fv[i][k]);
    		        fv[col[j]][k] -= step_size *(err[j] * fv[i][k] + SGD_LAMBDA*fv[col[j]][k]);
   		}		
*/
	}
      }
       
}


int main(){


int n=NUMROWS;
float a[NZ], y[NUMROWS],x[NUMROWS];
int   index[NUMROWS+1];
int col[NZ];;
float **fv;
float step_size = 0.09f;
float err[NZ];
sgd_kernel_global_simd(n,index,a,y,x,col,fv, step_size,err);




return 0;
}
