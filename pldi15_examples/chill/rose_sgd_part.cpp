#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA1_(_t8) _P_DATA1[_t8]
#define _P_DATA1__(_t8) _P_DATA1[_t8 + 1]
#define col_(_t10) col[_t10]
#define index_(i) index[i]
#define index__(i) index[i + 1]
#define NZ 1666
#define NUMROWS 494
#define N 1000
#define SGD_LAMBDA 0.05f
#define SGD_FEATURE_SIZE 16

struct a_list 
{
  float data[1];
  int col;
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void sgd_kernel_global_simd(int n,int *index,float *ratings,float *y,float *x,int *col,float **fv,float step_size,float *err)
{
  int t6;
  int t4;
  int t2;
  int In_1;
  int _t14;
  int _t13;
  int _t12;
  struct a_list *_P_DATA5;
  struct mk *_P_DATA4;
  struct a_list **new_ratings;
  struct a_list *_P_DATA3;
  int *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t6;
  int _t4;
  int _t5;
  int l;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  int k;
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * (1999 + 1))));
  _P_DATA3 = 0;
  _P_DATA4 = ((struct mk *)(malloc(sizeof(struct mk ) * 1000)));
  chill_count_1 = 0;
  _P_DATA1[0] = 0;
  for (t2 = 0; t2 <= 999; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) 
      for (t6 = 0; t6 <= 15; t6 += 1) {
        _t8 = col_(t4) - t2 + 999;
        if (t2 + (_t8 - 999) == col[t4]) 
          _P_DATA4[_t9].mk::ptr = 0;
      }
  for (t2 = 0; t2 <= 999; t2 += 1) 
    for (t4 = index_(t2); t4 <= index__(t2) - 1; t4 += 1) 
      for (t6 = 0; t6 <= 15; t6 += 1) {
        _t8 = col_(t4) - t2 + 999;
        if (t2 + (_t8 - 999) == col[t4]) {
          if (_P_DATA4[_t9].mk::ptr == 0) {
            _P_DATA5 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
            _P_DATA5 -> a_list::next = _P_DATA3;
            _P_DATA3 = _P_DATA5;
            _P_DATA4[_t9].mk::ptr = _P_DATA3;
            _P_DATA4[_t9].mk::ptr -> a_list::data[0] = 0;
            _P_DATA4[_t9].mk::ptr -> a_list::col = t2;
            chill_count_1 += 1;
          }
          _P_DATA4[_t9].mk::ptr -> a_list::data[0] = ratings[t4];
        }
      }
  _P_DATA2 = ((int *)(malloc(sizeof(int ) * chill_count_1)));
  new_ratings = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 1))));
  new_ratings[-(-chill_count_1 + 1) * 1] = _P_DATA3 -> a_list::data[0];
  _P_DATA2[-(-chill_count_1 + 1)] = _P_DATA3 -> a_list::col;
  _P_DATA5 = _P_DATA3 -> a_list::next;
  free(_P_DATA3);
  _P_DATA3 = _P_DATA5;
  for (t2 = -chill_count_1 + 2; t2 <= 0; t2 += 1) {
    new_ratings[-t2 * 1] = _P_DATA3 -> a_list::data[0];
    _P_DATA2[-t2] = _P_DATA3 -> a_list::col;
    _P_DATA5 = _P_DATA3 -> a_list::next;
    free(_P_DATA3);
    _P_DATA3 = _P_DATA5;
  }
  for (t2 = 0; t2 <= 1998; t2 += 1) 
    for (t4 = _P_DATA1_(t2); t4 <= _P_DATA1__(t2) - 1; t4 += 1) 
      err[_t10] += (new_ratings[t4 * 1] * fv[_P_DATA2[t4] + (t2 - 999)][_t11]);
  _P_DATA1[_t8 + 1] = chill_count_1;
  free(_P_DATA1);
  free(_P_DATA2);
  free(new_ratings);
  free(_P_DATA4);
}

int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[494 + 1];
  int col[1666UL];
  float **fv;
  float step_size = 0.09f;
  float err[1666UL];;
  sgd_kernel_global_simd(n,index,a,y,x,col,fv,step_size,err);
  return 0;
}
