#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA1_(_t5) _P_DATA1[_t5]
#define _P_DATA1__(_t5) _P_DATA1[_t5 + 1]
#define col_(_t7) col[_t7]
#define index_(i) index[i]
#define index__(i) index[i + 1]
#define NZ 1666
#define NUMROWS 494

struct a_list 
{
  float data[1];
  int col;
  struct a_list *next;
}
;

struct mk 
{
  struct a_list *ptr;
}
;

void spmv(int n,int index[494UL],float a[1666UL],float y[494UL],float x[494UL],int col[1666UL])
{
  int t10;
  int t8;
  int t6;
  int t4;
  int t2;
  int In_1;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  struct a_list *_P_DATA5;
  struct mk *_P_DATA4;
  float *a_prime;
  struct a_list *_P_DATA3;
  int *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t7;
  int _t6;
  int _t4;
  int _t5;
  int _t3;
  int k;
  int _t2;
  int _t1;
  int i;
  int j;
  _P_DATA1 = ((int *)(malloc(sizeof(int ) * (3 + 1))));
  _P_DATA3 = 0;
  _P_DATA4 = ((struct mk *)(malloc(sizeof(struct mk ) * 3)));
  chill_count_1 = 0;
  _P_DATA1[0] = 0;
  for (t2 = 0; t2 <= 20816; t2 += 1) 
    for (t4 = 0; t4 <= 20816; t4 += 1) 
      for (t6 = 0; t6 <= 2; t6 += 1) {
        for (t8 = 0; t8 <= 2; t8 += 1) 
          for (t10 = index_(3 * t2 + t6); t10 <= index__(3 * t2 + t6) - 1; t10 += 1) 
            if (3 * t4 + t8 == col[t10]) 
              _P_DATA4[_t6].ptr = 0;
        for (t8 = 0; t8 <= 2; t8 += 1) 
          for (t10 = index_(3 * t2 + t6); t10 <= index__(3 * t2 + t6) - 1; t10 += 1) 
            if (3 * t4 + t8 == col[t10]) {
              if (_P_DATA4[_t6].ptr == 0) {
                _P_DATA5 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
                _P_DATA5 -> next = _P_DATA3;
                _P_DATA3 = _P_DATA5;
                _P_DATA4[_t6].ptr = _P_DATA3;
                _P_DATA4[_t6].ptr -> data[0] = 0;
                _P_DATA4[_t6].ptr -> col = t8;
                chill_count_1 += 1;
              }
              _P_DATA4[_t6].ptr -> data[0] = a[t10];
            }
        _P_DATA1[t6 + 1] = chill_count_1;
        chill_count_1 = 0;
        _P_DATA1[0] = 0;
      }
  _P_DATA2 = ((int *)(malloc(sizeof(int ) * chill_count_1)));
  a_prime = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 1))));
  a_prime[-(-chill_count_1 + 1) * 1] = _P_DATA3 -> data[0];
  _P_DATA2[-(-chill_count_1 + 1)] = _P_DATA3 -> col;
  _P_DATA5 = _P_DATA3 -> next;
  free(_P_DATA3);
  _P_DATA3 = _P_DATA5;
  for (t2 = -chill_count_1 + 2; t2 <= 0; t2 += 1) {
    a_prime[-t2 * 1] = _P_DATA3 -> data[0];
    _P_DATA2[-t2] = _P_DATA3 -> col;
    _P_DATA5 = _P_DATA3 -> next;
    free(_P_DATA3);
    _P_DATA3 = _P_DATA5;
  }
  for (t2 = 0; t2 <= 20816; t2 += 1) 
    for (t4 = 0; t4 <= 20816; t4 += 1) 
      for (t6 = 0; t6 <= 2; t6 += 1) 
        for (t8 = _P_DATA1_(t6); t8 <= _P_DATA1__(t6) - 1; t8 += 1) 
          y[3 * t2 + t6] += (a_prime[t8 * 1] * x[3 * t4 + _P_DATA2[t8]]);
  free(_P_DATA1);
  free(_P_DATA2);
  free(a_prime);
  free(_P_DATA4);
}

int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[494 + 1];
  int col[1666UL];
  spmv(n,index,a,y,x,col);
  return 0;
}
