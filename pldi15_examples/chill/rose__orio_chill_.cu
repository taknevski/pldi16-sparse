#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define nelt 100
#define lz 10
#define lx 10
#define ly 10

void tensor(double *u,double *D,double *Dt,double *ur,double *us,double *ut)
{
  int e;
  int j;
  int i;
  int k;
  int m;
  for (e = 0; e < 100; e++) {
    for (j = 0; j < 10 * 10; j++) {
      for (i = 0; i < 10; i++) {
        for (k = 0; k < 10; k++) {
          ur[((((e * 10) * 10) * 10) + (j * 10)) + i] += (D[(k * 10) + i] * u[((((e * 10) * 10) * 10) + (j * 10)) + k]);
        }
      }
    }
  }
  for (e = 0; e < 100; e++) {
    for (j = 0; j < 10; j++) {
      for (i = 0; i < 10; i++) {
        for (k = 0; k < 10; k++) {
          for (m = 0; m < 10; m++) {
            us[(((((e * 10) * 10) * 10) + ((j * 10) * 10)) + (i * 10)) + k] += (u[(((((e * 10) * 10) * 10) + ((j * 10) * 10)) + (m * 10)) + k] * Dt[(i * 10) + m]);
          }
        }
      }
    }
  }
  for (e = 0; e < 100; e++) {
    for (j = 0; j < 10; j++) {
      for (i = 0; i < 10 * 10; i++) {
        for (k = 0; k < 10; k++) {
          ut[((((e * 10) * 10) * 10) + ((j * 10) * 10)) + i] += (u[((((e * 10) * 10) * 10) + ((k * 10) * 10)) + i] * Dt[(j * 10) + k]);
        }
      }
    }
  }
}
