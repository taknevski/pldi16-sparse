#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define SIZE 64
#define GHOSTS 1
#define PR_SIZE 64
#define ALPHA 0.9
#define BETA 0.9
#define GAMMA 0.9
#define DELTA 0.9
#define PENCIL (SIZE + 2 * GHOSTS)
#define PLANE  ( PENCIL * PENCIL )

void TwentySevenPointStencil_Hull_Chill(double *input,double *temp)
{
  int t6;
  int t4;
  int t2;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  int k;
  int ii;
  int jj;
  int kk;
// i.e. [0] = first non ghost zone point
  double *__restrict__ in = (((input + 1 * ((64 + 2 * 1) * (64 + 2 * 1))) + 1 * (64 + 2 * 1)) + 1);
  double *__restrict__ out = (((temp + 1 * ((64 + 2 * 1) * (64 + 2 * 1))) + 1 * (64 + 2 * 1)) + 1);
  double (*_in)[66UL][66UL];
  double (*_out)[66UL][66UL];
  _in = ((double (*)[66UL][66UL])in);
  _out = ((double (*)[66UL][66UL])out);
  for (t2 = 0; t2 <= 63; t2 += 1) 
    for (t4 = 0; t4 <= 63; t4 += 1) 
      for (t6 = 0; t6 <= 63; t6 += 1) 
        _out[_t1][_t2][_t3] = ((((0.9 * _in[_t1][_t2][_t3]) + (0.9 * (((((_in[_t1 - 1][_t2][_t3] + _in[_t1][_t2 - 1][_t3]) + _in[_t1][_t2 + 1][_t3]) + _in[_t1 + 1][_t2][_t3]) + _in[_t1][_t2][_t3 - 1]) + _in[_t1][_t2][_t3 + 1]))) + (0.9 * (((((((((((_in[_t1 - 1][_t2][_t3 - 1] + _in[_t1][_t2 - 1][_t3 - 1]) + _in[_t1][_t2 + 1][_t3 - 1]) + _in[_t1 + 1][_t2][_t3 - 1]) + _in[_t1 - 1][_t2 - 1][_t3]) + _in[_t1 - 1][_t2 + 1][_t3]) + _in[_t1 + 1][_t2 - 1][_t3]) + _in[_t1 + 1][_t2 + 1][_t3]) + _in[_t1 - 1][_t2][_t3 + 1]) + _in[_t1][_t2 - 1][_t3 + 1]) + _in[_t1][_t2 + 1][_t3 + 1]) + _in[_t1 + 1][_t2][_t3 + 1]))) + (0.9 * (((((((_in[_t1 - 1][_t2 - 1][_t3 - 1] + _in[_t1 - 1][_t2 + 1][_t3 - 1]) + _in[_t1 + 1][_t2 - 1][_t3 - 1]) + _in[_t1 + 1][_t2 + 1][_t3 - 1]) + _in[_t1 - 1][_t2 - 1][_t3 + 1]) + _in[_t1 - 1][_t2 + 1][_t3 + 1]) + _in[_t1 + 1][_t2 - 1][_t3 + 1]) + _in[_t1 + 1][_t2 + 1][_t3 + 1])));
}
