#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define N 512

int main()
{
  int t4;
  int t2;
  int _t2;
  int _t1;
  double a[512UL];
  double b[512UL];
  int t;
  int i;
  for (t2 = 1; t2 <= 100; t2 += 1) {
    b[2] = ((((double )0.25) * (a[2 - 1] + a[2 + 1])) + (((double )0.5) * a[2]));
    for (t4 = 3; t4 <= 511; t4 += 1) {
      b[t4] = ((((double )0.25) * (a[t4 - 1] + a[t4 + 1])) + (((double )0.5) * a[t4]));
      a[t4 - 1] = b[t4 - 1];
    }
    a[512 - 1] = b[512 - 1];
  }
  return 0;
}
