#include <stdio.h>
#include <stdlib.h>
#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define _P_DATA1_(_t25) _P_DATA1[_t25]
#define _P_DATA1__(_t25) _P_DATA1[_t25 + 1]
#define col_(_t29) col[_t29]
#define index_(i) index[i]
#define index__(i) index[i + 1]
#define NZ 1666
#define NUMROWS 494
#define N 1000
#define SGD_LAMBDA 0.05f
#define SGD_FEATURE_SIZE 16

struct a_list {
	int col_[1];
	float ratings[16];
	struct a_list *next;
}
;

struct mk {
	struct a_list *ptr;
}
;

//change 2 : mk2 for outermost by-diagonal loop
//struct mk2 {
//	struct a_list **ptr;
//}
//;
void sgd_kernel_global_simd(int n, int *index, float *ratings, float *y,
		float *x, int *col, float **fv, float step_size, float err) {
	int t10;
	int t8;
	int t6;
	int t4;
	int t2;
	int In_3;
	int In_2;
	int In_1;
	struct a_list *_P_DATA5;
	int newVariable2;
	int newVariable1;
	int newVariable0;
	struct mk *_P_DATA4;
	struct mk *_P_DATA6;

	float *new_ratings;
	struct a_list *_P_DATA3[625];
	struct a_list *_P_DATA4_;
	int *_P_DATA2;
	int chill_count_1;
	int *_P_DATA1;
	int chill_count_0;
	int _t29;
	int _t28;
	int _t27;
	int _t26;
	int _t25;
	int _t24;
	int _t23;
	int _t22;
	int _t20;
	int _t21;
	int _t19;
	int _t18;
	int _t17;
	int _t16;
	int _t15;
	int _t14;
	int _t13;
	int _t12;
	int _t11;
	int _t9;
	int _t8;
	int _t7;
	int _t5;
	int _t4;
	int _t3;
	int l;
	int _t2;
	int _t1;
	int i;
	int j;
	int k;
	_P_DATA1 = ((int *) (malloc(sizeof(int) * 625)));
	_P_DATA1[0] = 0;
	//_P_DATA3 = 0;
	chill_count_1 = 0;
	_P_DATA1[0] = 0;
	_P_DATA3[0] = 0;
	//change 1: initialize marked t2 and t4

	_P_DATA4 = (struct mk *) malloc(250 * sizeof(struct mk));
	_P_DATA6 = (struct mk *) malloc(624 * sizeof(struct mk));

	for (t2 = 0; t2 <= 623; t2 += 1) {
		_P_DATA6[t2].ptr = NULL;
		_P_DATA1[t2 + 1] = 0;
		_P_DATA3[t2 + 1] = 0;
	}
	for (t4 = 0; t4 <= 249; t4 += 1) {
		_P_DATA4[t4].ptr = NULL;
	}
	//change 3: derive outermost loop
	//for (t2 = 0; t2 <= 623; t2 += 1)
	for (t4 = 0; t4 <= 249; t4 += 1)
		for (t6 = 0; t6 <= 3; t6 += 1)
			for (t8 = 0; t8 <= 3; t8 += 1)
				for (t10 = index_(4 * t4 + t6); t10 <= index__(4 * t4 + t6) - 1;t10 += 1){
	    //change 5: derive t8 and t2
	    t8 = col[t10]%4;
	    t2 = col[t10]/4 - t4 + 249; 
            //change 4:  no if check
	    //if (4 * t4 + 4 * (t2 - 249) + t8 == col[t10]) {
	    //change 5: use both _P_DATA4 and _P_DATA6
	    if (_P_DATA4[t4].ptr == NULL || _P_DATA6[t2].ptr == NULL ) {
                _P_DATA5 = ((struct a_list *)(malloc(sizeof(struct a_list ) * 1)));
		_P_DATA5 -> next = _P_DATA3[t2];
                _P_DATA3[t2] = _P_DATA5;
		_P_DATA4[t4].ptr = _P_DATA5;
		_P_DATA6[t2].ptr = _P_DATA3[t2];
                //_P_DATA4[t4].ptr = _P_DATA3;
                for (newVariable0 = 0; newVariable0 <= 3; newVariable0 += 1) 
                  for (newVariable1 = 0; newVariable1 <= 3; newVariable1 += 1) 
                    _P_DATA4[t4].ptr -> ratings[4 * newVariable0 + 1 * newVariable1] = 0;
                _P_DATA4[t4].ptr -> col_[0] = t4;
                chill_count_1 += 1;
                _P_DATA1[1 * t2 + 1] += 1;
              }
              _P_DATA4[t4].ptr -> ratings[4 * t6 + 1 * t8] = ratings[t10];
            }

				//change 6 change below to account for _P_DATA6
				/*
				 for (t2 = -chill_count_1 + 1; t2 <= 0; t2 += 1) {
				 if (chill_count_1 + t2 <= 1) {
				 _P_DATA2 = ((int *)(malloc(sizeof(int ) * chill_count_1)));
				 new_ratings = ((float *)(malloc(sizeof(float ) * (chill_count_1 * 16))));
				 }
				 for (t4 = 0; t4 <= 3; t4 += 1)
				 for (t6 = 0; t6 <= 3; t6 += 1)
				 new_ratings[-t2 * 16 + 4 * t4 + 1 * t6] = _P_DATA3 -> ratings[4 * t4 + 1 * t6];
				 _P_DATA2[-t2] = _P_DATA3 -> col_;
				 _P_DATA5 = _P_DATA3 -> next;
				 free(_P_DATA3);
				 _P_DATA3 = _P_DATA5;
				 }
				 */
	_P_DATA2 = ((int *) (malloc(sizeof(int) * chill_count_1)))
	;
	new_ratings = ((float *) (malloc(sizeof(float) * (chill_count_1 * 16))))
	;
	for (t2 = 0; t2 <= 623; t2 += 1) {
		for (newVariable0 = 1 - _P_DATA1[1 * t2 + 1]; newVariable0 <= 0;
				newVariable0 += 1) {
			_P_DATA2[_P_DATA1[1 * t2] - newVariable0] =
					_P_DATA3[1 * t2]->col_[0];

			for (newVariable1 = 0; newVariable1 <= 3; newVariable1 += 1)
				for (newVariable2 = 0; newVariable2 <= 3; newVariable2 += 1)
					new_ratings[1 * (_P_DATA1[1 * t2] - newVariable0)
							+ 4 * newVariable1 + newVariable2] =
							_P_DATA3[1 * t2]->ratings[4 * newVariable1
									+ newVariable2];
			_P_DATA4_ = _P_DATA3[1 * t2]->next;
			free(_P_DATA3[1 * t2]);
			_P_DATA3[1 * t2] = _P_DATA4_;
		}
		_P_DATA1[1 * t2 + 1] += _P_DATA1[1 * t2];
	}

	/*
	 for (t2 = 0; t2 <= 623; t2 += 1)
	 for (t4 = _P_DATA1_(t2); t4 <= _P_DATA1__(t2) - 1; t4 += 1)
	 for (t6 = 0; t6 <= 15; t6 += 1)
	 err[t6] += (new_ratings[t4 * 1] * fv[_P_DATA2[t4] + (t2 - 999)][_t11]);

	 for (t2 = 0; t2 <= 623; t2 += 1)
	 for (t4 = _P_DATA1_(t2); t4 <= _P_DATA1__(t2) - 1; t4 += 1)
	 for (t6 = 0; t6 <= 3; t6 += 1)
	 for (t8 = 0; t8 <= 3; t8 += 1)
	 //err[j] = -ratings[j];
	 //		for(k=0; k < SGD_FEATURE_SIZE; k++)
	 err += (new_ratings[t4 * 16 + 4 * t6 + 1 * t8] * fv[4 * _P_DATA2[t4] + 4 * (t2 - 249) + t8][k]);
	 free(_P_DATA1);
	 free(_P_DATA2);
	 free(new_ratings);
	 free(_P_DATA4);
	 */

}


int main() {
	int n = 494;
	float a[1666UL];
	float y[494UL];
	float x[494UL];
	int index[495UL];
	int col[1666UL];
	float **fv;
	float step_size = 0.09f;
	float err;
	;
	sgd_kernel_global_simd(n, index, a, y, x, col, fv, step_size, err);
	return 0;
}

