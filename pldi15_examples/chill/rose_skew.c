#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define N 512

int main()
{
  int t2;
  int t4;
  int i;
  int j;
  int n;
  int a[10UL][10UL];
  if (1 <= n) 
    for (t2 = 0; t2 <= n + 1; t2 += 1) 
      for (t4 = __rose_gt(0,t2 - 2); t4 <= __rose_lt(t2,n - 1); t4 += 1) 
        a[t4][t4 + (t2 - t4)] = 0;
  return 0;
}
