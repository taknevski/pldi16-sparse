#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define ifhp 14
#define ofhp 12
#define ifwp 14
#define ofwp 12
#define nIfm 16
#define nOfm 16
#define KH 3
#define KW 3
#define ofw 12
#define ofh 12

void naive_conv_fp(int nImg,float *input[16UL][14UL][14UL],float *output[16UL][12UL][12UL],float *wt[16UL][3UL][3UL])
{
  int t14;
  int t12;
  int t10;
  int t8;
  int t6;
  int t4;
  int t2;
  int _t20;
  float _P1[16];
  int _t17;
  int _t16;
  int _t15;
  int _t14;
  int _t13;
  int _t12;
  int _t11;
  float newVariable0;
  int _t3;
  int _t2;
  int _t7;
  int _t6;
  int _t5;
  int _t4;
  int _t1;
/*
   float (* __restrict input )[nIfm][ifhp][ifwp] = (float (*)[*][*][*])inp;
   float (* __restrict output)[nOfm][ofhp][ofwp] = (float (*)[*][*][*])outp;
   float (* __restrict wt    )[nIfm][KH][KW]     = (float (*)[*][KH][KW])wp;
   */
  int img;
  int ofm;
  int ifm;
  int oj;
  int oi;
  int kj;
  int ki;
  for (t2 = 0; t2 <= nImg - 1; t2 += 1) 
    for (t4 = 0; t4 <= 2; t4 += 1) 
      for (t6 = 0; t6 <= 2; t6 += 1) 
        for (t8 = 0; t8 <= 15; t8 += 1) 
          for (t10 = 0; t10 <= 15; t10 += 1) {
            newVariable0 = wt[t8][t10][t4];
            for (t12 = 0; t12 <= 11; t12 += 1) 
              _P1[t12 - 0] = output[t2][t8][t12];
            for (t12 = 0; t12 <= 11; t12 += 1) 
              for (t14 = 0; t14 <= 11; t14 += 1) 
                _P1[t12 - 0] += (input[t2][t10][t4 + t12][t14 + t6] * newVariable0);
            for (t12 = 0; t12 <= 11; t12 += 1) 
              output[t2][t8][t12] = _P1[t12 - 0];
          }
}
