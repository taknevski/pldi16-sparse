#define __rose_lt(x,y) ((x)<(y)?(x):(y))
#define __rose_gt(x,y) ((x)>(y)?(x):(y))
#define index_(i) index[i]
#define index__(i) index[i + 1]
__global__ void spmv_ell_GPU(int m, int n, float *y,float *_P_DATA4,float *x,int *_P_DATA5);
#include <sys/time.h>
#include <stdio.h>
#define BLOCKDIM 1024

void spmv_bcsr(int n,int *index,float *a,float *y,float *x,int *col, int nnz){
  int *devI3Ptr;
  float *devI2Ptr;
  float *devI1Ptr;
  float *devO1Ptr;
  int t8;
  int t6;
  int t4;
  float newVariable1;
  int _t35;
  int _t37;
  int _t36;
  int _t34;
  int _t31;
  int _t30;
  int t2;
  int _t24;
  int _t25;
  int _t23;
  int _t22;
  int _t21;
  int *_P_DATA5;
  int _t20;
  int _t19;
  float *_P_DATA4;
  int _t17;
  int _t18;
  int _t16;
  int _t15;
  int _t14;
  int newVariable0;
  int *_P_DATA3;
  int *col_prime;
  float *a_prime;
  int *_P_DATA2;
  int chill_count_1;
  int *_P_DATA1;
  int chill_count_0;
  int _t13;
  int _t12;
  int _t11;
  int _t10;
  int _t9;
  int _t8;
  int _t7;
  int _t5;
  int _t4;
  int _t3;
  int _t2;
  int _t1;
  int i;
  int j;
  struct timeval t0, t1, t_2;
  int m =0; 	
  cudaError_t err = cudaSuccess;
  cudaEvent_t start_event, stop_event;
  cudaEventCreate(&start_event);
  cudaEventCreate(&stop_event);
  float cuda_elapsed_time;
  gettimeofday(&t0, NULL);

        for(i = 0; i < n; i++)
        	if(index[i+1] - index[i] > m) 
			m = index[i+1] - index[i]; 



  _P_DATA1 = ((int *)(malloc(sizeof(int ) * (n+1))));
  _P_DATA2 = ((int *)(malloc(sizeof(int ) * n)));
  a_prime = ((float *)(malloc(sizeof(float ) * (n * m))));
  col_prime = ((int *)(malloc(sizeof(int ) * (n * m))));
  _P_DATA3 = ((int *)(malloc(sizeof(int ) * 1)));
  _P_DATA4 = ((float *)(malloc(sizeof(float ) * (m * (n - 1 + 1)))));
  _P_DATA5 = ((int *)(malloc(sizeof(int ) * (m * (n - 1 + 1)))));
  chill_count_1 = 0;

  for (t2 = 0; t2 <= n - 1; t2 += 1) {
    for (t4 = 0; t4 <= (index__(t2) - index_(t2) - 1) / m; t4 += 1) 
      for (t6 = 0; t6 <= __rose_lt(m-1,index__(t2) - index_(t2) - m* t4 - 1); t6 += 1) 
        _P_DATA3[t4] = -1;
    for (t4 = 0; t4 <= (index__(t2) - index_(t2) - 1) / m; t4 += 1) 
      for (t6 = 0; t6 <= __rose_lt(m-1,index__(t2) - index_(t2) - m* t4 - 1); t6 += 1) {
        if (_P_DATA3[t4] == -1) {
          _P_DATA3[t4] = chill_count_1;
          for (newVariable0 = 0; newVariable0 <= m-1; newVariable0 += 1) {
            a_prime[_P_DATA3[t4] * m + 1 * newVariable0] = 0;
            col_prime[_P_DATA3[t4] * m + 1 * newVariable0] = 0;
          }
          _P_DATA2[_P_DATA3[t4]] = t4;
          chill_count_1 += 1;
        }
        a_prime[_P_DATA3[t4] * m + 1 * t6] = a[m * t4 + t6 + index_(t2)];
        col_prime[_P_DATA3[t4] * m + 1 * t6] = col[m * t4 + t6 + index_(t2)];
      }
    _P_DATA1[t2 + 1] = chill_count_1;
  }

  gettimeofday(&t1, NULL);
  double elapsed = t1.tv_sec - t0.tv_sec  + (t1.tv_usec - t0.tv_usec) / 1000000.0;
  printf("Inspector Time: %.6lf seconds\n", elapsed);

  if (1 <= n) 
    for (t2 = 0; t2 <= m-1; t2 += 1) 
      for (t4 = 0; t4 <= n - 1; t4 += 1) {
        _P_DATA4[t4 + t2 * (n - 1 + 1)] = a_prime[t4 * m + 1 * t2];
        _P_DATA5[t4 + t2 * (n - 1 + 1)] = col_prime[t4 * m + 1 * t2];
      }

  printf("Starting actual computation\n");

  cudaMalloc(((void **)(&devO1Ptr)),n* sizeof(float ));
  cudaMemcpy(devO1Ptr,y,n* sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI1Ptr)),m* (n - 1 + 1) * sizeof(float ));
  cudaMemcpy(devI1Ptr,_P_DATA4,m* (n - 1 + 1) * sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI2Ptr)),n* sizeof(float ));
  cudaMemcpy(devI2Ptr,x,n* sizeof(float ),cudaMemcpyHostToDevice);
  cudaMalloc(((void **)(&devI3Ptr)),m* (n - 1 + 1) * sizeof(int ));
  cudaMemcpy(devI3Ptr,_P_DATA5,m* (n - 1 + 1) * sizeof(int ),cudaMemcpyHostToDevice);
  dim3 dimGrid3 = dim3((n - 1) / BLOCKDIM + 1,1);
  dim3 dimBlock3 = dim3(BLOCKDIM,1);

  cudaEventRecord(start_event,0);

  spmv_ell_GPU<<<dimGrid3,dimBlock3>>>(m,n,devO1Ptr,devI1Ptr,devI2Ptr,devI3Ptr);
  cudaEventRecord(stop_event, 0);
  cudaEventSynchronize(stop_event);
  cudaEventElapsedTime(&cuda_elapsed_time, start_event, stop_event);
  cudaMemcpy(y,devO1Ptr,n* sizeof(float ),cudaMemcpyDeviceToHost);

  printf("Elapsed time : %f ms\n" ,cuda_elapsed_time);
  double mflop_count = 1e-6 *(double)nnz * (double)2.0;
  printf("Performance :%.6lf GFLOPS\n", mflop_count/(double)cuda_elapsed_time);


  cudaFree(devO1Ptr);
  cudaFree(devI1Ptr);
  cudaFree(devI2Ptr);
  cudaFree(devI3Ptr);
  
  free(_P_DATA1);
  free(_P_DATA2);
  free(a_prime);
  free(col_prime);
  free(_P_DATA3);
  free(_P_DATA4);
  free(_P_DATA5);
}

/*int main()
{
  int n = 494;
  float a[1666UL];
  float y[494UL];
  float x[494UL];
  int index[494 + 1];
  int col[1666UL];
  spmv(n,index,a,y,x,col);
  return 0;
}
*/
__global__ void spmv_ell_GPU(int m, int n,float *y,float *_P_DATA4,float *x,int *_P_DATA5)
{
  int jj;
  int bx;
  bx = blockIdx.x;
  int tx;
  tx = threadIdx.x;
  //int n;
  int t2;
  float newVariable1;
  int t4;
  int t8;
  if (tx <= n - BLOCKDIM* bx - 1) {
    newVariable1 = y[BLOCKDIM* bx + tx];
    for (jj = 0; jj <= m-1; jj += 1) 
      newVariable1 += (_P_DATA4[BLOCKDIM* bx + tx + jj * (n - 1 + 1)] * x[_P_DATA5[BLOCKDIM* bx + tx + jj * (n - 1 + 1)]]);
    y[BLOCKDIM* bx + tx] = newVariable1;
  }
}
