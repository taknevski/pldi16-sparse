PAPER = sc_paper
TEX = $(wildcard *.tex tex/*.tex)
BIB = csu.bib solver.bib spmv.bib utah1.bib
FIGS = $(wildcard figures/*.pdf figures/*.png)

.PHONY: all clean

$(PAPER).pdf: $(TEX) $(BIB) $(FIGS) IEEEtran.cls
				echo $(FIGS)
				pdflatex $(PAPER)
				bibtex $(PAPER)
				pdflatex $(PAPER)
				pdflatex $(PAPER)

clean:
				rm -f *.aux *.bbl *.blg *.log *.out $(PAPER).pdf
