\section{Introduction}

Sparse matrix computations represent an important class of algorithms
that arise frequently in numerical simulation and 
graph analytics.  
Sparse matrix computations have been considered mostly beyond the reach of parallelizing compilers 
due to information required for optimization only becoming available
at execution time.
For example, static analysis lacks the necessary information to analyze access patterns 
within the context of indirect array accesses, e.g., A[B[i]], where the contents of B are determined 
dynamically.  Such index expressions are termed \emph{non-affine}, since they are not linear functions
of loop index variables.
Earlier work has addressed this challenge by incorporating the \emph{inspector/executor} approach, where 
an inspector
determines memory access patterns at runtime and transforms the performance
bottleneck loops into executors that use optimizations
that rely on the groundwork and information gathering of the 
inspectors~\cite{Saltz91,Rauchwerger95,Saltz97,DingKen99,MitchellCarFer99,Mellor-Crummey2001,HanT:TPDS06,Wu2013,Basumallik06}.

There has been much prior work on using inspector/executor for sparse matrix computation 
that focuses on primitives like sparse matrix-vector multiply (SpMV)~\cite{pOSKI12, Gkountouvas13, Saule13-PPAM, Buttari2007, Vuduc02, Bell:SpMV:NVIDIA:2008}.  
For SpMV,
parallelization is straightforward, so the optimization focus is on managing
locality and load balance, often using specialized sparse matrix representations.
However, there are more complex sparse matrix algorithms where loop-carried
data dependences make efficient parallelization challenging;
%In numerical linear algebra, SpMV-like computations with no loop carried dependences are
%represented by Jacobi iterative methods, while sparse matrix operations with
%loop carried dependences are represented by Gauss-Seidel iterative methods.
examples include
%Examples of Gauss-Seidel like 
%sparse matrix operations with loop-carried
%dependences are 
sparse triangular solver, Gauss-Seidel relaxation, and
Incomplete LU factorization with zero level of fill-in (ILU0)~\cite{Saad1}. Many of these appear as preconditioners of
iterative algorithms that accelerate the convergence. %~\cite{Saad1}.
%below added by Anand:
%In this paper we explore the Gauss-Seidel and Incomplete LU (ILU(0)) algorithms.
%ILU(0) uses the structure of the input matrix to compute an approximate LU factorization, hence the term 'incomplete', which can then be used as a precondtioner for solution to the original linear system using an iterative method like Conjugate Gradient. ILU(0) uses the sparsity pattern of the input matrix,say A. In general we can pick any matrix power, that is $A^{k-1}$, where k is a positive integer, for the initial sparsity pattern, with the general algorithm being ILU(k). In this paper we only consider ILU(0), referred to as ILU hereafter. 
These %Gauss-Seidel like 
operations account for a large fraction of total
execution time of many sparse iterative linear algorithms, and due to the
challenges in their parallelization, their proportion will only increase as the
trend of increasing number of cores continues.
For example, the High Performance Conjugate Gradient (HPCG) benchmark was recently
proposed as an alternative to HPL for ranking high performance computing
systems~\cite{hpcg}; about 2/3 of its total execution time is spent on a symmetric
Gauss-Seidel preconditioner with loop carried dependences~\cite{sc14-hpcg}.

Consider the following code that implements a forward Gauss-Seidel relaxation, with the system $Ay = b$.

\begin{comment}
\begin{lstlisting}[frame=single,caption={Gauss-Seidel Code.},label={lst:Gauss Seidel}]
for(i=0; i < N; i++)
for(j=index[i]; j < index[i+1]; j++)
x[i] -= A[j]*x[col[j]];
\end{lstlisting}
\end{comment}

\begin{lstlisting}[frame=single,caption={Gauss Seidel Code.},label={lst:Gauss Seidel}]
for (i=0; i < N; i++)  {
  y[i] = b[i];
  for (j=rowptr[i];j<rowptr[i + 1];j++) {
    y[i] -= values[j]*y[colidx[j]];     }
  y[i] = y[i]*idiag[i]; }
\end{lstlisting}

%Here, t
\noindent
There are data dependences between reads and writes of vector \texttt{y}, and these
cannot be determined until runtime when the values of \texttt{colidx[j]} are known.  One way to parallelize
this computation is to inspect at runtime the values of \texttt{colidx[j]} to identify the dependent iterations
of \texttt{i} such that \texttt{i==colidx[j]}.  
Dependences are represented by 
directed acyclic graphs (DAGs), which are traversed in a topological order.
This traversal produces
a list of \emph{level sets}; a level set consists of iterations that can execute
in parallel, and the ordering in the list captures dependences among level sets.
The approach of parallelizing within level sets, with synchronization between level sets, is 
%called 
\emph{wavefront parallelism}.  

%Most work on wavefront-parallel Gauss Seidel and related algorithms employs manual 
%parallelization~\cite{Maxim,isc14}.  Prior work develops a compiler-generated inspector/executor 
%approach, but relies on an inspector data structure and traversal time that is larger than the sparse
%matrix itself~\cite{Rauchwerger95}.
%In this paper, we produce an optimized inspector that uses time and space comparable
%to the sparse matrix size.  Our approach 
%extends techniques for integrating non-affine computations into a 
%polyhedral transformation and code generation framework,
%and automatically generating inspectors to collect runtime information needed 
%to optimize the executor~\cite{Venkat:CGO2014,Venkat15}. 
%This paper makes the following contributions:
%(1) it performs compile-time dependence simplification for a class of sparse matrix computations
%which allows reduced overhead for both inspector and executor;
%(2) it automatically generates the inspector for dependence checking and integrates 
%this with a breadth-first library implementation
%for deriving level sets, and these are integrated with a compiler-generated executor;
%(3) it demonstrates
%the effectiveness of parallelization over sequential execution for executor for the component algorithms for a parallel conjugate gradient algorithm; and,
%(4) it demonstrates the efficiency and accuracy of the compiler-generated inspector.


There has been a long tradition of the compiler-generated 
inspector/executor approach that optimizes %the approach of
recording memory accesses %for each data item 
to derive the
data dependences~\cite{ZhuAndYew87,Leung93,Rauchwerger95,Zhuang09}.
Nonetheless, many wavefront parallelizations of Gauss-Seidel
and related algorithms employ manually written inspectors and
executors~\cite{StroutLCPC2002,Maxim,Anantpur13,isc14}.
The hand-optimized inspectors reduce the inspector time and storage by
%keep the storage requirements for
%discovering a loop's data dependence graph to a minimum by 
iterating over the dependences in the loop directly at runtime.
This avoids the need to store information about which iterations
access each data element, and usually results in an inspector loop
that uses space and time comparable to the sparse matrix size.
%thus improving upon the time and space of existing compiler-generated inspectors.
%.  Often the data dependence graph
%and the inspector loop that generates it use space and
%time comparable to the sparse matrix size. 

In this paper we present an algorithm for automatically
simplifying the compile-time specification of the data dependences
so that the compiler-generated inspector uses time and 
space comparable to the sparse matrix size.
This paper makes the following
contributions: (1) it performs compile-time dependence simplification
for a class of sparse matrix computations that allows reduced overhead
for both inspector and executor; (2) it automatically generates the
inspector for dependence checking and integrates this with a
library implementation for deriving level sets, and these
are integrated with a compiler-generated executor; (3) it demonstrates
the effectiveness of parallelization over sequential execution 
%for executor for the component algorithms 
for a preconditioned conjugate gradient
algorithm (PCG); and, (4) it demonstrates the efficiency and accuracy of the
compiler-generated inspector.
