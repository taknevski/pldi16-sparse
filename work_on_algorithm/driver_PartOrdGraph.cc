/* driver_PartOrdGraph.cc */
/* 
   Driver to test out PartOrdGraph usage (see PartOrdGraph.h).

   usage:
    g++ -g -O0 driver_PartOrdGraph.cc PartOrdGraph.cc
    ./a.out


   Michelle Strout, 11/4/15
   Copyright (c) 2015, University of Arizona
*/
#include "PartOrdGraph.h"
#include <iostream>

int main() {

    // construct and populate the data structure
    PartOrdGraph g(3);              // N is the number of vertices in PO
    g.strict(2,1);                  // inserts ordering 2 < 1
                                    // remember numbers are just ids

    std::cout << g.toString();      // print it!


    bool no = g.isNoOrder(1,2);     // returns true if 2,0 in partial order
    std::cout << "(1,2) no = " << no << std::endl;

                                      
    g.strict(1,0);
    
    std::cout << g.toString();      // print it!
    
    //g.strict(0,2);                // This should assert.  And it does.
    
    
    g.nonStrict(2,0);               // inserts ordering 2 <= 0
                                    // should be ignored because already 2<0   


    // query the data structure
    bool lt, lte, eq;

    lt = g.isStrict(2,0);           // returns true if 2,0 in partial order
    std::cout << "(2,0) lt = " << lt << std::endl;

    lte = g.isNonStrict(2,0);       // returns true if a<=b
    std::cout << "lte = " << lte << std::endl;

    eq = g.isEqual(2,0);       // returns true if a<=b
    std::cout << "eq = " << eq << std::endl;

    std::cout << g.toString();      // print it!

    return 0;
}

