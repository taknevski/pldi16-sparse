%\externaldocument{results}
\section{Related Work}
\label{sec:related}

This section broadly classifies related research into three categories and
describes them in details below.

\subsection{Compiler-Based Approaches}

There have been several efforts in wavefront parallelization of loop nests that
contain statically unanalyzable access patterns. % in programs.
%we summarize
%some widely-used approaches.
One of the most widely used %partial parallelization 
techniques is the runtime
approach via inspector-executor~\cite{rauch}. Our paper uses a similar approach
but with some key differences.  Although they parallelize the inspector like
ours, their inspector code still retains the original structure of the loop
nest, which can give rise to increased time and space complexity.  
For example, in the ILU0 example, since we remove some constraints thus
approximate the dependences, our inspector loop is less deeply nested
than their approach.
Moreover, during
inspection they explicitly build a dependence graph for each memory location,
which as they show benefits reduction and array privatization, but results in
increased space consumption for inspection. In contrast, our approach
simplifies the inspector via assertions~\cite{Rice-TR} and results in
simplified and faster inspector code.  
%In terms of dependence checking,
%~\cite{rauch} is more precise than ours, but from our experience precise
%dependency checking buys little additional parallelism as shown in Table~\ref{tab:Parallelism}.


Previous compiler-based research includes many techniques for reducing the
inspector overhead by parallelizing the inspector~\cite{rauch,Zhuang09},
overlapping inspectors with executors~\cite{Zhuang09,Anantpur13},
and using a GPU to perform inspection~\cite{Wu2013,Anantpur13}.
One of the compilation approaches by Zhuang et al.~\cite{Zhuang09}
incorporated the techniques into the IBM XL compiler and observed speedups for some 
benchmarks from SPEC2000, SPEC2006, and SEQUOIA on a 16 core Power 5 machine.
Their approach is similar to ours in that they use approximation, although not one based
on the data dependence relations.  Their approach is also complementary 
in that it would be possible to use their technique of overlapping the generated
inspector and executor in the code we generate.
Performance improvements in executors are possible by using a finer-grained,
run-time
dependence analysis that enables some overlapping of 
dependent iterations in the executor~\cite{Chen94}.


Ravishankar et al.~\cite{Ravishankar12SC} present compiler algorithms for generating
inspectors and executors that parallelize producer/consumer loop
sequences when there are indirect memory accesses.
This is a different parallelization than wavefront parallelism, which is within a
single loop instead of across multiple loops.
Our technique for simplifying data dependences at compile-time could also
help reduce the overhead of the inspectors in this work.


Vanroose et al.~\cite{Vanroose} propose to optimize pipelined Krylov solvers on multiple nodes. 
Specifically they target the multiple preconditioned SpMV operations applied per time step. 
They reformulate the SpMV operations as stencil accumulations and then extract wavefront computations by applying compiler-based time-skewing. 
This would improve the arithmetic intensity of the SpMV computations. 
The skewing compiler transformation can extract wavefronts as the dependence distance is constant, i.e. the 
values computed in each time step is dependent on those from the previous one. 
In the case of the sparse matrix computations we optimize, the dependence distance, 
in terms of iterations of the outermost loop are not constant and hence necessitate 
the use of an inspector/executor method to extract dependences at run-time and reorganize the computation into wavefronts.  
Further, SpMV operations can be rewritten as stencil applications on a grid only if the matrix has certain structure, eg. diagonal or banded, 
and our evaluation includes unstructured matrices as well, hence necessitating sparse matrix formats

In the HELIX project~\cite{Campanoni12} speedups on the SPEC CPU200 benchmarks 
were observed on an Intel i7 with 6 cores.  Their technique involves a profiling step to 
feed in information to a compiler algorithm that determines which loops to parallelize.  
Inter-iteration dependences within those loops are handled by injected code that performs 
synchronization between dependent iterations~\cite{Midkiff87}.  
In~\cite{Campanoni12}, a number of optimizations are presented 
to make this process as efficient as possible.

Other compile-time and run-time techniques use as much compile-time information
as possible to generate efficient run-time checks to determine if a loop
is fully parallelizable~\cite{Nonlinear94,Oancea2012}.  Loops that only admit
wavefront parallelization will be determined not parallelizable by these approaches,
but the inspector overhead is significantly reduced.

The work in~\cite{Rice-TR} takes a different approach than runtime techniques: it uses
user assertions about index arrays (which may otherwise be statically
unanalyzable) to increase the precision of dependence testing.  The assertions
certify common properties of index arrays, e.g., an index array can be a
permutation array, monotonically increasing, and monotonically decreasing.  Our
paper also uses these assertions, but it uses them to improve the performance
of runtime techniques, i.e., it reduces the complexity of inspector code.
Other data dependence analysis techniques that seek to find more parallelizable loops
at compile-time include~\cite{Maydan91,PW93,barthou97fuzzy,Lin:2000:CAI}.


\subsection{Hand-crafted implementations for Sparse Matrix Kernels}

There have been efforts on manually optimizing sparse matrix kernels with
loop carried dependences, from Saltz and Rothberg’s work on sparse triangular
solver in 1990s~\cite{Saltz, Edward} to work on optimizing sparse triangular
solver for recent NVIDIA GPUs and Intel's multi-core
CPUs~\cite{Maxim,isc14,sc14-hpcg}.
Even though these manual optimizations have been successful at achieving high
performance in some cases, the significant software engineering efforts
involved can be affordable only for a few high profile software projects with manageable
code complexity.
%It would take too much effort to apply such manual
%optimizations to widely used sparse solver libraries like PETSc, HYPRE, and
%Trilinos with large code base.
This software engineering challenge is also indicated by the fact that Intel MKL started
incorporating inspector-executor optimization only very recently in their 11.3
beta version released around April 2015~\cite{Intel-Inspector}.
One of the biggest challenges is composing the manual parallelization with
other optimizations.
Parallelization of kernels like sparse triangular solve needs to be composed
with other optimizations such as reordering to realize its full potential as
shown in~\cite{sc14-hpcg}.
Various preconditioners with different dependency pattern can be composed with
numerous choices of iterative solver algorithms~\cite{Saad1}.
%Oftentimes, a sparse matrix kernel needs to have multiple versions to support
%different sparse matrix formats, where manually parallelizing all of them
%require too much efforts.
Our work addresses this composability issue with a compiler.

\subsection{Polyhedral Compiler Optimization of Sparse Matrices}
%\TODO{Anand: summarie your CGO, PLDI papers}

% FIXME: in the final we should put in the citation to the now published journal paper.
Strout et al.~\cite{RTRTtech13} first demonstrated the feasibility of automatically
generating inspectors and composing separately specified inspectors using the
sparse polyhedral framework; the inspector-executor generator prototype
composed inspectors by representing them using a data structure called an
inspector dependence graph.  Subsequently, Venkat et al.~\cite{Venkat:CGO2014,Venkat15}
developed  automatically-generated inspectors for non-affine transformations
and for data transformations to reorganize sparse matrices into different
representations that exploit the structure of their nonzeros.  Both these works
demonstrated that the performance of the \emph{executors} generated by this approach match and
sometimes exceed performance of manually tuned libraries such as OSKI and
CUSP~\cite{Venkat:CGO2014,Venkat15}; further, the automatically generated
\emph{inspectors} match and sometimes exceed the performance of preprocessing used by these
libraries to convert into new matrix representations~\cite{Venkat15}.
The data dependence simplification algorithm in this work could be used 
by compilers to reduce the overhead of the generated inspectors when
the transformation in question needs to determine data dependences.

% MMS, took out 4/9/16, don't see how it relates
%Muralidharan et al.~\cite{Muralidharan:2014} has developed a tool 
%called Nitro that uses a
%classifier to learn a model for code/data representations, and then applies
%this model to input data set features to make decisions; this was used to
%automatically select sparse matrix representations.

%Our work extends past work
%by exploiting assertions in sparse matrix applications in order to generate
%efficient inspector code. 
% FIXME: not sure what this meant
% via the polyhedral compiler in~\cite{RTRTtech13}.

\REM{In recent work~\cite{Venkat15}, we developed three new compiler transformations
to support both loop and data transformations, which work as follows:
\begin{compactitem}
\item First, \textit{make-dense}, an affine transformation, 
takes as input any set of non-affine 
array
index expressions and 
introduces a guard condition and as many dense loops as necessary to replace the non-affine
index expressions with an affine access.  The \textit{make-dense} transformation enables
further loop transformations such as tiling.
\item The \textit{compact} and \textit{compact-and-pad}
transformations are 
\textit{inspector-executor} transformations;
 an 
automatically-generated inspector 
gathers the iterations of a dense loop that are actually executed
and the optimized executor only visits those iterations.
The executor 
represents the transformed code that 
uses the compacted loop, which 
can then be further optimized.
\item In the \textit{compact-and-pad} transformation, the inspector
also performs a data transformation, 
inserting explicit zeros when necessary to correspond with the optimized executor.
\end{compactitem}

The current non-affine transformations incorporated into CHiLL
were applied in composition with affine transformations but not
other transformations that required inspectors~\cite{Venkat14CGO,Venkat15}.
The inspectors and executors generated from CHiLL were highly efficient
in that they were competitive with or beat hand-optimized or algorithm-specific
optimized inspectors.
The sparse polyhedral framework prototype compiler~\cite{RTRTtech13} 
was able to compose non-affine transformations such as reordering 
molecules with a permutation and sparse tiling across loops, but
the resulting performance was not competitive with existing hand-optimized
inspectors.  With clear abstractions (the inspector/executor API), we
can reason about the composition of inspectors and optimizing those compositions in
the compiler.
}


\REM{
\begin{comment}
This section expands on the discussions of prior work in Sections~\ref{sec:intro} and
~\ref{sec:back} to focus on the most closely-related compilers
targeting sparse matrix codes.
%{\bf Sparse Matrix Compilers}\\
\subsection{Sparse Matrix Compilers}
%%% sparse matrix compilers and languages for converting between sparse matrix formats
Previous work has developed compiler optimizations for 
sparse matrices
%using several techniques.
%Several prior approaches
beginning with a dense abstraction
of a sparse matrix computation, as optimizations for dense matrix computations 
are well understood; these compilers
generate sparse data representations during code generation
~\cite{Bik:93,SIPR98,Mateev2000next,Arnold10}.
These compilers either incorporate a small, fixed set of matrix representations 
for which code generation is straightforward or
rely on the user to provide implementations for accessing data in sparse formats for operations such as searching, enumeration and de-referencing. 
Shpeisman and Pugh ~\cite{SIPR98} specify an intermediate program representation for transforming sparse matrix codes. The specification directs an underlying C++ library for efficient enumeration, permutation and scatter-gather access of nonzeros stored according to some compressed stripe storage.
The Bernoulli compiler permits extension to new formats by 
abstracting sparse matrix computations into relational 
expressions that describe constraints on the iteration space 
and predicates to identify nonzero elements~\cite{Kotlyar:Europar1997,Kotlyar:ICS1997,Mateev2000next}.  Using an approach
similar to optimizing relational database queries, the compiler derives a
plan for efficiently evaluating the relational expressions, and generates corresponding
code.  
Gilad et al.~\citep{Arnold10} use the LL functional language for expressing and verifying sparse matrix codes with their dense analogs, under the assumption
that the matrix is dense initially.
\end{comment}
}
