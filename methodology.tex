
%%%%%%%%%%%%%%%%%%%%%%%%%% MMS started 11/12/15
% Incorporating some of the text from above.
% FIXME: Should put each of the relations and code segments into Figures
%              so can easily refer to them in later subsections.
\section{Simplifying Data Dependences for Efficient Inspectors}
\label{sec:method2}

The overall approach to implementing partial parallelism for a loop with indirect 
array accesses is to do the following at compile time:
\begin{enumerate}
\item Perform data dependence analysis to derive constraints sets for data access dependencies in the target code.
\item Simplify the data dependence constraints sets by projecting out as many iterators as possible,
\item Generate the inspector code for simplified data dependency sets that are satisfiable.
\end{enumerate}
Algorithms for performing the last two steps are the main contributions of this paper. We also perform an approximation in the first step to improve functionality of our simplification algorithm.

\subsection{Demonstrating General Approach with Gauss-Seidel Example}

For instance, considering the Gauss-Seidel Code in Listing~\ref{lst:Gauss Seidel}, 
the data dependence relation for the $i$ loop would be as follows:

\begin{gather*}
\{[i,j] \rightarrow [i',j']: i < i'  \wedge  i = col(j') \\
\wedge 0 \leq i,i' < N  \wedge  index(i) \leq j < index(i+1) \\
\wedge  index(i') \leq j' < index(i'+1)  \} \\
\cup
\{[i',j'] \rightarrow [i,j]: i' < i  \wedge  i = col(j') \\
\wedge 0 \leq i,i' < N  \wedge  index(i) \leq j < index(i+1) \\
\wedge  index(i') \leq j' < index(i'+1)  \}
\end{gather*}
%\begin{gather*}
%\{[i] \rightarrow [i']: \exists j,j', i < i'  \wedge  i = col(j') \\
%\wedge 0 \leq i,i' < N  \wedge  index(i) \leq j < index(i+1) \\
%\wedge  index(i') \leq j' < index(i'+1)  \} \\
%\cup
%\{[i'] \rightarrow [i]: \exists j,j', i' < i  \wedge  i = col(j') \\
%\wedge 0 \leq i,i' < N  \wedge  index(i) \leq j < index(i+1) \\
%\wedge  index(i') \leq j' < index(i'+1)  \}
%\end{gather*}

If we use the above constraints sets directly to generate an inspector that
populates the task graph representing the data dependence relation, then
we would need two loop nests (one for each conjunction) and 
each loop nest would be four deep ($i, i', j, j'$).

The data dependence simplification algorithm projects out as many iterators
as possible and determines equalities that can aid in such projections or eliminate
a level of loop nesting during code generation.
For the Gauss-Seidel example, it is possible to project out the $j$ iterator
and the equality $i=col(j')$ is identified leading to:

%\begin{gather*}
%\{[i] \rightarrow [i']: \exists j', i < i'  \wedge  i = col(j') \\
%\wedge 0 \leq i' < N  \wedge  index(i) < index(i+1) \\
%\wedge  index(i') \leq j' < index(i'+1)  \} \\
%\cup
%\{[i'] \rightarrow [i]: \exists j', i' < i  \wedge  i = col(j') \\
%\wedge 0 \leq i' < N  \wedge  index(i) < index(i+1) \\
%\wedge  index(i') \leq j' < index(i'+1)  \}
%\end{gather*}
\begin{gather*}
\{[i] \rightarrow [i',j']: i < i'  \wedge  i = col(j') \\
\wedge 0 \leq i' < N  \wedge  index(i) < index(i+1) \\
\wedge  index(i') \leq j' < index(i'+1)  \} \\
\cup
\{[i',j'] \rightarrow [i]: i' < i  \wedge  i = col(j') \\
\wedge 0 \leq i' < N  \wedge  index(i) < index(i+1) \\
\wedge  index(i') \leq j' < index(i'+1)  \}
\end{gather*}

Note that the bounds on $i$ have been removed because the equality
$i=col(j')$ enables the $i$ values in the dependence relation to be
directly computed from values of the $j'$ iterator. 
Now without considering further optimization, each of two nested loops for the data dependence relation will only be two-deep (for $i'$ and $j'$).

The simplified data dependence is then specified to the code generator. Here, the code generator is able to determine that the iteration spaces are the same except for the ordering constraints on $i$ and $i'$. Therefore the loops can be fused and those ordering constraints placed into the loop body to select which statement to execute.
There will be two statements in inspector, one for each of the conjunctions. The first 
statement places the edge $i \rightarrow i'$ into the task graph and the
second statement places the edge $i' \rightarrow i$:
\begin{verbatim}
#define S0(i,i')  graph.insert(i,i');
#define S1(i',i)  graph.insert(i',i);
\end{verbatim}
The simplified inspector code for checking data access dependecis of Gauss-Seidel Code would be the following:
\begin{verbatim}
for (i'=0; i'<N; i'++) {
    for (j'=index(i'); j'<index(i'+1);
                       j'++) {
        i=col(j');
        if (i<i') { S0(i,i') }
        if (i'<i) { S1(i',i) }
    }
}
\end{verbatim}

%Then each statement is provided an iteration space... FIXME: Anand, what will this look like?
%\begin{gather*}
%S0:= \{[i,j, i'] : 0 \leq i \wedge i < i' \wedge i' < N \wedge \\
% index(i) \leq j < index(i+1) \wedge i' = col(j)\} \\
%S1:= \{[i,j, i'] : 0 \leq i' < N \wedge i' < i \wedge i < N \\
%\wedge  index(i) \leq j < index(i+1) \wedge i' = col(j) \}\\
%\end{gather*}
%\fix{Anand: This is not correct actually, let me describe it in the inspector code generation section ....... The code generator is able to determine that the iteration spaces are the same except for the ordering constraints on $i$ and $i'$.  Therefore the loops can be fused and those ordering constraints placed into the loop body to select which statement to execute. }

%%%%%%%%
\subsection{Data Dependence Analysis}

The data dependence analysis determines constraints that describe the iteration space 
of the computation and the constraints that must hold for any dependence.
A dependence occurs between two iterations when in the sequential version of the code one
of the  iterations
occurs strictly before the other (i.e., they are not the same iteration),
both of the iterations access the same memory locality and at least one of those accesses
is a write, and the iterations lie within the loop bounds.

We define two instances of the input code's iteration space $I=[i_{1},i_{2},\ldots,i_{N}]$  
and $I'= [i'_{1},i'_{2},\ldots,i'_{N}]$. 
Let us suppose that the outer most loop level is chosen for partial parallelization. Then 
the input constraints to the projection algorithm may involve up to a maximum of $2N$ variables. 
By capturing all constraints on  $i_{1}$ and $i'_{1}$  we can enumerate all pairs of 
iterations of the outermost level involved in
a dependence. These constraints are then fed to a polyhedral code generator to 
generate the inspector code that enumerates all dependences. 
We then explicitly construct the dependence graph to connect all such pairs of iterations where a dependence exists.

\subsubsection{IF-Conditional Constraints Approximation}

In the sparse ILU example in Figure~\ref{fig:ilu0depconstraints}, 
four innermost iterators, $j1$, $j2$, $j1'$, and $j2'$, 
are involved in constraints (12) and (13). 
These equality constraints are comming from a if-conditional statement in the code.
The problem with these constraints is that they force loops 
for all of the iterators to occur in the inspector.
The resulting eight-deep nested loop takes an extraordinary amount of time.
%FIXME: ordering between this subsubsection and the previous one.

It is possible to remove constraints from the dependence problem and
therefore compute a data dependence set that over-approximates the
dependences.  The resulting parallel schedule will lose a certain amount
of parallelism, but it will still satisfy the precise dependencies.

%We provide a mechanism for the programmer to specify which constraints can be removed to cause an approximation. For sparse ILU, we removed the $col(j1)=col(j2)$ and $col(j1')=col(j2')$ constraints.
Section~\ref{sec:results} presents the impact the
approximation had on the execution time of the inspector
and the average parallelism of the resulting dependence graph
for various input sparse matrices. As results show, taking out constraints related to if-conditionals do not impact parallelisim signicantly while decreasing execution time by order of magnitude. Considering this, we think this approximation can be generalized. Consequently, we are using it in our simplification process to automatically take out constraints that are hidering simplification without lossing too much parallelization. Given that this approximation is chiefly  part of data dependence analysis, in our implementations, it is cariied out in this phase. Therefore, constraints for if-conditionals are removed from the constraints sets for dependence relation before feeding them to simplification algorithm.

%%%%%%%%
\subsection{Data Dependence Simplification}

The main goal in simplification algorithm  
is to project all possible variables, except those that represent iterations
of the loop targeted for parallelization (e.g., $i_{1}$ and $i'_{1}$),
without sacrificing the integrity of the solution. %(over-approximation allowed/but not under-approximation) 
%and derive constraints/(in)equalities on these two variables.

The simplification is carried out in two steps:
\begin{enumerate}
\item Derive new constraints based on information known about the uninterpreted functions.
\item Being cognizant of uninterpreted functions, use an existing polyhedral tool to 
project out all variables that are not being passed to uninterpreted functions.
\end{enumerate}
%There are four aspects to this
%\begin{enumerate}
%\item Derive new constraints based on information we have about the uninterpreted functions.
%\item Being cognizant of uninterpreted functions, use an existing polyhedral tool to project out 
%all variables that are not being passed to uninterpreted functions.
%\item Remove constraints involving only iterators upon which the main loop iterator 
%does not depend.
%\item Approximate the dependences by removing some additional constraints, thus enabline
%more iterators to be projected out.
%\end{enumerate}

\subsubsection{Step 1: Derive New Constraints}
We use information about the uninterpreted functions such as their domain and range,
whether the uninterpreted function is monotonic, and
additional constraints that a programmer might be able to provide such as
relationships between various uninterpreted functions.  This information can be used
to derive new equality constraints between iterators, which enables specifying
one iterator in a dependence in terms of another iterator and thus requires the
inspector to have one fewer loops.

As an example, in Incomplete LU factorization on a compressed sparse row matrix
with an index array indicating diagonals,
the constraints in the dependence relation are shown in Figure~\ref{fig:ilu0depconstraints}.

The uninterpreted functions $diagptr()$ and $rowptr()$ have an output range of $0$
through the number of non-zeros $nnz$ since these data structures index into arrays
that are storing the non-zero values themselves and the column index for each
non-zero value.  This leads to new constraints such as $0 \leq rowptr(i) < nnz$ and
$0 \leq diagptr(col(k')) < nnz$.  The $col()$ function has a domain equal to the 
number of of non-zeros, $0$ through $nnz-1$.
%domain of all rows in the matrix plus one, $0 \leq i \leq n$.  
Using the domain information
results in some new constraints involving parameter expressions for all uninterpreted functions.
In this example, some new constraints that are added include $0 \leq j1 < nnz$ and
$0 \leq j2 < nnz$ due to constraint (10) in Figure~\ref{fig:ilu0depconstraints}.

User-defined constraints involving the uninterpreted functions includes constraints
such as $\forall e, rowptr(e) \leq diagptr(e)$, which indicates that the start of the
non-zeros for any particular row will start before or at the same time as the
index to the diagonal for a row.  This information is specific to the sparse data structure
itself and cannot be derived through program analysis.
The algorithm for adding these constraints will instantiate the user-defined
constraint for every expression that is passed to either of the uninterpreted
functions involved in the constraint. 
% FIXME: shouldn't we mention that those variables that are passed to one of the UFCs, 
%but are  out of bound for the other UFC must not be part of this process?  

The algorithm enables the specification of any equality or inequality constraint.
For the ILU example in Figure~\ref{fig:ilu0depconstraints} and
the user-defined constraint that $\forall e, rowptr(e) \leq diagptr(e)$,
some of the constraints that are added included 
$rowptr(i) \leq diagptr(i)$ and $rowptr(col(k')+1) \leq diagptr(col(k'+1))$.

Programmers can also indicate when an uninterpreted function is monotonically
nondecreasing, increasing, decreasing or nonincreasing\footnote{Compiler
analyses can determine monotonicity in some cases~\cite{Lin:2000:CAI}}.
We can use the monotonicity rules to leverage that partial ordering and 
create more constraints on expressions being passed into uninterpreted function calls.
Specifically, if a function $f$ is monotonically non-decreasing, then the following
two properties hold:
\begin{gather}
f(e_1) \leq f(e_2)  \Leftrightarrow e_1 \leq e_2\\
f(e_1) < f(e_2)  \Rightarrow e_1 < e_2.
\end{gather}
Thus partial orderings between uninterpreted function calls can lead to 
constraints being derived for their parameter expressions.
The first property would enable the derivation of inequalities between uninterpreted
function calls from inequalities between expressions, but that process
could infinitely recurse so our current implementation does not utilize that
direction of the property.

The simplification algorithm collects partial orderings between terms in the
constraints.  Figure~\ref{fig:partord} illustrates some of the partial ordering
that exists for the ILU example from Figure~\ref{fig:ilu0depconstraints}
after the constraints due to the user-defined property 
$\forall e, rowptr(e) \leq diagptr(e)$ have been added.
This subset of the partial ordering highlights that we can derive the
constraints $rowptr(i) < rowptr(col(k')+1)$ and
$rowptr(col(k')) < rowptr(i+1)$.  Using the fact that
$rowptr()$ is monotonically non-decreasing, these result in the constraints
$i<col(k')+1$ and $col(k')<i+1$, which lead to $i=col(k')$.
This equality constraint enables the expressing one of the
outer loop iterators $i$ in terms of $k'$.  The next subsection
details how equality constraints help remove unnecessary iterators
from the dependence relation.

%\begin{verbatim}
%  (monotonicity)
%    iff rowptr(x)<=rowptr(y) then x <= y, rowptr is monotonically non-decreasing
%    if rowptr(x)<rowptr(y) then x<y.  Other direction doesn?t hold.
%\end{verbatim}


%FIXME: complexity of all these approaches to adding constraints?

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\begin{gather}
0 \leq i <n\\
0 \leq i' <n\\
rowptr(i) \leq k < diagptr(i)\\
rowptr(i') \leq k' < diagptr(i')\\
k+1 \leq j1 < rowptr(i+1) \\
k'+1 \leq j1' < rowptr(i'+1)\\
diagptr(col(k))+1 \leq j2 < rowptr(col(k)+1) \\
diagptr(col(k'))+1 \leq j2' < rowptr(col(k')+1) \\
j1=j2'\\
col(j1) = col(j2)\\
col(j1') = col(j2')
\end{gather}
\caption{Data dependence constraints for Incomplete LU Factorization on a compressed sparse
matrix.}
\label{fig:ilu0depconstraints}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
\begin{center}
$\begin{array}{c}
\includegraphics[scale=0.4]{figures/part_ord_example.pdf} \\
\end{array}$
\caption{Partial ordering of terms that leads to the constraint $rowptr(i)<rowptr(col(i)+1)$.} 
\label{fig:partord}
\end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%

\subsubsection{Step 2: Project Out Variables}

Any iterator that does not have a dependent uninterpreted function call
(i.e., the iterator is not passed to an uninterpreted function call in all of the
constraints) can be projected out of the constraints.
However, the problem is that there is no straight forward method 
to project out variables from an integer space with uninterpreted functions.
(FIXME: related work is this true?  need citation?)
To solve this problem, the simplification
algorithm replaces all uninterpreted function call terms 
with fresh parameter variables. 
By doing so, we have created an affine relation that is a superset of our non-affine relation. 
The affine superset relation covers 
the entire iteration space of non-affine relation because
earlier in the algorithms domain and range related 
constraints of the uninterpreted function calls were added. 
Next, the simplification algorithm uses a polyhedral algorithm to project out 
the targeted iterators from super affine relation. 
Finally, the super affine relation that is now simplified is converted back 
to non-affine relation by replacing introduced parameter variables 
with their respected uninterpreted function calls.

In the sparse ILU example in Figure~\ref{fig:ilu0depconstraints}, it becomes
possible to project out $j1$, $j2$, $j1'$, and $j2'$ 
when constraints (12) and (13) are removed due to the if-conditional approximation in data dependence analysis phase.
For the Gauss-Seidel example described at the beginning of 
Section~\ref{sec:method2}, the $j$ iterator can be projected out
using this approach.

% FIXME: how do we know this is correct?

% FIXME: since we are not doing the bijective optimization right now, isn't it better not to talk about it?
%If an uninterpreted function is bijective, then it is possible to have an inspector
%generated the inverse of the function.  In cases where an iterator is part of 
%a function parameter expression such as $f(i+N)$ and that uninterpreted
%function call is in an equality constraint such as $f(i+N)=j$, then if $f()$ is bijective,
%the constraint can be rewritten as $i+N = f^{-1}(j)$.  This modification might enable
%the iterator $i$ to be projected out, but will also cause the iterator $j$ to not be
%a candidate for projection.  Therefore use of the bijective property leads to
%the order of projecting out iterators as being important.
%Since loop nesting depth is typically less than ten, an exhaustive
%search of all possible projection orders can be done.
%The Incomplete LU Factorization example in Figure~\ref{fig:ilu0depconstraints}
%does not include any bijective uninterpreted functions.  This was
%also the case for all of the benchmarks used in the evaluation section
%of this paper.

%\subsubsection{Simplification Algorithm}
%
%FIXME: Do we need to iterate?
%
%FIXME: Do we even want to show the algorithm unless we show all of the details for the
%subroutines?  Does the reader have enough information from the above to implement the algorithm
%themselves?

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}[t]
\small
\begin{ntabbing}xx\=xx\=xx\=xx\=xx\=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\=xx\=xx\=\kill
\textbf{\emph{DataDepSimplify}}($DEP$, $KNOWN$)\\
{\bf input:} \\ % $LEVEL$: Loop Level specified for partial parallelization;\\
% MMS: safe to assume that the outermost level is the one being parallelized
\>\>\>$IS$: Iteration Space Constraints;\\
\>\>\>$DEP$: Dependence Constraints;\\
\>\>\>$KNOWN$: User-supplied constraints;\\

{\bf output:} $SimpleDEP$: A simplified data dependence relation.\\
\\
\reset
$ExpandedDEP$ = AddConstraints($DEP$, $KNOWN$) \label{}\\

$AfterProjOutDep$ = ProjectOutMax($ExpandedDEP$) \label{}\\

%$SimpleDEP$ = RemoveUnnecessary($AfterProjOutDep$) \label{}\\

%%$IS_{inspector}$= $IS_{LEVEL}$; \label{}\\
%
%{\bf for} (each loop variable $L_{I} \in 2 \cdots L_{MAX\_LEVEL}$ ) \label{}\\
%\> $R$ = $U$ \label{}\\
%\> // Assuming the outermost loop is level 1.
%\>{\bf for} (each constraint $C \in (IS_{L_{I}} \cap DEP$) involving loop variable $L_{I}$) \label{}\\
%\>\> {\bf if} ($unin \notin IS_{L_{I}} \parallel reducible(unin)$) \label{}\\
%\>\>\> $R$ = $R \cap Project(IS, L_{I})$ \label{}\\
%\>{\bf for} (each constraint $C \in R$)\label{}\\
%\>\>{\bf if} ($unin \in C$ \&\& $unin \in KNOWN$)\label{}\\
%\>\>\> $R$ = $R \cap simplifyUnin(R, KNOWN)$\label{}\\
%\>$IS$ = $IS \cap R$ \label{}\\
%\>{\bf if}($Gist_{LEVEL}(IS \cap DEP, IS_{inspector}) \neq \emptyset$)\label{}\\
%\>\>$IS_{inspector} = IS_{inspector}  \cap Gist_{LEVEL}(IS \cap DEP, IS_{inspector})$ \label{}\\
%%{\bf for} (each loop variable $L_{I} \in IS_{inspector}$)\\
%%\> $IS_{inspector} = IS_{inspector} \cap IS_{L_{I}}$ \\
{\bf return} $SimpleDEP$;\label{}\\
\end{ntabbing}
\caption{Algorithm for Dependence Simplification.}
\label{algo:depcomp}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%



%%%%%%%%
\subsection{Optimizations for Inspector Code Generation}


FIXME: Anand please describe the inspector code generation
The polyhedral code generator CodeGen+ is utilized to derive the loop code that enumerates all dependences given the simplified constraints
from the projection algorithm. As observed from the generated code,the presence of the equality condition $i'=col(j)$ is exploited to generate an assignment statement for 
$i'$ rather than a loop and IF-condition to enforce the equality check, hence avoiding extra overhead. 


%% MMS: This whole next session was our initial take on the description of the dependence simplification.
%% It includes good examples and should not be commented out.
%% GOTO line 336 to see the beginning of the rewritten section.
%\begin{comment}
%
%\section{Overview of Approach}
%\label{sec:method}
%
%
%
%\subsection{Projection}
%
%\subsubsection{Problem Definition}
%
%The projection algorithm takes as input the constraints that describe the iteration space 
%of the computation and the constraints that must hold for any dependence.
%Dependence constraints are equality constraints while the constraints on the iteration space are inequality constraints.
%
%In our approach, we seek to identify the dependences of a specified loop level, or in other words, 
%two iterations/instances of the specified loop level, which access the same memory location and one of which is a write. 
%
%We define two instances of the input code's iteration space $I=[i_{1},i_{2},\ldots,i_{N}]$  and $I'= [i'_{1},i'_{2},\ldots,i'_{N}]$. 
%Let us suppose that the outer most loop level is chosen for partial parallelization. Then 
%the input constraints to the projection algorithm may involve upto a maximum of $2N$ variables. The goal then 
%is to project all possible variables, except $i_{1}$ and $i'_{1}$ 
%without sacrificing the integrity of the solution %(over-approximation allowed/but not under-approximation) 
%and derive  constraints/(in)equalities on these two variables. 
%
%For instance, considering the Gauss-Seidel Code in Listing~\ref{lst:Gauss Seidel}, the iteration space and dependence constraints 
%are specified below:
%\begin{gather*}
%\{[i,i',j,j']: 0 \leq i < N  \wedge i \neq i' \\
%\wedge  0 \leq i' < N \wedge index(i) \leq j < index(i+1) \\
%\wedge  index(i') \leq j' < index(i'+1) \wedge  i = col(j') \} 
%\end{gather*}
%
%
%By capturing all constraints on  $i_{1}$ and $i'_{1}$  we can enumerate all pairs of iterations of the outermost level involved in
%a dependence. These constraints are then fed to a polyhedral code generator that generates the inspector code iteration space 
%that enumerates all dependences. We then explicitly construct the dependence graph to connect all such pairs of iterations. 
%
%
%
%
%
%%\begin{comment}
%%Hence, given the input constraints we output a minimal set of constraints that retain 
%%the original space or an over approximation of the original space and this output describes the 
%%iteration space of the code that will enumerate all pairs involved in the dependence.
%%\end{comment}
%
%
%%Insert Example of Dependence and inequality constraints in relation form for ilu0 code
%%If the dependence constraints only involved affine/one-to-one functions, determining dependences is straightforward
%%insert why here
%%However, when the dependence constraints stem from complicated array subscript expressions due to index arrays, it is impossible to model the
%%dependence computation using affine functions. 
%
%
%
%\subsubsection{Polyhedral Functions and Definitions}
%
%We introduce the \emph{Gist} and \emph{Project} functions in the context of our projection algorithm. \emph{Gist} is a function implemented in the Omega library. that takes two relations $A$ and $B$ such that
%
%%taken from PLDI2012 Chen's paper
%\begin{align*}
%\textrm{Gist}(A,B) \wedge B = A \wedge B.
%\end{align*}
%Example:
%
%\begin{align*}
%&\textrm{Gist}(\{i > 10 \wedge j > 10\}, \{ j>10\}) = \{ i > 10\},\\
%&\textrm{Gist}(\{1 \le i \le 100\}, \{ i > 10\}) = \{i \le 100\}.
%\end{align*} 
%
%The function can be interpreted as this: given that we know $B$, what is the extra knowledge in
%$A$ that has not been known in $B$. \emph{Project(R,v)} takes a  variable \emph{v} and a set of (in)equalities, \emph{R} as input, and eliminates all instances of \emph{v} in R. 
%%insert example of project
%Both these polyhedral abstractions are explained in~\cite{Chen:2012}.
%\emph{unin} refers to uninterpreted function  symbols that are used to represent index arrays. For example in Listing~\ref{lst:Gauss Seidel}
%The uninterpreted function \emph{index}, with argument arity $1$, is used to represent the array of the same name appearing in the loop bounds on $j$.
%\emph{reducible} is a term we use to describe monotonic/one-to-one uninterpreted functions. For example \emph{index} in Listing~\ref{lst:Gauss Seidel} is \emph{reducible} while \emph{col} is not since \emph{col} is a many-to-one function since multiple non zero elements in the matrix can have the same column id.
%
%\begin{figure}[t]
%\small
%\begin{ntabbing}xx\=xx\=xx\=xx\=xx\=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\=xx\=xx\=\kill
%\textbf{\emph{computeDep}}($LEVEL$, $IS$, $DEP$, $KNOWN$)\\
%{\bf input:} $LEVEL$: Loop Level specified for partial parallelization;\\
%\>\>\>$IS$: Iteration Space Constraints;\\
%\>\>\>$DEP$: Dependence Constraints;\\
%\>\>\>$KNOWN$: User supplied constraints;\\
%
%{\bf output:} Constraints on $LEVEL$, $IS_{inspector}$\\
%\\
%
%$IS_{inspector}$= $IS_{LEVEL}$; \label{}\\
%
%{\bf for} (each loop variable $L_{I} \in L_{MAX\_LEVEL} \cdots L _{(LEVEL+1)}$ ) \label{}\\
%\> $R$ = $U$ \label{}\\
%\>{\bf for} (each constraint $C \in (IS_{L_{I}} \cap DEP$) involving loop variable $L_{I}$) \label{}\\
%\>\> {\bf if} ($unin \notin IS_{L_{I}} \parallel reducible(unin)$) \label{}\\
%\>\>\> $R$ = $R \cap Project(IS, L_{I})$ \label{}\\
%\>{\bf for} (each constraint $C \in R$)\label{}\\
%\>\>{\bf if} ($unin \in C$ \&\& $unin \in KNOWN$)\label{}\\
%\>\>\> $R$ = $R \cap simplifyUnin(R, KNOWN)$\label{}\\
%\>$IS$ = $IS \cap R$ \label{}\\
%\>{\bf if}($Gist_{LEVEL}(IS \cap DEP, IS_{inspector}) \neq \emptyset$)\label{}\\
%\>\>$IS_{inspector} = IS_{inspector}  \cap Gist_{LEVEL}(IS \cap DEP, IS_{inspector})$ \label{}\\
%%{\bf for} (each loop variable $L_{I} \in IS_{inspector}$)\\
%%\> $IS_{inspector} = IS_{inspector} \cap IS_{L_{I}}$ \\
%{\bf return} $IS_{inspector}$;\label{}\\
%\end{ntabbing}
%\caption{Algorithm for Dependence Computation.}
%\label{algo:depcomp}
%\end{figure}
%
%%\begin{comment}
%%Two other notions that are central to our projection algorithm are:
%%\begin{itemize}
%%\item Eliminating redundant constraints or avoiding injecting constraints that are already present
%%\item Extracting implied constraints or simplifying more than two constraints to a single one implied by both. 
%%\end{itemize}
%%\end{comment} 
%
%\subsubsection{Algorithm}
%
%\begin{enumerate}
%
%\item The dependence computation algorithm in Figure~\ref{algo:depcomp} is given as input the loop level targeted for partial parallelization,$LEVEL$,
%the iteration space constraints on all levels,$IS$ and the constraints for the dependence, $DEP$, as well as the user supplied constraints in $KNOWN$. 
%It outputs $IS_{inspector}$, the iteration space constraints
%needed to construct the inspector code that enumerates all iterations of $i_{1}$ and $i'_{1}$ that are involved in a dependence. 
%\item Prior to projecting any variable it checks if any uninterpreted function symbol $unin$ is involved in the constraints involving the current loop variable (\textbf{Line 5}). If so, it checks if $unin$ is not a one-to-one function, Eg.$col(j)$, (this means the loop level j is not $reducible$). If $unin$ is not $reducible$ no projection is possible.
%\item It projects out the loop variables of Figure\ref{algo:depcomp}, if it is possible 
%from innermost loops to the outermost loop nested within the loop level chosen for partial parallelization(\textbf{Line 6}). 
%\item If, for the current variable a constraint involving a one-to-one uninterpreted function is encountered then, simplification of the uninterpreted function so as to uncover constraints on its arguments is attempted by $simplifyUnin$(\textbf{Line 9}). depicted in Figure~\ref{algo:simpUnin}. Extracting implied constraints is especially relevant in the context of simplifying constraints involving uninterpreted functions that result from index arrays. 
%For example, we could exploit certain properties such as monotonicity of index arrays if provided to the constraint solver in this form:
%\begin{align*}
%\{index(i) \le index(i+1) \forall i \} 
%\end{align*} 
%where \textit{index} is an uninterpreted symbol representing an index array. Similarly certain properties about index arrays known a priori can also be useful in simplifying constraints. Eg:
%\begin{align*}
%\{index(i) \le diag(i) \forall i \} 
%\end{align*} 
%Such constraints can uncover implied constraints, extracting constraints on the arguments to the uninterpreted functions. For instance:
%\begin{align*}
%\{index(x) \le index(y) \forall x, y \implies x \le y \} 
%\end{align*} 
%Such constraints are simplified in $simplifyUnin$.
%\item Once the current loop variables has been projected, the \emph{Gist} operator is used to identify if there is any extra constraint on the outermost loop level $IS_{LEVEL}$.If there is extra information then the inspector iteration space is updated with the extra information.(\textbf{Line 12}). 
%\end{enumerate}
%
%
%
%
%
%
%
%
%
%
%
%\begin{figure}[t]
%\small
%\begin{ntabbing}xx\=xx\=xx\=xx\=xx\=xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\=xx\=xx\=\kill
%\textbf{\emph{simplifyUnin}}($IS_{L_{I}}$, $KNOWN$)\\
%{\bf input:} $IS_{L_{I}}$:Iteration Space Relation of Level $L_{I}$;\\
%{\bf input:} $KNOWN$:User supplied relations;\\
%{\bf output:} Simplified Constraints on $IS_{L_{I}}$, $IS_{OUT}$\\
%\\
%\reset 
%$IS_{OUT}$= $TRUE$;\label{}\\
%
%
%{\bf if} ($\exists unin_{1},unin_{2},lb,ub$ s.t.  $unin_{2}(x) \in lb$ and $unin_{1}(y) \in ub$ \label{}\\
%\> are lower and  upper bounds in $IS_{L_{I}}$ \&\& \label{}\\
%\> $unin_{1}(x) \le unin_{2}(x) \forall x \in KNOWN$ ) \label{}\\
%\>	Replace $unin_{2}(x) \le unin_{1}(y)$ with $unin_{1}(x) \le unin_{1}(y)$ in $IS_{L_{I}}$; \label{}\\	
%{\bf if} ($\exists unin$,$lb$,$ub$ s.t. $lb$ and $ub$ \label{} \\are lower and  upper bounds in $IS_{L_{I}}$ \label{} \\
%\>      \&\& only $unin$ is involved in $lb$ and $ub$ ) \label{}\\			
%\>{\bf if} ($unin(x) \le unin(y) \implies x \le y \forall x,y \in KNOWN$) \label{}\\
%\>\>  $IS_{OUT}$ = $IS_{OUT} \cap$  $x \le y$ ; \label{}\\			    
%
%{\bf return} $IS_{OUT}$; \label{} \\
%
%\end{ntabbing}
%\caption{Algorithm for Uninterpreted Function Simplification.}
%\label{algo:simpUnin}
%\end{figure}
%
%
%\subsubsection{Case Study: Gauss Seidel}
%Given the code in Listing~\ref{lst:Gauss Seidel}. The dependences that arise due to the pair of references x[i] and x[col[j]]
%are:
%
%\begin{gather}
%0 \le i \le n \\
%0 \le i' \le n \\
%i \neq i' \\
%index(i) \le j < index(i+1) \\
%index(i') \le j' < index(i'+1) \\
%i = col(j') 
%\end{gather} 
%
%\begin{itemize}
%\item \textbf{Step 1}: Attempt to project $j'$, but $j'$ is involved in  $col$ which is not a one-to-one function and so cannot be reduced. $IS_{inspector}$ is initially 
%{$0 \le i < n \wedge 0 \le i' < n \wedge i \neq i'$}. $Gist_{LEVEL}(IS \cap DEP, IS_{inspector})$ returns $i=col(j')$ so the new value of $IS_{inspector}$ is 
%{$0 \le i < n \wedge 0 \le i' < n \wedge i \neq i' \wedge i=col(j')$}.
%\item \textbf{Step 2}: Attempt to project $j$, but $j$ is involved in  $col$ which is not a one-to-one function and so cannot be reduced. $IS_{inspector}$ is initially
%{$0 \le i < n \wedge 0 \le i' < n \wedge i \neq i' \wedge i=col(j')$}. $Gist_{LEVEL}(IS \cap DEP, IS_{inspector})$ returns $\emptyset$ so $IS_{inspector}$ is unchanged.
%\item \textbf{Step 3}: We are left with $i$ and $i'$, there are no more variables to project. we are left with $IS_{inspector}$ = {$0 \le i < n \wedge 0 \le i' < n \wedge i \neq i' \wedge i=col(j')$}.
%\item \textbf{Step 4}: Iterate over all variables in $IS_{inspector}$ and copy associated constraints. $i$ and $i'$ constraints already present. $j'$ constraints copied over to form
%{$0 \le i < n \wedge 0 \le i'< n \wedge i \neq i' \wedge index(i') \le j' < index(i'+1) \wedge i=col(j')$}.
%\end{itemize}
%
%
%\subsubsection{Case Study: Incomplete LU Factorization}
%
%Given the code in Listing~\ref{lst:ilu0}, the dependences that arise due to the pair of references b[j1]
%and b[j2] are:
%
%\begin{gather}
%0 \le i \le n \\
%0 \le i' \le n \\
%i \neq i' \\
%rowptr(i) \le k < diagptr(i) \\
%rowptr(i') \le k' < diagptr(i') \\
%k + 1 \le j1 < rowptr(i+1) \\
%k' + 1 \le j1' < rowptr(i'+1) \\
%diagptr(col(k)) + 1 \le j2 < rowptr(col(k) + 1) \\
%diagptr(col(k')) + 1 \le j2' < rowptr(col(k') + 1) \\
%j1 = j2'
%\end{gather} 
%
%
%\begin{itemize}
%
%\item \textbf{Step 1}: Our projection algorithm starts from the innermost variable $j2'$. After projecting j2' from all equalities we derive:
%\begin{gather}
%0 \le i \le n \\
%0 \le i' \le n \\
%i \neq i' \\
%rowptr(i) \le k < diagptr(i) \\
%rowptr(i') \le k' < diagptr(i') \\
%k + 1 \le j1 < rowptr(i+1) \\
%k' + 1 \le j1' < rowptr(i'+1) \\
%diagptr(col(k)) + 1\le j2 < rowptr(col(k) + 1) \\
%diagptr(col(k')) + 1 \le j1< rowptr(col(k') + 1) 
%\end{gather} 
%
%The $Gist_{LEVEL}$ routine will return no extra knowledge in these constraints with regards to i.
%\item \textbf{Step 2}: Next we project j2 and j1' to yield:
%
%\begin{gather}
%0 \le i \le n \\
%0 \le i' \le n \\
%i \neq i' \\
%rowptr(i) \le k < diagptr(i) \\
%rowptr(i') \le k' < diagptr(i') \\
%k + 1 \le j1 < rowptr(i+1) \\
%k' + 1   < rowptr(i'+1) \\
%diagptr(col(k)) + 1 < rowptr(col(k) + 1) \\
%diagptr(col(k')) + 1 \le j1< rowptr(col(k') + 1) 
%\end{gather} 
%
%Again Gist returns no additional information. 
%
%\item \textbf{Step 3}: Now we want to project j1. After projecting j1 we get:
%
%\begin{gather}
%0 \le i \le n \\
%0 \le i' \le n \\
%i \neq i' \\
%rowptr(i) \le k < diagptr(i) \\
%rowptr(i') \le k' < diagptr(i') \\
%k + 1 < rowptr(i+1) \\
%k' + 1   < rowptr(i'+1) \\
%diagptr(col(k)) + 1 < rowptr(col(k) + 1) \\
%diagptr(col(k')) + 1 < rowptr(col(k') + 1)\\ 
%k+1 < rowptr(col(k') + 1) \\
%diagptr(col(k')) + 1  < rowptr(i + 1) 
%\end{gather} 
%
%
%\item \textbf{Step 4}: Now we attempt to  project k'
%
%\begin{gather}
%0 \le i \le n \\
%0 \le i' \le n \\
%i \neq i' \\
%rowptr(i) \le k < diagptr(i) \\
%rowptr(i') \le k' < diagptr(i') \\
%k + 1 < rowptr(i+1) \\
%k' + 1   < rowptr(i'+1) \\
%diagptr(col(k)) + 1 < rowptr(col(k) + 1) \\
%diagptr(col(k')) + 1 < rowptr(col(k') + 1)\\ 
%k+1 < rowptr(col(k') + 1) \\
%rowptr(col(k')) < rowptr(col(k')) + 1 \\ \nonumber
% <= diagptr(col(k')) + 1 < rowptr(i + 1)\\ \nonumber
% \implies col(k') <= i 
%\end{gather}
%
%$k'$ is involved in $col(k')$, so  $k'$ is not projected. But
%$simplifyUnin$ returns $col(k') \le i$. $Gist_{LEVEL}(IS \cap DEP, IS_{inspector})$ returns $col(k') \le i$
%So now $IS_{inspector}$ equals $0 \le i < n \wedge 0 \le i' < n\wedge col(k') \le i$. 
%
%\item \textbf{Step 5}: Project k.
%\begin{gather}
%0 \le i \le n \\
%0 \le i' \le n \\
%i \neq i' \\
%rowptr(i) \le k < diagptr(i) \\
%rowptr(i') \le k' < diagptr(i') \\
%k + 1 < rowptr(i+1) \\
%k' + 1  < rowptr(i'+1) \\
%diagptr(col(k)) + 1 < rowptr(col(k) + 1) \\
%diagptr(col(k')) + 1 < rowptr(col(k') + 1)\\ 
%rowptr(i) < rowptr(col(k') + 1) \nonumber \\ 
%\implies i \le col(k') \nonumber \\
%col(k') <= i
%\end{gather} 
% 
%$k$ is involved in $col(k)$, so  $k$ is not projected. But
%$simplifyUnin$ returns $col(k) \geq i$. $Gist_{LEVEL}(IS \cap DEP, IS_{inspector})$ returns $col(k') \geq i$
%So now $IS_{inspector}$ equals $0 \le i < n \wedge 0 \le i' < n\wedge col(k') \le i \wedge col(k') \geq i $ 
% 
%\item \textbf{Step 6}: Iterate over all variables in $IS_{inspector}$ and copy associated constraints. $i$ and $i'$ constraints already present. $k'$ constraints copied over to form $0 \le i < n \wedge 0 \le i' < n\wedge  rowptr(i') \le k' < diagptr(i') \wedge col(k') \le i \wedge col(k') \geq i $.
%\end{itemize}
%
%\end{comment}

